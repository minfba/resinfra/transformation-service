package de.unibamberg.minf.transformation.backend.db.config;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
@ConfigurationProperties(prefix="transformation.db")
@ComponentScan("de.unibamberg.minf.transformation.service")
@EnableJpaRepositories(
		basePackages = {"de.unibamberg.minf.transformation.backend.db", "de.unibamberg.minf.transformation.dao"},
		entityManagerFactoryRef = "transformationEntityManagerFactory",
		transactionManagerRef = "transformationTransactionManager")
public class EmbeddedDbConfig extends BaseDbConfig {

	@Bean
	@Primary
	@Override
	public DataSource transformationDataSource() {
		return new EmbeddedDatabaseBuilder()
				.setType(EmbeddedDatabaseType.HSQL)
				.setName("test")
				.build();
	}
	
}