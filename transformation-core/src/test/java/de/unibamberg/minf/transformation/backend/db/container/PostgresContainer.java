package de.unibamberg.minf.transformation.backend.db.container;

import org.testcontainers.containers.PostgreSQLContainer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PostgresContainer extends PostgreSQLContainer<PostgresContainer> {
    private static final String IMAGE_VERSION = "postgres:14";
    private static PostgresContainer container;
    
    private PostgresContainer() {
         super(IMAGE_VERSION);
    }
    
    public static PostgresContainer getInstance() {
         if (container == null) {
              container = new PostgresContainer();
         }
         return container;
    }
    
    @Override
    public void start() {
         super.start();
         log.info("PostgreSQL container started");
    }
    
    @Override
    public void stop() {
    	log.info("PostgreSQL container stopped");
    }
}