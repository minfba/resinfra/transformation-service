package de.unibamberg.minf.transformation.backend.db.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import de.unibamberg.minf.transformation.backend.db.container.PostgresContainer;

@Configuration
@ConfigurationProperties(prefix="transformation.db")
@ComponentScan("de.unibamberg.minf.transformation.service")
@EnableJpaRepositories(
		basePackages = {"de.unibamberg.minf.transformation.backend.db", "de.unibamberg.minf.transformation.dao"},
		entityManagerFactoryRef = "transformationEntityManagerFactory",
		transactionManagerRef = "transformationTransactionManager")
public class ContainerDbConfig extends BaseDbConfig {
	@Override
	public int getPort() {
		return PostgresContainer.getInstance().getMappedPort(5432);
	}
}