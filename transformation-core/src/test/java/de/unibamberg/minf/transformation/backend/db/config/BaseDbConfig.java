package de.unibamberg.minf.transformation.backend.db.config;

import javax.sql.DataSource;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import de.unibamberg.minf.transformation.config.PersistenceConfig;
import de.unibamberg.minf.transformation.data.service.ArtifactService;
import de.unibamberg.minf.transformation.service.CollectionService;
import de.unibamberg.minf.transformation.service.CollectionServiceImpl;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.DatamodelServiceImpl;
import de.unibamberg.minf.transformation.service.DatasetService;
import de.unibamberg.minf.transformation.service.DatasetServiceImpl;

public abstract class BaseDbConfig extends PersistenceConfig {
	
	@Bean public DatasetService datasetService() { return new DatasetServiceImpl(); }
	@Bean public CollectionService collectionService() { return new CollectionServiceImpl(); }
	
	@MockBean public DatamodelService datamodelService;
	@MockBean public ArtifactService artifactService;
	
	@Bean
	public DataSource transformationDataSource() {
	    return DataSourceBuilder
	        .create()
		        .driverClassName(getDriverClassName())
		        .url(String.format("%s://%s:%s/%s", this.getProtocol(), this.getHost(), this.getPort(), this.getDatabase()))
		        .username(getUsername())
		        .password(getPassword())
	        .build();
	}
	
	@Override
	protected String[] getPackagesToScan() {
		return ArrayUtils.addAll(super.getPackagesToScan(), 
			"de.unibamberg.minf.transformation.backend.db",
			"de.unibamberg.minf.dme.migration.model"
		);
	}
	
	@Override
	protected String[] getMappingResources() {
		return ArrayUtils.addAll(super.getMappingResources(), 
				"orm.xml"
			);
	}
}