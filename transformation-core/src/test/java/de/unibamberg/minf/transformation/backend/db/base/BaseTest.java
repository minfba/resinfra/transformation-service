package de.unibamberg.minf.transformation.backend.db.base;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

@Transactional(transactionManager = "transformationTransactionManager")
public abstract class BaseTest {
	@Autowired @Qualifier("transformationEntityManagerFactory") protected EntityManager em;
}
