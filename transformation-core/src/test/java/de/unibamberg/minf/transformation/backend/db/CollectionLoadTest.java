package de.unibamberg.minf.transformation.backend.db;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.HashSet;
import java.util.List;

import org.hibernate.LazyInitializationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.backend.db.base.BaseEmbeddedTest;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.Interface;
import de.unibamberg.minf.transformation.model.OnlineDataset;
import de.unibamberg.minf.transformation.service.CollectionService;

@SpringBootTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
class CollectionLoadTest extends BaseEmbeddedTest {
	
	@Autowired private CollectionService collectionService;
	
	@Test
	void findsAndLoadsCollections() {
		this.saveCollectionWithReferencedEntities();
		
		List<Collection> foundCollections = collectionService.findAll();
		assertThat(foundCollections).hasSize(1);
		
		// Must fail because collections are not loading associated entities
		assertThatThrownBy(() -> getFirstDatasourceUniqueId(foundCollections))
			.isInstanceOf(LazyInitializationException.class);
		
		List<Collection> loadedCollections = collectionService.findAndLoadAll();
		assertThat(loadedCollections).hasSize(1);
		
		// Not failing because collections are loaded up to interfaces and datasets
		Datasource source = this.getFirstDatasourceUniqueId(loadedCollections);
		assertThat(source).isNotNull();
		
		OnlineDataset ds = source.getDatasets().iterator().next();
		assertThat(ds).isNotNull();
		
		Endpoint endpoint = loadedCollections.get(0).getAccess().stream().filter(a -> a.isEndpoint()).map(a -> a.asEndpoint()).findFirst().get();
		assertThat(endpoint).isNotNull();
		
		Interface inf = endpoint.getInterfaces().iterator().next();
		assertThat(inf).isNotNull();
	}
	
	private Datasource getFirstDatasourceUniqueId(List<Collection> collections) {
		return collections.get(0).getAccess().stream().filter(a -> a.isDatasource()).map(a -> a.asDatasource()).findFirst().get();
	}
	
	private void saveCollectionWithReferencedEntities() {
		Collection collection = new Collection();
		Datasource source = new Datasource();

		OnlineDataset ods = new OnlineDataset();
		ods.setDatasource(source);
		source.setDatasets(new HashSet<>());
		source.getDatasets().add(ods);
		source.setCollection(collection);
		
		Endpoint endpoint = new Endpoint();
		endpoint.setCollection(collection);
		
		Interface inf = new Interface();
		inf.setEndpoint(endpoint);
		endpoint.setInterfaces(new HashSet<>());
		endpoint.getInterfaces().add(inf);
		
		collection.setAccess(new HashSet<Access>());
		collection.getAccess().add(source);
		collection.getAccess().add(endpoint);
		
		collectionService.saveCollection(collection);
		assertThat(collection.getId()).isPositive();
	}
	
}
