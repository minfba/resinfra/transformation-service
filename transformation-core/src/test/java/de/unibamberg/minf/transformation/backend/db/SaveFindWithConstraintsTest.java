package de.unibamberg.minf.transformation.backend.db;

import static org.assertj.core.api.Assertions.*;

import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.backend.db.base.BaseEmbeddedTest;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.model.LocalDataset;
import de.unibamberg.minf.transformation.model.OnlineDataset;
import de.unibamberg.minf.transformation.service.CollectionService;
import de.unibamberg.minf.transformation.service.DatasetService;

@SpringBootTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
class SaveFindWithConstraintsTest extends BaseEmbeddedTest {
	
	@Autowired private DatasetService datasetService;
	@Autowired private CollectionService collectionService;

	@Test
	void succeedSaveCascadeWithUniqueID() {
		Datasource source = this.saveCollectionReturnDatasource();
		
		assertThat(source.getUniqueId()).isNotBlank();
		assertThat(source.getCollection().getUniqueId()).isNotBlank();
		assertThat(source.getId()).isPositive();
		assertThat(source.getCollection().getId()).isPositive();
	}
		
	@Test
	void findsDatasets() {
		Datasource source = this.saveCollectionReturnDatasource();
		
		OnlineDataset ods = new OnlineDataset();
		ods.setDatasource(source);
		datasetService.saveDataset(ods);
		
		LocalDataset lds = new LocalDataset();
		datasetService.saveDataset(lds);
		
		// datasetService -> universal datasetDao returns both as typed datasets
		List<?> result = datasetService.findAll();
		result = datasetService.findAllLocal();
		assertThat(result)
			.hasOnlyElementsOfTypes(LocalDataset.class, OnlineDataset.class)
			.isNotEmpty();
		
		// datasetService -> localDatasetDao returns both as typed datasets
		result = datasetService.findAllLocal();
		assertThat(result)
			.hasOnlyElementsOfTypes(LocalDataset.class)
			.isNotEmpty();
		
		// datasetService -> onlineDatasetDao returns both as typed datasets
		result = datasetService.findAllOnline();
		assertThat(result)
			.hasOnlyElementsOfTypes(OnlineDataset.class)
			.isNotEmpty();
	}
	
	private Datasource saveCollectionReturnDatasource() {
		Collection collection = new Collection();
		Datasource source = new Datasource();
	
		source.setCollection(collection);
		
		collection.setAccess(new HashSet<Access>());
		collection.getAccess().add(source);
			
		collectionService.saveCollection(collection);
		
		return source;
	}
}
