package de.unibamberg.minf.transformation.automation;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.transformation.automation.base.BaseScheduledRunnable;
import de.unibamberg.minf.transformation.crawling.CrawlManager;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlCompleteFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlErrorFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlOnlineFlag;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.service.CrawlService;
import de.unibamberg.minf.transformation.service.DatasetService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CrawlCleanupService extends BaseScheduledRunnable {
	@Autowired protected CrawlManager crawlManager;
	@Autowired protected CrawlService crawlService;
	@Autowired protected DatasetService datasetService;
	
	@Override protected void init() {
		log.info("Initializing crawl cleanup service");
		this.cleanupCrawlData(false);
	}
	
	@Override 
	protected void executeAutomation() {
		this.cleanupCrawlData(this.isAutomationEnabled());
	}
	
	
	public void cleanupCrawlData(boolean doCleanup) {
		log.info("Cleaning outdated crawl data for all datasets");
		datasetService.findAllOnline().stream().forEach(ds -> this.cleanupCrawlDataForDataset(ds.getId(), doCleanup));		
		log.info("Completed cleaning of outdated crawl data for all datasets");
	}
	
	
	public void cleanupCrawlDataForDataset(Long datasetId, boolean doCleanup) {	
		log.trace("Cleaning outdated crawl data for individual dataset: {}",  datasetId);
		this.executeCleanup(datasetId, doCleanup);
	}
	
	private void executeCleanup(Long datasetId, boolean doCleanup) {
		// In-progress crawls are excluded from this processing
		List<Crawl> allCrawls = crawlService.findCrawls(datasetId, CrawlOnlineFlag.Both, CrawlCompleteFlag.Complete, CrawlErrorFlag.Both, -1);
		List<Long> clearCrawlIds = new ArrayList<>();
		
		boolean passedCompleteNoErrorCrawl = false;
		int errorCount = 0;
		int onlineCount = 0;
		int offlineCount = 0;
		for (Crawl c : allCrawls) {
			passedCompleteNoErrorCrawl = passedCompleteNoErrorCrawl || (!c.isError() && c.isComplete());
			/* Error and offline crawls -> only the most recent three after an online crawl are kept; anything before the last complete online crawl is discarded
			 * Online crawls -> the three most recent are kept 
			 */
			if (c.isError()) {
				errorCount += this.handleCountedCrawl(clearCrawlIds, c, passedCompleteNoErrorCrawl, errorCount); 
			} else if (c.isOffline()) {
				offlineCount += this.handleCountedCrawl(clearCrawlIds, c, passedCompleteNoErrorCrawl, offlineCount);
			} else if (c.isOnline()) {
				onlineCount += this.handleCountedCrawl(clearCrawlIds, c, false, onlineCount);
			}			
		}
		
		if (!clearCrawlIds.isEmpty()) {
			if (doCleanup) {
				log.info("Cleaning up data of {} crawls", clearCrawlIds.size());
				for (Long clearCrawlId : clearCrawlIds) {
					crawlService.removeCrawlById(clearCrawlId);
				}
			} else {
				log.debug("Data of {} crawls qualifies for deletion; no action performed", clearCrawlIds.size());
			}
		}
	}
	
	private int handleCountedCrawl(List<Long> clearCrawlIds, Crawl c, boolean clearAll, int count) {
		if (clearAll || count>=3) {
			clearCrawlIds.add(c.getId());
			return 0;
		} 
		return 1;
	}
}
