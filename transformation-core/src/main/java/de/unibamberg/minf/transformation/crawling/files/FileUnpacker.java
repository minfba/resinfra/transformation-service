package de.unibamberg.minf.transformation.crawling.files;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.springframework.beans.factory.annotation.Value;

import de.unibamberg.minf.processing.exception.ResourceProcessingException;
import de.unibamberg.minf.transformation.crawling.crawler.Crawler;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.base.AccessOperation;

public class FileUnpacker extends BaseFileStreamCrawler implements Crawler {
	
	@Value("${processing.download.filename:download.tmp}")
	private String downloadFileName;
	
	@Value("${processing.unpack.filename:archive.tmp}")
	private String unpackFileName;
	
	
	private boolean initialized = false;
	
	
	private int bufferSize = 1024;
	
	public int getBufferSize() { return bufferSize; }
	public void setBufferSize(int bufferSize) { this.bufferSize = bufferSize; }
		
	@Override public boolean isInitialized() { return this.initialized; }
	
	@Override
	public String getUnitMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.file.unpacker.unit";
	}

	@Override
	public String getTitleMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.file.unpacker.title";
	}
	
	@Override
	public void init(Access access, AccessOperation op, ExtendedDatamodelContainer sc) {
		super.init(access, op, sc);
		try {
			this.setupPaths(op);
			this.initialized = true;
		} catch (IOException e) {
			logger.error("File downloader initialization failed: " + e.getMessage(), e);
		}
	}
	
	@Override
	public void run() {
		super.run();
		if (this.getListener()!=null) {
			this.getListener().start(this.getUuid());
		}
		
		this.unpack();
	}
		
	private void unpack() {
		if (this.getInputFilenames()!=null) {
			for (String inputFile : this.getInputFilenames()) {
				try {
					if (this.isCancellationRequested()) {
						throw new ResourceProcessingException("Service cancellation has been requested");
					}
					File f = new File(inputFile); 
					if (!f.exists()) {
						logger.info("Input file for unpacking does not exist. File might have been unpacked already");
						continue;
					}
					
					if (f.isDirectory()) {
						for (File f1 : f.listFiles()) {
							if (f1.isDirectory()) {
								continue;
							}
							this.unpackFile(f1);
						}
					} else {
						this.unpackFile(f);
					}
				} catch (Exception e) {
					logger.error("An exception occurred while uncompressing file", e);
					if (this.getListener() != null) {
						this.getListener().error(this.getUuid());
					}
				}
			}
			try {
				this.moveInputToBackup();
			} catch (IOException e) {
				logger.error("Failed to move input to backup", e);
			}
		}
		if (this.getListener()!=null) {
			this.getListener().finished(getUuid());
		}
	}
	
	private void unpackFile(File f) {
		FileInputStream fin;
		BufferedInputStream in;
		CompressorInputStream bzIn = null;
		FileOutputStream out = null;
		try {
			fin = new FileInputStream(f);
			in = new BufferedInputStream(fin);
			
			try {
				bzIn = new CompressorStreamFactory(true).createCompressorInputStream(in);
			} catch (CompressorException e) {
				logger.info("Could not detect compression based on stream signature. Assuming non-compressed file.");
				this.addOutputFilename(f.getAbsolutePath());
				return;
			}
			
			if (bzIn!=null) {
				logger.info("Identified correct unpacker as " + bzIn.getClass().getSimpleName());
			}		
			
			String outFilename = f.getAbsolutePath() + ".1";
			out = new FileOutputStream(outFilename);
			
			final byte[] buffer = new byte[this.getBufferSize()];
			int n = 0;
			while (-1 != (n = bzIn.read(buffer))) {
				if (this.isCancellationRequested()) {
					throw new ResourceProcessingException("Service cancellation has been requested");
				}
			    out.write(buffer, 0, n);
			    if (this.getListener()!=null) {
					this.getListener().processed(this.getUuid(), bzIn.getBytesRead());
				}
			}
			
			this.addOutputFilename(outFilename);
			
			f.delete();
		} catch (Exception e) {
			logger.error("An exception occurred while uncompressing file", e);
			if (this.getListener() != null) {
				this.getListener().error(this.getUuid());
			}
		} finally {
			if (bzIn!=null) {
				try {
					bzIn.close();
				} catch (IOException e) {}
			}
			if (out!=null) {
				try {
					out.close();
				} catch (IOException e) {}
			}
		}
	}
}
