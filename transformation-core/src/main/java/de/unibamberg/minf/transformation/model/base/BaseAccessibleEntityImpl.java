package de.unibamberg.minf.transformation.model.base;

import javax.persistence.MappedSuperclass;

import de.unibamberg.minf.transformation.model.PersistedUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper=true)
public abstract class BaseAccessibleEntityImpl extends BaseEntityImpl implements UniqueEntity {
	private String uniqueId;
	
	private PersistedUser owner;
	
	private boolean deleted;
	
	private boolean readProtected;
	private boolean writeProtected;
}
