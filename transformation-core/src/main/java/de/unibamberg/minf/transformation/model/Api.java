package de.unibamberg.minf.transformation.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import de.unibamberg.minf.transformation.model.base.BaseAccessibleEntityImpl;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * An API encapsulates access to one or more interfaces that should be addressed by a common input datamodel 
 *  and should return data in terms of a common output datamodel. An API thus harmonizes access to heterogeneous,
 *  distributed access interfaces.
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"interfaces"})
@Table(name="api",uniqueConstraints={@UniqueConstraint(name="uniqueId",columnNames={"uniqueId"})})
@NamedEntityGraph( attributeNodes = { @NamedAttributeNode("interfaces") } )
public class Api extends BaseAccessibleEntityImpl {

	private String label;
	
	private String inputDatamodelId;	
	private String outputDatamodelId;
	
	private String metadata;
	
	@Transient
	private ExtendedDatamodelContainer inputDatamodel;
	
	@Transient
	private ExtendedDatamodelContainer outputDatamodel;
	
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
			  name = "api_interface", 
			  joinColumns = @JoinColumn(name = "api_id"), 
			  inverseJoinColumns = @JoinColumn(name = "interface_id"))
	private Set<Interface> interfaces;
}
