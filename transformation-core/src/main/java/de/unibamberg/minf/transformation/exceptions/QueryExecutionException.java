package de.unibamberg.minf.transformation.exceptions;

public class QueryExecutionException extends Exception {
	private static final long serialVersionUID = 1939858105695091669L;

	public QueryExecutionException(String message) {
		super(message);
	}
	
	public QueryExecutionException(String message, Throwable source) {
		super(message, source);
	}
	
	public QueryExecutionException(Throwable source) {
		super(source);
	}
}
