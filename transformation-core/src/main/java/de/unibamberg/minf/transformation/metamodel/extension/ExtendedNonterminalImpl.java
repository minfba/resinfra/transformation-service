package de.unibamberg.minf.transformation.metamodel.extension;

import de.unibamberg.minf.dme.model.base.Nonterminal;
import de.unibamberg.minf.dme.model.datamodel.NonterminalImpl;

public class ExtendedNonterminalImpl extends NonterminalImpl implements ExtendedElement {
	private static final long serialVersionUID = -3993636666903673648L;
	
	private boolean nested;

	@Override public boolean isNested() { return nested; }
	@Override public void setNested(boolean nested) { this.nested = nested; }
	
	public ExtendedNonterminalImpl() {}
	
	public ExtendedNonterminalImpl(Nonterminal element) {
		this.setChildNonterminals(element.getChildNonterminals());
		this.setDisabled(element.isDisabled());
		this.setEntityId(element.getEntityId());
		this.setGrammars(element.getGrammars());
		this.setId(element.getId());
		this.setIncludeHeader(element.isIncludeHeader());
		this.setLocked(element.isLocked());
		this.setName(element.getName());
		this.setProcessingRoot(element.isProcessingRoot());
		this.setTransient(element.isTransient());
	}
}