package de.unibamberg.minf.transformation.config.nested;

import lombok.Data;

@Data
public class DataProvisioningConfigProperties {
	private String provisioningModelId;
}
