package de.unibamberg.minf.transformation.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.unibamberg.minf.transformation.model.base.AccessOperation;
import de.unibamberg.minf.transformation.model.base.BaseAccessibleEntityImpl;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A crawl retrieves data of an OnlineDataset and - if successful - results in the creation of an 
 *  associated bag, replacing any previous bags as current data bag for that dataset.  
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"dataset"})
@Table(uniqueConstraints={@UniqueConstraint(name="uniqueId",columnNames={"uniqueId"})})
@NamedEntityGraph( attributeNodes = { @NamedAttributeNode("dataset"), @NamedAttributeNode("baseCrawl") } )
public class Crawl extends BaseAccessibleEntityImpl implements AccessOperation {

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="dataset_id", nullable=false)
	private OnlineDataset dataset;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="basecrawl_id")
	private Crawl baseCrawl;
	
	@OneToMany(cascade = CascadeType.PERSIST, mappedBy="baseCrawl", fetch = FetchType.LAZY)
	private List<Crawl> childCrawls;
	
	private String prefix;
	
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "crawl_messages", joinColumns = @JoinColumn(name = "id"))
	@Column(name = "messages")
	private List<String> messages;
	
	private boolean error;
	private boolean complete;
	
	public boolean isOnline() { return this.baseCrawl==null; }
	public boolean isOffline() { return this.baseCrawl!=null; }
	
	public void addMessage(String message) {
		if (this.getMessages()==null) {
			this.setMessages(new ArrayList<String>());
		}
		this.getMessages().add(message);
	}
}
