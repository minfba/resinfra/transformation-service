package de.unibamberg.minf.transformation.crawling.oaipmh.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="OAI-PMH")
public class OaiPmhResponseContainer {
	private String responseDate;
	private OaiPmhRequest request;
	
	private List<OaiPmhRecord> record;
	private List<OaiPmhRecord> records;
	private List<OaiPmhMetadataFormat> formats;
	
	@XmlElement
	public String getResponseDate() { return responseDate; }
	public void setResponseDate(String responseDate) { this.responseDate = responseDate; }
	
	public OaiPmhRequest getRequest() { return request; }
	public void setRequest(OaiPmhRequest request) { this.request = request; }
	
	@XmlElementWrapper(name="GetRecord")
	@XmlElement(name="record")
	public List<OaiPmhRecord> getIndividualRecord() { return record; }
	public void setIndividualRecord(List<OaiPmhRecord> record) { this.record = record; }
	
	@XmlElementWrapper(name="ListRecords")
	@XmlElement(name="record")
	public List<OaiPmhRecord> getRecords() { return records; }
	public void setRecords(List<OaiPmhRecord> records) { this.records = records; }
	
	@XmlElementWrapper(name="ListMetadataFormats")
	@XmlElement(name="metadataFormat")
	public List<OaiPmhMetadataFormat> getFormats() { return formats; }
	public void setFormats(List<OaiPmhMetadataFormat> formats) { this.formats = formats; }
}