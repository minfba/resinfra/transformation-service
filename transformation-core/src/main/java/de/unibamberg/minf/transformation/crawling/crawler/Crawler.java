package de.unibamberg.minf.transformation.crawling.crawler;

import java.io.IOException;

import de.unibamberg.minf.processing.service.base.ProcessingService;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.base.AccessOperation;

public interface Crawler extends ProcessingService {
	public boolean isInitialized();
	
	public String getUnitMessageCode();
	public String getTitleMessageCode();
	
	public void init(Access access, AccessOperation op, ExtendedDatamodelContainer sc) throws IOException;
}
