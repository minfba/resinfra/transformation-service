package de.unibamberg.minf.transformation.dao.db;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.model.PersistedUser;

@Repository
public interface PersistedUserDao extends JpaRepository<PersistedUser, Long> {
		
	@Transactional
	@Query("SELECT u FROM PersistedUser u LEFT JOIN FETCH u.authorities a WHERE u.issuer = ?1 AND u.username = ?2")
	PersistedUser findByUsername(String domain, String username);
	
	@Transactional
	//@Query("SELECT u FROM PersistedUser u LEFT JOIN FETCH u.authorities a LEFT JOIN FETCH u.accessTokens t LEFT JOIN FETCH t.allowedAdresses d WHERE u.uniqueId=?1")
	@EntityGraph(attributePaths = {"accessTokens", "authorities"}, type = EntityGraphType.LOAD)
	Optional<PersistedUser> findByUniqueId(String uniqueId);

	@Transactional
	@Query("SELECT u FROM PersistedUser u LEFT JOIN FETCH u.authorities a JOIN FETCH u.accessTokens t LEFT JOIN FETCH t.allowedAdresses d WHERE t.uniqueId=?1")
	Optional<PersistedUser> findByTokenUniqueId(String tokenId);
	
	@Override
	@Transactional
	@EntityGraph(attributePaths = {"accessTokens"}, type = EntityGraphType.LOAD)
	List<PersistedUser> findAll();
}