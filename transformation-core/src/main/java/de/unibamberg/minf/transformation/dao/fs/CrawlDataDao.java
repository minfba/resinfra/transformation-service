package de.unibamberg.minf.transformation.dao.fs;

import java.io.IOException;

import de.unibamberg.minf.transformation.model.Crawl;
import lombok.Data;

public interface CrawlDataDao {
	@Data
	public class FileCountAndSize {
		private int files;
		private String size;
	}
	
	public boolean existsAndHasData(Crawl c);
	public FileCountAndSize getFilesAndSize(Crawl c);
	public void delete(Crawl c) throws IOException;
	
	public String getCrawlDirPath(Long crawlId);
}
