package de.unibamberg.minf.transformation.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import de.unibamberg.minf.transformation.model.base.BaseAccessibleEntityImpl;
import de.unibamberg.minf.transformation.model.base.OnlineAccessMethod;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * An accessible query interface that is defined by the characteristics of the endpoint (URL, type etc.)
 *  and the result datamodel of the interface.
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"endpoint", "apis"})
@Table(uniqueConstraints={@UniqueConstraint(name="uniqueId",columnNames={"uniqueId"})})
@NamedEntityGraph(
		attributeNodes = {
			@NamedAttributeNode("apis"),
			@NamedAttributeNode("endpoint")
		}
	)
public class Interface extends BaseAccessibleEntityImpl implements OnlineAccessMethod {
	
	private String datamodelId;
	
	@Transient
	private ExtendedDatamodelContainer datamodel;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="endpoint_id", nullable=false)
	private Endpoint endpoint;
	
	@ManyToMany(mappedBy = "interfaces", fetch = FetchType.LAZY)
	public Set<Api> apis;

	@Override
	public Access getAccess() { return this.getEndpoint(); }
}
