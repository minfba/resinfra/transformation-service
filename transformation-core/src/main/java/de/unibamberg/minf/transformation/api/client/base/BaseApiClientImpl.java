package de.unibamberg.minf.transformation.api.client.base;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.minf.core.util.Stopwatch;
import de.unibamberg.minf.transformation.api.client.ApiConsumptionException;
import de.unibamberg.minf.transformation.pojo.ApiStatusPojo;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public abstract class BaseApiClientImpl<P1, P2> implements ApiClient {
	protected ObjectMapper apiObjectMapper;
	
	@Autowired protected RestTemplate restTemplate;
	
	private Class<P2> entityClass;
	private Class<? extends P1[]> arrayClass;
	
	protected BaseApiClientImpl(Class<P2> entityClass, Class<? extends P1[]> arrayClass) {
 		this.entityClass = entityClass;
 		this.arrayClass = arrayClass;
 		
 		this.apiObjectMapper = new ObjectMapper()
 			.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	@Override
 	public void sync() throws ApiConsumptionException {
		log.debug("Starting update process");
		Stopwatch sw = new Stopwatch().start();
		try {
			P1[] fetchedEntities = this.fetchAll();
			if (fetchedEntities==null || fetchedEntities.length==0) {
				log.info("API call to fetch all entities returned none; nothing to synchronize");
			} 
			this.syncEntities(fetchedEntities);
			log.info(String.format("Update process took %sms", sw.getElapsedTime()));
		} catch (Exception e) {
			log.error("Failed to execute update process", e);
			throw e;
		}
 	}

	protected P1[] fetchAll() throws ApiConsumptionException {
		if (this.getFetchAllUrl()==null) {
			return null;
		}
		return this.fetch(this.getFetchAllUrl());
	}
	
	protected P2 fetchDetails(String id) throws ApiConsumptionException {
		if (id==null || this.getFetchDetailsUrl()==null) {
			return null;
		}
		try {
			// Workaround with explicit type conversion because RestTemplate sometimes uses superclass of entityClass
			String result = restTemplate.getForObject(String.format(this.getFetchDetailsUrl(), id), String.class);
			return parseDetails(result);
		} catch (Exception e) {
			log.error(String.format("Error while fetching details [%s] for id [%s]: %s", this.getFetchDetailsUrl(), id, e.getMessage()));
			throw new ApiConsumptionException(this.getClass().getSimpleName() + "::fetchDetails", e);
		}
	}
	
	public P1[] fetch(String requestUrl) throws ApiConsumptionException {
		try {
			String result = restTemplate.getForObject(requestUrl, String.class);
			return parse(result);		
		} catch (Exception e) {
			log.error(String.format("Error while executing [%s]: %s", requestUrl, e.getMessage()));
			throw new ApiConsumptionException(this.getClass().getSimpleName() + "::fetch", e);
		}
	}
			
	public ApiStatusPojo ping() {
		ApiStatusPojo result = new ApiStatusPojo();
		Stopwatch sw = new Stopwatch().start();
		try {
			String location;
			URL baseURL = new URL(this.getPingUrl());
			URL nextURL;
		    HttpURLConnection connection = (HttpURLConnection)baseURL.openConnection();
		    int redirects = 0;
		    
		    while(connection.getResponseCode()==HttpURLConnection.HTTP_MOVED_PERM || 
		    		connection.getResponseCode()==HttpURLConnection.HTTP_MOVED_TEMP) {
	    		if (++redirects > 10) {
	    			throw new IOException("Too many redirects; connect cancelled");
	    		}
		    	location = connection.getHeaderField("Location");
		        nextURL = new URL(baseURL, location);  // Handle relative URLs
		        baseURL = nextURL;
		        connection = (HttpURLConnection)baseURL.openConnection();
		    }
		    
		    result.setStatusCode(connection.getResponseCode());
		    result.setRoundtime(sw.getElapsedTime());
		    result.setAccessible(true);
		} catch (Exception e) {
			result.setRoundtime(sw.getElapsedTime());
			result.setAccessible(false);
			log.error("Failed to ping service at {}: {}", this.getPingUrl(), e.getMessage());
		}
		return result;
	}
	
	protected P2 parseDetails(String jsonString) throws JsonMappingException, JsonProcessingException {
		return apiObjectMapper.readValue(jsonString, entityClass);
	}
	
	protected P1[] parse(String jsonString) throws JsonMappingException, JsonProcessingException {
		return apiObjectMapper.readValue(jsonString, arrayClass);
	}
	
	protected abstract void syncEntities(P1[] fetchedEntities);

	protected abstract String getFetchAllUrl();
	protected abstract String getFetchDetailsUrl();
	protected abstract String getPingUrl();
}