package de.unibamberg.minf.transformation.crawling;

import java.util.Map;
import java.util.UUID;

import de.unibamberg.minf.processing.listener.ProcessingListener;
import de.unibamberg.minf.processing.service.base.ProcessingService;
import de.unibamberg.minf.transformation.crawling.crawler.Crawler;

public interface CrawlPipeline extends ProcessingListener, ProcessingService, Runnable {
	public UUID getUuid();
	//public Long getCrawlId();
	public ProcessingServiceStates getState();
	public int getSize();
	public int getIndex();
	public long getStageSize();
	public long getStageProgress();
	public Map<UUID, Crawler> getRunnablesMap();

	public String[] getServiceTitleMessageCodes();
	public String[] getServiceUnitMessageCodes();
}
