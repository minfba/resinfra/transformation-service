package de.unibamberg.minf.transformation.config.nested;

import lombok.Data;

@Data
public class ImagesConfigProperties {
	private int width = 1000;
	private int height = 1000;
	private ThumbnailsConfigProperties thumbnails;	
}
