package de.unibamberg.minf.transformation.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import de.unibamberg.minf.transformation.model.base.BaseAccessibleEntityImpl;
import de.unibamberg.minf.transformation.model.base.OnlineAccessMethod;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Abstract base class of access mechanisms that a collection can provide. 
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"collection", "params", "fileAccessPatterns", "accessHeaders"})
@Inheritance(strategy = InheritanceType.JOINED)
@NamedEntityGraph( attributeNodes = { 
		@NamedAttributeNode("collection"),
		@NamedAttributeNode("params"),
		@NamedAttributeNode("fileAccessPatterns") } )
public class Access extends BaseAccessibleEntityImpl {
	private String url;
	private String accessType;
	private String httpMethod;
	private String accessModelId;
	private String fileType;
	private String localName;
	private boolean authRequired;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="collection_id", nullable=false)
	private Collection collection;
	
	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy="access", fetch = FetchType.LAZY)
	private Set<AccessParam> params;
	
	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy="access", fetch = FetchType.LAZY)
	private Set<AccessHeader> accessHeaders;
	
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "datasource_file_access_patterns", joinColumns = @JoinColumn(name = "id"))
	@Column(name = "file_access_patterns")
	private Set<String> fileAccessPatterns;
	
    public List<AccessParam> getParams(String param) {
    	return params==null ? null : this.params.stream()
    			.filter(p -> p.getParam().equals(param))
    			.collect(Collectors.toList());
    }
    
    public List<String> getParamValues(String param) {
    	return params==null ? null : this.params.stream()
    			.filter(p -> p.getParam().equals(param))
    			.map(AccessParam::getValue)
    			.collect(Collectors.toList());
    }
    
    public String getSingleParamValue(String param) {
    	List<String> matchingParamValues = this.getParamValues(param);
    	return (matchingParamValues==null || matchingParamValues.isEmpty()) ? null : matchingParamValues.get(matchingParamValues.size()-1);
    }
        
    public AccessParam getSingleParam(String param) {
    	List<AccessParam> matchingParams = this.getParams(param);
    	return (matchingParams==null || matchingParams.isEmpty()) ? null : matchingParams.get(matchingParams.size()-1);
    }
        
    @Transient
    public boolean isDatasource() { return false; }
    
    @Transient
    public boolean isEndpoint() { return false; }
    
    @Transient
    public Datasource asDatasource() { return Datasource.class.cast(this); }
    
    @Transient
    public Endpoint asEndpoint() { return Endpoint.class.cast(this); }
    
    public Set<OnlineAccessMethod> getAccessMethods() { return new HashSet<>(); }
}
