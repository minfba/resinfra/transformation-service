package de.unibamberg.minf.transformation.crawling;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.unibamberg.minf.processing.consumption.ResourceConsumptionService;
import de.unibamberg.minf.processing.exception.GenericProcessingException;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.service.base.ResourceProcessingService;
import de.unibamberg.minf.processing.service.base.ProcessingService.ProcessingServiceStates;
import de.unibamberg.minf.transformation.TransformationConstants.AccessMethods;
import de.unibamberg.minf.transformation.TransformationConstants.FileTypes;
import de.unibamberg.minf.transformation.automation.NextExecution;
import de.unibamberg.minf.transformation.automation.base.BaseScheduledRunnable;
import de.unibamberg.minf.transformation.config.model.AccessChain;
import de.unibamberg.minf.transformation.config.model.FileProcessingChain;
import de.unibamberg.minf.transformation.crawling.crawler.Crawler;
import de.unibamberg.minf.transformation.crawling.crawler.Processor;
import de.unibamberg.minf.transformation.crawling.gtf.CrawlingExecutionContext;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.OnlineDataset;
import de.unibamberg.minf.transformation.service.AccessService;
import de.unibamberg.minf.transformation.service.CollectionService;
import de.unibamberg.minf.transformation.service.CrawlService;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.DatasetService;
import lombok.Getter;
import lombok.Setter;


public class CrawlManagerImpl implements CrawlManager, ApplicationContextAware, InitializingBean {
	protected static final Logger logger = LoggerFactory.getLogger(CrawlManagerImpl.class);
	
	@Autowired protected CrawlService crawlService;
	@Autowired protected CollectionService collectionService;
	@Autowired protected DatamodelService datamodelService;
	@Autowired protected DatasetService datasetService;
	@Autowired protected AccessService accessService;
	@Autowired private ObjectMapper objectMapper;
	
	@Getter @Setter private int maxPoolSize;
	@Getter @Setter private List<AccessChain> accessChains;
	@Getter @Setter private List<FileProcessingChain> fileProcessingChains;
	@Getter @Setter protected String baseDownloadPath;
	
	
	protected ApplicationContext appContext;
	protected ExecutorService pipelineExecutor;
	
	protected ReentrantLock statusMapslock = new ReentrantLock();
	protected Map<Long, UUID> crawlIdServiceIdMap = new HashMap<>();
	protected Map<UUID, CrawlPipeline> serviceIdServiceMap = new HashMap<>();
	
	
	@Override
	public void afterPropertiesSet() throws Exception {
		pipelineExecutor = Executors.newFixedThreadPool(this.getMaxPoolSize());
		logger.info("CrawlManager initialized");
	}
	
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;
	}
	
	@Override
	public Set<String> getSupportedAccessTypes() {
		return this.getAccessChains().stream().map(a -> a.getKey()).collect(Collectors.toSet());
	}
	
	@Override
	public Set<String> getSupportedFileTypes() {
		return this.getFileProcessingChains().stream().map(a -> a.getKey()).collect(Collectors.toSet());
	}
	
	@Override
	public void performOnlineCrawl(OnlineDataset dataset) {
		if (dataset.getDatamodel()==null) {
			logger.warn("Unknown datamodel; skipping online crawl");
			return;
		}
		
		/*boolean modified = false;
		if (collection.isNew()) {
			collection.setNew(false);
			modified = true;
		}
		if (endpoint.isNew() || endpoint.isError()) {
			endpoint.setNew(false);
			endpoint.setError(false);
			modified = true;
		}
		Dataset dataset = null;
		for (Dataset ds : endpoint.getDatasets()) {
			if (ds.getDatamodelId().equals(datamodel.getId())) {
				dataset = ds;
				if (ds.isNew() || ds.isError()) {				
					ds.setNew(false);
					ds.setError(false);
					modified = true;
				}
			}
		}
		if (modified) {
			collectionService.saveCollection(collection);
		}*/
		this.performCrawl(crawlService.createOnlineCrawl(dataset), dataset, this.getSessionData(dataset.getDatasource().getCollection()));
	}
	
	@Override
	public void performOfflineCrawl(OnlineDataset dataset, Long baseCrawlId) {
		this.performCrawl(crawlService.createOfflineCrawl(baseCrawlId), dataset, this.getSessionData(dataset.getDatasource().getCollection()));
	}
	
	private JsonNode getSessionData(Collection collection) {
		if (collection.getCollectionMetadata()==null || collection.getCollectionMetadata().isBlank()) {
			return MissingNode.getInstance();
		}
		try {
			ObjectNode cNode = objectMapper.createObjectNode();
			cNode.set("collection", objectMapper.readTree(collection.getCollectionMetadata()));
			return cNode;
		} catch (Exception e) {
			logger.error("Failed to read collection metadata to session data", e);
			return MissingNode.getInstance();
		}
	}
		
	@Override
	public CrawlState getCrawlState(Long crawlId) {
		CrawlState state = new CrawlState();
		CrawlPipeline pipeline = null;
		
		try {
			statusMapslock.lock();
			if (crawlId==null) {
				state.setState(ProcessingServiceStates.NONE);
				return state;
			} else if (this.crawlIdServiceIdMap.containsKey(crawlId)) {
				UUID pipelineUuid = this.crawlIdServiceIdMap.get(crawlId);
				pipeline = this.serviceIdServiceMap.get(pipelineUuid);
			} 
		} finally {
			statusMapslock.unlock();
		}
		
		if (pipeline!=null) {
			state.setState(ProcessingServiceStates.ACTIVE);
			state.setPipelineLength(pipeline.getSize());
			state.setPipelineIndex(pipeline.getIndex());
			state.setCurrentProgress(pipeline.getStageProgress());
			state.setCurrentSize(pipeline.getStageSize());
			
			if (pipeline.getSize()>0) {
				state.setServiceTitleMessageCode(pipeline.getServiceTitleMessageCodes());
				state.setServiceUnitMessageCode(pipeline.getServiceUnitMessageCodes());	
			}
		} else {
			Crawl c = this.crawlService.findById(crawlId);
			if (c==null) {
				state.setState(ProcessingServiceStates.NONE);
			} else if (c.isComplete()) {
				state.setState(ProcessingServiceStates.COMPLETE);
				return state;
			} else {
				state.setState(ProcessingServiceStates.ERROR);
				return state;
			}
		}
		return state;
	}
	
	@Override
	public void tryCancelCrawl(Long crawlId) {
		if (crawlId==null) {
			return;
		}
		CrawlPipeline pipeline = null;
		try {
			statusMapslock.lock();
			// Means active
			UUID pipelineUuid = this.crawlIdServiceIdMap.get(crawlId);
			pipeline = this.serviceIdServiceMap.get(pipelineUuid);
			pipeline.requestCancellation();
		} finally {
			statusMapslock.unlock();
		} 
	}
	
	private void performCrawl(Crawl crawl, OnlineDataset dataset, JsonNode sessionData) {
		if (crawl==null || dataset.getDatasource()==null || dataset.getDatamodel()==null) {
			logger.warn("Could not create crawl pipeline. Either crawl, endpoint or datamodel were unset");
			return;
		}
		try {
			MDC.put("uid", String.format("crawl_%s", crawl.getId()));

			CrawlPipeline pipeline = this.createPipeline(dataset.getDatasource(), dataset.getDatamodel(), crawl, sessionData);
			if (pipeline!=null) {
				this.enqueue(pipeline, crawl);
			}
		} catch (Exception e) {
			logger.error("Failed to create and enqueue crawl pipeline", e);
			crawl.setError(true);
			crawl.setComplete(false);
			crawlService.save(crawl);
		} finally {
			MDC.remove("uid");
		}
	}
	
	protected CrawlPipeline createPipeline(Datasource ds, ExtendedDatamodelContainer sc, Crawl c, JsonNode sessionData) throws ProcessingConfigException, GenericProcessingException, IOException {		
		String access = null;
		String file = null;
		for (AccessMethods mAv : AccessMethods.values()) {
			if (mAv.toString().equalsIgnoreCase(ds.getAccessType())) {
				access = mAv.toString();
				break;
			}
		}
		for (FileTypes ftv : FileTypes.values()) {
			if (ftv.toString().equalsIgnoreCase(ds.getFileType())) {
				file = ftv.toString();
				break;
			}
		}
		// Online but no access type detected
		if (access==null && c.getBaseCrawl()==null) {
			logger.error("Unknown access type [{}]; cancelling crawl", ds.getAccessType());
			this.updateCrawlAndCollection(c.getId(), ProcessingServiceStates.ERROR);
			return null;
		}
		if (file==null) {
			logger.error("Unknown file type method [{}]; cancelling crawl", ds.getFileType());
			this.updateCrawlAndCollection(c.getId(), ProcessingServiceStates.ERROR);
			return null;
		}

		CrawlingExecutionContext ctx = new CrawlingExecutionContext(this.baseDownloadPath, c, sessionData);
		
		List<Crawler> crawlers = this.getCrawlers(access, file, c.getBaseCrawl()==null);
		CrawlPipelineImpl pipeline = new CrawlPipelineImpl(c.getId().toString(), crawlers);
		pipeline.setListener(this);
		
		ResourceConsumptionService consumptionService = this.getConsumptionService(c, sc);
		for (Crawler crawler : pipeline.getRunnablesMap().values()) {
			if (consumptionService!=null && crawler instanceof Processor) {
				((Processor)crawler).addConsumptionService(consumptionService);
			}
			if (ResourceProcessingService.class.isAssignableFrom(crawler.getClass())) {
				((ResourceProcessingService)crawler).setExecutionContext(ctx);
			}
			crawler.init(ds, c, sc);
		}

		return pipeline;
	}
	
	protected ResourceConsumptionService getConsumptionService(Crawl c, ExtendedDatamodelContainer dmc) {
		return null;
	}
	
	protected void enqueue(CrawlPipeline pipeline, Crawl c) {
		try {
			statusMapslock.lock();
			this.crawlIdServiceIdMap.put(c.getId(), pipeline.getUuid());
			this.serviceIdServiceMap.put(pipeline.getUuid(), pipeline);
			pipelineExecutor.execute(pipeline);
			
			logger.debug("Enqueued {} crawl [{}] for datasource [{}] with dataset [{}]", (c.isOnline() ? "ONLINE" : "OFFLINE"), c.getId(), c.getDataset().getDatasource().getId(), c.getDataset().getId());
		} catch (Exception e) {
			logger.error("Failed to setup processing pipeline", e);
			this.error(pipeline.getUuid());
		} finally {
			statusMapslock.unlock();
		}
	}
	
		
	@Override 
	public void start(UUID serviceId) { 
		
	}
	
	@Override 
	public void updateSize(UUID serviceId, long size) { 
		
	}
	
	@Override 
	public void processed(UUID serviceId, long process) { 
		
	}
	
	@Override 
	public synchronized void finished(UUID serviceId) {
		Long crawlId = this.removeServiceFromCache(serviceId);
		this.updateCrawlAndCollection(crawlId, ProcessingServiceStates.COMPLETE);
	}
	
	@Override
	public synchronized void error(UUID serviceId) {
		Long crawlId = this.removeServiceFromCache(serviceId);
		this.updateCrawlAndCollection(crawlId, ProcessingServiceStates.ERROR);
	}
	
	
	private void updateCrawlAndCollection(Long crawlId, ProcessingServiceStates state) {
		Crawl cr = this.updateCrawl(crawlId, state);
		if (cr!=null) {
			this.updateCollection(cr);
		}		
	}
	
	private void updateCollection(Crawl cr) {
		Collection c = collectionService.findByDatasetId(cr.getDataset().getId()); 
		NextExecution ne = BaseScheduledRunnable.calculateNextExecution(c.getUpdatePeriod(), cr.getCreated(), cr.getModified());
		
		OnlineDataset ds = (OnlineDataset)datasetService.findById(cr.getDataset().getId());
		ds.setNextExecution(ne.getNextExecutionTimestamp());
		//ds.setOutdated(false);
				
		datasetService.saveDataset(ds);
	}
	
	private Crawl updateCrawl(Long crawlId, ProcessingServiceStates state) {
		if (crawlId==null || state==null) {
			return null;
		}
		Crawl c = crawlService.findById(crawlId);
		if (state==ProcessingServiceStates.ERROR) {
			c.setError(true);
			c.setComplete(false);
		} else if (state==ProcessingServiceStates.COMPLETE) {
			c.setComplete(true);		
		} else {
			c.setError(false);
			c.setComplete(false);
		}
		crawlService.save(c);
		return c;
	}
		
	private Long removeServiceFromCache(UUID uuid) {
		Long cId = null;
		try {
			statusMapslock.lock();
			if (serviceIdServiceMap.containsKey(uuid)) {
				serviceIdServiceMap.remove(uuid);
			}
			for (Long crawlId : this.crawlIdServiceIdMap.keySet()) {
				if (this.crawlIdServiceIdMap.get(crawlId).equals(uuid)) {
					this.crawlIdServiceIdMap.remove(crawlId);
					cId = crawlId;
					break;
				}
			}
		} finally {
			statusMapslock.unlock();
		}
		return cId;
	}
	
	private List<Crawler> getCrawlers(String accessType, String fileType, boolean online) throws ProcessingConfigException {
		List<Crawler> chain = new ArrayList<>();
		if (online) {
			chain.addAll(this.getCrawlers(accessType, accessChains.stream().collect(Collectors.toMap(AccessChain::getKey, AccessChain::getValue))));
		}
		chain.addAll(this.getCrawlers(fileType, fileProcessingChains.stream().collect(Collectors.toMap(FileProcessingChain::getKey, FileProcessingChain::getValue))));
		return chain;
	}
	
	private List<Crawler> getCrawlers(String method, Map<String, String> processingChain) throws ProcessingConfigException {
		if (!processingChain.containsKey(method)) {
			logger.error("No processing service implemented/configured for method [{}]", method);
			throw new ProcessingConfigException(String.format("No processing service implemented/configured for method [%s]", method));
		}
		try {
			String[] serviceRefs = processingChain.get(method).split(",");
			List<Crawler> result = new ArrayList<>(serviceRefs.length);
			for (int i=0; i<serviceRefs.length; i++) {
				result.add((Crawler)appContext.getBean(serviceRefs[i].trim()));
			}
			return result;
		} catch (Exception e) {
			logger.error("An error occurred while initializing processing", e);
			throw new ProcessingConfigException(e);
		}
	}

}
