package de.unibamberg.minf.transformation.dao.fs;

import java.io.IOException;

import de.unibamberg.minf.core.util.DirectoryInfo;

public interface DownloadDataDao {
	public String getDownloadParentPath(String... subdirs);
	public void clearAllData(String... subdirs) throws IOException;
	public void resetRenderedData(String... subdirs) throws IOException;
	public boolean hasData(String... subdirs);
	public DirectoryInfo getDataInfo(String... subdirs);
}
