package de.unibamberg.minf.transformation.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.dme.model.mapping.base.Mapping;
import de.unibamberg.minf.transformation.dao.fs.MappingConfigDao;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;

public class MappingServiceImpl implements MappingService, InitializingBean {
	@Autowired private MappingConfigDao mappingConfigDao;
	
	
	private Map<String, ExtendedMappingContainer> mappingsMap;
	private Map<String, Map<String, ExtendedMappingContainer>> sourceAndTargetMappingsMap;
	private Map<String, List<ExtendedMappingContainer>> targetMappingsMap;
	
	private ReentrantLock lock = new ReentrantLock();
	
	
	@Override
	public void afterPropertiesSet() throws Exception {
		this.findAll(true);
	}
	

	@Override
	public ExtendedMappingContainer findById(String id) {
		return mappingConfigDao.findById(id);
	}
	
	@Override
	public List<ExtendedMappingContainer> findAll() {
		return this.findAll(false);
	}
	
	@Override
	public List<ExtendedMappingContainer> findAll(boolean rebuildCache) {
		lock.lock();
		try {
			if (!rebuildCache && this.mappingsMap!=null) {
				return new ArrayList<ExtendedMappingContainer>(this.mappingsMap.values());
			}
			this.mappingsMap = new HashMap<String, ExtendedMappingContainer>();
			this.sourceAndTargetMappingsMap = new HashMap<String, Map<String, ExtendedMappingContainer>>();
			this.targetMappingsMap = new HashMap<String, List<ExtendedMappingContainer>>();;
		} finally {
			lock.unlock();
		}
		
		List<ExtendedMappingContainer> result = mappingConfigDao.findAll();
		if (result!=null) {
			for (ExtendedMappingContainer mc : result) {
				this.putMapping(mc);
			}
		}
		return result;
	}
	
	@Override
	public void saveMapping(ExtendedMappingContainer mapping) {
		this.putMapping(mapping);
		mappingConfigDao.saveOrUpdate(mapping);
	}
	
	@Override
	public void saveMappings(List<ExtendedMappingContainer> mappings) {
		for (ExtendedMappingContainer mapping : mappings) {
			this.saveMapping(mapping);
		}
	}
	
	@Override
	public boolean deleteMapping(ExtendedMappingContainer removeMc) {
		this.removeMapping(removeMc);
		mappingConfigDao.deleteMapping(removeMc.getId());
		return true;
	}

	@Override
	public List<ExtendedMappingContainer> getMappingsBySource(String sourceId) {
		List<ExtendedMappingContainer> result = new ArrayList<ExtendedMappingContainer>();
		lock.lock();
		try {
			if (sourceAndTargetMappingsMap.containsKey(sourceId)) {
				Map<String, ExtendedMappingContainer> targetMappingsMap = sourceAndTargetMappingsMap.get(sourceId);
				for (String targetId : targetMappingsMap.keySet()) {
					result.add(targetMappingsMap.get(targetId));
				}
			}
		} finally {
			lock.unlock();
		}
		return result;
	}

	@Override
	public List<ExtendedMappingContainer> getMappingsByTarget(String targetId) {
		List<ExtendedMappingContainer> result = new ArrayList<ExtendedMappingContainer>();
		lock.lock();
		try {
			if (targetMappingsMap.containsKey(targetId)) {
				result.addAll(targetMappingsMap.get(targetId));
			}
		} finally {
			lock.unlock();
		}
		return result;
	}

	@Override
	public ExtendedMappingContainer getMappingBySourceAndTarget(String sourceId, String targetId) {
		lock.lock();
		try {
			if (sourceAndTargetMappingsMap.containsKey(sourceId)) {
				Map<String, ExtendedMappingContainer> targetMappingsMap = sourceAndTargetMappingsMap.get(sourceId);
				for (String avTargetId : targetMappingsMap.keySet()) {
					if (avTargetId.equals(targetId)) {
						return targetMappingsMap.get(targetId);
					}
				}
			}
		} finally {
			lock.unlock();
		}
		return null;
	}
	
	private void removeMapping(ExtendedMappingContainer removeMc) {
		lock.lock();
		try {
			// Mappings by id
			mappingsMap.remove(removeMc.getId());
			
			// Mappings by target
			List<ExtendedMappingContainer> mappings;
			if (targetMappingsMap.containsKey(removeMc.getTargetSchemaId())) {
				mappings = targetMappingsMap.get(removeMc.getTargetSchemaId());
				for (ExtendedMappingContainer existMc : mappings) {
					if (existMc.getId().equals(removeMc.getId())) {
						mappings.remove(existMc);
						break;
					}
				}
			}
			
			// Mappings by source and target
			Map<String, ExtendedMappingContainer> targetMap;
			if (sourceAndTargetMappingsMap.containsKey(removeMc.getSourceSchemaId())) {
				targetMap = sourceAndTargetMappingsMap.get(removeMc.getSourceSchemaId());
				if (targetMap.containsKey(removeMc.getTargetSchemaId())) {
					targetMap.remove(removeMc.getTargetSchemaId());
				}
			} 
		} finally {
			lock.unlock();
		}
	}
	
	private void putMapping(ExtendedMappingContainer setMc) {
		lock.lock();
		try {
			// Mappings by id
			mappingsMap.put(setMc.getId(), setMc);
			
			// Mappings by target
			List<ExtendedMappingContainer> mappings;
			if (targetMappingsMap.containsKey(setMc.getTargetSchemaId())) {
				mappings = targetMappingsMap.get(setMc.getTargetSchemaId());
				for (ExtendedMappingContainer existMc : mappings) {
					if (existMc.getId().equals(setMc.getId())) {
						mappings.remove(existMc);
						break;
					}
				}
			} else {
				mappings = new ArrayList<ExtendedMappingContainer>(1);
				targetMappingsMap.put(setMc.getTargetSchemaId(), mappings);
			}
			mappings.add(setMc);
			
			// Mappings by source and target
			Map<String, ExtendedMappingContainer> targetMap;
			if (sourceAndTargetMappingsMap.containsKey(setMc.getSourceSchemaId())) {
				targetMap = sourceAndTargetMappingsMap.get(setMc.getSourceSchemaId());
			} else {
				targetMap = new HashMap<String, ExtendedMappingContainer>(1);
				sourceAndTargetMappingsMap.put(setMc.getSourceSchemaId(), targetMap);
			}
			targetMap.put(setMc.getTargetSchemaId(), setMc);
		} finally {
			lock.unlock();
		}
	}
}