package de.unibamberg.minf.transformation.automation.base;

import org.springframework.context.ApplicationContextAware;

import de.unibamberg.minf.transformation.pojo.ApiStatusPojo;

public interface SyncService extends ApplicationContextAware {
	public ApiStatusPojo getServiceStatus();
	/*public DateTime getTimestampOfLastExecution();
	public DateTime getTimestampOfPlannedExecution();*/
	/*public boolean isInProgress();
	public Boolean callAsync();
	public Boolean call();*/
}
