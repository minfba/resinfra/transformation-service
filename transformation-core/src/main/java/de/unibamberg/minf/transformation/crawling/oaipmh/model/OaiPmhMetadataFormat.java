package de.unibamberg.minf.transformation.crawling.oaipmh.model;

import javax.xml.bind.annotation.XmlElement;

public class OaiPmhMetadataFormat {
	private String metadataPrefix;
	private String schema;
	private String metadataNamespace;
	
	@XmlElement(name="metadataPrefix")
	public String getMetadataPrefix() { return metadataPrefix; }
	public void setMetadataPrefix(String metadataPrefix) { this.metadataPrefix = metadataPrefix; }
	
	@XmlElement(name="schema")
	public String getSchema() { return schema; }
	public void setSchema(String schema) { this.schema = schema; }
	
	@XmlElement(name="metadataNamespace")
	public String getMetadataNamespace() { return metadataNamespace; }
	public void setMetadataNamespace(String metadataNamespace) { this.metadataNamespace = metadataNamespace; }
}