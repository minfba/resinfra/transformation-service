package de.unibamberg.minf.transformation.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.unibamberg.minf.transformation.dao.db.BagDao;
import de.unibamberg.minf.transformation.model.Bag;

@Service
public class BagServiceImpl implements BagService {

	@Autowired private BagDao bagDao;
	
	@Override
	public Optional<Bag> findByUniqueId(String uniqueId) {
		return bagDao.findByUniqueId(uniqueId);
	}
}
