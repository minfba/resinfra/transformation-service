package de.unibamberg.minf.transformation.dao.fs;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.EnumMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.InitializingBean;

import de.unibamberg.minf.dme.model.grammar.AuxiliaryFile;
import de.unibamberg.minf.dme.model.grammar.GrammarContainer;
import de.unibamberg.minf.dme.model.grammar.AuxiliaryFile.FileTypes;
import de.unibamberg.minf.gtf.compilation.GrammarCompiler;
import de.unibamberg.minf.gtf.exceptions.GrammarProcessingException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GrammarDaoImpl extends BaseFsDao implements GrammarDao, InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		File grammarsDir = new File(config.getPaths().getGrammars());
		if (!grammarsDir.exists()) {
			FileUtils.forceMkdir(grammarsDir);
			log.info("Created grammars directory: " + config.getPaths().getGrammars());
		}
	}
	
	@Override
	public void deleteGrammar(String grammarId) throws IOException {
		FileUtils.deleteDirectory(new File(this.getGrammarDirectory(grammarId)));
	}
	
	/*
	 * TODO: refactor to a common method somewhere in gtf (@see de.unibamberg.minf.dme.service.GrammarServiceImpl) 
	 */
	@Override
	public boolean saveGrammar(GrammarContainer gc) throws IOException {
		if (gc.getParserGrammar()==null || gc.getParserGrammar().trim().isEmpty()) {
			log.info(String.format("Grammar %s is passthrough grammar. Nothing to save.", gc.getId()));
			return false;
		}
		/*if (grammar.isError()) {
			log.warn(String.format("Grammar %s is on error state. Nothing to save.", grammar.getId()));
			return false;
		}*/
		
		String dirPath = getGrammarDirectory(gc.getId());
		String filePathPrefix = getGrammarFilePrefix(gc.getId());
		
		if (Files.exists(Paths.get(dirPath))) {				
			FileUtils.deleteDirectory(new File(dirPath));
		}
		Files.createDirectories(Paths.get(dirPath));
				
		String lexerGrammar = gc.getLexerGrammar();
		String parserGrammar = gc.getParserGrammar();

		Map<FileTypes, String> fileTypeNameMap = new EnumMap<>(FileTypes.class);
		
		if (gc.getAuxiliaryFiles()!=null) {
			String content;
			for (AuxiliaryFile f : gc.getAuxiliaryFiles()) {
				content = f.getContent().replace("{LEXER}", gc.getId() + "Lexer").replace("{PARSER}", gc.getId() + "Parser");
				Files.write(Paths.get(dirPath + f.getFileType().getFileName()), content.getBytes());
				fileTypeNameMap.put(f.getFileType(), f.getFileType().getFileName().substring(0, f.getFileType().getFileName().indexOf('.')));
			}
		}
		
		if (lexerGrammar!=null && !lexerGrammar.trim().isEmpty()) {
			if (fileTypeNameMap.containsKey(FileTypes.LEXER_SUPERCLASS)) {
				lexerGrammar = "options { superClass= " + fileTypeNameMap.get(FileTypes.LEXER_SUPERCLASS) + "; }\n\n" + lexerGrammar;
			}
			lexerGrammar = "lexer grammar " + gc.getId() + "Lexer;\n\n" + lexerGrammar;
			
			Files.write(Paths.get(filePathPrefix + "Lexer.g4"), lexerGrammar.getBytes());
			
			parserGrammar = "parser grammar " + gc.getId() + "Parser; " + 
							"options { tokenVocab= " + gc.getId() + "Lexer; }\n\n" + 
							parserGrammar;
			Files.write(Paths.get(filePathPrefix + "Parser.g4"), parserGrammar.getBytes());
		} else {
			parserGrammar = "grammar " + gc.getId() + ";\n\n" + parserGrammar;
			Files.write(Paths.get(filePathPrefix + ".g4"), parserGrammar.getBytes());
		}
		return true;
	}	
	
	@Override
	public void compileGrammar(GrammarContainer gc) throws GrammarProcessingException, IOException {
		GrammarCompiler grammarCompiler = new GrammarCompiler();
		grammarCompiler.init(new File(this.getGrammarDirectory(gc.getId())), gc.getId());
		grammarCompiler.generateGrammar();
		grammarCompiler.compileGrammar();
				
		/*if ( (grammar.getBaseMethod()==null || grammar.getBaseMethod().trim().isEmpty()) && 
				(grammarCompiler.getParserRules()!=null && grammarCompiler.getParserRules().size()>0 ) ) {
			grammar.setBaseMethod(grammarCompiler.getParserRules().get(0));
		}
		if (grammarCompiler.getParserRules()!=null && !grammarCompiler.getParserRules().contains(grammar.getBaseMethod())) {
			grammar.setError(true);
		} else {
			grammar.setError(false);
		}*/
	}
	
	@Override
	public String getGrammarDirectory(String grammarId) {
		return config.getPaths().getGrammars() + File.separator + grammarId + File.separator;
	}
	
	private String getGrammarFilePrefix(String grammarId) {
		return getGrammarDirectory(grammarId) + grammarId;
	}
}
