package de.unibamberg.minf.transformation.automation;

import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import de.unibamberg.minf.transformation.api.client.ApiConsumptionException;
import de.unibamberg.minf.transformation.api.client.MappingClient;
import de.unibamberg.minf.transformation.api.client.ModelClient;
import de.unibamberg.minf.transformation.automation.base.BaseScheduledRunnable;
import de.unibamberg.minf.transformation.automation.base.SyncService;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import de.unibamberg.minf.transformation.pojo.ApiStatusPojo;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.DatasetService;
import de.unibamberg.minf.transformation.service.GrammarService;
import de.unibamberg.minf.transformation.service.MappingService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DmeSyncService extends BaseScheduledRunnable implements SyncService {

	@Autowired protected DatamodelService datamodelService;
	@Autowired private MappingService mappingService;
	@Autowired private GrammarService grammarService;
	
	@Autowired private DatasetService datasetService;
	
	private final ReentrantLock reLock = new ReentrantLock(true);
	
	@Value("${datamodels.indexing}")
	private String indexingModelEntityId;
	
	@Value("${datamodels.integration}")
	private String integrationModelEntityId;
	
	@Value("${datamodels.presentation}")
	private String presentationModelEntityId;
	
	@Value("${datamodels.metadata}")
	private String metadataModelEntityId;
	
	
	@Value(value="${indexing.data.prefix:gs_}")
	private String indexNamePrefix;
	
	
	private ApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	@Override
	protected void init() {
		// No initialization required for this service
	}
	
	@Override
	protected void executeAutomation() {
		if (this.isAutomationEnabled()) {
			this.syncModelsAndMappings();
		}
	}
	
	public void syncModelsAndMappings() {
		log.info("Updating datamodels/mappings from DME");
		try {
			reLock.lock();
			this.syncMappings();
			this.rescanDatamodels(this.syncModels());
			log.info("Completed update of datamodels/mappings from DME");
		} catch (Exception | ApiConsumptionException e) {
			log.error("Failed to update datamodels/mappings from DME", e);
		} finally {
			reLock.unlock();
		}
	}
	
	public boolean isBusy() {
		return reLock.isLocked();
	}
	
	@Override
	public ApiStatusPojo getServiceStatus() {
		try {
			return applicationContext.getBean(ModelClient.class).ping();
		} catch (Exception e) {
			log.error("An error occurred while connecting to API service", e);
		}
		return null;
	}
	
	private List<ExtendedMappingContainer> syncMappings() throws ApiConsumptionException {
		List<ExtendedMappingContainer> mappings = mappingService.findAll();
		MappingClient mappingClient = applicationContext.getBean(MappingClient.class);
		mappingClient.setCurrentMappings(mappings);
		mappingClient.sync();
		processMappingSynchronizationResult(mappings, mappingClient);
		return mappings;
	}
	
	private void processMappingSynchronizationResult(List<ExtendedMappingContainer> mappings, MappingClient client) {
		mappings.removeIf(m -> m.isDeleted() && mappingService.deleteMapping(m));
		mappings.stream()
			.filter(m -> m.isNew() || m.isUpdated())
			.forEach(m -> grammarService.importGrammarContainers(client.getImportGrammars(m.getMapping().getId())));
		
		if (!mappings.isEmpty()) {
			mappingService.saveMappings(mappings);
		}
	}
	
	private List<ExtendedDatamodelContainer> syncModels() throws ApiConsumptionException {
		List<ExtendedDatamodelContainer> datamodels = datamodelService.findAll();
		ModelClient modelClient = applicationContext.getBean(ModelClient.class);
		modelClient.setCurrentDatamodels(datamodels);
		modelClient.sync();
		processDatamodelSynchronizationResult(datamodels, modelClient);
		return datamodels;
	}

	private void processDatamodelSynchronizationResult(List<ExtendedDatamodelContainer> datamodels, ModelClient client) {
		// Deleted datamodels are deleted if there is no affected datasets otherwise -> flag
		datamodels.removeIf(d -> d.isDeleted() && datasetService.findOnlineByDatamodelId(d.getId()).isEmpty() && datamodelService.deleteDatamodel(d.getId()));
		datamodels.stream()
			.filter(d -> !d.isDeleted())
			.forEach(d -> grammarService.importGrammarContainers(client.getImportGrammars(d.getModel().getId())));
	}
		
	private void rescanDatamodels(List<ExtendedDatamodelContainer> datamodels) {
		Map<String, ExtendedDatamodelContainer> datamodelsMap = datamodels.stream().collect(Collectors.toMap(ExtendedDatamodelContainer::getId, d -> d));
		
		// Refresh auxiliary models first
		this.processAndRemoveAuxiliaryDatamodel(datamodelsMap, indexingModelEntityId, false, "index");
		this.processAndRemoveAuxiliaryDatamodel(datamodelsMap, presentationModelEntityId, false, "presentation");
		this.processAndRemoveAuxiliaryDatamodel(datamodelsMap, metadataModelEntityId, false, "metadata");
		this.processAndRemoveAuxiliaryDatamodel(datamodelsMap, integrationModelEntityId, true, "integration");
		
		// Refresh remaining datamodels
		datamodelsMap.values().stream().forEach(d -> processDatamodel(d, true));
	}
	
	private void processAndRemoveAuxiliaryDatamodel(Map<String, ExtendedDatamodelContainer> datamodelsMap, String modelId, boolean indexable, String label) {
		if (datamodelsMap.containsKey(modelId)) {
			this.processDatamodel(datamodelsMap.remove(modelId), indexable);
		} else {
			log.warn("Configured {} model [{}] not available. Please check configuration and/or assigned DME instance.", label, modelId);
		}
	}
	
	protected void processDatamodel(ExtendedDatamodelContainer datamodel, boolean indexable) {
		// Nothing to do if datamodel has been deleted
		if (datamodel.isDeleted()) {
			datamodelService.saveOrUpdate(datamodel);
			return;
		}
		ExtendedDatamodelContainer existDatamodel = datamodelService.loadById(datamodel.getId());
		if (existDatamodel!=null && existDatamodel.getIndexName()!=null) {
			datamodel.setIndexName(existDatamodel.getIndexName());
			datamodel.setAnalyzerFieldMap(existDatamodel.getAnalyzerFieldMap());
		}
		
		// Save creates index name on new models
		datamodelService.saveOrUpdate(datamodel);
		
	}
}
