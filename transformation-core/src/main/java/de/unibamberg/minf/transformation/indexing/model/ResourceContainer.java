package de.unibamberg.minf.transformation.indexing.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import de.unibamberg.minf.processing.model.SerializableResource;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.processing.model.helper.ResourceHelper;
import de.unibamberg.minf.transformation.TransformationConstants.ResourceEnrichmentStages;
import de.unibamberg.minf.transformation.TransformationConstants.RootElementKeys;

public class ResourceContainer implements Resource {	
	private ResourceEnrichmentStages currentStage;
	
	private Resource content;
	private Resource integrations;
	private Resource presentation;
	private JsonNode meta;
	private String id;

	public ResourceEnrichmentStages getCurrentStage() { return currentStage; }
	public void setCurrentStage(ResourceEnrichmentStages currentStage) { this.currentStage = currentStage; }
		
	public Resource getContent() { return content; }	
	public Resource getIntegrations() { return integrations; }
	public Resource getPresentation() { return presentation; }
	
	public JsonNode getMeta() { return meta; }
	public void setMeta(JsonNode meta) { this.meta = meta; }
	
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
	
	
	public ResourceContainer() { } 
	
	public ResourceContainer(Resource content) {
		this.content = content;
	}
	
	public void setCurrentResource(Resource currentResource) { 
		if (this.currentStage == ResourceEnrichmentStages.RESOURCE_PRESENTATION) {
			if (currentResource!=null) {
				this.presentation = currentResource;
			}
		} else if (this.currentStage == ResourceEnrichmentStages.INTEGRATIONS_TRANSFORMATION) {
			this.integrations = currentResource; 
		} else if (this.currentStage == ResourceEnrichmentStages.INTEGRATIONS_PRESENTATION) {
			if (this.presentation!=null && currentResource!=null) {
				ResourceHelper.mergeResourceTrees(this.presentation, (Resource)currentResource);
			} else if (currentResource!=null) {
				this.presentation = currentResource;
			}
		}
	}
		
	public Resource getCurrentResource() {
		// Resource currently at transformation => now presentation
		if (this.currentStage == ResourceEnrichmentStages.INTEGRATIONS_TRANSFORMATION) {
			return this.integrations;
		} else {
			return this.content;
		} 
	}
	
	public Map<String, Object> toSource() {
		Map<String, Object> source = new HashMap<>();
		source.put(RootElementKeys.CONTENT.toString(), this.getContent());
		if (this.getIntegrations()!=null) {
			source.put(RootElementKeys.INTEGRATIONS.toString(), this.getIntegrations());
		}
		if (this.getPresentation()!=null) {
			source.put(RootElementKeys.PRESENTATION.toString(), this.getPresentation());
		}		
		if (this.getMeta()!=null) {
			source.put(RootElementKeys.META.toString(), this.getMeta());
		}
		return source;
	}
		
	/* Facade to encapsulated resources */
	
	@Override public List<Resource> getChildResources() { return this.getCurrentResource().getChildResources(); }
	@Override public void setChildResources(List<Resource> childResources) { this.getCurrentResource().setChildResources(childResources); }
	
	@Override public String getKey() { return this.getCurrentResource().getKey(); }
	@Override public void setKey(String key) { this.getCurrentResource().setKey(key); }
	
	@Override public String getElementId() { return this.getCurrentResource().getElementId(); }
	@Override public void setElementId(String elementId) { this.getCurrentResource().setElementId(elementId); }
	
	@Override public Object getValue() { return this.getCurrentResource().getValue(); }
	@Override public void setValue(Object value) { this.getCurrentResource().setValue(value); }
	
	@Override public boolean isOutput() { return this.getCurrentResource().isOutput(); }
	@Override public void setOutput(boolean output) { this.getCurrentResource().setOutput(output); }
	
	@Override public boolean isAutoHierarchy() { return this.getCurrentResource().isAutoHierarchy(); }
	@Override public void setAutoHierarchy(boolean autoHierarchy) { this.getCurrentResource().setAutoHierarchy(autoHierarchy); }
	
	@Override public void addChildResource(Resource childResource) { this.getCurrentResource().addChildResource(childResource); }
	@Override public void addChildResource(String key, Object value) { this.getCurrentResource().addChildResource(key, value); }
	@Override public void addChildResources(Resource[] childResources) { this.getCurrentResource().addChildResources(childResources); }
	@Override public void addChildResources(List<Resource> childResources) { this.getCurrentResource().addChildResources(childResources); }
	
	@Override public List<Resource> findChildren(String path) { return this.getCurrentResource().findChildren(path); }
	
	@Override public void addChildResource(int position, Resource childResource) { this.getCurrentResource().addChildResource(position, childResource); }
	@Override public void addChildResource(int position, String key, Object value) { this.getCurrentResource().addChildResource(position, key, value); }
	
	@Override public boolean isEmpty() { return this.getCurrentResource().isEmpty(); }
	@Override public boolean hasValue() { return this.getCurrentResource().hasValue(); }
	@Override public boolean hasChildren() { return this.getCurrentResource().hasChildren(); }
	@Override public void setOrAppendValue(Object value) { this.getCurrentResource().setOrAppendValue(value); }
	
	@Override public boolean isExploded() { return this.getCurrentResource().isExploded(); }
	@Override public void setExploded(boolean exploded) { this.getCurrentResource().setExploded(exploded); }
}