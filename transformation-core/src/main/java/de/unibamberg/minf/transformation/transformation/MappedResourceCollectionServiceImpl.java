package de.unibamberg.minf.transformation.transformation;

import java.util.ArrayList;
import java.util.List;

import de.unibamberg.minf.processing.consumption.MappedResourceConsumptionService;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.transformation.transformation.model.MappedResourcePair;

public class MappedResourceCollectionServiceImpl implements MappedResourceConsumptionService {
	
	private List<MappedResourcePair> resourcePairs;
	
	
	public List<MappedResourcePair> getResourcePairs() { return resourcePairs; }
	public void setResourcePairs(List<MappedResourcePair> resourcePairs) { this.resourcePairs = resourcePairs; }
	

	@Override
	public void init(String schemaId) throws ProcessingConfigException {
		this.setResourcePairs(new ArrayList<MappedResourcePair>());
	}

	@Override
	public boolean consume(Resource obj) {
		this.getResourcePairs().add(new MappedResourcePair(obj, null));
		return true;
	}

	@Override
	public int commit() {
		return resourcePairs.size();
	}

	@Override
	public boolean consume(Resource rSource, Resource rTarget) {
		this.getResourcePairs().add(new MappedResourcePair(rSource, rTarget));
		return true;
	}

}
