package de.unibamberg.minf.transformation.config.nested;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ColregConfigProperties extends ApiServiceConfigProperties {
    private String fetchAllPath = "api/collections";
    private String fetchDetailPath = "api/collections/%s/";
    private String getCollectionPath = "collections/%s";
    
    private boolean autocrawlNewDatasets;
    
    public String getFetchAllUrl() {
    	return this.getUrl(fetchAllPath);
    }
    
    public String getFetchDetailUrl() {
    	return this.getUrl(fetchDetailPath);
    }

	public String getCollectionUrl() {
		return this.getUrl(getCollectionPath);
	}
}
