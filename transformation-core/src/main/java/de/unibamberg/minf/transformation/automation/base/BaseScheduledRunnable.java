package de.unibamberg.minf.transformation.automation.base;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.Seconds;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import de.unibamberg.minf.transformation.automation.NextExecution;
import de.unibamberg.minf.transformation.automation.NextExecution.CALCULATION_METHODS;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BaseScheduledRunnable implements ScheduledRunnable, DisposableBean, InitializingBean {
	@Getter @Setter private boolean automationEnabled;
	@Getter @Setter private String cronExpression;
	@Getter @Setter protected boolean debugging;
	@Getter @Setter private int timeout;
	
	@Getter private Date nextExecution;
	public synchronized void setNextExecution(Date nextExecution) {
		this.nextExecution = nextExecution;
	}
	
	@Getter private Date lastCompletion;
	public synchronized void setLastCompletion(Date lastCompletion) {
		this.lastCompletion = lastCompletion;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}
	
	@Override
	public void run() {	
		this.executeAutomation();
		this.setLastCompletion(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()));
	}
	
	
	@Override
	public void destroy() throws Exception {
		try {
			/*pipelineExecutor.shutdown();
		    // Wait until all threads are finished
		    while (!pipelineExecutor.isTerminated()) {}*/
		} catch (final Exception e) {
			log.error("Error closing sync executor", e);
		}
	}

	/* Currently this calculation and policy handling serves as base for
	 *  1. determining next online crawls of data sets
	 *  2. synchronization with CR and DME data
	 *  
	 * This works but could be changed in the future to support different mechanisms
	 *  for these totally different functionalities
	 */
	public static NextExecution calculateNextExecution(String updatePeriod, OffsetDateTime lastStart, OffsetDateTime lastEnd) {			
		if (lastStart==null) {
			throw new IllegalArgumentException("No lastStart value provided: NULL");
		}
		if (lastEnd==null) {
			throw new IllegalArgumentException("Next execution calculation not possible if still in progress; no lastEnd provided");
		}
		
		DateTime start = toDateTime(lastStart);
		DateTime end = toDateTime(lastEnd);
		if (start.isAfter(end)) {
			throw new IllegalArgumentException("LastStart cannot be after lastEnd");
		}
		
		int policyDays = getPolicyAsDays(end, updatePeriod);
		double lastDurationMinutes = Seconds.secondsBetween(start, end).getSeconds() / 60D;
		
		// durationBasedMinAge = (log15 lastDurationMinutes+15)^4
		double durationBasedMinAge = Math.round(Math.pow((Math.log(lastDurationMinutes+15)/Math.log(15)),4));
		double durationBasedMaxAge = durationBasedMinAge*5;
		
		// Helper for exceptionally long durations (longer than 670 days, so purely theoretical here)
		if (durationBasedMinAge < lastDurationMinutes/1440*2) {
			durationBasedMinAge = lastDurationMinutes/1440*2;
			durationBasedMaxAge = lastDurationMinutes/1440*2;
			log.debug("Resetting durationBasedMinAge/durationBasedMaxAge for long duration ({} days)", lastDurationMinutes/1440);
		}
	
		/*  durM	minAgeD maxAgeD
		 * 	0		1		5
			5		1		5
			10		2		10
			60		6		30
			120		11		55
			1440	52		260
			7200	116		580
		 */
		CALCULATION_METHODS calculationMethod;
		double targetAge;
		
		
		// Use policy if defined and valid
		if (policyDays > durationBasedMinAge) {
			targetAge = policyDays;
			calculationMethod = CALCULATION_METHODS.POLICY_BASED;
			log.debug("Setting target age as policyDays => {} days", targetAge);
		} 
		// Use minimum by duration if policy too low, but specified
		else if (policyDays > 0) {
			targetAge = durationBasedMinAge;
			calculationMethod = CALCULATION_METHODS.DURATION_AND_POLICY_BASED;
			log.debug("Setting target age as durationBasedMinAge due to low policy days ({} days) => {} days", policyDays, targetAge);
		} 
		// Use duration values if no policy set
		else {
			targetAge = (durationBasedMinAge+durationBasedMaxAge)/2;
			calculationMethod = CALCULATION_METHODS.DURATION_BASED;
			log.debug("No update period specified, setting target age to durationBasedMinAge+durationBasedMaxAge)/2 => {} days", targetAge);
		}

		DateTime jodaDateTime = end.plusDays((int)targetAge);		
		Instant instant = Instant.ofEpochMilli(jodaDateTime.getMillis());
		OffsetDateTime nextExecution = OffsetDateTime.ofInstant(instant, jodaDateTime.getZone().toTimeZone().toZoneId());
		
		return new NextExecution(calculationMethod, policyDays, lastDurationMinutes, durationBasedMinAge, durationBasedMaxAge, targetAge, nextExecution);
	}
	
	protected static DateTime toDateTime(OffsetDateTime offsetDateTime) {
		return new DateTime(DateTimeZone.forOffsetHours(offsetDateTime.getOffset().getTotalSeconds()/3600))
				.withDate(offsetDateTime.getYear(), offsetDateTime.getMonthValue(), offsetDateTime.getDayOfMonth())
				.withTime(offsetDateTime.getHour(), offsetDateTime.getMinute(), offsetDateTime.getSecond(), 0);
	} 
	
	protected static int getPolicyAsDays(DateTime lastEnd, String updatePeriod) {
		try {
			if (updatePeriod!=null && !updatePeriod.isEmpty()) {
				Period p = Period.parse(updatePeriod);
				return (int)p.toDurationFrom(lastEnd).getStandardDays();
			}
		} catch (Exception e) {
			log.error("Failed to determine update policy in days", e);
		}
		return -1;
	}
	
	protected abstract void init();
	protected abstract void executeAutomation();
}
