package de.unibamberg.minf.transformation.transformation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.base.Nonterminal;
import de.unibamberg.minf.dme.model.datamodel.NonterminalImpl;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.processing.service.online.OaiPmhHarvestingService;
import de.unibamberg.minf.transformation.TransformationConstants.ResourceEnrichmentStages;
import de.unibamberg.minf.transformation.TransformationConstants.RootElementKeys;
import de.unibamberg.minf.transformation.indexing.model.ResourceContainer;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.MappingService;

@Component
@Scope("prototype")
public class ResourceEnrichmentServiceImpl implements ResourceEnrichmentService {
	protected final Logger logger = LoggerFactory.getLogger(ResourceEnrichmentServiceImpl.class);
		
	@Autowired private MappingService mappingService;
	@Autowired private DatamodelService datamodelService;
	@Autowired private ResourceTransformationService transformationService;
		
	@Value("${datamodels.integration}")
	private String integrationModelEntityId;
	
	@Value("${datamodels.presentation}")
	private String presentationModelEntityId;
	
	@Value("${debugging.indexing.disable_downloads:#{false}}")
	private boolean disableDownloads;
	
	@Value("${indexing.resources.index_errors:#{true}}")
	private boolean indexErrors;
	
	
	private String integrationsProcessingRoot;
	private String presentationsProcessingRoot;
	
	private Queue<ResourceEnrichmentStages> stageQueue;
	private List<ResourceContainer> resourceBatch;
	

	private String sourceModelId;
	
	private boolean initialized = false;
	
	public boolean isInitialized() { return initialized; }
	
	
	public String getSourceModelId() { return sourceModelId; }
	public void setSourceModelId(String sourceModelId) { this.sourceModelId = sourceModelId; }
	
	@Override
	public List<ResourceContainer> getResourceBatch() { return resourceBatch; }


	@Override
	public void init(String datamodelId) {
		// Consequent calls from transformationService.init() need no action
		if (!initialized) {
			logger.debug("Initializing new resource enrichtment service...");
			this.sourceModelId = datamodelId;
			this.setupStages();
			this.initialized=true;
			this.resourceBatch = new ArrayList<>();
			
			if (integrationModelEntityId==null) {
				logger.warn("No integrations model set, indexing source data model only");
			} else {
				integrationsProcessingRoot = this.findProcessingRootElementId(integrationModelEntityId);				
			}
			if (presentationModelEntityId==null) {
				logger.warn("No index model set, no presentation layer available");
			} else {
				presentationsProcessingRoot = this.findProcessingRootElementId(presentationModelEntityId);
			}
			this.runStage();
		}
	}
	
	private void setupStages() {		
		stageQueue = new LinkedList<>();
		stageQueue.add(ResourceEnrichmentStages.RESOURCE_COLLECTION);
		
		// Collect available, mapping-based indexing stages
		try {
			if (presentationModelEntityId!=null && !this.getSourceModelId().equals(presentationModelEntityId) && this.mappingService.getMappingBySourceAndTarget(this.getSourceModelId(), presentationModelEntityId)!=null) {
				stageQueue.add(ResourceEnrichmentStages.RESOURCE_PRESENTATION);
			}
		} catch (Exception e) {
			logger.error("Failed to setup RESOURCE_PRESENTATION stage", e);
		}
		try {
			if (integrationModelEntityId!=null && !this.getSourceModelId().equals(integrationModelEntityId) && mappingService.getMappingBySourceAndTarget(this.getSourceModelId(), integrationModelEntityId)!=null) {
				stageQueue.add(ResourceEnrichmentStages.INTEGRATIONS_TRANSFORMATION);
				
				try {
					if (presentationModelEntityId!=null && !presentationModelEntityId.equals(integrationModelEntityId) && mappingService.getMappingBySourceAndTarget(integrationModelEntityId, presentationModelEntityId)!=null) {
						stageQueue.add(ResourceEnrichmentStages.INTEGRATIONS_PRESENTATION);
					}
				} catch (Exception e) {
					logger.error("Failed to setup INTEGRATIONS_PRESENTATION stage", e);
				}
			}
		} catch (Exception e) {
			logger.error("Failed to setup INTEGRATIONS_TRANSFORMATION stage", e);
			throw e;
		}
	}
	
	private void runStage() {
		if (!this.isCurrentStage(ResourceEnrichmentStages.RESOURCE_COLLECTION)) {
			try {
				if (this.isCurrentStage(ResourceEnrichmentStages.RESOURCE_PRESENTATION)) {
					transformationService.init(mappingService.getMappingBySourceAndTarget(this.getSourceModelId(), presentationModelEntityId), this);
				} else if (this.isCurrentStage(ResourceEnrichmentStages.INTEGRATIONS_TRANSFORMATION)) {
					transformationService.init(mappingService.getMappingBySourceAndTarget(this.getSourceModelId(), integrationModelEntityId), this);	
				} else if (this.isCurrentStage(ResourceEnrichmentStages.INTEGRATIONS_PRESENTATION)) {
					transformationService.init(mappingService.getMappingBySourceAndTarget(integrationModelEntityId, presentationModelEntityId), this);
				}
				// To prevent co-modification: presentation stages can create partial resources
				List<ResourceContainer> transformBatch = new ArrayList<>();
				transformBatch.addAll(this.resourceBatch);
				transformationService.transformResources(transformBatch);
			} catch (Exception e) {
				logger.error(String.format("Failed to execute indexing stage [%s]", this.getCurrentStage().toString()), e);
			}
		} 
	}

	@Override
	public boolean consume(Resource resource) {
		return this.consume(resource, null);
	}

	@Override
	public boolean consume(Resource resource, Resource rTransformed) {
		if (resource==null) {
			return false;
		}		
		if (this.isCurrentStage(ResourceEnrichmentStages.RESOURCE_COLLECTION)) {
			ResourceContainer rc = this.collectResource(resource);
			rc.setCurrentStage(this.getCurrentStage());
			resourceBatch.add(rc);
			
			return true;
		}
		if (rTransformed==null) {
			return false;
		}
		
		ResourceContainer rc = (ResourceContainer)resource;
		rc.setCurrentStage(this.getCurrentStage());
		if (this.getCurrentStage()==ResourceEnrichmentStages.INTEGRATIONS_TRANSFORMATION) {
			rc.setCurrentResource(this.findProcessingRoot(rTransformed, integrationsProcessingRoot));
		} else {
			rc.setCurrentResource(this.findProcessingRoot(rTransformed, presentationsProcessingRoot));
		}
		// For our split resources
		if (!resourceBatch.contains(rc)) {
			resourceBatch.add(rc);
		}
		return true;
	}
	
	@Override
	public int commit() {
		stageQueue.remove(stageQueue.peek());
		if (stageQueue.isEmpty()) {
			// Restart with further incoming resources -> occurrs on every chunk
			this.setupStages();
		}
		this.runStage();
		return 0;
	}
	
	private String findProcessingRootElementId(String entityId) {
		if (entityId!=null) {
			ExtendedDatamodelContainer dmc = datamodelService.findById(entityId);
			Element root = dmc.getOrRenderElementHierarchy();
			Element processingRoot = NonterminalImpl.findProcessingRoot(root);
			return processingRoot==null ? root.getId() : processingRoot.getId();
		}
		return null;
	}
	
	private ResourceContainer collectResource(Resource resource) {
		try {
			Resource r = null;
			if (resource.getKey().equals(OaiPmhHarvestingService.OAI_PMH_ENVELOPE_RESOURCE)) {
				for (Resource rChild : resource.getChildResources()) {
					if (rChild.getKey().equals(RootElementKeys.CONTENT.toString())) {
						r = rChild;						
					}
				}
				if (r==null || r.getChildResources().isEmpty()) {
					logger.debug("Skipping empty resource");
					return null;
				} else if (r.getChildResources().size()>1) {
					logger.warn("Muliple resources found under one content element...only first 'root' element is processed");
				}
				r = r.getChildResources().get(0);
			} else {
				r = resource;
			}
			return new ResourceContainer(r);
		} catch (Exception e) {
			logger.warn("Failed to index processed resource", e);
		}
		return null;
	}

	private Resource findProcessingRoot(Resource resource, String processingRootId) {
		if (!resource.getElementId().equals(processingRootId)) {
			if (resource.getChildResources()!=null) {
				Resource rRoot;
				for (Resource rChild : resource.getChildResources()) {
					rRoot = this.findProcessingRoot(rChild, processingRootId);
					if (rRoot!=null) {
						return rRoot;
					}
				}
			}
		} else {
			return resource;
		}
		return null;
	}
	
	private ResourceEnrichmentStages getCurrentStage() {
		return this.stageQueue.peek();
	}
	
	private boolean isCurrentStage(ResourceEnrichmentStages compareStage) {
		return this.getCurrentStage()==compareStage;
	}


	@Override
	public void reset() {
		this.resourceBatch.clear();
	}
}
