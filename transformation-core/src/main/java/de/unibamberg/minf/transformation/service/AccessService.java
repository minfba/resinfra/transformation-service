package de.unibamberg.minf.transformation.service;

import java.util.List;
import java.util.Optional;

import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.model.Endpoint;

public interface AccessService {
	public List<Access> findAllAccess();
	public List<Endpoint> findAllEndpoints();
	public List<Datasource> findAllDatasources();

	public List<Endpoint> findAllEndpoints(boolean loadInterfaces);
	public List<Datasource> findAllDatasources(boolean loadDatasets);
	
	public Optional<Datasource> findDatasourceByUniqueId(String uniqueId);
	public Optional<Endpoint> findEndpointByUniqueId(String uniqueId);
	
	public Optional<Datasource> findDatasourceById(Long id);
	public Optional<Endpoint> findEndpointById(Long id);
	
	public void delete(Access access);
	public void deleteAll(Iterable<? extends Access> access);
	public void saveAccess(Access access);
}
