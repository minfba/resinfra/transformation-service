package de.unibamberg.minf.transformation.model;

import java.util.UUID;

import de.unibamberg.minf.transformation.model.base.AccessOperation;
import lombok.Data;

@Data
public class TemporaryAccessOperation implements AccessOperation {
	private String uniqueId;
	
	public TemporaryAccessOperation() {
		this.uniqueId = UUID.randomUUID().toString();
	}
	
	public TemporaryAccessOperation(String uniqueId) {
		this.uniqueId = uniqueId;
	}
}
