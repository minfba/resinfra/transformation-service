package de.unibamberg.minf.transformation.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.unibamberg.minf.transformation.model.base.BaseAccessibleEntityImpl;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Data store of a dataset. A bag corresponds to an entry in the filesystem of the hosting machine
 *  and represents snapshots of the datasets. 
 *  
 * With online datasets, a successful crawl creates a new bag of that datasource.  
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"dataset"})
@Table(uniqueConstraints={@UniqueConstraint(name="uniqueId",columnNames={"uniqueId"})})
public class Bag extends BaseAccessibleEntityImpl {
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="dataset_id", nullable=false)
	private Dataset dataset;
}
