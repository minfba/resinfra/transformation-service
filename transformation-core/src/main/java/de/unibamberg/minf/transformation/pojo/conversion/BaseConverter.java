package de.unibamberg.minf.transformation.pojo.conversion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnitUtil;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.core.web.localization.MessageSource;
import de.unibamberg.minf.transformation.config.BaseApiConfig;

public abstract class BaseConverter<T1, T2> implements Converter<T1, T2>, InitializingBean {
	
	private static final DateTimeFormatter POJO_DATETIME_FORMATTER = DateTimeFormat.forStyle("MM");
	private static final DateTimeFormatter POJO_DATE_FORMATTER = DateTimeFormat.forStyle("M-");
	
	
	@Autowired protected MessageSource messageSource;
	@Autowired protected BaseApiConfig apiConfig;
	
	@Autowired private EntityManagerFactory emf;
	
	private PersistenceUnitUtil persistenceUnitUtil;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		persistenceUnitUtil = emf.getPersistenceUnitUtil();
	}
	
	@Override
	public List<T2> convert(Collection<? extends T1> entities) {
		return this.convert(entities, Locale.getDefault());
	}
	
	@Override
	public List<T2> convert(Collection<? extends T1> entities, Locale locale) {
		return this.convert(entities, locale, new ArrayList<>());
	}
	
	@Override
	public List<T2> convert(Collection<? extends T1> entities, Locale locale, List<Class<?>> convertedTypes) {
		if (entities==null) {
			return new ArrayList<>(0);
		}		
		List<T2> result = new ArrayList<>(entities.size());
		for (T1 entity : entities) {
			result.add(this.convert(entity, locale, convertedTypes));
		}
		return result;
	}
	
	@Override
	public T2 convert(T1 entity) {
		return this.convert(entity, null);
	}
	
	@Override
	public T2 convert(T1 entity, Locale locale) {
		return this.convert(entity, locale, new ArrayList<>());
	}
	
	protected boolean isLoaded(Object entity) {
		return persistenceUnitUtil.isLoaded(entity);
	}
	
	protected boolean isLoaded(Object entity, String attributeName) {
		return persistenceUnitUtil.isLoaded(entity, attributeName);
	}
		
	public static String pojoFormatDateTime(DateTime dateTime, Locale locale) {
		return dateTime.toString(POJO_DATETIME_FORMATTER.withLocale(locale));
	}
	
	public static String pojoFormatDate(DateTime dateTime, Locale locale) {
		return dateTime.toString(POJO_DATE_FORMATTER.withLocale(locale));
	}
	
	protected boolean canProceedWithType(Class<?> type, List<Class<?>> convertedTypes) {
		if (convertedTypes==null) {
			return false;
		}
		return !convertedTypes.contains(type);
	}
	
	public abstract T2 fill(T2 pojo, T1 object, Locale locale, List<Class<?>> convertedTypes);
}
