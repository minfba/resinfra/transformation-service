package de.unibamberg.minf.transformation.model;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.joda.time.DateTime;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Form of a dataset that is hosted locally and filled by uploading data directly to the transformation service.
 *  As the transformation service does not intend to be a repository, local datasets expire and are deleted.
 *  
 * The concept of local datasets is to provide means to easily enrich, transform and share work-in-progress data 
 *  - i.e. that is not ready to be published in a repository. 
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@PrimaryKeyJoinColumn(name = "local_dataset_id")
public class LocalDataset extends Dataset {
	private String label;
	
	@Column(name = "expires", columnDefinition = "TIMESTAMP WITH TIME ZONE")
	private OffsetDateTime expires;
	
	@Override
	public boolean isLocal() { return true; }
}
