package de.unibamberg.minf.transformation.config.nested;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class CrawlingAutomationConfigProperties {
    private String cronSchedule = "0 0 4 * * *";
    private String cronScheduleOnline;
    private String cronScheduleOffline;
    private String cronScheduleCleanup;
    
    private boolean autocrawlCollections;
    private boolean autorefreshIndices;
    private boolean collectionsExpire;
    private boolean cleanupCrawls;
    
	public void checkSchedules() {
		if (cronSchedule!=null) {
			if (cronScheduleOnline==null) {
				cronScheduleOnline = cronSchedule;
				log.debug("cron_schedule_online not explicitly configured: using cron_schedule parameter");
			}
			if (cronScheduleOffline==null) {
				cronScheduleOffline = cronSchedule;
				log.debug("cron_schedule_offline not explicitly configured: using cron_schedule parameter");
			}
			if (cronScheduleCleanup==null) {
				cronScheduleCleanup = cronSchedule;
				log.debug("cron_schedule_cleanup not explicitly configured: using cron_schedule parameter");
			}
		}
		
		if (cronScheduleOnline!=null && cronScheduleOffline!=null) {
			log.warn("cron_schedule_offline and cron_schedule_offline are explicitly configured: ignoring cron_schedule parameter");
		}
	}
}
