package de.unibamberg.minf.transformation.service;

import java.util.Optional;

import de.unibamberg.minf.transformation.model.Bag;
import de.unibamberg.minf.transformation.model.Interface;

public interface BagService {

	Optional<Bag> findByUniqueId(String uniqueId);

}
