package de.unibamberg.minf.transformation.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import de.unibamberg.minf.transformation.config.nested.DatamodelConfigProperties;
import de.unibamberg.minf.transformation.config.nested.IndexingConfigProperties;
import de.unibamberg.minf.transformation.config.nested.PathsConfigProperties;
import de.unibamberg.minf.transformation.dao.fs.CrawlDataDao;
import de.unibamberg.minf.transformation.dao.fs.CrawlDataDaoImpl;
import de.unibamberg.minf.transformation.dao.fs.DatamodelConfigDao;
import de.unibamberg.minf.transformation.dao.fs.DatamodelConfigDaoImpl;
import de.unibamberg.minf.transformation.dao.fs.DownloadDataDao;
import de.unibamberg.minf.transformation.dao.fs.DownloadDataDaoImpl;
import de.unibamberg.minf.transformation.dao.fs.GrammarDao;
import de.unibamberg.minf.transformation.dao.fs.GrammarDaoImpl;
import de.unibamberg.minf.transformation.dao.fs.LogFileDao;
import de.unibamberg.minf.transformation.dao.fs.LogFileDaoImpl;
import de.unibamberg.minf.transformation.dao.fs.MappingConfigDao;
import de.unibamberg.minf.transformation.dao.fs.MappingConfigDaoImpl;
import de.unibamberg.minf.transformation.data.service.DownloadDataService;
import de.unibamberg.minf.transformation.data.service.DownloadDataServiceImpl;
import de.unibamberg.minf.transformation.service.AccessService;
import de.unibamberg.minf.transformation.service.AccessServiceImpl;
import de.unibamberg.minf.transformation.service.CollectionService;
import de.unibamberg.minf.transformation.service.CollectionServiceImpl;
import de.unibamberg.minf.transformation.service.CrawlService;
import de.unibamberg.minf.transformation.service.CrawlServiceImpl;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.DatamodelServiceImpl;
import de.unibamberg.minf.transformation.service.DatasetService;
import de.unibamberg.minf.transformation.service.DatasetServiceImpl;
import de.unibamberg.minf.transformation.service.GrammarService;
import de.unibamberg.minf.transformation.service.GrammarServiceImpl;
import de.unibamberg.minf.transformation.service.InterfaceService;
import de.unibamberg.minf.transformation.service.InterfaceServiceImpl;
import de.unibamberg.minf.transformation.service.MappingService;
import de.unibamberg.minf.transformation.service.MappingServiceImpl;
import de.unibamberg.minf.transformation.service.UserService;
import de.unibamberg.minf.transformation.service.UserServiceImpl;
import de.unibamberg.minf.transformation.transformation.ResourceEnrichmentService;
import de.unibamberg.minf.transformation.transformation.ResourceEnrichmentServiceImpl;
import de.unibamberg.minf.transformation.transformation.ResourceTransformationService;
import de.unibamberg.minf.transformation.transformation.ResourceTransformationServiceImpl;
import lombok.Data;

@Data
public class TransformationConfig {
	protected DatamodelConfigProperties datamodels;
	protected PathsConfigProperties paths; 
	protected IndexingConfigProperties indexing;
	
	
	/* =====================================
	 *  Filesystem DAOs
	 * ===================================== 
	 */
	@Bean public CrawlDataDao crawlDataDao() { return new CrawlDataDaoImpl(); }
	@Bean public DatamodelConfigDao datamodelConfigDao() { return new DatamodelConfigDaoImpl(); }
	@Bean public DownloadDataDao downloadDataDao() { return new DownloadDataDaoImpl(); }
	@Bean public GrammarDao grammarDao() { return new GrammarDaoImpl(); }
	@Bean public MappingConfigDao mappingConfigDao() { return new MappingConfigDaoImpl(); }
	@Bean public LogFileDao logFileDao() { return new LogFileDaoImpl(); }

	/* =====================================
	 *  Services
	 * ===================================== 
	 */
	@Bean public DownloadDataService downloadDataService() { return new DownloadDataServiceImpl(); }
	
	@Bean public CollectionService collectionService() { return new CollectionServiceImpl(); }
	@Bean public CrawlService crawlService() { return new CrawlServiceImpl(); }
	@Bean public DatamodelService datamodelService() { return new DatamodelServiceImpl(); }
	@Bean public DatasetService datasetService() { return new DatasetServiceImpl(); }
	@Bean public GrammarService grammarService() { return new GrammarServiceImpl(); }
	@Bean public MappingService mappingService() { return new MappingServiceImpl(); }
	@Bean public UserService userService() { return new UserServiceImpl(); }
	@Bean public InterfaceService interfaceService() { return new InterfaceServiceImpl(); }
	@Bean public AccessService accessService() { return new AccessServiceImpl(); }
	
	@Bean @Scope("prototype") public ResourceTransformationService resourceTransformationService() {return new ResourceTransformationServiceImpl(); }
	@Bean @Scope("prototype") public ResourceEnrichmentService resourceEnrichmentServicel() {return new ResourceEnrichmentServiceImpl(); }
}
