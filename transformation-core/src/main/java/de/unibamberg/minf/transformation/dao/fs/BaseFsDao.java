package de.unibamberg.minf.transformation.dao.fs;

import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.transformation.config.TransformationConfig;

public abstract class BaseFsDao {
	@Autowired protected TransformationConfig config;
	
	protected String getCrawlsPath() {
		return config.getPaths().getCrawls();
	}
	
	protected String getMappingPath() {
		return config.getPaths().getMappings();
	}
}