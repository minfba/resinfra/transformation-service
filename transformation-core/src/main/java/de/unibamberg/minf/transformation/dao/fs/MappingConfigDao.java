package de.unibamberg.minf.transformation.dao.fs;

import java.util.List;

import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;

public interface MappingConfigDao {
	public ExtendedMappingContainer findById(String mapping);
	public List<ExtendedMappingContainer> findAll();
	public void saveOrUpdate(ExtendedMappingContainer mc);
	public void deleteMapping(String id);
}