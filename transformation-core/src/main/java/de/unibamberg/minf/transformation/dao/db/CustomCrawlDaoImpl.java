package de.unibamberg.minf.transformation.dao.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlCompleteFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlErrorFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlOnlineFlag;
import de.unibamberg.minf.transformation.model.Crawl;

public class CustomCrawlDaoImpl implements CustomCrawlDao {
	
	@PersistenceContext private EntityManager entityManager;


	@Override
	public List<Crawl> findCrawls(Long datasetId, CrawlOnlineFlag online, CrawlCompleteFlag complete, CrawlErrorFlag error, int limit) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Crawl> query = cb.createQuery(Crawl.class);
        Root<Crawl> crawl = query.from(Crawl.class);
        
        List<Predicate> predicates = new ArrayList<>();
        
        predicates.add(cb.equal(crawl.get("dataset"), datasetId));
        
        
        if (!complete.equals(CrawlCompleteFlag.Both)) {
        	predicates.add(cb.equal(crawl.get("complete"), complete.equals(CrawlCompleteFlag.Complete)));
    	}
    	if (!error.equals(CrawlErrorFlag.Both)) {
    		predicates.add(cb.equal(crawl.get("error"), error.equals(CrawlErrorFlag.Error)));
    	}
    	
    	if (!online.equals(CrawlOnlineFlag.Both)) {
    		if (online.equals(CrawlOnlineFlag.Offline)) {
    			predicates.add(cb.isNotNull(crawl.get("baseCrawl")));
    		} else {
    			predicates.add(cb.isNull(crawl.get("baseCrawl")));
    		}
    	}
        
        query
        	.select(crawl)
        	.where(cb.and(predicates.toArray(new Predicate[predicates.size()])))
        	.orderBy(cb.desc(crawl.get("created")));

        TypedQuery<Crawl> q = entityManager.createQuery(query);
        if (limit>0) {
        	q.setMaxResults(limit);
        } 
        return q.getResultList();
	}
}
