package de.unibamberg.minf.transformation.dao.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import de.unibamberg.minf.transformation.model.Access;

@Repository
public interface AccessDao extends JpaRepository<Access, Long> {
	
}
