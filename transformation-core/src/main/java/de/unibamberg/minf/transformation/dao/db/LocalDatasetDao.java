package de.unibamberg.minf.transformation.dao.db;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import de.unibamberg.minf.transformation.model.LocalDataset;

@Repository
public interface LocalDatasetDao extends JpaRepository<LocalDataset, Long> {

	Optional<LocalDataset> findByUniqueId(String uniqueId); 

}
