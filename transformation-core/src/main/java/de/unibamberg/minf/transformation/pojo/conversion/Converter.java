package de.unibamberg.minf.transformation.pojo.conversion;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

public interface Converter<T1, T2> {
	public T2 convert(T1 entity);
	public T2 convert(T1 entity, Locale locale);
	public T2 convert(T1 entity, Locale locale, List<Class<?>> convertedTypes);
	public List<T2> convert(Collection<? extends T1> entities);
	public List<T2> convert(Collection<? extends T1> entities, Locale locale);
	public List<T2> convert(Collection<? extends T1> entities, Locale locale, List<Class<?>> convertedTypes);
}
