package de.unibamberg.minf.transformation.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import de.unibamberg.minf.gtf.GtfElementProcessor;
import de.unibamberg.minf.gtf.GtfMappingProcessor;
import de.unibamberg.minf.mapping.service.MappingExecutionServiceImpl;
import de.unibamberg.minf.processing.git.adapter.GitRepositoryAdapterImpl;
import de.unibamberg.minf.processing.service.json.JsonProcessingService;
import de.unibamberg.minf.processing.service.json.YamlProcessingService;
import de.unibamberg.minf.processing.service.tabular.CsvProcessingService;
import de.unibamberg.minf.processing.service.tabular.TsvProcessingService;
import de.unibamberg.minf.processing.service.text.TextProcessingService;
import de.unibamberg.minf.processing.service.xml.XmlProcessingService;
import de.unibamberg.minf.transformation.automation.CrawlCleanupService;
import de.unibamberg.minf.transformation.automation.CronTrigger;
import de.unibamberg.minf.transformation.automation.OfflineCrawlRunner;
import de.unibamberg.minf.transformation.automation.OnlineCrawlRunner;
import de.unibamberg.minf.transformation.automation.base.BaseScheduledRunnable;
import de.unibamberg.minf.transformation.config.model.AccessChain;
import de.unibamberg.minf.transformation.config.model.AccessChain.AccessTypes;
import de.unibamberg.minf.transformation.config.model.FileProcessingChain;
import de.unibamberg.minf.transformation.config.nested.CrawlingAutomationConfigProperties;
import de.unibamberg.minf.transformation.crawling.CrawlManagerImpl;
import de.unibamberg.minf.transformation.crawling.crawler.FileProcessor;
import de.unibamberg.minf.transformation.crawling.crawler.GitCrawlerImpl;
import de.unibamberg.minf.transformation.crawling.crawler.OaiPmhCrawlerImpl;
import de.unibamberg.minf.transformation.crawling.files.FileUnarchiver;
import de.unibamberg.minf.transformation.crawling.files.FileUnpacker;
import de.unibamberg.minf.transformation.crawling.files.XmlChunker;
import de.unibamberg.minf.transformation.crawling.oaipmh.OaiPmhClientImpl;
import lombok.Data;

@Data
public class BaseCrawlingConfig implements SchedulingConfigurer {
	private CrawlingAutomationConfigProperties automation;
	private boolean debugging = false;
	private int maxThreads = 4;
	private int timeout = 172800; // 48 hours
	
	private int maxThreadsPerServiceType = 6;
	private int apiAccessPolitenessSpanMs = 200;
	
	@PostConstruct
    public void completeConfiguration() {
		if (this.getAutomation()==null) {
			this.setAutomation(new CrawlingAutomationConfigProperties());
		}
		this.getAutomation().checkSchedules();
	}
	
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		if (this.getAutomation().isCollectionsExpire() || this.getAutomation().isAutocrawlCollections()) {
			this.configureRunnerTask(taskRegistrar, this.onlineCrawlRunner(), this.getAutomation().getCronScheduleOnline());
		}
		
		// Should run periodically in order to outdate datamodels
		this.configureRunnerTask(taskRegistrar, this.offlineCrawlRunner(), this.getAutomation().getCronScheduleOffline());
		
		// Nothing to do if not configured
		if (this.getAutomation().isCleanupCrawls()) {
			this.configureRunnerTask(taskRegistrar, this.crawlCleanupService(), this.getAutomation().getCronScheduleCleanup());
		}
	}
	
	
	private void configureRunnerTask(ScheduledTaskRegistrar taskRegistrar, BaseScheduledRunnable runner, String cronExpression) {
		if (cronExpression==null) {
			return;
		}
		CronTrigger runnerTrigger = new CronTrigger(runner.getClass().getSimpleName(), cronExpression, runner);
		
		taskRegistrar.setScheduler(syncAutomationTaskExecutor());
		taskRegistrar.addTriggerTask(runner, runnerTrigger);
		
		runner.setCronExpression(cronExpression);
	}
	
    @Bean
    public Executor syncAutomationTaskExecutor() {
    	// Crawling automation is not executed in parallel to prevent inconsistencies  
        return Executors.newSingleThreadScheduledExecutor();
    }
	
    @Bean
	public CrawlCleanupService crawlCleanupService() {
    	CrawlCleanupService runner = new CrawlCleanupService();
		runner.setAutomationEnabled(this.getAutomation().isCleanupCrawls());
		
		return runner;
	}
    
	@Bean
	public OfflineCrawlRunner offlineCrawlRunner() {
		OfflineCrawlRunner runner = new OfflineCrawlRunner();
		runner.setAutomationEnabled(this.getAutomation().isAutorefreshIndices());
		
		return runner;
	}
	
	@Bean
	public OnlineCrawlRunner onlineCrawlRunner() {
		OnlineCrawlRunner runner = new OnlineCrawlRunner();
		runner.setAutomationEnabled(this.getAutomation().isAutocrawlCollections());
		runner.setCollectionsExpire(this.getAutomation().isCollectionsExpire());
		
		return runner;
	}
	
	@Bean
	public CrawlManagerImpl crawlManager(TransformationConfig mainConfig, List<AccessChain> accessChains, List<FileProcessingChain> fileProcessingChains) {
		CrawlManagerImpl crawlManager = new CrawlManagerImpl();
		crawlManager.setMaxPoolSize(this.getMaxThreads());
		crawlManager.setBaseDownloadPath(mainConfig.getPaths().getDownloads());
		crawlManager.setAccessChains(accessChains);
		crawlManager.setFileProcessingChains(fileProcessingChains);
		return crawlManager;
	}
	
	// Overridden in implementations of BaseCrawlingConfig
	protected List<AccessChain> accessChains() {
		List<AccessChain> accessChains = new ArrayList<>();
		accessChains.add(new AccessChain("OAI-PMH", "oaiPmhCrawler", AccessTypes.DATASOURCE));
		accessChains.add(new AccessChain("Git Repository", "gitCrawler", AccessTypes.DATASOURCE));
		accessChains.add(new AccessChain("Online file", "fileCrawler", AccessTypes.DATASOURCE));
		return accessChains;
	}
	
	// Overridden in implementations of BaseCrawlingConfig
	protected List<FileProcessingChain> fileProcessingChains() {
		return new ArrayList<>();
	}
	
		
	@Bean
	@Scope("prototype")
	public OaiPmhCrawlerImpl oaiPmhCrawler(GtfElementProcessor gtfElementProcessor) {
		OaiPmhCrawlerImpl oaipmhCrawler = new OaiPmhCrawlerImpl();
		oaipmhCrawler.setPolitenessTimespan(500);
		return oaipmhCrawler;
	}
	
	@Bean
	@Scope("prototype")
	public OaiPmhClientImpl oaiPmhClient() {
		return new OaiPmhClientImpl();
	}
	
	@Bean
	@Scope("prototype")
	public GitCrawlerImpl gitCrawler() {
		return new GitCrawlerImpl();
	}

	@Bean
	@Scope("prototype")
	public GitRepositoryAdapterImpl gitRepositoryAdapter() {
		return new GitRepositoryAdapterImpl();
	}
		
	@Bean
	@Scope("prototype")
	public FileUnpacker fileUnpacker() {
		return new FileUnpacker();
	}
	
	@Bean
	@Scope("prototype")
	public FileUnarchiver fileUnarchiver() {
		return new FileUnarchiver();
	}
	
	@Bean
	@Scope("prototype")
	public XmlChunker xmlChunker() {
		return new XmlChunker();
	}
	
	@Bean
	@Scope("prototype")
	public XmlProcessingService xmlStringProcessor(GtfElementProcessor gtfElementProcessor) {
		XmlProcessingService xmlStringProcessor = new XmlProcessingService();
		xmlStringProcessor.setElementProcessors(new ArrayList<>());
		xmlStringProcessor.getElementProcessors().add(gtfElementProcessor);
		return xmlStringProcessor;
	}
	
	@Bean
	@Scope("prototype")
	public JsonProcessingService jsonProcessingService(GtfElementProcessor gtfElementProcessor) {
		JsonProcessingService jsonProcessingService = new JsonProcessingService();
		jsonProcessingService.setElementProcessors(new ArrayList<>());
		jsonProcessingService.getElementProcessors().add(gtfElementProcessor);
		return jsonProcessingService;
	}
	
	@Bean
	@Scope("prototype")
	public YamlProcessingService yamlProcessingService(GtfElementProcessor gtfElementProcessor) {
		YamlProcessingService yamlProcessingService = new YamlProcessingService();
		yamlProcessingService.setElementProcessors(new ArrayList<>());
		yamlProcessingService.getElementProcessors().add(gtfElementProcessor);
		return yamlProcessingService;
	}
	
	@Bean
	@Scope("prototype")
	public CsvProcessingService csvStringProcessor(GtfElementProcessor gtfElementProcessor) {
		CsvProcessingService csvStringProcessor = new CsvProcessingService();
		csvStringProcessor.setElementProcessors(new ArrayList<>());
		csvStringProcessor.getElementProcessors().add(gtfElementProcessor);
		csvStringProcessor.setUseHeadings(true);
		return csvStringProcessor;
	}
	
	@Bean
	@Scope("prototype")
	public TsvProcessingService tsvStringProcessor(GtfElementProcessor gtfElementProcessor) {
		TsvProcessingService tsvStringProcessor = new TsvProcessingService();
		tsvStringProcessor.setElementProcessors(new ArrayList<>());
		tsvStringProcessor.getElementProcessors().add(gtfElementProcessor);
		return tsvStringProcessor;
	}

	@Bean
	@Scope("prototype")
	public TextProcessingService textStringProcessor(GtfElementProcessor gtfElementProcessor) {
		TextProcessingService textStringProcessor = new TextProcessingService();
		textStringProcessor.setElementProcessors(new ArrayList<>());
		textStringProcessor.getElementProcessors().add(gtfElementProcessor);
		return textStringProcessor;
	}

	@Bean
	@Scope("prototype")
	public MappingExecutionServiceImpl mappingExecutionService(GtfMappingProcessor gtfMappingProcessor) {
		MappingExecutionServiceImpl mappingExecutionService = new MappingExecutionServiceImpl();
		mappingExecutionService.setMappingProcessors(new ArrayList<>());
		mappingExecutionService.getMappingProcessors().add(gtfMappingProcessor);
		return mappingExecutionService;
	}
	
		
	@Bean
	@Scope("prototype")
	public FileProcessor xmlBatchFileProcessor() {
		FileProcessor fileProcessor = new FileProcessor();
		fileProcessor.setWrappedServiceName("xmlStringProcessor");
		fileProcessor.setMaxParallelThreads(6);
		return fileProcessor;
	}

	@Bean
	@Scope("prototype")
	public FileProcessor jsonBatchFileProcessor() {
		FileProcessor fileProcessor = new FileProcessor();
		fileProcessor.setWrappedServiceName("jsonProcessingService");
		fileProcessor.setMaxParallelThreads(6);
		return fileProcessor;
	}

	@Bean
	@Scope("prototype")
	public FileProcessor yamlBatchFileProcessor() {
		FileProcessor fileProcessor = new FileProcessor();
		fileProcessor.setWrappedServiceName("yamlProcessingService");
		fileProcessor.setMaxParallelThreads(6);
		return fileProcessor;
	}
	
	@Bean
	@Scope("prototype")
	public FileProcessor textBatchFileProcessor() {
		FileProcessor fileProcessor = new FileProcessor();
		fileProcessor.setWrappedServiceName("textStringProcessor");
		fileProcessor.setMaxParallelThreads(6);
		return fileProcessor;
	}

	@Bean
	@Scope("prototype")
	public FileProcessor csvBatchFileProcessor() {
		FileProcessor fileProcessor = new FileProcessor();
		fileProcessor.setWrappedServiceName("csvStringProcessor");
		fileProcessor.setMaxParallelThreads(6);
		return fileProcessor;
	}

	@Bean
	@Scope("prototype")
	public FileProcessor tsvBatchFileProcessor() {
		FileProcessor fileProcessor = new FileProcessor();
		fileProcessor.setWrappedServiceName("tsvStringProcessor");
		fileProcessor.setMaxParallelThreads(6);
		return fileProcessor;
	}
	
	@Bean
	public List<String> fileProcessingAntiPatterns() {
		List<String> fileProcessingAntiPatterns = new ArrayList<>();
		fileProcessingAntiPatterns.add(".git/**");
		return fileProcessingAntiPatterns;
	}
}
