package de.unibamberg.minf.transformation.exceptions;

public class ConfigurationException extends Exception {
	private static final long serialVersionUID = 4795772596718649834L;
	public ConfigurationException(String message) {
		super(message);
	}
	
	public ConfigurationException(String message, Throwable source) {
		super(message, source);
	}
	
	public ConfigurationException(Throwable source) {
		super(source);
	}
}
