package de.unibamberg.minf.transformation.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import de.unibamberg.minf.dme.model.datamodel.base.Datamodel;
import de.unibamberg.minf.transformation.dao.fs.DatamodelConfigDao;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;

public class DatamodelServiceImpl implements DatamodelService, InitializingBean {
	protected final Logger logger = LoggerFactory.getLogger(DatamodelServiceImpl.class);	
				
	@Autowired private DatamodelConfigDao datamodelConfigDao;
	
	private HashMap<String, ExtendedDatamodelContainer> schemaMap;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.findAll(true);
	}
	
	@Override
	public List<Datamodel> findAllModels() {
		List<ExtendedDatamodelContainer> schemaContainers = this.findAll();
		List<Datamodel> schemas = new ArrayList<>();
		for (ExtendedDatamodelContainer sc : schemaContainers) {
			schemas.add(sc.getModel());
		}
		return schemas;
	}
	
	@Override
	public List<ExtendedDatamodelContainer> findAll(boolean rebuildCache) {
		List<ExtendedDatamodelContainer> result = new ArrayList<>();
		if (!rebuildCache && this.schemaMap!=null) {
			for (ExtendedDatamodelContainer esc : this.schemaMap.values()) {
				result.add(esc);
			}
			return result;
		}
		this.schemaMap = new HashMap<>();
		List<ExtendedDatamodelContainer> schemas = datamodelConfigDao.findAll();
		if (schemas!=null) {
			for (ExtendedDatamodelContainer sc : schemas) {
				this.schemaMap.put(sc.getId(), sc);
				sc.clearRenderedHierarchies();
				result.add(sc);
			}
		}
		return result;
	}
	
	@Override
	public ExtendedDatamodelContainer loadById(String modelId) {
		ExtendedDatamodelContainer edc = datamodelConfigDao.findById(modelId);
		if (edc==null) {
		  logger.warn("Attempt to load nonexisting datamodel {}", modelId);
		}
		return edc;
	}
	
	@Override
	public List<ExtendedDatamodelContainer> loadAll() {
		return datamodelConfigDao.findAll();
	}
	
	@Override
	public List<ExtendedDatamodelContainer> findAll() {
		return this.findAll(false);
	}

	@Override
	public ExtendedDatamodelContainer findById(String id) {
		if (!this.schemaMap.containsKey(id)) {
			logger.warn("Attempt to find nonexisting datamodel {}", id);
		}
		return this.schemaMap.get(id);
	}
	
	@Override
	public List<ExtendedDatamodelContainer> findByIds(List<String> datamodelIds) {
		if (datamodelIds==null) {
			return new ArrayList<>(0);
		}
		List<ExtendedDatamodelContainer> result = new ArrayList<>();
		for (String id : datamodelIds) {
			result.add(this.findById(id));
		}		
		return result;
	}
	
	@Override
	public List<String> findAllSchemaIds() {
		return new ArrayList<>(this.schemaMap.keySet());
	}
	
	@Override
	public void saveOrUpdate(ExtendedDatamodelContainer sc) {
		datamodelConfigDao.saveOrUpdate(sc);
		sc.clearRenderedHierarchies();
		this.schemaMap.put(sc.getId(), sc);
	}
	
	@Override
	public boolean deleteDatamodel(String id) {
		ExtendedDatamodelContainer datamodel = this.findById(id);
		if (datamodel!=null) {
			this.schemaMap.remove(id);
		} else {
			datamodel = this.loadById(id);
		}
		if (datamodel==null) {
			return false;
		}
		
		datamodelConfigDao.deleteSchema(id);
		return true;
	}

	@Override
	public String getIndexName(String modelId) {
		return datamodelConfigDao.getIndexName(modelId);
	}
}