package de.unibamberg.minf.transformation.dao.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.unibamberg.minf.dme.migration.model.version.VersionInfoImpl;

@Repository
public interface VersionDao extends JpaRepository<VersionInfoImpl, Long> {}
