package de.unibamberg.minf.transformation.data.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.core.util.DirectoryInfo;
import de.unibamberg.minf.transformation.dao.fs.DownloadDataDao;

public class DownloadDataServiceImpl implements DownloadDataService {
	protected static final Logger logger = LoggerFactory.getLogger(DownloadDataServiceImpl.class);
			
	@Autowired private DownloadDataDao downloadDataDao;

	
	@Override
	public void clearDatasetData(String datasetUniqueId) {
		try {
			downloadDataDao.clearAllData(datasetUniqueId);
		} catch (Exception e) {
			logger.error("Failed to clear data", e);
		}
	}
}
