package de.unibamberg.minf.transformation.metamodel.extension;

public interface ExtendedElement {
	public boolean isNested();
	public void setNested(boolean nested); 
}
