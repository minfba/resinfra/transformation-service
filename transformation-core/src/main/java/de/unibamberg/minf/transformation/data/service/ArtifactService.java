package de.unibamberg.minf.transformation.data.service;

public interface ArtifactService {
	public int removeByCollectionId(Long collectionId);
	public int removeByEndpointId(Long endpointId);
	public int removeByDatasetIds(Long datamodelId, Long endpointId);
}
