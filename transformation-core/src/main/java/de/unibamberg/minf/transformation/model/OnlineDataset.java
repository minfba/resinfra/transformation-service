package de.unibamberg.minf.transformation.model;

import java.time.OffsetDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import de.unibamberg.minf.transformation.model.base.OnlineAccessMethod;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Form of a dataset that is provided by a collection - datasource relation and thus linked to an entry in 
 *  a connected Collection Registry. It is not editable within the transformation service as all descriptive 
 *  information is fetched and possibly overridden. 
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"crawls", "datasource"})
@PrimaryKeyJoinColumn(name = "online_dataset_id")
public class OnlineDataset extends Dataset implements OnlineAccessMethod {
	private static final long serialVersionUID = 7525320529342770126L;

	private String remoteAlias;
	private boolean nocache;
	
	@Column(name = "nextExecution", columnDefinition = "TIMESTAMP WITH TIME ZONE")
	private OffsetDateTime nextExecution;
	
	@OneToMany(orphanRemoval = true, cascade = CascadeType.PERSIST, mappedBy="dataset", fetch = FetchType.LAZY)
	private Set<Crawl> crawls;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="datasource_id", nullable=false)
	private Datasource datasource;
	
	@Override
	public boolean isOnline() { return true; }

	@Override
	public Access getAccess() { return this.getDatasource(); }
}
