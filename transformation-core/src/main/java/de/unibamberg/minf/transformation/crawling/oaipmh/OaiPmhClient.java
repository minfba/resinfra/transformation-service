package de.unibamberg.minf.transformation.crawling.oaipmh;

import java.io.IOException;

import de.unibamberg.minf.transformation.crawling.oaipmh.model.OaiPmhResponseContainer;

public interface OaiPmhClient {
	public OaiPmhResponseContainer executeCommand(String baseUrl, String verb, String id, String metadataPrefix, String from, String until, String set, String resumptionToken) throws IOException;

	public OaiPmhResponseContainer listRecords(String baseUrl, String from, String until, String metadataPrefix, String set, String resumptionToken);
	public OaiPmhResponseContainer listIdentifiers(String baseUrl, String from, String until, String metadataPrefix, String set, String resumptionToken);
	public OaiPmhResponseContainer listSets(String baseUrl, String resumptionToken);
	public OaiPmhResponseContainer listMetadataFormats(String baseUrl, String id) ;
	public OaiPmhResponseContainer identify(String baseUrl);
	public OaiPmhResponseContainer getRecord(String baseUrl, String id, String metadataPrefix);
}
