package de.unibamberg.minf.transformation.dao.db;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.model.OnlineDataset;

@Repository
public interface OnlineDatasetDao extends JpaRepository<OnlineDataset, Long> { 

	@Transactional
	@Query("SELECT d FROM OnlineDataset d LEFT JOIN FETCH d.datasource s LEFT JOIN FETCH s.collection c")
	List<OnlineDataset> findAndLoadAll();
	
	@Transactional
	@Query("SELECT d FROM OnlineDataset d LEFT JOIN FETCH d.datasource s LEFT JOIN FETCH s.collection c WHERE d.id=?1")
	Optional<OnlineDataset> findAndLoadById(Long id);

	@Transactional
	@Query("SELECT d FROM OnlineDataset d LEFT JOIN FETCH d.datasource s LEFT JOIN FETCH s.collection c WHERE d.datamodelId=?1")
	List<OnlineDataset> findAndLoadByDatamodelId(String datamodelId);

	Optional<OnlineDataset> findByUniqueId(String uniqueId);
}
