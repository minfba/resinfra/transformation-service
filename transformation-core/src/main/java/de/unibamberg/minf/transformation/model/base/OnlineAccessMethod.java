package de.unibamberg.minf.transformation.model.base;

import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;

public interface OnlineAccessMethod {
	public String getDatamodelId();
	public void setDatamodelId(String datamodelId);
	
	public ExtendedDatamodelContainer getDatamodel();
	public void setDatamodel(ExtendedDatamodelContainer datamodel);
	
	public Access getAccess();
}
