package de.unibamberg.minf.transformation.crawling;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.processing.model.helper.ResourceHelper;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.AccessParam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CrawlHelper {

	private CrawlHelper() {}
	
	public static String renderAccessUrl(Access ac) {
		return renderAccessUrl(ac.getUrl(), ac.getParams(), null, null);
	}
	
	public static String renderAccessUrl(String url, Set<AccessParam> accessParams, List<Resource> dynamicParams, List<Resource> removeParams) {
    	if (url==null) {
    		return null;
    	}
    	try {
    		URIBuilder b = new URIBuilder(url);
        	b.addParameters(filterNameValuePairs(accessParams, removeParams));
        	b.addParameters(createNameValuePairs(dynamicParams, removeParams));
        	return b.build().toString();
    	} catch (URISyntaxException e) {
    		log.error("Failed to build URL", e);
    	}
    	return null;	
    }
	
	private static List<NameValuePair> filterNameValuePairs(Set<AccessParam> accessParams, List<Resource> removeParams) {
		if (accessParams==null) {
			return new ArrayList<>();
		}
		List<NameValuePair> pairs = new ArrayList<>();
		
		// Both structures are allowed: RemoveParam with value & RemoveParam with child 'name'
		List<String> removeParamNames = new ArrayList<>();
		if (removeParams!=null) {
			for (Resource r : removeParams) {
				if (r.getValue()!=null) {
					removeParamNames.add(r.getValue().toString());
				} 
				if (r.getChildResources()!=null && !r.getChildResources().isEmpty()) {
					for (Resource name : ResourceHelper.findRecursive(r, "Name")) {
						if (name.getValue()!=null) {
							removeParamNames.add(name.getValue().toString());
						}
					}
				}	
			}
		}
		
		for (AccessParam p : accessParams) {
			if (!removeParamNames.contains(p.getParam())) {
				pairs.add(new BasicNameValuePair(p.getParam(), p.getValue()));
			}
		}
		return pairs;
	}
	
	private static List<NameValuePair> createNameValuePairs(List<Resource> dynamicParams, List<Resource> removeParams) {
		if (dynamicParams==null) {
			return new ArrayList<>();
		}
		List<NameValuePair> pairs = new ArrayList<>();
		List<Resource> names = null;
		List<Resource> values;
		for (Resource r : dynamicParams) {
			names = ResourceHelper.findRecursive(r, "Name");
			values = ResourceHelper.findRecursive(r, "Value");
			for (Resource name : names) {
				if (name.getValue()==null || name.getValue().toString().trim().isEmpty()) {
					continue;
				}
				for (Resource value : values) {
					if (value.getValue()==null || value.getValue().toString().trim().isEmpty()) {
						continue;
					}
					pairs.add(new BasicNameValuePair(name.getValue().toString().trim(), value.getValue().toString().trim()));
				}	
			}
		}
		return pairs;
	}
}