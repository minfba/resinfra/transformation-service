package de.unibamberg.minf.transformation.pojo;

public class ApiStatusPojo {
	private boolean accessible;
	private int statusCode;
	private long roundtime;
	
	
	public boolean isAccessible() { return accessible; }
	public void setAccessible(boolean accessible) { this.accessible = accessible; }
	
	public long getRoundtime() { return roundtime; }
	public void setRoundtime(long roundtime) { this.roundtime = roundtime; }
	
	public int getStatusCode() { return statusCode; }
	public void setStatusCode(int statusCode) { this.statusCode = statusCode; }
}
