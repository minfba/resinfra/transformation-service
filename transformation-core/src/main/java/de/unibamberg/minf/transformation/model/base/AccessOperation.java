package de.unibamberg.minf.transformation.model.base;

public interface AccessOperation {
	String getUniqueId();
}
