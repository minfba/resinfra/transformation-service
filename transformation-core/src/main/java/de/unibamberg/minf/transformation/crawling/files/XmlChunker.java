package de.unibamberg.minf.transformation.crawling.files;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import de.unibamberg.minf.dme.model.base.ModelElement;
import de.unibamberg.minf.dme.model.base.Nonterminal;
import de.unibamberg.minf.dme.model.datamodel.natures.XmlDatamodelNature;
import de.unibamberg.minf.dme.model.datamodel.natures.xml.XmlTerminal;
import de.unibamberg.minf.processing.exception.ResourceProcessingException;
import de.unibamberg.minf.transformation.crawling.crawler.Crawler;
import de.unibamberg.minf.transformation.crawling.model.HierarchicalXmlTerminal;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.base.AccessOperation;

public class XmlChunker extends BaseFileStreamCrawler implements Crawler {

	private String outputFilePattern = "%s_%08d.xml";
	private long chunkByteSize = 2000000; // ~ 2MiB
	private int totalNodes = -1;

	private XMLInputFactory xif;
	private TransformerFactory tf;
	private DocumentBuilderFactory docBuilderFactory;
	private XPathFactory xPathFactory;
	private DocumentBuilder docBuilder;
	private XPathExpression compiledExpression;
	private LinkedList<Document> documentQueue;
	private int fileIndex = 0;

	private QName processingRootXmlName;

	private ExtendedDatamodelContainer datamodel;

	public String getOutputFilePattern() {
		return outputFilePattern;
	}

	public void setOutputFilePattern(String outputFilePattern) {
		this.outputFilePattern = outputFilePattern;
	}

	public long getChunkByteSize() {
		return chunkByteSize;
	}

	public void setChunkByteSize(long chunkByteSize) {
		this.chunkByteSize = chunkByteSize;
	}

	public int getTotalNodes() {
		return totalNodes;
	}

	public void setTotalNodes(int totalNodes) {
		this.totalNodes = totalNodes;
	}

	@Override
	public String getUnitMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.file.chunker.unit";
	}

	@Override
	public String getTitleMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.file.chunker.title";
	}

	@Override
	public void init(Access access, AccessOperation op, ExtendedDatamodelContainer sc) {
		super.init(access, op, sc);
		try {
			this.setupPaths(op);
			this.datamodel = sc;

			System.setProperty("javax.xml.transform.TransformerFactory",
					"com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
			ModelElement processingRoot = sc.getOrRenderProcessingRoot(true);

			xif = XMLInputFactory.newInstance();
			tf = TransformerFactory.newInstance();

			try {
				tf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, false);
			} catch (TransformerConfigurationException e) {
				logger.warn("Failed to disable XMLConstants.FEATURE_SECURE_PROCESSING", e);
			}

			docBuilderFactory = DocumentBuilderFactory.newInstance();
			xPathFactory = XPathFactory.newInstance();

			XmlDatamodelNature xmlNature = sc.getModel().getNature(XmlDatamodelNature.class);

			String processingRrootTerminalId = xmlNature.getTerminalId(processingRoot.getId());
			for (XmlTerminal t : xmlNature.getTerminals()) {
				if (t.getId().equals(processingRrootTerminalId)) {
					this.processingRootXmlName = new QName(((XmlTerminal) t).getNamespace(), t.getName());
					break;
				}
			}
			if (this.processingRootXmlName == null) {
				throw new NullPointerException("Root XML Terminal could not be identified and is therefore NULL");
			}

			compiledExpression = xPathFactory.newXPath().compile("/*");

		} catch (Exception e) {
			logger.error("XML Chunker initialization failed: " + e.getMessage(), e);
		}
	}

	@Override
	public void run() {
		super.run();
		try {
			if (this.isCancellationRequested()) {
				throw new ResourceProcessingException("Service cancellation has been requested");
			}
			if (this.getListener() != null) {
				this.getListener().start(this.getUuid());
			}
			this.chunkXml();
		} catch (Exception e) {
			logger.error("Failed to chunk XML file", e);
		}
	}

	private void chunkXml() throws ResourceProcessingException, XPathExpressionException, ParserConfigurationException,
			TransformerConfigurationException, IOException {
		
		for (String inputFile : this.getInputFilenames()) {
			if (((Nonterminal) this.datamodel.getOrRenderProcessingRoot(true)).isIncludeHeader()) {
				logger.warn("XML chunking not yet supporting cloned headers; input will not be chunked");
				if (this.getListener() != null) {
					this.getListener().finished(this.getUuid());
				}
				return;
			}
			Transformer t = tf.newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			t.setOutputProperty(OutputKeys.ENCODING, "UTF-16");
			t.setOutputProperty(OutputKeys.INDENT, "no");
			t.setOutputProperty(OutputKeys.VERSION, "1.1");
	
			docBuilderFactory.setIgnoringElementContentWhitespace(true);
			docBuilder = docBuilderFactory.newDocumentBuilder();
	
			long pageIndex = 0;
			int docCount = 0;
	
			documentQueue = new LinkedList<Document>();
			long queuedSize = 0;
	
			StringWriter sw;
			String s;
			InputSource src;
			Document inD;
	
			int type;
	
			List<File> inputFiles = new ArrayList<File>();
	
			if (Files.isDirectory(Paths.get(inputFile))) {
				File dir = new File(inputFile);
				for (File f : dir.listFiles()) {
					if (!f.isDirectory()) {
						inputFiles.add(f);
					}
				}
			} else {
				inputFiles.add(new File(inputFile));
			}
	
			Stack<HierarchicalXmlTerminal> terminalsStack = new Stack<HierarchicalXmlTerminal>();
	
			HierarchicalXmlTerminal xmlTerminal;
	
			for (int i = 0; i < inputFiles.size(); i++) {
				if (this.isCancellationRequested()) {
					throw new ResourceProcessingException("Service cancellation has been requested");
				}
				boolean chunk = true;
				boolean error = false;
				boolean setup = false; // To prevent teardown of terminal hierarchy before last document is written
				try {
					// *2 to include files over chunk size (defined chunk size is not absolute max)
					if (inputFiles.get(i).length() <= this.getChunkByteSize() * 2) {
						chunk = false;
					} else {
	
						XMLStreamReader xsr = xif.createXMLStreamReader(new FileReader(inputFiles.get(i)));
						// xsr.nextTag(); // Advance to statements element
	
						while (xsr.hasNext() && (type = xsr.next()) > 0) {
							if (this.isCancellationRequested()) {
								throw new ResourceProcessingException("Service cancellation has been requested");
							}
							if (type == XMLStreamConstants.START_ELEMENT) {
								if (!xsr.getName().equals(processingRootXmlName)) {
									xmlTerminal = new HierarchicalXmlTerminal();
									xmlTerminal.setName(xsr.getLocalName());
									xmlTerminal.setNamespace(xsr.getNamespaceURI());
									if (!terminalsStack.isEmpty()) {
										terminalsStack.peek().addChildTerminal(xmlTerminal);
									}
									for (int j = 0; j < xsr.getNamespaceCount(); j++) {
										xmlTerminal.putNamespace(xsr.getNamespaceURI(j), xsr.getNamespacePrefix(j));
									}
									terminalsStack.push(xmlTerminal);
	
									continue;
								}
							} else {
								if (!setup && type == XMLStreamConstants.END_ELEMENT) {
									terminalsStack.pop();
								}
								continue;
							}
	
							setup = true;
							if (terminalsStack.isEmpty()) {
								logger.info("Cannot chunk, as processing root for datasets is the document root");
								chunk = false;
								continue;
							} else {
								sw = new StringWriter();
								try {
									t.transform(new StAXSource(xsr), new StreamResult(sw));
									s = sw.toString();
									src = new InputSource(new StringReader(s));
									inD = docBuilder.parse(src);
	
									documentQueue.add(inD);
									queuedSize += s.getBytes().length;
								} catch (TransformerException | SAXException | IOException e) {
									logger.error("Exception while splitting XML", e);
									throw e;
								}
	
								if (queuedSize >= this.getChunkByteSize()) {
									writeDocument(terminalsStack);
									if (this.getListener() != null) {
										this.getListener().processed(this.getUuid(), ++docCount);
									}
									queuedSize = 0;
									if (docCount % 10 == 0) {
										logger.info(String.format("XML chunking: wrote  %s documents", docCount));
									}
								}
	
								if (this.getTotalNodes() > 0 && ++pageIndex >= this.getTotalNodes()) {
									break;
								}
							}
						}
					}
	
				} catch (Exception e) {
					logger.error("Failed to chunk XML file", e);
					error = true;
					if (this.isCancellationRequested()) {
						throw new ResourceProcessingException("Service cancellation has been requested");
					}
				}
	
				if (documentQueue.size() > 0) {
					writeDocument(terminalsStack);
					if (this.getListener() != null) {
						this.getListener().processed(this.getUuid(), ++docCount);
					}
					queuedSize = 0;
				}
				if (!error && chunk) {
					this.moveInputToBackup(inputFiles.get(i).getName());
				}
			}
	
			if (this.getListener() != null) {
				this.getListener().finished(this.getUuid());
			}
			logger.info(String.format("XML chunking completed: Wrote %s documents", docCount));
		}

	}

	private int writeDocument(Stack<HierarchicalXmlTerminal> terminalsStack) throws ResourceProcessingException {
		File outFile = null;
		try {
			Document outD = docBuilder.newDocument();
			Node nRoot = outD;
			if (!terminalsStack.isEmpty()) {
				nRoot = this.generateDocumentHeaderElements(outD, terminalsStack, outD, terminalsStack.peek());
			}

			Document inD;
			Node n;
			while (!documentQueue.isEmpty()) {
				inD = documentQueue.removeFirst();
				n = (Node) compiledExpression.evaluate(inD, XPathConstants.NODE);
				if (n != null) {
					inD.removeChild(n);
					n = outD.importNode(n, true);
					nRoot.appendChild(n);
				}
			}

			if (nRoot.hasChildNodes()) {
				Transformer transformer = tf.newTransformer();
				DOMSource source = new DOMSource(outD);
				outFile = new File(this.getOutputPath(++fileIndex));
				transformer.transform(source, new StreamResult(outFile));

				this.addOutputFilename(outFile.getAbsolutePath());
				
				return nRoot.getChildNodes().getLength();
				// logger.info(String.format("Wrote document %s with %s pages", fileIndex,
				// pages.getChildNodes().getLength()));
			}
		} catch (Exception e) {
			logger.error("Exception writing chunk XML", e);
			if (outFile != null && outFile.exists()) {
				outFile.delete();
			}
			throw new ResourceProcessingException(e);
		}
		return 0;
	}

	private Element generateDocumentHeaderElements(Document xDoc, List<HierarchicalXmlTerminal> terminals, Node parent,
			HierarchicalXmlTerminal payloadParentTerminal) {
		Element e, subE;
		String nsPrefix;
		for (HierarchicalXmlTerminal term : terminals) {
			e = xDoc.createElementNS(term.getNamespace(), term.getName());
			parent.appendChild(e);

			if (term.getNamespacePrefixMap() != null) {
				for (String ns : term.getNamespacePrefixMap().keySet()) {
					nsPrefix = term.getNamespacePrefixMap().get(ns);
					if (nsPrefix != null && !nsPrefix.isEmpty()) {
						e.setAttribute("xmlns:" + nsPrefix, ns);
					}
				}
			}

			if (term.getChildTerminals() != null) {
				subE = this.generateDocumentHeaderElements(xDoc, term.getChildTerminals(), e, payloadParentTerminal);
				if (subE != null) {
					return subE;
				}
			}
			if (term.equals(payloadParentTerminal)) {
				return e;
			}
		}
		return null;
	}

	private String getOutputPath(int index) {
		return this.getDirPath() + File.separator
				+ String.format(this.getOutputFilePattern(), this.getUuid().toString(), fileIndex);
	}
}
