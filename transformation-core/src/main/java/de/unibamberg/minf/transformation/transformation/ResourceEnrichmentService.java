package de.unibamberg.minf.transformation.transformation;

import java.util.List;

import de.unibamberg.minf.processing.consumption.MappedResourceConsumptionService;
import de.unibamberg.minf.transformation.indexing.model.ResourceContainer;

public interface ResourceEnrichmentService extends MappedResourceConsumptionService {
	public List<ResourceContainer> getResourceBatch();

	public void reset();
}
