package de.unibamberg.minf.transformation.dao.db;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.model.PersistedAccessToken;

@Repository
public interface PersistedAccessTokenDao extends JpaRepository<PersistedAccessToken, Long> {
	
	@EntityGraph(attributePaths = {"allowedAdresses"}, type = EntityGraphType.LOAD)
	Optional<PersistedAccessToken> findByUniqueId(String uniqueId);
	
	@EntityGraph(attributePaths = {"allowedAdresses"}, type = EntityGraphType.LOAD)
	List<PersistedAccessToken> findByUserId(Long userId);
	
	@EntityGraph(attributePaths = {"allowedAdresses"}, type = EntityGraphType.LOAD)
	PersistedAccessToken findByUniqueIdAndUserId(String uniqueId, Long userId);
	
	/*@Transactional
	@Query("SELECT t FROM PersistedAccessToken t WHERE t.uniqueId=?1")
	Optional<PersistedAccessToken> findByUniqueId(String uniqueId);

	@Transactional
	@Query("SELECT t FROM PersistedAccessToken t WHERE t.user.id=?1")
	List<PersistedAccessToken> findByUserId(Long userId);

	@Transactional
	@Query("SELECT t FROM PersistedAccessToken t WHERE t.uniqueId=?1 AND t.user.id=?2")
	PersistedAccessToken findByUniqueIdAndUserId(String uniqueId, Long userId);

	@Transactional
	@Query("SELECT t FROM PersistedAccessToken t WHERE t.name=?1 AND t.user.id=?2")
	PersistedAccessToken findByNameAndUserId(String name, Long userId);*/
}