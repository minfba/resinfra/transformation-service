 package de.unibamberg.minf.transformation.automation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import de.unibamberg.minf.dme.model.base.BaseLongIdentifiable;
import de.unibamberg.minf.transformation.api.WrappedApiEntity;
import de.unibamberg.minf.transformation.api.client.ApiConsumptionException;
import de.unibamberg.minf.transformation.api.client.CollectionClient;
import de.unibamberg.minf.transformation.automation.base.BaseScheduledRunnable;
import de.unibamberg.minf.transformation.automation.base.SyncService;
import de.unibamberg.minf.transformation.crawling.CrawlManager;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.Interface;
import de.unibamberg.minf.transformation.model.OnlineDataset;
import de.unibamberg.minf.transformation.pojo.ApiStatusPojo;
import de.unibamberg.minf.transformation.service.ApiService;
import de.unibamberg.minf.transformation.service.CollectionService;
import de.unibamberg.minf.transformation.service.DatamodelService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CollectionSyncService extends BaseScheduledRunnable implements SyncService {
	@Autowired private CollectionService collectionService;
	@Autowired private DatamodelService datamodelService;
	@Autowired private CrawlManager crawlManager;
	
	@Autowired private ApiService apiService;

	private final ReentrantLock reLock = new ReentrantLock(true);
	
	private ApplicationContext applicationContext;
	
	@Getter @Setter private boolean autocrawlNewDatasets;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	@Override
	protected void init() { }
	
	@Override
	protected void executeAutomation() {
		if (this.isAutomationEnabled()) {
			this.syncCollections();
		}
	}
	
	public boolean isBusy() {
		return reLock.isLocked();
	}
	
	public void syncCollections() {
		log.info("Updating collections from CR");
		try {
			reLock.lock();
			List<Collection> collections = collectionService.findAndLoadAll(); 
			
			CollectionClient client = applicationContext.getBean(CollectionClient.class);		
			client.setCurrentCollections(collections);
			client.sync();
					
			this.processSynchronizationResult(client.getCollections());
			log.info("Completed update of collections from CR");
		} catch (Exception | ApiConsumptionException e) {
			log.error("Failed to update collections from CR", e);
		} finally {
			reLock.unlock();
		}
	}

	@Override
	public ApiStatusPojo getServiceStatus() {
		try {
			return applicationContext.getBean(CollectionClient.class).ping();
		} catch (Exception e) {
			log.error("An error occurred while connecting to API service", e);
		}
		return null;
	}	
	
	protected void processSynchronizationResult(List<WrappedApiEntity<Collection>> collections) {
		final List<OnlineDataset> newOnlineDatasets = new ArrayList<>();
		collections.stream()
			.forEach(wc -> {
				Collection c = wc.getWrappedEntity();
				if (wc.isDelete()) {
					collectionService.delete(c);
				} else {
					this.processAccess(wc, c, newOnlineDatasets);
					// No access remaining -> delete collection
					if (c.getAccess().isEmpty()) {
						collectionService.delete(wc.getWrappedEntity());
					} else {
						collectionService.saveCollection(wc.getWrappedEntity());
					}
				}
			});
		
		postprocessNewOnlineDatasets(newOnlineDatasets);
	}
	
	private void processAccess(WrappedApiEntity<Collection> w, Collection c, List<OnlineDataset> newOnlineDatasets) {
		w.getWrappedSubentities().stream()
			.forEach(ws -> { 
				Access a = Access.class.cast(ws.getWrappedEntity());
				if (ws.isDelete()) {
					this.deleteOrSetDeletedAccess(c, a);
				} else {
					// Process datasources
					if (Datasource.class.isAssignableFrom(a.getClass())) {
						processDatasets(ws, (Datasource)a, newOnlineDatasets);
					} else { // Process endpoints
						processInterfaces(ws, (Endpoint)a);
					}
					// No methods remaining -> delete access
					if (a.getAccessMethods().isEmpty()) {
						this.deleteOrSetDeletedAccess(c, a);
					}
				}
				if (ws.isAdd() && !c.getAccess().contains(a)) {
					c.getAccess().add(a);
					a.setCollection(c);
				} 
			});
	}
	
	private void deleteOrSetDeletedAccess(Collection c, Access a) {
		boolean delete = true;
		
		if (Endpoint.class.isAssignableFrom(a.getClass())) {
			Endpoint e = Endpoint.class.cast(a);
			if (e.getInterfaces()!=null && !e.getInterfaces().isEmpty() && 
				!apiService.findByInterfaceIds(e.getInterfaces().stream().map(BaseLongIdentifiable::getId).collect(Collectors.toList())).isEmpty()) {
					delete = false;
			}
		}
		if (delete) {
			c.getAccess().remove(a);
		} else {
			a.setDeleted(true);
		}
	}
	
	private void processDatasets(WrappedApiEntity<?> w, Datasource s, List<OnlineDataset> newOnlineDatasets) {
		long add = w.getWrappedSubentities().stream().filter(WrappedApiEntity::isAdd).count();
		long del = w.getWrappedSubentities().stream().filter(WrappedApiEntity::isDelete).count();
		boolean simpleMerge = add==1 && del==1; 
		
		// Simple merge to avoid API modifications for datamodel changes on the only dataset
		OnlineDataset mergeDs = null;
		for (WrappedApiEntity<?> ws : w.getWrappedSubentities()) {
			if (!ws.isDelete()) {
				continue;
			}
			OnlineDataset ds = OnlineDataset.class.cast(ws.getWrappedEntity());
			mergeDs = ds;
			if (!simpleMerge) {
				s.getDatasets().remove(ds);
			}
		}
		
		for (WrappedApiEntity<?> ws : w.getWrappedSubentities()) {
			if (ws.isDelete()) {
				continue;
			}
			OnlineDataset ds = OnlineDataset.class.cast(ws.getWrappedEntity());
			ds.setDatasource(s);
			if (ws.isAdd()) {
				if (simpleMerge && mergeDs!=null) {
					mergeDs.setDatamodel(null);
					mergeDs.setDatamodelId(ds.getDatamodelId());
				} else {
					newOnlineDatasets.add(ds);
					if (!s.getDatasets().contains(ds)) {
						s.getDatasets().add(ds);
					}
				}
			}
		}
	}
	
	private void processInterfaces(WrappedApiEntity<?> w, Endpoint e) {
		long add = w.getWrappedSubentities().stream().filter(WrappedApiEntity::isAdd).count();
		long del = w.getWrappedSubentities().stream().filter(WrappedApiEntity::isDelete).count();
		boolean simpleMerge = add==1 && del==1; 

		// Simple merge to avoid API modifications for datamodel changes on the only interface
		Interface mergeIn = null;
		for (WrappedApiEntity<?> ws : w.getWrappedSubentities()) {
			if (!ws.isDelete()) {
				continue;
			}
			Interface in = Interface.class.cast(ws.getWrappedEntity());
			mergeIn = in;
			if (!simpleMerge) {
				e.getInterfaces().remove(in);
				apiService.removeInterfaceFromApis(in.getId());
			}
		}
		
		for (WrappedApiEntity<?> ws : w.getWrappedSubentities()) {
			if (ws.isDelete()) {
				continue;
			}
			Interface in = Interface.class.cast(ws.getWrappedEntity());
			in.setEndpoint(e);
			if (ws.isAdd()) {
				if (simpleMerge && mergeIn!=null) {
					mergeIn.setDatamodel(null);
					mergeIn.setDatamodelId(in.getDatamodelId());
				} else if (!e.getInterfaces().contains(in)) {
					e.getInterfaces().add(in);
				}
			}
		}
	}
	
	private void postprocessNewOnlineDatasets(List<OnlineDataset> newOnlineDatasets) {
		if (!this.isAutocrawlNewDatasets()) {
			return;
		}
		// Autocrawl new datasets		
		newOnlineDatasets.stream().forEach(ds -> {
			ExtendedDatamodelContainer dmc = datamodelService.findById(ds.getDatamodelId());
			if (dmc!=null) {
				ds.setDatamodel(dmc);
				crawlManager.performOnlineCrawl(ds);
				log.info("Enqueuing online crawl for new dataset");
			} else {
				log.info("Skipping online crawl for new dataset due to missing datamodel [{}]", ds.getDatamodelId());
			}
		});
	}
}
