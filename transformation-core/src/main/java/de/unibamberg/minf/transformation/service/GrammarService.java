package de.unibamberg.minf.transformation.service;

import java.util.Map;

import de.unibamberg.minf.dme.model.base.Grammar;
import de.unibamberg.minf.dme.model.grammar.GrammarContainer;

public interface GrammarService {
	public void importGrammarContainers(Map<String, GrammarContainer> importedGrammarContainers);
	
	public void clearGrammar(String grammarId);
	public void saveGrammar(GrammarContainer gc);
}
