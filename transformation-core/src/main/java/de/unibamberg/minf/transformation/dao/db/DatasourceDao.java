package de.unibamberg.minf.transformation.dao.db;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.stereotype.Repository;
import de.unibamberg.minf.transformation.model.Datasource;

@Repository
public interface DatasourceDao extends JpaRepository<Datasource, Long> {

	@Transactional
	@EntityGraph(attributePaths = {"datasets", "params", "fileAccessPatterns", "collection"}, type = EntityGraphType.LOAD)
	Optional<Datasource> findByUniqueId(String uniqueId);

	@Transactional
	@Query("SELECT d FROM Datasource d")
	@EntityGraph(attributePaths = {"datasets", "params", "fileAccessPatterns", "collection"}, type = EntityGraphType.LOAD)
	List<Datasource> findAndLoadAll();
	
	@Override
	@Transactional
	@Query("SELECT d FROM Datasource d")
	@EntityGraph(attributePaths = {"datasets", "collection", "collection.names"}, type = EntityGraphType.LOAD)
	List<Datasource> findAll();
}
