package de.unibamberg.minf.transformation.config.model;

import lombok.Data;

@Data
public class AccessChain {
	public enum AccessTypes { ENDPOINT, DATASOURCE }
	
	private final String key;
	private final String value;
	private final AccessTypes accessType;
}
