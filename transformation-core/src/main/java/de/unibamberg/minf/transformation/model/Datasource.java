package de.unibamberg.minf.transformation.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import de.unibamberg.minf.transformation.model.base.OnlineAccessMethod;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A specific implementation of a collection access, a datasource represents an access point 
 *  with harvesting or crawling capabilities.
 * 
 * A datasource provides access to data by means of multiple datamodels (as configured in the 
 *  Collection Registry) and can thus have multiple individual datasets - one per datamodel. 
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"datasets"})
@PrimaryKeyJoinColumn(name = "datasourceId")
@NamedEntityGraph( attributeNodes = { @NamedAttributeNode("datasets") } )
public class Datasource extends Access {

	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy="datasource", fetch = FetchType.LAZY)
	private Set<OnlineDataset> datasets;
	
	@Override
	public boolean isDatasource() { return true; }
	
	@Override
	public Set<OnlineAccessMethod> getAccessMethods() {
		if (this.getDatasets()!=null) {
			return new HashSet<>(this.getDatasets());
		} else {
			return new HashSet<>();
		}
	}
}
