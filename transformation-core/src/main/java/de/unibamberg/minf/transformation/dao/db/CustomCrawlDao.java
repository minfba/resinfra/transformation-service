package de.unibamberg.minf.transformation.dao.db;

import java.util.List;

import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlCompleteFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlErrorFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlOnlineFlag;
import de.unibamberg.minf.transformation.model.Crawl;

public interface CustomCrawlDao {
	List<Crawl> findCrawls(Long datasetId, CrawlOnlineFlag online, CrawlCompleteFlag complete, CrawlErrorFlag error, int limit);
}
