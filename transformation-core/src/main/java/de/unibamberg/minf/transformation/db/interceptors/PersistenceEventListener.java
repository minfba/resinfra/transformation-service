package de.unibamberg.minf.transformation.db.interceptors;

import java.time.OffsetDateTime;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;

import org.hibernate.HibernateException;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreInsertEventListener;
import org.hibernate.event.spi.PreUpdateEvent;
import org.hibernate.event.spi.PreUpdateEventListener;
import org.hibernate.event.spi.SaveOrUpdateEvent;
import org.hibernate.event.spi.SaveOrUpdateEventListener;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.base.BaseEntity;
import de.unibamberg.minf.transformation.model.base.UniqueEntity;

@Component
public class PersistenceEventListener implements PreInsertEventListener, PreUpdateEventListener, SaveOrUpdateEventListener {

	@Autowired private EntityManagerFactory entityManagerFactory;

    @PostConstruct
    private void init() {
        SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.PRE_INSERT).appendListener(this);
    }

    @Override
    public boolean onPreInsert(PreInsertEvent preInsertEvent) {
    	if (BaseEntity.class.isAssignableFrom(preInsertEvent.getEntity().getClass())) {
    		BaseEntity be = (BaseEntity)preInsertEvent.getEntity();
    		be.setCreated(OffsetDateTime.now());
    		be.setModified(be.getCreated());
	    	if (UniqueEntity.class.isAssignableFrom(preInsertEvent.getEntity().getClass())) {
	    		UniqueEntity uq = (UniqueEntity)preInsertEvent.getEntity();
	    		if (!uq.hasUniqueId() || uq.getUniqueId().length()<32) {
	    			uq.assignUniqueId();
	    		}
	    	}
	    	
	    	Object[] newState = preInsertEvent.getPersister().getPropertyValuesToInsert(preInsertEvent.getEntity(), null, preInsertEvent.getSession());
	    	for (int i=0; i<preInsertEvent.getState().length; i++) {
	    		preInsertEvent.getState()[i] = newState[i];
	    	}
    	} 
        return false;
    }
    
	@Override
	public boolean onPreUpdate(PreUpdateEvent preUpdateEvent) {
		if (BaseEntity.class.isAssignableFrom(preUpdateEvent.getEntity().getClass())) {
    		BaseEntity be = (BaseEntity)preUpdateEvent.getEntity();
    		be.setModified(OffsetDateTime.now());
    		
    		Object[] newState = preUpdateEvent.getPersister().getPropertyValuesToInsert(preUpdateEvent.getEntity(), null, preUpdateEvent.getSession());
	    	for (int i=0; i<preUpdateEvent.getState().length; i++) {
	    		preUpdateEvent.getState()[i] = newState[i];
	    	}
		}
		return false;
	}

	@Override
	public void onSaveOrUpdate(SaveOrUpdateEvent saveOrUpdateEvent) throws HibernateException {
    	if (BaseEntity.class.isAssignableFrom(saveOrUpdateEvent.getEntity().getClass())) {
    		BaseEntity be = (BaseEntity)saveOrUpdateEvent.getEntity();
    		if (be.getCreated()==null) {
    			be.setCreated(OffsetDateTime.now());
    		}
    		be.setModified(be.getCreated());
	    	if (UniqueEntity.class.isAssignableFrom(saveOrUpdateEvent.getEntity().getClass())) {
	    		UniqueEntity uq = (UniqueEntity)saveOrUpdateEvent.getEntity();
	    		if (!uq.hasUniqueId()) {
	    			uq.assignUniqueId();
	    		}
	    	}
    	}
	}
}
