package de.unibamberg.minf.transformation.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.dao.db.CollectionDao;
import de.unibamberg.minf.transformation.dao.db.DatasetDao;
import de.unibamberg.minf.transformation.dao.db.EndpointDao;
import de.unibamberg.minf.transformation.data.service.ArtifactService;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.Endpoint;

public class CollectionServiceImpl implements CollectionService {
	protected static final Logger logger = LoggerFactory.getLogger(CollectionServiceImpl.class);
			
	@Autowired private CollectionDao collectionDao;
	@Autowired private EndpointDao endpointDao;
	@Autowired private DatasetDao datasetDao;
	
	@Autowired private ArtifactService artifactService;

	
	@Override
	public List<Collection> findAll() {
		return collectionDao.findAll();
	}
	
	@Override
	public List<Collection> findAndLoadAll() {
		return collectionDao.findAndLoadAll();
	}
	
	/*@Override
	public List<Collection> getAll(Order order) {
		Query q = new Query();
		q.with(Sort.by(order));	
		return collectionDao.findByQuery(q);
	}*/
	
	@Override
	@Transactional
	public void saveCollection(Collection c) {
		/*if (c.getId()==null) {
			Set<Endpoint> eps = c.getEndpoints();
			c.setEndpoints(null);
			collectionDao.save(c);
			
			saveNewEndpoints(eps, c);
			c.setEndpoints(eps);
		} else {
			saveNewEndpoints(c.getEndpoints(), c);
		}*/
		collectionDao.save(c);
	}

	@Override
	public Collection findByColregEntityId(String colregEntityId) {
		return collectionDao.findByUniqueId(colregEntityId).orElse(null);
	}

	@Override
	public Collection findById(Long id) {
		return collectionDao.findById(id).orElse(null);
	}

	@Override
	public long getCount() {
		return collectionDao.count();
	}

	/*@Override
	public List<Collection> findByDatamodelId(String datamodelId) {
		return collectionDao.findByDatamodelId(datamodelId);
	}*/

	@Override
	public boolean delete(Collection collection) {
		// TODO: Further cleanup on datasets and interfaces		
		artifactService.removeByCollectionId(collection.getId());
		collectionDao.delete(collection);
		logger.info("Deleted collection [{}]", collection.getId());
		return true;
	}

	@Override
	public List<Collection> findByIds(List<Long> ids) {
		if (ids==null) {
			return new ArrayList<>();
		}
		return collectionDao.findAllById(ids);
	}

	
	private void saveNewEndpoints(Set<Endpoint> eps, Collection c) {
		if (eps==null) {
			return;
		}
		/*eps.stream().filter(ep -> ep.getId()==null).forEach(ep -> {
			Set<Dataset> dss = ep.getDatasets();
			ep.setDatasets(null);
			ep.setCollection(c);
			endpointDao.save(ep);
			
			ep.setDatasets(dss);
			saveNewDatasets(dss, ep);
			
			endpointDao.save(ep);
		});*/
	}
	
	private void saveNewDatasets(Set<Dataset> dss, Endpoint e) {
		if (dss==null) {
			return;
		}
		/*dss.stream().filter(ds -> ds.getId()==null).forEach(ds -> {
			ds.setEndpoint(e);
			datasetDao.save(ds);
		});*/
	}

	@Override
	public Optional<Collection> findByUniqueId(String uniqueId) {
		return collectionDao.findByUniqueId(uniqueId);
	}

	@Override
	public Collection findByDatasetId(Long datasetId) {
		return collectionDao.findByDatasetId(datasetId);
	}
}
