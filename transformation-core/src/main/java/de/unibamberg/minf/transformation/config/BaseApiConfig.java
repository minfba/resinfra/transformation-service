package de.unibamberg.minf.transformation.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import javax.annotation.PostConstruct;

import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import de.unibamberg.minf.transformation.api.client.CollectionClient;
import de.unibamberg.minf.transformation.api.client.MappingClient;
import de.unibamberg.minf.transformation.api.client.ModelClient;
import de.unibamberg.minf.transformation.automation.CollectionSyncService;
import de.unibamberg.minf.transformation.automation.CronTrigger;
import de.unibamberg.minf.transformation.automation.DmeSyncService;
import de.unibamberg.minf.transformation.automation.base.BaseScheduledRunnable;
import de.unibamberg.minf.transformation.config.nested.ColregConfigProperties;
import de.unibamberg.minf.transformation.config.nested.DmeConfigProperties;
import de.unibamberg.minf.transformation.exceptions.ConfigurationException;
import lombok.Data;

@Data
public class BaseApiConfig implements SchedulingConfigurer {
	private ColregConfigProperties colreg;
	private DmeConfigProperties dme;
	
	@Autowired private Executor syncAutomationTaskExecutor;
	
	@PostConstruct
    public void completeConfiguration() throws ConfigurationException {
		if (this.getColreg().getBaseUrl()==null || this.getColreg().getBaseUrl().isEmpty()) {
    		throw new ConfigurationException("baseUrl for ColReg must be set");
    	}
		if (this.getDme().getBaseUrl()==null || this.getDme().getBaseUrl().isEmpty()) {
    		throw new ConfigurationException("baseUrl for DME must be set");
    	}
	}
	
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		if (this.getColreg().isAutosync()) {
			this.configureRunnerTask(taskRegistrar, this.collectionSyncService(), this.getColreg().getCronSchedule());
		}
		if (this.getDme().isAutosync()) {
			this.configureRunnerTask(taskRegistrar, this.dmeSyncService(), this.getDme().getCronSchedule());
		}
	}
	
	
	private void configureRunnerTask(ScheduledTaskRegistrar taskRegistrar, BaseScheduledRunnable runner, String cronExpression) {
		if (cronExpression==null) {
			return;
		}
		CronTrigger runnerTrigger = new CronTrigger(runner.getClass().getSimpleName(), cronExpression, runner);
		
		taskRegistrar.setScheduler(syncAutomationTaskExecutor);
		taskRegistrar.addTriggerTask(runner, runnerTrigger);
		
		runner.setCronExpression(cronExpression);
	}
	
	@Bean
	public CollectionSyncService collectionSyncService() {
		CollectionSyncService collectionSyncService = new CollectionSyncService();
		collectionSyncService.setAutomationEnabled(getColreg().isAutosync());
		collectionSyncService.setAutocrawlNewDatasets(getColreg().isAutocrawlNewDatasets());
		return collectionSyncService;
	}
	
	@Bean
	public DmeSyncService dmeSyncService() {
		DmeSyncService dmeSyncService = new DmeSyncService();
		dmeSyncService.setAutomationEnabled(getDme().isAutosync());
		return dmeSyncService;
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public CollectionClient collectionSyncClient() {
		CollectionClient client = new CollectionClient();
		client.setColregConfig(getColreg());
		return client;
	}
	
	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public MappingClient mappingClient() {
		MappingClient client = new MappingClient();
		client.setDmeConfig(getDme());
		return client;
	}
	
	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public ModelClient modelClient() {
		ModelClient client = new ModelClient();
		client.setDmeConfig(getDme());
		return client;
	}
	
	@Bean
	public Map<String, Period> updateFrequencyMap() {
		 Map<String, Period> updateFrequencyMap = new HashMap<>();
		 updateFrequencyMap.put("http://purl.org/cld/freq/triennial", Period.parse("P3Y"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/biennial", Period.parse("P2Y"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/annual", Period.parse("P1Y"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/semiannual", Period.parse("P6M"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/threeTimesAYear", Period.parse("P4M"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/quarterly", Period.parse("P3M"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/bimonthly", Period.parse("P2M"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/monthly", Period.parse("P1W"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/semimonthly", Period.parse("P2W"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/biweekly", Period.parse("P2W"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/threeTimesAMonth", Period.parse("P1W"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/weekly", Period.parse("P1W"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/semiweekly", Period.parse("P3D"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/threeTimesAWeek", Period.parse("P2D"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/daily", Period.parse("P1D"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/continuous", Period.parse("P1D"));
		 updateFrequencyMap.put("http://purl.org/cld/freq/completelyIrregular", Period.parse("P2W"));
		 updateFrequencyMap.put("_defaultUnclosed", Period.parse("P1M"));
		 return updateFrequencyMap;
	}
	
	@Bean
	public List<String> knownUpdatePolicies() {
		List<String> knownUpdatePolicies = new ArrayList<>();
		knownUpdatePolicies.add("http://purl.org/cld/accpol/passive");
		knownUpdatePolicies.add("http://purl.org/cld/accpol/active");
		knownUpdatePolicies.add("http://purl.org/cld/accpol/partial");
		return knownUpdatePolicies;
	}	
}
