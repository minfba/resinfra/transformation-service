package de.unibamberg.minf.transformation.service;

import java.util.List;

import de.unibamberg.minf.dme.model.datamodel.base.Datamodel;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;


public interface DatamodelService {
	public List<Datamodel> findAllModels();
	public ExtendedDatamodelContainer findById(String id);
	

	public boolean deleteDatamodel(String id);
	
	//public ExtendedDatamodelContainer saveOrUpdate(DatamodelReferenceContainer drc, JsonNode mapping);
	//public void saveOrUpdate(ExtendedDatamodelContainer sc, JsonNode mapping, boolean staticTemplate);

	public List<ExtendedDatamodelContainer> findAll();
	public List<ExtendedDatamodelContainer> findByIds(List<String> datamodelIds);
	
	public List<String> findAllSchemaIds();
		
	public void saveOrUpdate(ExtendedDatamodelContainer sc);

	public List<ExtendedDatamodelContainer> findAll(boolean rebuildCache);
	//public void saveOrUpdate(ExtendedDatamodelContainer sc, JsonNode mapping);

	/**
	 * Loads individual datamodel from storage. All caching mechanisms of the DatamodelService are bypassed. 
	 * 
	 * @param modelId Identifier of datamodel
	 * @return ExtendedDatamodelContainer if found in storage
	 */
	public ExtendedDatamodelContainer loadById(String modelId);
	
	/**
	 * Loads datamodels from storage. All caching mechanisms of the DatamodelService are bypassed.
	 * 
	 * @return List of ExtendedDatamodelContainer if available 
	 */
	public List<ExtendedDatamodelContainer> loadAll();
	//public Map<String, Long> getEndpointDocumentCount(String indexName, List<String> collectionIds);
	
	public String getIndexName(String modelId);
}
