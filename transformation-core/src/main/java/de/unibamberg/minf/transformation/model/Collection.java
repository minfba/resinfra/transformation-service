package de.unibamberg.minf.transformation.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.MapKeyColumn;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.unibamberg.minf.transformation.model.base.BaseAccessibleEntityImpl;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * A collection represents an entity represented by the DARIAH-DE Collection Registry and thus somewhat corresponds 
 *  to the concept of a thematic research collection. It provides one ore many access mechanisms.
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@NamedEntityGraph(name = "list_collections",
	attributeNodes = {
		@NamedAttributeNode("names"),
		@NamedAttributeNode("access"),
	}
)
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"names","access"})
@Table(uniqueConstraints={@UniqueConstraint(name="uniqueId",columnNames={"uniqueId"})})
public class Collection extends BaseAccessibleEntityImpl {

	private String colregEntityId;
	private String colregVersionId;
	private String updatePeriod;
	
	private String imageUrl;
	
	@Lob
	private String collectionMetadata;
			
	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy="collection", fetch = FetchType.LAZY)
	private Set<Access> access;
	
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "collection_names", joinColumns = { @JoinColumn(name = "id") })
	@MapKeyColumn(name = "lang")
	@Column(name = "name")
	private Map<String, String> names;	
	
	public String getName(String locale) { 
		if (names!=null && names.containsKey(locale)) {
			return names.get(locale);
		}
		if (names!=null && !names.isEmpty()) {
			return names.get(names.keySet().iterator().next());
		}
		return null;
	}
	public void setName(String locale, String name) { 
		if (names==null) {
			names = new HashMap<>();
		}
		names.put(locale, name);
	}
}