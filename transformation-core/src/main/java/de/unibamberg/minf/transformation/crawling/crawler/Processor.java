package de.unibamberg.minf.transformation.crawling.crawler;

import java.util.List;

import de.unibamberg.minf.processing.ElementProcessor;
import de.unibamberg.minf.processing.consumption.ResourceConsumptionService;

public interface Processor extends Crawler {
	public List<ResourceConsumptionService> getConsumptionServices();
	public void addConsumptionService(ResourceConsumptionService consumptionService);
	
	public List<ElementProcessor> getElementProcessors();
	public void setElementProcessors(List<ElementProcessor> elementProcessors);
}
