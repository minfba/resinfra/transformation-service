package de.unibamberg.minf.transformation.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import de.unibamberg.minf.dme.model.base.BaseLongIdentifiable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Headers as required for accessing an endpoint or datasource. 
 *  Examples are ContentType specifications or API keys.
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AccessHeader extends BaseLongIdentifiable {
	private String param;
	private String value;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="access_id", nullable=false)
	private Access access;
}
