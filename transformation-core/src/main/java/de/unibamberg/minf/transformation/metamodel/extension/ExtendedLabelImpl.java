package de.unibamberg.minf.transformation.metamodel.extension;

import de.unibamberg.minf.dme.model.base.Label;
import de.unibamberg.minf.dme.model.datamodel.LabelImpl;

public class ExtendedLabelImpl extends LabelImpl implements ExtendedElement {
	private static final long serialVersionUID = -5318390467992960327L;
	
	private boolean nested;

	@Override public boolean isNested() { return nested; }
	@Override public void setNested(boolean nested) { this.nested = nested; }
	
	public ExtendedLabelImpl() {}
	
	public ExtendedLabelImpl(Label element) {
		this.setSubLabels(element.getSubLabels());
		this.setDisabled(element.isDisabled());
		this.setEntityId(element.getEntityId());
		this.setGrammars(element.getGrammars());
		this.setId(element.getId());
		this.setLocked(element.isLocked());
		this.setName(element.getName());
		this.setTransient(element.isTransient());
	}
}