package de.unibamberg.minf.transformation.dao.fs;

import java.util.List;

import de.unibamberg.minf.dme.model.serialization.DatamodelReferenceContainer;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;

public interface DatamodelConfigDao {
	public ExtendedDatamodelContainer findById(String schemaId);
	public List<ExtendedDatamodelContainer> findAll();

	public ExtendedDatamodelContainer saveOrUpdate(DatamodelReferenceContainer drc);
	public ExtendedDatamodelContainer saveOrUpdate(ExtendedDatamodelContainer datamodel);

	public void deleteSchema(String id);
	public String getIndexName(String modelId);
}
