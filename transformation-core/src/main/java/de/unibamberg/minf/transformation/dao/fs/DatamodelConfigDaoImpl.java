package de.unibamberg.minf.transformation.dao.fs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.minf.dme.model.serialization.DatamodelReferenceContainer;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DatamodelConfigDaoImpl extends BaseFsDao implements DatamodelConfigDao, InitializingBean {
	@Autowired private ObjectMapper objectMapper;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		File schemaDir = new File(config.getPaths().getDatamodels());
		if (!schemaDir.exists()) {
			FileUtils.forceMkdir(schemaDir);
			log.info("Created schema directory: " + config.getPaths().getDatamodels());
		}
	}
	
	@Override
	public ExtendedDatamodelContainer findById(String schemaId) {
		File rootDir = new File(config.getPaths().getDatamodels());
		if (!rootDir.isDirectory() || rootDir.listFiles()==null) {
			log.error(String.format("Failed to open schema directory [%s]", config.getPaths().getDatamodels()));
		} else {
			for (File schemaConfig : rootDir.listFiles()) {
				try {
					ExtendedDatamodelContainer s = objectMapper.readValue(schemaConfig, ExtendedDatamodelContainer.class);					
					this.renameIfIndicated(schemaConfig, s.getModel().getId());
										
					if (s.getId().equals(schemaId)) {
						return s;
					}
				} catch (Exception e) {
					log.error("Failed to parse schema configuration from filesystem: " + schemaConfig.getAbsolutePath(), e);
				}
			}	
		}
		return null;
	}
	
	@Override
	public List<ExtendedDatamodelContainer> findAll() {
		List<ExtendedDatamodelContainer> schemas = new ArrayList<>();
		
		File rootDir = new File(config.getPaths().getDatamodels());
		if (!rootDir.isDirectory() || rootDir.listFiles()==null) {
			log.error(String.format("Failed to open schema directory [%s]", config.getPaths().getDatamodels()));
		} else {
			for (File schemaConfig : rootDir.listFiles()) {
				if (schemaConfig.isDirectory()) {
					continue;
				}
				try {
					ExtendedDatamodelContainer s = objectMapper.readValue(schemaConfig, ExtendedDatamodelContainer.class);					
					schemas.add(s);
					this.renameIfIndicated(schemaConfig, s.getModel().getId());
				} catch (Exception e) {
					log.error("Failed to parse schema configuration from filesystem: " + schemaConfig.getAbsolutePath(), e);
				}
			}	
		}
		if (schemas.isEmpty()) {
			log.warn(String.format("No schema was found at configured directory [%s]", config.getPaths().getDatamodels()));
		}
		return schemas;
	}
	
	@Override
	public ExtendedDatamodelContainer saveOrUpdate(DatamodelReferenceContainer drc) {
		ExtendedDatamodelContainer saveSc = this.findById(drc.getModel().getId());
		if (saveSc==null) {
			saveSc = new ExtendedDatamodelContainer();
		}
		saveSc.setModel(drc.getModel());
		saveSc.setElements(drc.getElements());
		saveSc.setRoot(drc.getRoot());
		
		this.saveSchema(saveSc);
		return saveSc;
	}

	@Override
	public ExtendedDatamodelContainer saveOrUpdate(ExtendedDatamodelContainer sc) {
		this.saveSchema(sc);
		return sc;
	}
	
	@Override
	public void deleteSchema(String id) {
		File rootDir = new File(config.getPaths().getDatamodels());
		if (!rootDir.isDirectory() || rootDir.listFiles()==null) {
			log.error("Failed to open schema directory [{}]", config.getPaths().getDatamodels());
		} else {
			ExtendedDatamodelContainer s;
			try {
				for (File schemaConfig : rootDir.listFiles()) {
					s = objectMapper.readValue(schemaConfig, ExtendedDatamodelContainer.class);	
					if (s.getModel().getId()!=null && s.getModel().getId().equals(id)) {
						FileUtils.forceDelete(schemaConfig);
						return;
					}
				}
			} catch (Exception e) {
				log.error("Failed to delete schema configuration from filesystem", e);
			}
		}
	}
	
	private boolean saveSchema(ExtendedDatamodelContainer sc) {
		File rootDir = new File(config.getPaths().getDatamodels());
		if (!rootDir.isDirectory() || rootDir.listFiles()==null) {
			log.error(String.format("Failed to open schema directory [%s]", config.getPaths().getDatamodels()));
		} else {
			JsonNode s;
			JsonNode id;
			sc.setIndexName(this.getIndexName(sc.getId()));
			try {
				// Update?
				for (File schemaConfig : rootDir.listFiles()) {
					s = objectMapper.readTree(schemaConfig);
					id = s.path("model").path("id");
					if (!id.isMissingNode() && id.textValue().equals(sc.getModel().getId())) {
						objectMapper.writer().writeValue(schemaConfig, sc);
						log.debug("Updated schema config at: " + schemaConfig.getAbsolutePath());
						return true;
					}					
				}
				// No match found -> new schema
				File newFilePath = new File(this.getAbsoluteFilePath(sc.getId()));
				objectMapper.writer().writeValue(newFilePath, sc);
				log.debug("Created schema config at: " + newFilePath.getAbsolutePath());
				return true;
			} catch (Exception e) {
				log.error("Failed to write schema configuration to filesystem", e);
				return false;
			}
		}
		return false;
	}

	private void renameIfIndicated(File schemaConfigFile, String schemaId) {
		try {
			if (!schemaConfigFile.getName().equals(schemaId+".json".toLowerCase())) {
				log.info(String.format("Renamed out-of-sync schema [%s]", schemaId));
			}
		} catch (Exception e) {
			log.warn("Failed to rename out-of-sync schema", e);
		}
	}
	
	private String getAbsoluteFilePath(String schemaId) {
		return String.format("%s%s%s", config.getPaths().getDatamodels(), File.separator, (schemaId+".json").toLowerCase());
	}

	@Override
	public String getIndexName(String modelId) {
		return config.getIndexing()!=null && config.getIndexing().getIndexNamePrefix()!=null ? (config.getIndexing().getIndexNamePrefix() + modelId) : modelId;
	}
}
