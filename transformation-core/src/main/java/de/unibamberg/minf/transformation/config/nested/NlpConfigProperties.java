package de.unibamberg.minf.transformation.config.nested;

import lombok.Data;

@Data
public class NlpConfigProperties {
	private String language;
	private String models;
	private String taggerModel;
	private String lexParseModel;
	private String classifierModel;
}