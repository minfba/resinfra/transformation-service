package de.unibamberg.minf.transformation.config;

import de.unibamberg.minf.transformation.config.nested.IndexingLogConfigProperties;
import lombok.Data;

@Data
public class LogConfigProperties {
	private IndexingLogConfigProperties indexing;
	private String logFile = "search.log";
	private String oldlogSuffix = "-%d{yyyy-MM-dd-HH-mm}-%i.log.gz";
	private String dir;
	private String pattern = "%d [%thread] %-5level %logger{36}[%line] - %msg%n";
	private int maxHistory = 90;
	private String totalSizeCap = "1GB";
	private String maxFileSize = "10MB";
	
	private boolean logQueries;
}
