package de.unibamberg.minf.transformation.dao.fs;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.unibamberg.minf.core.util.DirectoryInfo;

public class DownloadDataDaoImpl extends BaseFsDao implements DownloadDataDao {
	protected static final Logger logger = LoggerFactory.getLogger(DownloadDataDaoImpl.class);
	
	private static Pattern originalFilePattern = Pattern.compile("^(?i)original.*$");
	
	
	@Override
	public String getDownloadParentPath(String... subdirs) {
		StringBuilder pathBldr = new StringBuilder();
		pathBldr.append(config.getPaths().getDownloads());
		for (String subdir : subdirs) {
			pathBldr
				.append(File.separator)
				.append(subdir);
		}		
		return pathBldr.toString();
	}
	
	@Override
	public void clearAllData(String... subdirs) throws IOException {
		File cDir = new File(this.getDownloadParentPath(subdirs));
		if (cDir.exists() && cDir.isDirectory()) {
			for (File fContent : cDir.listFiles()) {
				FileUtils.forceDelete(fContent);
			}
		}
	}
	
	@Override
	public void resetRenderedData(String... subdirs) throws IOException {
		File cDir = new File(this.getDownloadParentPath(subdirs));
		File tmpFile;
		Matcher filenameMatcher;
		if (cDir.exists() && cDir.isDirectory()) {
			for (File fContent : cDir.listFiles()) {
				// Original file is already in place 
				if (fContent.isFile()) {
					continue;
				}
				for (File fSubContent : fContent.listFiles()) {
					filenameMatcher = originalFilePattern.matcher(fSubContent.getName());
					if (filenameMatcher.matches()) {
						tmpFile = new File(cDir, "tmp_" + fContent.getName());
						FileUtils.moveFile(fSubContent, tmpFile);
						FileUtils.forceDelete(fContent);
						FileUtils.moveFile(tmpFile, new File(cDir, fContent.getName()));
						break;
					}
				}
			}	
			
		}
	}
	
	@Override
	public boolean hasData(String... subdirs) {
		try {
			File cDir = new File(this.getDownloadParentPath(subdirs));
			if (cDir.exists() && cDir.isDirectory() && cDir.listFiles().length>0) {
				return true;
			}
		} catch (Exception e) {
			logger.error("Failed to determine hasData on downloaded data", e);
		}
		
				
		return false;
	}

	
	
	@Override
	public DirectoryInfo getDataInfo(String... subdirs) {
		File cDir = new File(this.getDownloadParentPath(subdirs));
		DirectoryInfo di = new DirectoryInfo();
				
		if (cDir.exists() && cDir.isDirectory()) {	
			di.setExists(true);
			try {
				this.collectionDataInfo(di, cDir);
			} catch (Exception e) {
				logger.error("Failed to scan data info for downloaded data", e);
			}	
		}
		return di;
	}
	
	private void collectionDataInfo(DirectoryInfo di, File directory) throws IOException {
		AtomicLong size = new AtomicLong(0);
		AtomicInteger count = new AtomicInteger(0);
		
		Files.walkFileTree(Paths.get(directory.toURI()), new SimpleFileVisitor<Path>() {
	        @Override
	        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
	            size.addAndGet(attrs.size());
	            count.addAndGet(1);
	            return FileVisitResult.CONTINUE;
	        }
	    });
		di.setFileCount(count.get());
		di.setSize(size.get());
	}
}
