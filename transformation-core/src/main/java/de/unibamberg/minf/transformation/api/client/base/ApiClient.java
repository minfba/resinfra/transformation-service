package de.unibamberg.minf.transformation.api.client.base;

import de.unibamberg.minf.transformation.api.client.ApiConsumptionException;
import de.unibamberg.minf.transformation.pojo.ApiStatusPojo;

public interface ApiClient {
	public void sync() throws ApiConsumptionException;
	public ApiStatusPojo ping();
}
