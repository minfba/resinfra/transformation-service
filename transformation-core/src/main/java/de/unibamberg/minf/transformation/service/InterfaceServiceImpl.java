package de.unibamberg.minf.transformation.service;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.dme.model.datamodel.DatamodelImpl;
import de.unibamberg.minf.transformation.dao.db.InterfaceDao;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.Interface;

public class InterfaceServiceImpl implements InterfaceService {

	@Autowired private InterfaceDao interfaceDao;
	@Autowired private DatamodelService datamodelService;
	
	@Override
	public void delete(Interface in) {
		// TODO: Cleanup cache once implemented
		interfaceDao.delete(in);
	}

	@Override
	public void deleteAll(Set<Interface> interfaces) {
		// TODO: Cleanup cache once implemented
		interfaceDao.deleteAll(interfaces);
	}

	@Override
	public Optional<Interface> findByUniqueId(String uniqueId) {
		return this.setDatamodel(interfaceDao.findByUniqueId(uniqueId));
	}

	@Override
	public Optional<Interface> findById(Long id) {
		return this.setDatamodel(interfaceDao.findById(id));
	}

	private Optional<Interface> setDatamodel(Optional<Interface> inf) {
		if (inf.isPresent() && inf.get().getDatamodelId()!=null) {
			ExtendedDatamodelContainer dmc = datamodelService.findById(inf.get().getDatamodelId());
			if (dmc==null) {
				dmc = new ExtendedDatamodelContainer();
				dmc.setId(inf.get().getDatamodelId());
				dmc.setDeleted(true);
				dmc.setModel(new DatamodelImpl());
				dmc.getModel().setId(inf.get().getDatamodelId());
			}
			inf.get().setDatamodel(dmc);
		}
		return inf;
	}

	@Override
	public void saveInterface(Interface in) {
		interfaceDao.save(in);
	}
}
