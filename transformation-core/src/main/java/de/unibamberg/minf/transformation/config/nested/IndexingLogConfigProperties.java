package de.unibamberg.minf.transformation.config.nested;

import lombok.Data;

@Data
public class IndexingLogConfigProperties {
	private String baseDir;
	private String pattern = "%d%level%logger{0}%msg%htmlLink";
	private String remoteFilesPrefix = "../remoteFiles";
}
