package de.unibamberg.minf.transformation.config.nested;

import lombok.Data;

@Data
public class PathsConfigProperties {
	private String main;
	private String config;
	private String data;
	private String backups;
	private String datamodels;
	private String mappings;
	private String grammars;
	private String models;	
	private String crawls;
	private String pictures;
	private String downloads;
	private String parseErrors;
	private String mappingLogs;
	private String temporary;
}