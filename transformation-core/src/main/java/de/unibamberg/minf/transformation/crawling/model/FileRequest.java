package de.unibamberg.minf.transformation.crawling.model;

import java.nio.charset.StandardCharsets;
import java.util.List;

import lombok.Data;

@Data
public class FileRequest {	
	private String uri;
	private String method;
	private String format;
	private byte[] requestBody;
	protected List<FileRequestPair> requestHeaders = null;
	
	public FileRequest(String uri) {
		this(uri, null);
	}
	
	public FileRequest(String uri, String method) {
		this.uri = uri;
		this.method = method==null ? "GET" : method;
	}
	
	public FileRequest(String uri, String method, String requestBody, List<FileRequestPair> requestHeaders) {
		this.uri = uri;
		this.method = method==null ? "GET" : method;
		this.requestBody = requestBody.getBytes(StandardCharsets.UTF_8);
		this.requestHeaders = requestHeaders;
	}
	
	public FileRequest(String uri, String method, byte[] requestBody, List<FileRequestPair> requestHeaders) {
		this.uri = uri;
		this.method = method==null ? "GET" : method;
		this.requestBody = requestBody;
		this.requestHeaders = requestHeaders;
	}
}