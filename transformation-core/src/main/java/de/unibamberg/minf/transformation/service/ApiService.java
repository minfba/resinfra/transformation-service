package de.unibamberg.minf.transformation.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import de.unibamberg.minf.transformation.model.Api;

public interface ApiService {
	public Optional<Api> findByUniqueId(String uniqueId);
	public Collection<Api> findAll();
	
	public void deleteById(Long id);
	public void saveApi(Api api);
	public void removeInterfaceFromApis(Long interfaceId);
	public Collection<Api> findByInterfaceIds(Collection<Long> collect); 
	
}
