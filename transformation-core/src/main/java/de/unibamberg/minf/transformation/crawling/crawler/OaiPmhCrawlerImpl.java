package de.unibamberg.minf.transformation.crawling.crawler;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.dme.model.datamodel.natures.XmlDatamodelNature;
import de.unibamberg.minf.dme.model.datamodel.natures.xml.XmlTerminal;
import de.unibamberg.minf.processing.service.online.OaiPmhHarvestingService;
import de.unibamberg.minf.transformation.crawling.oaipmh.OaiPmhClient;
import de.unibamberg.minf.transformation.crawling.oaipmh.model.OaiPmhMetadataFormat;
import de.unibamberg.minf.transformation.crawling.oaipmh.model.OaiPmhResponseContainer;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.TemporaryAccessOperation;
import de.unibamberg.minf.transformation.model.base.AccessOperation;
import de.unibamberg.minf.transformation.service.CrawlService;

public class OaiPmhCrawlerImpl extends OaiPmhHarvestingService implements Crawler {
	@Autowired private CrawlService crawlService; 
	@Autowired private OaiPmhClient oaiPmhClient;
	
	private boolean initialized = false;
	
	private String accessOperationUid;
	
	@Override
	public String getUnitMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.oai_crawling.unit";
	}

	@Override
	public String getTitleMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.oai_crawling.title";
	}
	
	@Override
	public boolean isInitialized() {
		return super.isInitialized() && initialized;
	}
	
	@Override
	public void run() {
		this.setMdcUid(String.format("crawl_%s", accessOperationUid));
		MDC.put("uid", this.getMdcUid());
		super.run();
	}
	
	@Override
	public void init(Access access, AccessOperation op, ExtendedDatamodelContainer sc) {
		this.setUrl(access.getUrl());
		this.setSet(access.getSingleParamValue("set"));
		this.accessOperationUid = op.getUniqueId();
		
		if (TemporaryAccessOperation.class.isAssignableFrom(op.getClass())) {
			Path path = Paths.get(FileUtils.getTempDirectory().getAbsolutePath(), op.getUniqueId());
			this.setCrawlDir(path.toString());
		} else {
			Crawl crawl = (Crawl)op;
			if (crawl.getPrefix()==null || crawl.getPrefix().trim().isEmpty()) {
				String prefix = this.detectMetadataPrefix(access, sc);
				if (prefix==null || prefix.trim().isEmpty()) {
					logger.warn("Failed to automatically detect metadata prefix for OAI-PMH endpoint");
					this.initialized = false;
					return;
				} else {
					logger.warn(String.format("Metadata prefix for OAI-PMH endpoint [%s] automatically detected [%s]", this.getUrl(), prefix));
					crawl.setPrefix(prefix);
					crawlService.save(crawl);
				}
			}
			
			this.setPrefix(crawl.getPrefix());
			this.setCrawlDir(crawlService.getCrawlDirPath(crawl));
		}
		
		this.initialized = true;
	}

	
	private String detectMetadataPrefix(Access access, ExtendedDatamodelContainer sc) {
		String rootNs = null;
		
		XmlDatamodelNature xmlNature = sc.getModel().getNature(XmlDatamodelNature.class);
		String rootTerminalId = xmlNature.getTerminalId(sc.getRoot().getId());
		
		for (XmlTerminal t : xmlNature.getTerminals()) {
			if (t.getId().equals(rootTerminalId)) {
				rootNs = t.getNamespace().trim().toLowerCase();
			}
		}

		String prefix = null;
		OaiPmhResponseContainer oaiFormatsResponse = oaiPmhClient.listMetadataFormats(access.getUrl(), null);
		if (oaiFormatsResponse!=null && oaiFormatsResponse.getFormats()!=null) {
			for (OaiPmhMetadataFormat format : oaiFormatsResponse.getFormats()) {
				if (format.getMetadataNamespace().trim().toLowerCase().equals(rootNs)) {
					if (prefix==null) {
						prefix = format.getMetadataPrefix();	
					} else {
						logger.warn("Multiple metadata prefixes matched for schema. Using first");
					}
				}
			}
		}
		if (prefix==null) {
			logger.warn("Could not detect metadata prefix from namespaced. Trying schema names");
			if (oaiFormatsResponse!=null && oaiFormatsResponse.getFormats()!=null) {
				for (OaiPmhMetadataFormat format : oaiFormatsResponse.getFormats()) {
					if (format.getMetadataPrefix().trim().toLowerCase().equals(sc.getModel().getName().trim().toLowerCase())) {
						if (prefix==null) {
							prefix = format.getMetadataPrefix();	
						} else {
							logger.warn("Multiple metadata prefixes matched for schema. Using first");
						}
					}
				}
			}
		}
		return prefix;
	}
}