package de.unibamberg.minf.transformation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import de.unibamberg.minf.transformation.dao.db.PersistedUserDao;
import de.unibamberg.minf.transformation.model.PersistedUser;

public class UserServiceImpl implements UserService {
	@Autowired private PersistedUserDao userDetailsDao;
		
	@Override
	public PersistedUser loadUserByUsername(String domain, String username) throws UsernameNotFoundException {
		return userDetailsDao.findByUsername(domain, username);
	}

	@Override
	public void saveUser(PersistedUser persistedUser) {
		userDetailsDao.save(persistedUser);
	}

	@Override
	public PersistedUser findById(Long id) {
		return userDetailsDao.findById(id).orElse(null);
	}

	@Override
	public List<PersistedUser> findAll() {
		return userDetailsDao.findAll();
	}

	@Override
	public Optional<PersistedUser> findByUniqueId(String uniqueId) {
		return userDetailsDao.findByUniqueId(uniqueId);
	}

	@Override
	public Optional<PersistedUser> findByTokenUniqueId(String tokenId) {
		return userDetailsDao.findByTokenUniqueId(tokenId);
	}
}
