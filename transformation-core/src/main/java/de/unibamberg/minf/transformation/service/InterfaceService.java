package de.unibamberg.minf.transformation.service;

import java.util.Optional;
import java.util.Set;

import de.unibamberg.minf.transformation.model.Interface;

public interface InterfaceService {
	public void delete(Interface in);
	public void deleteAll(Set<Interface> interfaces);
	public Optional<Interface> findByUniqueId(String uniqueId);
	public Optional<Interface> findById(Long id);
	public void saveInterface(Interface in);
}
