package de.unibamberg.minf.transformation.dao.db;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.model.Crawl;

@Repository
public interface CrawlDao extends JpaRepository<Crawl, Long>, CustomCrawlDao {
	public enum CrawlOnlineFlag { Online, Offline, Both };
	public enum CrawlCompleteFlag { Complete, Incomplete, Both };
	public enum CrawlErrorFlag { Error, NoError, Both };
	
	@Transactional
	@Query("SELECT c FROM Crawl c WHERE dataset_id = ?1 order by created desc")
	List<Crawl> findByEndpointAndDatamodel(Long datasetId, int limit);
	
	@Transactional
	@Query("SELECT c FROM Crawl c WHERE dataset_id = ?1 order by created desc")
	List<Crawl> findAllByEndpointAndDatamodel(Long datasetId);

	@Transactional
	@Query("SELECT c FROM Crawl c WHERE error = false AND complete = false order by created desc")
	List<Crawl> findCurrentCrawls();
	
	@Transactional
	@Query("SELECT c FROM Crawl c WHERE error = false AND complete = false AND dataset_id = ?1 order by created desc")
	List<Crawl> findCurrentCrawls(Long datasetId);

	
	@Override
	@Transactional
	@EntityGraph(attributePaths = {"dataset", "baseCrawl"}, type = EntityGraphType.LOAD)
	Optional<Crawl> findById(Long id);
	
	Optional<Crawl> findByUniqueId(String uniqueId);
}
