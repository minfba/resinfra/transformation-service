package de.unibamberg.minf.transformation.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.dme.model.datamodel.DatamodelImpl;
import de.unibamberg.minf.transformation.dao.db.DatasetDao;
import de.unibamberg.minf.transformation.dao.db.LocalDatasetDao;
import de.unibamberg.minf.transformation.dao.db.OnlineDatasetDao;
import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.LocalDataset;
import de.unibamberg.minf.transformation.model.OnlineDataset;

public class DatasetServiceImpl implements DatasetService {
	
	@Autowired private DatasetDao datasetDao;
	@Autowired private LocalDatasetDao localDatasetDao;
	@Autowired private OnlineDatasetDao onlineDatasetDao;
	
	@Autowired private DatamodelService datamodelService;

	@Override
	public Optional<Dataset> findByUniqueId(String uniqueId) {
		return this.setDatamodel(datasetDao.findByUniqueId(uniqueId));
	}

	@Override
	public Optional<LocalDataset> findLocalByUniqueId(String uniqueId) {
		return this.setDatamodel(localDatasetDao.findByUniqueId(uniqueId));
	}

	@Override
	public Optional<OnlineDataset> findOnlineByUniqueId(String uniqueId) {
		return this.setDatamodel(onlineDatasetDao.findByUniqueId(uniqueId));
	}	

	@Override
	public List<Dataset> findAll() {
		return datasetDao.findAll();
	}

	@Override
	public List<LocalDataset> findAllLocal() {
		return localDatasetDao.findAll();
	}

	@Override
	public List<OnlineDataset> findAllOnline() {
		return onlineDatasetDao.findAndLoadAll();
	}

	@Override
	public List<OnlineDataset> findOnlineByDatamodelId(String datamodelId) {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public void saveDataset(Dataset dataset) {
		datasetDao.save(dataset);
		
	}

	@Override
	public Dataset findById(Long id) {
		return datasetDao.findById(id).orElse(null);
	}


	@Override
	public void delete(Dataset dataset) {
		// TODO: Cleanup resources etc.
		datasetDao.delete(dataset);
	}

	@Override
	public void deleteAll(Iterable<? extends Dataset> datasets) {
		// TODO: Cleanup resources etc.
		datasetDao.deleteAll(datasets);
	}
	
	
	private <T extends Dataset> Optional<T> setDatamodel(Optional<T> dataset) {
		if (dataset.isPresent() && dataset.get().getDatamodelId()!=null) {
			ExtendedDatamodelContainer dmc = datamodelService.findById(dataset.get().getDatamodelId());
			if (dmc==null) {
				dmc = new ExtendedDatamodelContainer();
				dmc.setId(dataset.get().getDatamodelId());
				dmc.setDeleted(true);
				dmc.setModel(new DatamodelImpl());
				dmc.getModel().setId(dataset.get().getDatamodelId());
			}
			dataset.get().setDatamodel(dmc);
		}
		return dataset;
	}
}
