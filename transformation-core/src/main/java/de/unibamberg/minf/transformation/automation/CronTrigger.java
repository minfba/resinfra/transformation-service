package de.unibamberg.minf.transformation.automation;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.support.CronExpression;

import de.unibamberg.minf.transformation.automation.base.ScheduledRunnable;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CronTrigger implements Trigger {
	private final String triggerName;
	private final CronExpression cronExpression;
	
	@Getter private final ScheduledRunnable runnable;
	
	public CronTrigger(String triggerName, String cronExpression, ScheduledRunnable runnable) {
		this.triggerName = triggerName;
		this.cronExpression = this.parseCronExpression(cronExpression);
		this.runnable = runnable;
		this.setRunnableDates(getNextExecution(), null);
	}
	
	@Override
	public Date nextExecutionTime(TriggerContext context) {
		Optional<Date> lastCompletion = Optional.ofNullable(context.lastCompletionTime());
		Optional<Date> nextExecution = Optional.ofNullable(this.getNextExecution());
		
		this.logExecutionInfo(lastCompletion, nextExecution);
		this.setRunnableDates(nextExecution.orElse(null), lastCompletion.orElse(null));
		
		return nextExecution.orElse(null);
	}
	
	private void setRunnableDates(Date nextExecution, Date lastCompletion) {
		if (this.runnable!=null) {
			this.runnable.setNextExecution(nextExecution);
			this.runnable.setLastCompletion(lastCompletion);
		}
	}
	
	private CronExpression parseCronExpression(String expression) {
		CronExpression cronEx = null;
		if (expression!=null) {
			try {
				cronEx = CronExpression.parse(expression);
			} catch (Exception e) {
				log.error("Failed to parse cron expression", e);
			}
		}
		return cronEx;
	}
	
	public Date getNextExecution() {
		if (cronExpression==null) {
			return null;
		}
		LocalDateTime nextExecutionLocalTime = cronExpression.next(LocalDateTime.now());
		if (nextExecutionLocalTime==null) {
			return null;
		}
		return Date.from(nextExecutionLocalTime.atZone(ZoneId.systemDefault()).toInstant());
	}
	
	private void logExecutionInfo(Optional<Date> lastCompletion, Optional<Date> nextExecution) {
		StringBuilder infoBldr = new StringBuilder();
		if (triggerName!=null) {
			infoBldr.append(triggerName + ": ");
		} 

		infoBldr.append("next execution ");
		if (nextExecution.isPresent()) {
			infoBldr.append(nextExecution.get().toString());
		} else {
			infoBldr.append("never");
		}
		
		if (lastCompletion.isPresent()) {
			infoBldr.append(", last completion ").append(lastCompletion.get());
		}
		
		log.info(infoBldr.toString());
	}
}