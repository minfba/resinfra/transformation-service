package de.unibamberg.minf.transformation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import de.unibamberg.minf.transformation.model.PersistedUser;

public interface UserService {
	public PersistedUser loadUserByUsername(String domain, String username) throws UsernameNotFoundException;
	public void saveUser(PersistedUser persistedUser);
	public PersistedUser findById(Long id);
	//public List<String> getUsernames(String query);
	public List<PersistedUser> findAll();
	public Optional<PersistedUser> findByUniqueId(String uniqueId);
	public Optional<PersistedUser> findByTokenUniqueId(String tokenId);
}
