package de.unibamberg.minf.transformation.config.nested;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class DmeConfigProperties extends ApiServiceConfigProperties {
	private boolean autoname = true;
	private boolean autocreate = true;

    private String modelLinkPath = "model/editor/%s/";
    private String modelsPath = "api/models";
    private String modelPath = "api/models/%s";
    private String mappingsPath = "api/mappings";
    private String mappingPath = "api/mappings/%s";
    private String mappingBySTPath = "api/mappings/by-source-and-target/%s/%s";
    private String mappingBySPath = "api/mappings/by-source/%s";
    private String mappingByTPath = "api/mappings/by-target/%s";
    
    
    public String getModelLinkUrl() {
    	return this.getUrl(modelLinkPath);
    }
    
    public String getModelsUrl() {
    	return this.getUrl(modelsPath);
    }
    
    public String getModelUrl() {
    	return this.getUrl(modelPath);
    }
    
    public String getMappingsUrl() {
    	return this.getUrl(mappingsPath);
    }
    
    public String getMappingUrl() {
    	return this.getUrl(mappingPath);
    }
    
    public String getMappingBySTUrl() {
    	return this.getUrl(mappingBySTPath);
    }
    
    public String getMappingBySUrl() {
    	return this.getUrl(mappingBySPath);
    }
    
    public String getMappingByTUrl() {
    	return this.getUrl(mappingByTPath);
    }
}