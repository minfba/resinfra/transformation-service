package de.unibamberg.minf.transformation.config;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.uniba.minf.core.rest.client.security.SecurityTokenIssuer;
import de.unibamberg.minf.gtf.DescriptionEngineImpl;
import de.unibamberg.minf.gtf.GtfElementProcessor;
import de.unibamberg.minf.gtf.GtfMappingProcessor;
import de.unibamberg.minf.gtf.MainEngine;
import de.unibamberg.minf.gtf.MainEngineImpl;
import de.unibamberg.minf.gtf.TransformationEngineImpl;
import de.unibamberg.minf.gtf.commands.CommandDispatcher;
import de.unibamberg.minf.gtf.commands.dispatcher.CoreCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.file.commands.OnlineFileCommands;
import de.unibamberg.minf.gtf.extensions.file.dispatcher.FileCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.geo.commands.GeoCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.geo.commands.SimpleGeoCommands;
import de.unibamberg.minf.gtf.extensions.person.commands.PersonCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.tika.commands.PdfCommands;
import de.unibamberg.minf.gtf.extensions.vocabulary.commands.SimpleVocabularyCommands;
import de.unibamberg.minf.gtf.extensions.vocabulary.commands.VocabularyCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.wiki.commands.WikiCommandsDispatcher;
import de.unibamberg.minf.gtf.transformation.processing.GlobalCommandDispatcher;
import de.unibamberg.minf.gtf.vocabulary.VocabularyEngine;
import de.unibamberg.minf.processing.output.csv.CsvStringOutputService;
import de.unibamberg.minf.processing.output.json.JsonStringOutputService;
import de.unibamberg.minf.processing.output.xml.XmlOutputService;
import de.unibamberg.minf.transformation.config.nested.FileConfigProperties;
import lombok.Data;

@Data
public class BaseProcessingConfig {
	private FileConfigProperties file;
	
	@Autowired protected TransformationConfig mainConfig;
	@Autowired private BaseApiConfig apiConfig;
	
	@Autowired private SecurityTokenIssuer securityTokenIssuer;
	
	@PostConstruct
    public void completeConfiguration() {
		if (this.getFile()==null) {
    		this.setFile(new FileConfigProperties());
    	}
	}
		
	@Bean
	@Scope("prototype")
	public CoreCommandsDispatcher coreCommandsDispatcher() {
		return new CoreCommandsDispatcher();
	}
	
	@Bean
	@Scope("prototype")
	public FileCommandsDispatcher fileCommandsDispatcher() {
		FileCommandsDispatcher fileCommandsDispatcher = new FileCommandsDispatcher();
		OnlineFileCommands onlineFileCommands = new OnlineFileCommands();
		onlineFileCommands.setBaseDownloadDirectory(mainConfig.getPaths().getDownloads());
		onlineFileCommands.setDisabled(this.getFile().isDownloadsDisabled());
		onlineFileCommands.setSecurityTokenIssuer(securityTokenIssuer);
		
		fileCommandsDispatcher.setCommands(onlineFileCommands);
		fileCommandsDispatcher.setPdfCommands(new PdfCommands());
		return fileCommandsDispatcher;
	}
	
	@Bean
	@Scope("prototype")
	public WikiCommandsDispatcher wikiCommandsDispatcher() {
		return new WikiCommandsDispatcher();
	}
	
	@Bean
	@Scope("prototype")
	public PersonCommandsDispatcher personCommandsDispatcher() {
		PersonCommandsDispatcher personCommandsDispatcher = new PersonCommandsDispatcher();
		personCommandsDispatcher.setRestTemplate(restTemplate());
		return personCommandsDispatcher;
	}
	
	@Bean
	@Scope("prototype")
	public VocabularyCommandsDispatcher vocabularyCommandsDispatcher() {
		VocabularyCommandsDispatcher vocabularyCommandsDispatcher = new VocabularyCommandsDispatcher();
		vocabularyCommandsDispatcher.setSimple(new SimpleVocabularyCommands());
		return vocabularyCommandsDispatcher;
	}
	
	@Bean
	@Scope("prototype")
	public GeoCommandsDispatcher geoCommandsDispatcher() {
		GeoCommandsDispatcher geoCommandsDispatcher = new GeoCommandsDispatcher();
		geoCommandsDispatcher.setSimple(new SimpleGeoCommands());
		return geoCommandsDispatcher;
	}
	
	@Bean
	@Scope("prototype")
	public GtfElementProcessor gtfElementProcessor(MainEngine mainEngine) {
		GtfElementProcessor gtfElementProcessor = new GtfElementProcessor();
		gtfElementProcessor.setMainEngine(mainEngine);
		return gtfElementProcessor;
	}

	@Bean
	@Scope("prototype")
	public GtfMappingProcessor gtfMappingProcessor(MainEngine mainEngine) {
		GtfMappingProcessor gtfMappingProcessor = new GtfMappingProcessor();
		gtfMappingProcessor.setMainEngine(mainEngine);
		return gtfMappingProcessor;
	}
	
	@Bean
	public VocabularyEngine vocabularyEngine() {
		return new VocabularyEngine();
	} 
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	@Scope("prototype")
	public XmlOutputService xmlOutputService() {
		XmlOutputService xmlOutputService = new XmlOutputService();
		xmlOutputService.setOutputBaseDirectory(mainConfig.getPaths().getTemporary());
		xmlOutputService.setExtensionNamespacePattern(Paths.get(apiConfig.getDme().getBaseUrl(), "schema/%s").toString());
		return xmlOutputService;
	}
	
	@Bean
	@Scope("prototype")
	public JsonStringOutputService jsonOutputService() {
		return new JsonStringOutputService();
	}

	
	@Bean
	@Scope("prototype")
	public CsvStringOutputService csvOutputService() {
		CsvStringOutputService csvOutputService = new CsvStringOutputService();
		//csvOutputService.setOutputBaseDirectory(mainConfig.getPaths().getTemporary());
		//csvOutputService.setExtensionNamespacePattern(Paths.get(apiConfig.getDme().getBaseUrl(), "schema/%s").toString());
		return csvOutputService;
	}
}
