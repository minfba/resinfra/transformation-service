package de.unibamberg.minf.transformation.dao.fs;

public interface LogFileDao {
	public String find(String id);
	public boolean exists(String id);
	public void delete(String id);
}
