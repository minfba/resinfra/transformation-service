package de.unibamberg.minf.transformation.config.nested;

import lombok.Data;

@Data
public class ApiServiceConfigProperties {
	private String baseUrl;
	private boolean autosync = false;
    private String cronSchedule = "0 0 */1 * * *";
    
    protected String getUrl(String path) {
    	return String.format("%s%s", baseUrl, path);
    }
}
