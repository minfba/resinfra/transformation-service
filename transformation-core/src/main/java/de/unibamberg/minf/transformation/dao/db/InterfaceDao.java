package de.unibamberg.minf.transformation.dao.db;

import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.model.Interface;

public interface InterfaceDao extends JpaRepository<Interface, Long> {

	@Transactional
	@EntityGraph(attributePaths = {"endpoint", "endpoint.params", "endpoint.fileAccessPatterns"}, type = EntityGraphType.LOAD)
	Optional<Interface> findByUniqueId(String uniqueId);
	
	@Override
	@Transactional
	@EntityGraph(attributePaths = {"endpoint", "endpoint.params", "endpoint.fileAccessPatterns", "endpoint.accessHeaders"}, type = EntityGraphType.LOAD)
	Optional<Interface> findById(Long id);
}
