package de.unibamberg.minf.transformation.model.base;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import de.unibamberg.minf.dme.model.base.BaseLongIdentifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@MappedSuperclass
public abstract class BaseEntityImpl extends BaseLongIdentifiable implements BaseEntity {
	private static final long serialVersionUID = 6495635371246677485L;
		
	@Column(name = "created", columnDefinition = "TIMESTAMP WITH TIME ZONE", nullable = false)
	private OffsetDateTime created;
	
	@Column(name = "modified", columnDefinition = "TIMESTAMP WITH TIME ZONE", nullable = false)
	private OffsetDateTime modified;
}
