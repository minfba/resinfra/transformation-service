package de.unibamberg.minf.transformation.transformation.base;

import de.unibamberg.minf.mapping.model.MappingExecGroup;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;

public interface TransformationService {
	public MappingExecGroup buildMappingExecutionGroup(ExtendedMappingContainer mc);
}
