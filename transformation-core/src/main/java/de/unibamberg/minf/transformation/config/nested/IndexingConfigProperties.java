package de.unibamberg.minf.transformation.config.nested;

import lombok.Data;

@Data
public class IndexingConfigProperties {
	private String indexNamePrefix = "gs_";
	private String indexConfigFile = "index_config.json";
}