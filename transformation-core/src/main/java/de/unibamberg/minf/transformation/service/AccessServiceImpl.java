package de.unibamberg.minf.transformation.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.dme.model.datamodel.DatamodelImpl;
import de.unibamberg.minf.transformation.dao.db.AccessDao;
import de.unibamberg.minf.transformation.dao.db.DatasourceDao;
import de.unibamberg.minf.transformation.dao.db.EndpointDao;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.base.OnlineAccessMethod;

public class AccessServiceImpl implements AccessService {

	@Autowired private DatamodelService datamodelService;
	
	@Autowired private AccessDao accessDao;
	@Autowired private EndpointDao endpointDao;
	@Autowired private DatasourceDao datasourceDao;
	
	@Override
	public List<Access> findAllAccess() {
		return accessDao.findAll();
	}

	@Override
	public List<Endpoint> findAllEndpoints() {
		return endpointDao.findAll();
	}

	@Override
	public List<Datasource> findAllDatasources() {
		return datasourceDao.findAll();
	}

	@Override
	public List<Endpoint> findAllEndpoints(boolean loadInterfaces) {
		if (loadInterfaces) {
			return endpointDao.findAndLoadAll().stream().map(it -> { 
				this.setDatamodelOnAccessMethods(it);
				return it; 
			}).collect(Collectors.toList());
		} else {
			return endpointDao.findAll();
		} 
	}

	@Override
	public List<Datasource> findAllDatasources(boolean loadDatasets) {
		if (loadDatasets) {
			return datasourceDao.findAndLoadAll().stream().map(ds -> { 
				this.setDatamodelOnAccessMethods(ds);
				return ds; 
			}).collect(Collectors.toList());
		} else {
			return datasourceDao.findAll();
		}
	}
	
	@Override
	public Optional<Datasource> findDatasourceByUniqueId(String uniqueId) {
		Optional<Datasource> datasource = datasourceDao.findByUniqueId(uniqueId);
		if (datasource.isPresent()) {
			this.setDatamodelOnAccessMethods(datasource.get());
		}
		return datasource;
	}
	
	@Override
	public Optional<Endpoint> findEndpointByUniqueId(String uniqueId) {
		Optional<Endpoint> endpoint = endpointDao.findByUniqueId(uniqueId);
		if (endpoint.isPresent()) {
			this.setDatamodelOnAccessMethods(endpoint.get());
		}
		return endpoint;
	}	
	
	@Override
	public void delete(Access access) {
		// TODO: Cleanup resources etc.
		accessDao.delete(access);
	}

	@Override
	public void deleteAll(Iterable<? extends Access> access) {
		// TODO: Cleanup resources etc.
		accessDao.deleteAll(access);
	}
	
	
	
	private void setDatamodelOnAccessMethods(Access access) {
		for (OnlineAccessMethod acm : access.getAccessMethods()) {
			if (acm.getDatamodelId()!=null) {
				ExtendedDatamodelContainer dmc = datamodelService.findById(acm.getDatamodelId());
				if (dmc==null) {
					dmc = new ExtendedDatamodelContainer();
					dmc.setId(acm.getDatamodelId());
					dmc.setDeleted(true);
					dmc.setModel(new DatamodelImpl());
					dmc.getModel().setId(acm.getDatamodelId());
				}
				acm.setDatamodel(dmc);
			}
		}
	}

	@Override
	public void saveAccess(Access access) {
		accessDao.save(access);
	}

	@Override
	public Optional<Datasource> findDatasourceById(Long id) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

	@Override
	public Optional<Endpoint> findEndpointById(Long id) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}


}