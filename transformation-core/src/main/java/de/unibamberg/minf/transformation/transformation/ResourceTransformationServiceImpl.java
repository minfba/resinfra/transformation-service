package de.unibamberg.minf.transformation.transformation;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import de.unibamberg.minf.mapping.model.MappingExecGroup;
import de.unibamberg.minf.mapping.service.MappingExecutionService;
import de.unibamberg.minf.processing.consumption.MappedResourceConsumptionService;
import de.unibamberg.minf.processing.consumption.ResourceConsumptionService;
import de.unibamberg.minf.processing.consumption.ResourceWithSourceConsumptionService;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import de.unibamberg.minf.transformation.transformation.base.BaseTransformationServiceImpl;


public class ResourceTransformationServiceImpl extends BaseTransformationServiceImpl implements ResourceTransformationService, MappedResourceConsumptionService {
	private MappingExecGroup mexGroup;
	private MappingExecutionService mExService;
	
	private ResourceConsumptionService consumptionService;

	@Value("${datamodels.presentation}")
	private String presentationModelEntityId;
	
	@Override
	public void init(ExtendedMappingContainer emc, ResourceConsumptionService consumptionService) {
		mexGroup = this.buildMappingExecutionGroup(emc);
		mExService = this.getMappingExecutionService();
		
		this.consumptionService = consumptionService;
		mExService.addConsumptionService(this);
	}
	
	@Override
	public void transformResources(List<? extends Resource> resources) {
		try {
			mExService.init(mexGroup, resources);
			mExService.run();
		} catch (ProcessingConfigException e) {
			logger.error("Failed to initialize MappingExecutionService", e);
		}
	}

	@Override
	public List<Resource> syncTransformResources(List<? extends Resource> resources) {
		CollectingResourceConsumptionService consSvc = new CollectingResourceConsumptionService();
		try {
			mExService.init(mexGroup, resources);
			mExService.addConsumptionService(consSvc);
			mExService.run();
		} catch (ProcessingConfigException e) {
			logger.error("Failed to initialize MappingExecutionService", e);
		}
		return consSvc.getResources();		
	}

	@Override
	public void init(String schemaId) throws ProcessingConfigException {
		if (this.consumptionService!=null) {
			this.consumptionService.init(schemaId);
		}
	}

	@Override
	public boolean consume(Resource obj) {
		return this.consume(obj, null);
	}

	@Override
	public int commit() {
		if (this.consumptionService!=null) {
			if (ResourceWithSourceConsumptionService.class.isAssignableFrom(this.consumptionService.getClass())) {
				return ((ResourceWithSourceConsumptionService)this.consumptionService).commit();
			} else {
				this.consumptionService.commit();
			}
		}
		return 0;
	}

	@Override
	public boolean consume(Resource rSource, Resource rTarget) {
		if (this.consumptionService!=null) {
			if (rTarget!=null & MappedResourceConsumptionService.class.isAssignableFrom(this.consumptionService.getClass())) {
				((MappedResourceConsumptionService)this.consumptionService).consume(rSource, rTarget);
			} else {
				this.consumptionService.consume(rSource);
			}
			return true;
		}
		return false;
	}
}
