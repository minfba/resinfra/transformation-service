package de.unibamberg.minf.transformation.crawling.gtf;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.util.Assert;

import com.fasterxml.jackson.databind.JsonNode;

import de.unibamberg.minf.gtf.context.ExecutionContext;
import de.unibamberg.minf.transformation.model.Crawl;
import lombok.Data;

@Data
public class CrawlingExecutionContext implements ExecutionContext {
	private final String pathPrefix;
	//private final Long collectionId;
	//private final Long endpointId;
	private final String datasetUniqueId;
	private final JsonNode sessionData;
	private final String workingDir;
		
	public CrawlingExecutionContext(String pathPrefix, String datasetUniqueId, JsonNode sessionData) throws IOException {
		Assert.notNull(pathPrefix, "Required pathPrefix not set dor CrawlingExecutionContext");
		//Assert.notNull(collectionId, "Required collectionId prefix not set dor CrawlingExecutionContext");
		//Assert.notNull(endpointId, "Required endpointId prefix not set dor CrawlingExecutionContext");
		Assert.notNull(datasetUniqueId, "Required datasetId prefix not set dor CrawlingExecutionContext");
		
		this.pathPrefix = pathPrefix;
		//this.collectionId = collectionId;
		//this.endpointId = endpointId;
		this.datasetUniqueId = datasetUniqueId;
		this.sessionData = sessionData;
		//this.workingDir = pathPrefix + File.separator + this.getCollectionId() + File.separator + this.getEndpointId() + File.separator + this.getDatasetId() + File.separator;
		this.workingDir = pathPrefix + File.separator + this.getDatasetUniqueId() + File.separator;
		
		File dir = new File(this.workingDir); 
		if (!dir.exists()) {
			FileUtils.forceMkdir(dir);
		}
	}
	public CrawlingExecutionContext(String string, Crawl c, JsonNode sessionData) throws IOException {
		this(string, c.getDataset().getUniqueId(), sessionData);
	}
}