package de.unibamberg.minf.transformation.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import de.unibamberg.minf.transformation.model.base.BaseAccessibleEntityImpl;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Base class for datasets. Implementations are OnlineDataset for datasets synchronized with an external source and 
 *  LocalDataset for data that is uploaded directly to the transformation service.
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"bags"})
@Inheritance(strategy = InheritanceType.JOINED)
@Table(uniqueConstraints={@UniqueConstraint(name="uniqueId",columnNames={"uniqueId"})})
public class Dataset extends BaseAccessibleEntityImpl {
	private static final long serialVersionUID = 2020167544174140378L;
	
	private String datamodelId;
	
	@Transient
	private ExtendedDatamodelContainer datamodel;
	
	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy="dataset", fetch = FetchType.LAZY)
	private Set<Bag> bags;
	
    @Transient
    public boolean isOnline() { return false; }
    
    @Transient
    public boolean isLocal() { return false; }
    
    @Transient
    public boolean isNocache() { return false; }
}
