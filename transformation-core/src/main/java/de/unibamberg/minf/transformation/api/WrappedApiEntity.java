package de.unibamberg.minf.transformation.api;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.unibamberg.minf.transformation.model.base.UniqueEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class WrappedApiEntity<T1 extends UniqueEntity> {
	
	private boolean add;
	private boolean update;
	private boolean delete;
	
	private T1 wrappedEntity;
	
	private List<WrappedApiEntity<? extends UniqueEntity>> wrappedSubentities = new ArrayList<>(0);
	
	public WrappedApiEntity(T1 entity) {
		this.wrappedEntity = entity;
	}
	
	public WrappedApiEntity(T1 entity, boolean add, boolean update, boolean delete) {
		this.wrappedEntity = entity;
		this.add = add;
		this.update = update;
		this.delete = delete;
	}
	
	public <T2 extends UniqueEntity> void setWrappedSubentities(List<T2> subentities) {
		if (subentities==null || subentities.isEmpty()) {
			this.wrappedSubentities = new ArrayList<>(0);
		} else {
			this.wrappedSubentities = subentities.stream()
				.map(WrappedApiEntity<T2>::new)
				.collect(Collectors.toList());
		}
	}
	
	public <T2 extends UniqueEntity> void addWrappedSubentity(WrappedApiEntity<T2> subentity) {
		this.wrappedSubentities.add(subentity);
	}
}