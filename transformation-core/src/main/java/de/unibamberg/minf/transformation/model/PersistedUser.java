package de.unibamberg.minf.transformation.model;

import java.time.OffsetDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.unibamberg.minf.transformation.model.base.UniqueEntity;
import eu.dariah.de.dariahsp.model.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = "accessTokens")
@NamedEntityGraph(attributeNodes = { 
		@NamedAttributeNode("accessTokens"),
		@NamedAttributeNode("authorities")
} )
@Table(name="persisted_user",
		uniqueConstraints={	@UniqueConstraint(name="uniqueId",columnNames={"uniqueId"}),
							@UniqueConstraint(name="unique_username_per_issuer",columnNames={"issuer", "username"})
       					  })
public class PersistedUser extends User implements UniqueEntity {
	private static final long serialVersionUID = 6275027118772457841L;

	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy="user", fetch = FetchType.LAZY)
	private Set<PersistedAccessToken> accessTokens;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String uniqueId;

	@Column(name = "created", columnDefinition = "TIMESTAMP WITH TIME ZONE", nullable = false)
	private OffsetDateTime created;
	
	@Column(name = "modified", columnDefinition = "TIMESTAMP WITH TIME ZONE", nullable = false)
	private OffsetDateTime modified;
}
