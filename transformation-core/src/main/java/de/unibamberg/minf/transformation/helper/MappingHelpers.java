package de.unibamberg.minf.transformation.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import de.unibamberg.minf.dme.model.base.Grammar;
import de.unibamberg.minf.dme.model.function.FunctionImpl;
import de.unibamberg.minf.dme.model.grammar.GrammarImpl;
import de.unibamberg.minf.dme.model.mapping.base.MappedConcept;
import de.unibamberg.minf.dme.model.mapping.base.RelatedConcept;
import de.unibamberg.minf.mapping.model.MappingExecGroup;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;

public class MappingHelpers {

	private MappingHelpers() {}

	public static MappingExecGroup buildMappingExecutionGroup(ExtendedMappingContainer m, ExtendedDatamodelContainer dcSource, ExtendedDatamodelContainer dcTarget) {
		if (m==null) {
			return null;
		}
		MappingExecGroup exec = new MappingExecGroup(m.getMapping(), null, dcTarget.getOrRenderElementHierarchy(), m.getMapping().getTargetId(), dcSource.getModel(), dcSource.getOrRenderElementHierarchy());
		for (RelatedConcept c : m.getMapping().getConcepts()) {
			if (c==null) {
				continue;
			}
			exec.addGrammars(collectGrammars(m.getId(), c.getElementGrammarIdsMap(), m.getGrammars()));
			if (MappedConcept.class.isAssignableFrom(c.getClass())) {
				MappedConcept mc = MappedConcept.class.cast(c);
				FunctionImpl f = new FunctionImpl(m.getMapping().getId(), mc.getFunctionId());
				if (m.getFunctions().containsKey(mc.getFunctionId())) {
					f.setFunction(m.getFunctions().get(mc.getFunctionId()));
				}
				exec.addRelatedConcept(mc,f);
			} else {
				exec.addRelatedConcept(c, null);
			}
		}
		return exec;
	}
	
	private static Map<String, Grammar> collectGrammars(String mappingId, Map<String, String> elementGrammarIdsMap, Map<String, Grammar> grammarIdMap) {
		Map<String, Grammar> result = new HashMap<>();
		if (elementGrammarIdsMap==null) {
			return result;
		}
		Grammar g;
		for (Entry<String, String> e : elementGrammarIdsMap.entrySet()) {
			if (elementGrammarIdsMap.get(e.getKey())!=null && grammarIdMap!=null &&
					grammarIdMap.containsKey(elementGrammarIdsMap.get(e.getKey()))) {
				g = grammarIdMap.get(elementGrammarIdsMap.get(e.getKey()));
			} else {
				g = new GrammarImpl(mappingId, e.getValue());
				g.setId(e.getValue());
				g.setPassthrough(true);
			}				
			result.put(g.getId(), g);
		}
		return result;
	}
	
}
