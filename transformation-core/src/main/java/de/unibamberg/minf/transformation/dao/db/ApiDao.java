package de.unibamberg.minf.transformation.dao.db;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.model.Api;

@Repository
public interface ApiDao extends JpaRepository<Api, Long> {
	@Transactional
	@EntityGraph(attributePaths = {"interfaces"}, type = EntityGraphType.LOAD)
	public Optional<Api> findByUniqueId(String uniqueId);

	@Query("SELECT a FROM Api a LEFT JOIN FETCH a.interfaces i WHERE i.id = ?1")
	public Collection<Api> findByInterfaceId(Long interfaceId);

	@Query("SELECT a FROM Api a LEFT JOIN FETCH a.interfaces i WHERE i.id = ?1")
	public Collection<Api> findByInterfaceIds(Collection<Long> interfaceIds);
}
