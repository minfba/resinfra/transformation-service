package de.unibamberg.minf.transformation.automation;

import java.time.OffsetDateTime;
import java.util.Optional;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.automation.base.BaseScheduledRunnable;
import de.unibamberg.minf.transformation.crawling.CrawlManager;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlCompleteFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlErrorFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlOnlineFlag;
import de.unibamberg.minf.transformation.data.service.DownloadDataService;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.OnlineDataset;
import de.unibamberg.minf.transformation.service.CollectionService;
import de.unibamberg.minf.transformation.service.CrawlService;
import de.unibamberg.minf.transformation.service.DatasetService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OnlineCrawlRunner extends BaseScheduledRunnable {
	@Autowired protected CrawlManager crawlManager;
	@Autowired protected CrawlService crawlService;
	
	@Autowired protected CollectionService collectionService;

	@Autowired private DatasetService datasetService;
	
	@Autowired private DownloadDataService downloadDataService;
	
	@Getter @Setter private boolean collectionsExpire;
	
	@Override
	public void init() {
		log.debug("Initializing automatic online crawling capabilities");
		this.checkAndCrawlOnline(false);
	}
		
	@Override
	protected void executeAutomation() {
		log.debug("Checking status of available datasets");
		this.checkAndCrawlOnline(this.isAutomationEnabled());
	}
	
	public void crawlAllOnline() {
		this.checkAndCrawlOnline(false, true);
	}
	
	public void checkAndCrawlOnline(boolean enqueueExpired) {
		this.checkAndCrawlOnline(enqueueExpired, false);
	}
	
	public void checkAndCrawlOnline(boolean enqueueExpired, boolean forceCrawlAll) {
		datasetService.findAllOnline().stream()
			.filter(ds -> ds.getDatamodelId()!=null)
			.forEach(ds -> {
				Crawl lastOnlineCrawl = crawlService.findCrawls(ds.getId(), CrawlOnlineFlag.Online, CrawlCompleteFlag.Both, CrawlErrorFlag.Both, 1).stream().findFirst().orElse(null);
				this.updateNextExecutionIfChanged(ds, lastOnlineCrawl);
				if (forceCrawlAll || (enqueueExpired && this.getIsOutdated(ds, lastOnlineCrawl))) {
					crawlManager.performOnlineCrawl(ds);
				}
			});
	}
	
	@Transactional
	public void crawlOnline(String uniqueId, boolean clearAllData) {
		try {			
			Optional<OnlineDataset> ds = datasetService.findOnlineByUniqueId(uniqueId);
			if (ds.isPresent()) {
				Crawl lastOnlineCrawl = crawlService.findCrawls(ds.get().getId(), CrawlOnlineFlag.Online, CrawlCompleteFlag.Both, CrawlErrorFlag.Both, 1).stream().findFirst().orElse(null);
				this.updateNextExecutionIfChanged(ds.get(), lastOnlineCrawl);
				if (clearAllData) {
					downloadDataService.clearDatasetData(ds.get().getUniqueId());
				}
				crawlManager.performOnlineCrawl(ds.get());
				log.info("Enqueued online crawl for dataset: {}", uniqueId);
			} else {
				log.warn("Failed to enqueue online crawl for unknown dataset: {}", uniqueId);
			}
		} catch (Exception e) {
			log.error(String.format("Failed to enqueue online crawl for dataset: %s", uniqueId), e);
			throw e;
		}		
	}

	private boolean updateNextExecutionIfChanged(OnlineDataset ds, Crawl lastOnlineCrawl) {
		NextExecution ne = this.calculateNextExecution(ds, lastOnlineCrawl);
		
		if (ds.getNextExecution()==null || !ds.getNextExecution().isEqual(ne.getNextExecutionTimestamp())) {
			ds.setNextExecution(ne.getNextExecutionTimestamp());
			log.debug("Updating next execution for online dataset {} => {}", ds.getId(), ds.getNextExecution().toString());
			
			datasetService.saveDataset(ds);
			return true;
		}
		return false;
	}
	
	private boolean getIsOutdated(OnlineDataset ds, Crawl lastOnlineCrawl) {
		if (!this.isCollectionsExpire()) {
			return false;
		}
		if (lastOnlineCrawl==null || ds.getNextExecution().isEqual(OffsetDateTime.now()) || ds.getNextExecution().isBefore(OffsetDateTime.now())) {
			return true;
		}
		return false;
	}
	
	
	private NextExecution calculateNextExecution(OnlineDataset ds, Crawl lastOnlineCrawl) {
		if (lastOnlineCrawl==null || lastOnlineCrawl.getCreated()==null || lastOnlineCrawl.getModified()==null) {
			return new NextExecution(OffsetDateTime.now());
		}
		return BaseScheduledRunnable.calculateNextExecution(ds.getDatasource().getCollection().getUpdatePeriod(), lastOnlineCrawl.getCreated(), lastOnlineCrawl.getModified());
	}
}
