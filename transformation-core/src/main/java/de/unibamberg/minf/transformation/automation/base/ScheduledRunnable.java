package de.unibamberg.minf.transformation.automation.base;

import java.util.Date;

public interface ScheduledRunnable extends Runnable {
	public void setNextExecution(Date nextExecution);
	public void setLastCompletion(Date lastCompletion);
}
