package de.unibamberg.minf.transformation.api.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.reference.Reference;
import de.unibamberg.minf.dme.model.reference.ReferenceHelper;
import de.unibamberg.minf.dme.model.serialization.DatamodelReferenceContainer;
import de.unibamberg.minf.transformation.api.client.base.ApiClient;
import de.unibamberg.minf.transformation.api.client.base.BaseDmeClient;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ModelClient extends BaseDmeClient<DatamodelReferenceContainer, DatamodelReferenceContainer> implements ApiClient {	
	
	@Getter @Setter private List<ExtendedDatamodelContainer> currentDatamodels;
	
	@Override protected String getFetchAllUrl() { return dmeConfig.getModelsUrl(); }
	@Override protected String getFetchDetailsUrl() { return dmeConfig.getModelUrl(); }
	@Override protected String getPingUrl() { return dmeConfig.getBaseUrl(); }
		
	public ModelClient() {
		super(DatamodelReferenceContainer.class, (new DatamodelReferenceContainer[0]).getClass());
	}
	
	@Override
	protected void syncEntities(DatamodelReferenceContainer[] fetchedModels) {
		// Nothing from DME -> mark everything deleted
		if (fetchedModels==null || fetchedModels.length==0) {
			this.setModelsDeleted(Optional.ofNullable(this.getCurrentDatamodels()).orElse(new ArrayList<>()).stream());
			return;
		}
		Map<String, ExtendedDatamodelContainer> outdatedDatamodelsMap = new HashMap<>(Optional.ofNullable(this.getCurrentDatamodels()).orElse(new ArrayList<>(0)).stream().collect(Collectors.toMap(ExtendedDatamodelContainer::getId, d -> d)));
		ExtendedDatamodelContainer existingDatamodel;
		for (DatamodelReferenceContainer fetchedModel : fetchedModels) {
			try {
				// New datamodel
				if (!outdatedDatamodelsMap.containsKey(fetchedModel.getModel().getId())) {
					this.importOrUpdateDatamodel(null, fetchedModel.getModel().getId());
				} else {
					existingDatamodel = outdatedDatamodelsMap.get(fetchedModel.getModel().getId());
									
					// Unchanged
					// TODO: Version id does not reflect all updates, reevaluate after fixed in DME
					// Entity id !match -> new version of collection exists, update required
					
					// TODO Test equals
					
					/*if (existingDatamodel.getModel().getVersionId().equals(fetchedModel.getModel().getVersionId())) {
						log.trace("Skipping unmodified datamodel [{}]", existingDatamodel.getModel().getVersionId());
					} else {*/
						this.importOrUpdateDatamodel(existingDatamodel, fetchedModel.getModel().getId());
					//}
					outdatedDatamodelsMap.remove(fetchedModel.getModel().getId());
				}
			} catch (ApiConsumptionException e) {
				log.error("Failed to process fetched datamodel");
			}
		}
		// Flag deleted models
		this.setModelsDeleted(outdatedDatamodelsMap.values().stream());
	}
	
	private void setModelsDeleted(Stream<ExtendedDatamodelContainer> outdatedDatamodels) {
		outdatedDatamodels.forEach(d -> { 
			d.setDeleted(true);
			log.info("Setting datamodel deleted [{}]", d.getId());
		});
	}	
	
	private void importOrUpdateDatamodel(ExtendedDatamodelContainer existingDatamodel, String datamodelId) throws ApiConsumptionException {
		ExtendedDatamodelContainer importedDatamodel = this.fetchAndConvertModel(datamodelId);
		if (importedDatamodel==null || importedDatamodel.getRoot()==null || importedDatamodel.isDeleted()) {
			if (existingDatamodel!=null) {
				existingDatamodel.setDeleted(true);
			}
		} else if (existingDatamodel==null) {
			log.info("Importing new datamodel [{}]", datamodelId);
			this.getCurrentDatamodels().add(importedDatamodel);
			this.importGrammars(datamodelId, null, importedDatamodel.getElements());
		} else {
			log.info("Updating datamodel [{}]", datamodelId);
			existingDatamodel.setDeleted(importedDatamodel.isDeleted());
			this.importGrammars(datamodelId, existingDatamodel.getElements(), importedDatamodel.getElements());
			
			existingDatamodel.setRoot(importedDatamodel.getRoot());
			existingDatamodel.setElements(importedDatamodel.getElements());
			existingDatamodel.setModel(importedDatamodel.getModel());
		}
	}
	
	private ExtendedDatamodelContainer fetchAndConvertModel(String modelId) throws ApiConsumptionException {
		DatamodelReferenceContainer mrc = this.fetchDetails(modelId);;
		ExtendedDatamodelContainer result = new ExtendedDatamodelContainer();
		result.setModel(mrc.getModel());
		result.setElements(mrc.getElements());
		result.setRoot(mrc.getRoot());
		
		if (mrc.getRoot()!=null) {
			// Consistency validation
			Reference rootElementR = ReferenceHelper.findRootElementReference(mrc.getRoot());
			if (rootElementR!=null) {
				Element rootE = (Element)ReferenceHelper.fillElement(rootElementR, mrc.getElements());
				if (rootE!=null) {
					return result;
				}
			}
		}
		log.info("Imported datamodel without resolvable elements [{}]", modelId);
		return null;
	}

}