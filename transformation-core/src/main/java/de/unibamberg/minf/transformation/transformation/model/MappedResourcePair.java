package de.unibamberg.minf.transformation.transformation.model;

import de.unibamberg.minf.processing.model.base.Resource;

public class MappedResourcePair {
	private Resource source;
	private Resource target;
	
	
	public MappedResourcePair(Resource source, Resource target) {
		this.source = source;
		this.target = target;
	}
	
	public Resource getSource() { return source; 	}
	public void setSource(Resource source) { this.source = source; 	}
	
	public Resource getTarget() { return target; }
	public void setTarget(Resource target) { this.target = target; }
}

