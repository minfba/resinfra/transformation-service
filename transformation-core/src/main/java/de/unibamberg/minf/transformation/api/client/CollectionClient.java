package de.unibamberg.minf.transformation.api.client;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;

import de.unibamberg.minf.core.web.localization.LocaleConverter;
import de.unibamberg.minf.transformation.TransformationConstants.AccessMethods;
import de.unibamberg.minf.transformation.api.WrappedApiEntity;
import de.unibamberg.minf.transformation.api.client.base.BaseApiClientImpl;
import de.unibamberg.minf.transformation.config.model.AccessChain;
import de.unibamberg.minf.transformation.config.model.AccessChain.AccessTypes;
import de.unibamberg.minf.transformation.config.model.FileProcessingChain;
import de.unibamberg.minf.transformation.config.nested.ColregConfigProperties;
import de.unibamberg.minf.transformation.crawling.CrawlHelper;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.AccessHeader;
import de.unibamberg.minf.transformation.model.AccessParam;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.Interface;
import de.unibamberg.minf.transformation.model.OnlineDataset;
import eu.dariah.de.colreg.pojo.AccessPojo;
import eu.dariah.de.colreg.pojo.AccrualPojo;
import eu.dariah.de.colreg.pojo.DatamodelPojo;
import eu.dariah.de.colreg.pojo.api.CollectionApiPojo;
import eu.dariah.de.colreg.pojo.api.ExtendedCollectionApiPojo;
import eu.dariah.de.colreg.pojo.api.results.CollectionApiResultPojo;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CollectionClient extends BaseApiClientImpl<CollectionApiPojo, ExtendedCollectionApiPojo> {
	private static final String DEFAULT_UNCLOSED_FREQUENCY_KEY = "_defaultUnclosed";
	
	@Autowired private Map<String, Period> updateFrequencyMap;
	@Autowired private List<String> knownUpdatePolicies;
	@Autowired private List<AccessChain> accessChains;
	@Autowired List<FileProcessingChain> fileProcessingChains;

	@Getter @Setter private ColregConfigProperties colregConfig;
	
	@Getter private List<WrappedApiEntity<Collection>> collections;
	
	@Getter @Setter private String oaiDcModelId;
	
	@Override protected String getFetchAllUrl() { return colregConfig.getFetchAllUrl(); }
	@Override protected String getFetchDetailsUrl() { return colregConfig.getFetchDetailUrl(); }
	@Override protected String getPingUrl() { return colregConfig.getBaseUrl(); }
	
	public CollectionClient() {
		super(ExtendedCollectionApiPojo.class, (new CollectionApiPojo[0]).getClass());
	}
	
	public void setCurrentCollections(List<Collection> collections) {
		this.collections = collections.stream().map(WrappedApiEntity<Collection>::new).collect(Collectors.toList());
	}
	
	@Override
	protected ExtendedCollectionApiPojo parseDetails(String jsonString) throws JsonProcessingException {
		CollectionApiResultPojo<ExtendedCollectionApiPojo> rPojo = apiObjectMapper.readValue(jsonString, new TypeReference<CollectionApiResultPojo<ExtendedCollectionApiPojo>>() {});
		if (rPojo.getContent()!=null && !rPojo.getContent().isEmpty()) {
			return rPojo.getContent().iterator().next();
		}
		return null;
	}
	
	@Override
	protected CollectionApiPojo[] parse(String jsonString) throws JsonProcessingException {
		CollectionApiResultPojo<CollectionApiPojo> rPojo = apiObjectMapper.readValue(jsonString, new TypeReference<CollectionApiResultPojo<CollectionApiPojo>>() {});
		return rPojo.getContent().toArray(new CollectionApiPojo[0]);
	}
	
	@Override
	protected void syncEntities(CollectionApiPojo[] fetchedCollections) {
		// Nothing from CR -> mark everything deleted
		if (fetchedCollections==null || fetchedCollections.length==0) {
			this.setCollectionsDeleted(this.getCollections());
			return;
		}
		Map<String, WrappedApiEntity<Collection>> outdatedCollectionsMap = new HashMap<>(this.getCollections().stream().collect(Collectors.toMap(c -> c.getWrappedEntity().getColregEntityId(), c -> c)));
		WrappedApiEntity<Collection> existingCollection;
		for (CollectionApiPojo fetchedCollection : fetchedCollections) {
			try {
				// New collection
				if (!outdatedCollectionsMap.containsKey(fetchedCollection.getId())) {
					this.importOrUpdateCollection(null, fetchedCollection.getId());
				} else {
					existingCollection = outdatedCollectionsMap.get(fetchedCollection.getId());
					// Unchanged
					if (existingCollection.getWrappedEntity().getColregVersionId().equals(fetchedCollection.getVersionId())) {
						log.trace("Skipping unmodified collection [{}]", fetchedCollection.getId());
					} else {
						this.importOrUpdateCollection(existingCollection, fetchedCollection.getId());
					}
					outdatedCollectionsMap.remove(fetchedCollection.getId());
				}	
			} catch (ApiConsumptionException e) {
				log.error("Failed to process fetched collection");
			}
		}
		// Flag deleted collections
		this.setCollectionsDeleted(outdatedCollectionsMap.values());
	}
	
	private void setCollectionsDeleted(java.util.Collection<WrappedApiEntity<Collection>> outdatedCollections) {
		outdatedCollections.stream().forEach(c -> { 
			c.setDelete(true);
			log.info("Setting collection deleted [{}]", c.getWrappedEntity().getColregEntityId());
		});
	}
	
	private void importOrUpdateCollection(WrappedApiEntity<Collection> currentCollection, String foreignEntityId) throws ApiConsumptionException {	
		Collection importedCollection = this.fetchAndConvertCollection(foreignEntityId);
		if (importedCollection==null) {
			log.warn("Failed to fetch details of collection [{}]", foreignEntityId);
			return;
		}		
		
		// No endpoints at all or no usable endpoint -> skip import / mark existing as deleted
		if (!this.hasUsableAccess(importedCollection.getAccess())) {
			this.handleCollectionWithoutAccess(currentCollection, foreignEntityId);
			return;
		}
		
		if (currentCollection==null) {
			log.info("Importing new collection [{}]", foreignEntityId);
			this.importNewCollection(importedCollection);
		} else {
			log.info("Updating collection [{}]", foreignEntityId);
			this.updateCollectionData(currentCollection, importedCollection);
		}
	}
	
	private void importNewCollection(Collection importedCollection) {	
		WrappedApiEntity<Collection> wc = new WrappedApiEntity<>(importedCollection);
		wc.setAdd(true);
		this.getCollections().add(wc);
		
		importedCollection.getAccess().stream().forEach(ac -> this.importNewAccess(wc, ac));	
	}
	
	private void handleCollectionWithoutAccess(WrappedApiEntity<Collection> currentCollection, String foreignEntityId) {
		if (currentCollection==null) {
			log.debug("Skipping import of collection [{}]: no supported endpoints", foreignEntityId);
		} else {
			log.debug("Flagging collection as deleted [{}]: no supported endpoints", foreignEntityId);
			currentCollection.setDelete(true);
		}
	}
	
	private boolean hasUsableAccess(Set<Access> access) {
		if (access==null) {
			return false;
		}
		access.removeIf(ac -> accessChains.stream().noneMatch(ach -> ach.getKey().equals(ac.getAccessType())));
		return !access.isEmpty();
	}
	
	
	private void updateCollectionData(WrappedApiEntity<Collection> wrappedCollection, Collection cImported) {
		/* Overwrite only fields that really come from CR, not those that are altered in GS
		 *  Checks if/which fields were updated are irrelevant, as the collection metadata (the whole collection description)
		 *  is at least updated in the versionId and modified fields and any part of the collection metadata can be used in -
		 *  and thus influence data processing 
		 */  
		Collection cCurrent = wrappedCollection.getWrappedEntity();
		
		cCurrent.setColregVersionId(cImported.getColregVersionId());
		cCurrent.setImageUrl(cImported.getImageUrl());
		cCurrent.setNames(cImported.getNames());
		cCurrent.setModified(cImported.getModified());
		cCurrent.setUpdatePeriod(cImported.getUpdatePeriod());
		cCurrent.setCollectionMetadata(cImported.getCollectionMetadata());
		
		wrappedCollection.setUpdate(true);
		
		this.mergeAccess(wrappedCollection, cImported.getAccess());
	}
	
	
	private void mergeAccess(WrappedApiEntity<Collection> c, Set<Access> importedAccess) {
		Set<Access> currentAccess = Optional.ofNullable(c.getWrappedEntity().getAccess()).orElse(new HashSet<>());
				
		// Deletions and updates
		Access acImported;
		for (Access acCurrent : currentAccess) {
			acImported = importedAccess.stream().filter(ac -> this.accessAreSame(acCurrent, ac)).findFirst().orElse(null);
			if (acImported==null) {
				c.addWrappedSubentity(new WrappedApiEntity<>(acCurrent, false, false, true));
			} else {
				WrappedApiEntity<Access> wAccess = new WrappedApiEntity<>(acCurrent, false, true, false);
				this.mergeAccessData(wAccess, acImported);
				importedAccess.remove(acImported);
				c.addWrappedSubentity(wAccess);
			}
		}
		
		// All remaining in list are new
		importedAccess.stream().forEach(ac -> this.importNewAccess(c, ac));
	}
	
	private void importNewAccess(WrappedApiEntity<Collection> c, Access ac) {
		WrappedApiEntity<Access> wAccess = new WrappedApiEntity<>(ac, true, false, false);
		if (ac.isDatasource()) {
			((Datasource)ac).getDatasets().stream().forEach(ds -> wAccess.addWrappedSubentity(new WrappedApiEntity<>(ds, true, false, false)));	
		} else {
			((Endpoint)ac).getInterfaces().stream().forEach(in -> wAccess.addWrappedSubentity(new WrappedApiEntity<>(in, true, false, false)));
		}
		c.addWrappedSubentity(wAccess);
	}
	
	
	private void mergeAccessData(WrappedApiEntity<Access> wacCurrent, Access acImported) {
		Access acCurrent = wacCurrent.getWrappedEntity(); 
		acCurrent.setAccessModelId(acImported.getAccessModelId());
		acCurrent.setFileAccessPatterns(acImported.getFileAccessPatterns());
		acCurrent.setParams(acImported.getParams());
		if (acCurrent.getParams()!=null) {
			acCurrent.getParams().forEach(p -> p.setAccess(acCurrent));
		}
		acCurrent.setAccessHeaders(acImported.getAccessHeaders());
		if (acCurrent.getAccessHeaders()!=null) {
			acCurrent.getAccessHeaders().forEach(h -> h.setAccess(acCurrent));
		}
		
		if (acCurrent.isDatasource()) {
			this.mergeDatasets(wacCurrent, (Datasource)acImported);
		}
		if (acCurrent.isEndpoint()) {
			this.mergeInterfaces(wacCurrent, (Endpoint)acImported);
		}
	}
	
	
	
	private void mergeDatasets(WrappedApiEntity<Access> mergeSource, Datasource importedSource) {
		Set<OnlineDataset> importedDatasets = new HashSet<>(importedSource.getDatasets());
		
		// Deletions and updates
		OnlineDataset dsImported;
		for (OnlineDataset dsCurrent : ((Datasource)mergeSource.getWrappedEntity()).getDatasets()) {
			dsImported = importedDatasets.stream().filter(ds -> ds.getDatamodelId().equals(dsCurrent.getDatamodelId())).findFirst().orElse(null);
			if (dsImported==null) {
				mergeSource.addWrappedSubentity(new WrappedApiEntity<>(dsCurrent, false, false, true));
			} else {
				dsCurrent.setRemoteAlias(dsImported.getRemoteAlias());
				mergeSource.addWrappedSubentity(new WrappedApiEntity<>(dsCurrent, false, true, false));
				importedDatasets.remove(dsImported);
			}
		}
		// All remaining in list are new
		importedDatasets.stream().forEach(ds -> mergeSource.addWrappedSubentity(new WrappedApiEntity<>(ds, true, false, false)));
	}
	
	private void mergeInterfaces(WrappedApiEntity<Access> mergeEndpoint, Endpoint importedEndpoint) {
		Set<Interface> importedInterfaces = new HashSet<>(importedEndpoint.getInterfaces());
		
		// Deletions and updates
		Interface inImported;
		for (Interface inCurrent : ((Endpoint)mergeEndpoint.getWrappedEntity()).getInterfaces()) {
			inImported = importedInterfaces.stream().filter(ds -> ds.getDatamodelId().equals(inCurrent.getDatamodelId())).findFirst().orElse(null);
			if (inImported==null) {
				mergeEndpoint.addWrappedSubentity(new WrappedApiEntity<>(inCurrent, false, false, true));
			} else {
				mergeEndpoint.addWrappedSubentity(new WrappedApiEntity<>(inCurrent, false, true, false));
				importedInterfaces.remove(inImported);
			}
		}
		// All remaining in list are new
		importedInterfaces.stream().forEach(in -> mergeEndpoint.addWrappedSubentity(new WrappedApiEntity<>(in, true, false, false)));
	}
	
	private Collection fetchAndConvertCollection(String foreignEntityId) throws ApiConsumptionException {
		if (foreignEntityId==null) {
			return null;
		}
		
		ExtendedCollectionApiPojo fetchedCollection = this.fetchDetails(foreignEntityId);
		Collection convertedCollection = new Collection();
		
		try {
			convertedCollection.setCollectionMetadata(apiObjectMapper.writeValueAsString(fetchedCollection));
		} catch (JsonProcessingException e) {
			log.error("Failed to JSON serialized collection metadata");
		}
		
		convertedCollection.setNames(this.getRelevantTitles(fetchedCollection.getTitles()));
		convertedCollection.setUpdatePeriod(this.getUpdatePeriod(fetchedCollection.getAccrualPojos()));
		
		if (fetchedCollection.getPrimaryImage()!=null) {
			convertedCollection.setImageUrl(fetchedCollection.getPrimaryImage().getImageUrl());
		}
		convertedCollection.setColregEntityId(fetchedCollection.getId());
		convertedCollection.setColregVersionId(fetchedCollection.getVersionId());
		convertedCollection.setCreated(OffsetDateTime.now());
		convertedCollection.setModified(OffsetDateTime.now());
				
		convertedCollection.setAccess(new HashSet<>());
		if (fetchedCollection.getAccessPojos()!=null) {
			for (AccessPojo accessPojo : fetchedCollection.getAccessPojos()) {
				this.convertAndAddAccess(convertedCollection, accessPojo, convertedCollection.getAccess());
			}
		}
		return convertedCollection;
	}
	
	private Map<String, String> getRelevantTitles(Map<String, String> titles) {
		if (titles==null) {
			return new HashMap<>();
		}
		Map<String, String> relevantTitles = new HashMap<>();	
		String languageCode;
		for (Entry<String, String> entry : titles.entrySet()) {
			languageCode = LocaleConverter.getLanguageForIso3Code(entry.getKey());
			if (languageCode!=null) {
				relevantTitles.put(languageCode, entry.getValue());
			}
		}
		return relevantTitles;
	}
	
	private String getUpdatePeriod(List<AccrualPojo> accrualPojos) {
		if (this.knownUpdatePolicies==null || accrualPojos==null) {
			return null;
		}
		DateTime compDate = DateTime.now();
		Period shortestPeriod = null;
		Period updatePeriod;		
		for (AccrualPojo accrual : accrualPojos) {
			if (this.knownUpdatePolicies.contains(accrual.getAccrualPolicy())) {
				updatePeriod = this.updateFrequencyMap.get(accrual.getAccrualPeriodicity());
				if (updatePeriod==null) {
					updatePeriod = this.updateFrequencyMap.get(DEFAULT_UNCLOSED_FREQUENCY_KEY);
				}
				if (shortestPeriod==null || shortestPeriod.toDurationFrom(compDate).isLongerThan(updatePeriod.toDurationFrom(compDate))) {
					shortestPeriod = updatePeriod;
				}
			}
		}
		return shortestPeriod!=null ? shortestPeriod.toString() : null;		
	}
	
	private void convertAndAddAccess(Collection collection, AccessPojo accessPojo, Set<Access> existingAccess) {
		if (accessPojo==null) {
			return;
		}
		if (accessChains.stream().filter(ac -> ac.getAccessType().equals(AccessTypes.DATASOURCE)).anyMatch(ac -> ac.getKey().equals(accessPojo.getType()))) {
			this.addOrUpdateAccess(existingAccess, this.convertAccessPojoToDatasource(collection, accessPojo));
		}
		if (accessChains.stream().filter(ac -> ac.getAccessType().equals(AccessTypes.ENDPOINT)).anyMatch(ac -> ac.getKey().equals(accessPojo.getType()))) {
			this.addOrUpdateAccess(existingAccess, this.convertAccessPojoToEndpoint(collection, accessPojo));
		}
	}
	
	private Datasource convertAccessPojoToDatasource(Collection c, AccessPojo accessPojo) {
		Datasource ds = new Datasource();
		this.fillAccessPojoToAccess(c, ds, accessPojo);
		ds.setDatasets(this.convertDatamodelsToDatasets(ds, accessPojo.getDatamodels(), accessPojo.getType()));
		return ds;
	}
	
	private Endpoint convertAccessPojoToEndpoint(Collection c, AccessPojo accessPojo) {
		Endpoint ep = new Endpoint();
		this.fillAccessPojoToAccess(c, ep, accessPojo);
		ep.setInterfaces(this.convertDatamodelsToInterfaces(ep, accessPojo.getDatamodels(), accessPojo.getType()));
		return ep;
	}
	
	private void fillAccessPojoToAccess(Collection c, Access e, AccessPojo accessPojo) {		
		e.setAccessType(accessPojo.getType());
		e.setHttpMethod(accessPojo.getMethod());
		e.setFileType(this.getFileType(accessPojo.getType(), accessPojo.getSubtype()));
		e.setParams(Optional.ofNullable(accessPojo.getParams()).orElse(new ArrayList<>()).stream().map(ap -> new AccessParam(ap.getKey(), ap.getValue(), e)).collect(Collectors.toSet()));
		e.setAccessHeaders(Optional.ofNullable(accessPojo.getHeaders()).orElse(new ArrayList<>()).stream().map(ah -> new AccessHeader(ah.getKey(), ah.getValue(), e)).collect(Collectors.toSet()));
		e.setAccessModelId(accessPojo.getAccessModelId());
		e.setUrl(accessPojo.getUri());
		e.setFileAccessPatterns(accessPojo.getPatterns()==null ? null : new HashSet<>(accessPojo.getPatterns()));
		e.setCollection(c);
	}
	
	private Set<Interface> convertDatamodelsToInterfaces(Endpoint endpoint, List<DatamodelPojo> datamodels, String accessType) {
		if (datamodels==null) {
			return new HashSet<>(0);
		}
		return datamodels.stream().map(dm -> this.createInterface(endpoint, dm.getId())).collect(Collectors.toSet());
	}
	
	private Set<OnlineDataset> convertDatamodelsToDatasets(Datasource source, List<DatamodelPojo> datamodels, String accessType) {
		if (datamodels!=null) {
			return datamodels.stream().map(dm -> this.createDataset(source, dm.getId(), dm.getAlias())).collect(Collectors.toSet());
		} else if (accessType.equals(AccessMethods.OAI_PMH.toString())) {
			if (oaiDcModelId==null) {
				log.warn("Unable to create default oai_dc dataset on OAI-PMH dataset because oaiDcDatamodel is not configured");
			} else {
				Set<OnlineDataset> datasets = new HashSet<>();
				datasets.add(createDataset(source, oaiDcModelId, null));
				return datasets;
			}
		}
		return new HashSet<>(0); 
	}
	
	private OnlineDataset createDataset(Datasource source, String datamodelId, String datamodelAlias) {
		OnlineDataset ds = new OnlineDataset();
		ds.setDatasource(source);
		ds.setDatamodelId(datamodelId);
		ds.setRemoteAlias(datamodelAlias);
		return ds;
	}
	
	private Interface createInterface(Endpoint endpoint, String datamodelId) {
		Interface in = new Interface();
		in.setEndpoint(endpoint);
		in.setDatamodelId(datamodelId);
		return in;
	}
	
	private String getFileType(String accessType, String accessSubtype) {
		if (accessSubtype!=null) {
			return accessSubtype;
		} else {
			if (accessType.equals(AccessMethods.GIT.toString())) {
				return "TEXT";
			} else {
				return "XML";
			}
		}
	}
		
	private void addOrUpdateAccess(Set<Access> existingAccess, Access access) {
		// If type, url and set match, it is one endpoint for search
		Optional<Access> aExisting = existingAccess.stream().filter(a -> this.accessAreSame(a, access)).findAny();
		if (aExisting.isEmpty()) {
			// No match -> add to endpoints
			existingAccess.add(access);
		} else {
			if (access.isDatasource()) {
				for (OnlineDataset dsMerge : ((Datasource)access).getDatasets()) {
					if (((Datasource)aExisting.get()).getDatasets().stream().filter(ds -> ds.getId().equals(dsMerge.getId())).findAny().isEmpty()) {
						((Datasource)aExisting.get()).getDatasets().add(dsMerge);
					}
				}
			} else if (access.isEndpoint()) {
				for (Interface inMerge : ((Endpoint)access).getInterfaces()) {
					if (((Endpoint)aExisting.get()).getInterfaces().stream().filter(ds -> ds.getId().equals(inMerge.getId())).findAny().isEmpty()) {
						((Endpoint)aExisting.get()).getInterfaces().add(inMerge);
					}
				}
			}
		}		
	}
	
	private boolean accessAreSame(Access ac1, Access ac2) {
		try {
			return ac1.getClass().equals(ac2.getClass()) && 
						// TODO: Problem with multiple access definitions ~ same URL, different e.g. method					
						//ac1.getAccessType().equals(ac2.getAccessType()) &&
						//ac1.getFileType().equals(ac2.getFileType()) &&
						CrawlHelper.renderAccessUrl(ac1).equals(CrawlHelper.renderAccessUrl(ac2));
		} catch (Exception e) {
			log.error("Failed to compare access", e);
			return false;
		}
	}
	
}