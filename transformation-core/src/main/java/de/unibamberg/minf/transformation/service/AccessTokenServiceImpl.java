package de.unibamberg.minf.transformation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.unibamberg.minf.transformation.dao.db.PersistedAccessTokenDao;
import de.unibamberg.minf.transformation.model.PersistedAccessToken;

@Service
public class AccessTokenServiceImpl implements AccessTokenService {
	@Autowired private PersistedAccessTokenDao accessTokenDao;
	
	@Override
	public void saveAccessToken(PersistedAccessToken persistedAccessToken) {
		accessTokenDao.save(persistedAccessToken);
	}

	@Override
	public List<PersistedAccessToken> findByUserId(Long userId) {
		return accessTokenDao.findByUserId(userId);
	}

	@Override
	public PersistedAccessToken findByUniqueIdAndUserId(String uniqueId, Long userId) {
		return accessTokenDao.findByUniqueIdAndUserId(uniqueId, userId);
	}

	@Override
	public void deleteById(Long id) {
		accessTokenDao.deleteById(id);
	}

	@Override
	public Optional<PersistedAccessToken> findByUniqueId(String uniqueId) {
		return accessTokenDao.findByUniqueId(uniqueId);
	}
}
