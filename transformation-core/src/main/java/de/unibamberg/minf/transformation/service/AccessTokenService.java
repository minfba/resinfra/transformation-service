package de.unibamberg.minf.transformation.service;

import java.util.List;
import java.util.Optional;

import de.unibamberg.minf.transformation.model.PersistedAccessToken;

public interface AccessTokenService {
	public void saveAccessToken(PersistedAccessToken accessToken);
	public List<PersistedAccessToken> findByUserId(Long userId);
	public PersistedAccessToken findByUniqueIdAndUserId(String uniqueId, Long userId);
	public Optional<PersistedAccessToken> findByUniqueId(String uniqueId);
	public void deleteById(Long id);
}
