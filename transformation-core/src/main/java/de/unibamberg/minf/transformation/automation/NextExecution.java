package de.unibamberg.minf.transformation.automation;

import java.time.OffsetDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NextExecution {
	public enum CALCULATION_METHODS { STATIC, DURATION_BASED, POLICY_BASED, DURATION_AND_POLICY_BASED }
	
	private final CALCULATION_METHODS calculationMethod;
	private final int policyInDays;
	private final double lastDurationInMinutes;
	private final double durationBasedMinAge;
	private final double durationBasedMaxAge;
	private final double calculatedTargetAge;
	private final OffsetDateTime nextExecutionTimestamp;
	
	
	public NextExecution(OffsetDateTime nextExecutionTimestamp) {
		this.nextExecutionTimestamp = nextExecutionTimestamp;
		this.calculationMethod = CALCULATION_METHODS.STATIC;
		this.policyInDays = -1;
		this.lastDurationInMinutes = Double.NaN;
		this.durationBasedMinAge = Double.NaN;
		this.durationBasedMaxAge = Double.NaN;
		this.calculatedTargetAge = Double.NaN;
		
	}
	
	public boolean isOverdue() {
		return nextExecutionTimestamp!=null && this.getReferenceTimestamp().isAfter(nextExecutionTimestamp);
	}
	
	public boolean isCalculated() {
		return !Double.isNaN(calculatedTargetAge) && nextExecutionTimestamp!=null;
	}
	
	public OffsetDateTime getReferenceTimestamp() {
		return OffsetDateTime.now();
	}
}