package de.unibamberg.minf.transformation.dao.fs;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.InitializingBean;

import de.unibamberg.minf.transformation.model.Crawl;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CrawlDataDaoImpl extends BaseFsDao implements CrawlDataDao, InitializingBean {
	private File crawlsDir;
		
	@Override
	public void afterPropertiesSet() throws Exception {
		this.crawlsDir = new File(getCrawlsPath());
		if (!crawlsDir.exists()) {
			FileUtils.forceMkdir(this.crawlsDir);
			log.info("Created crawls base directory: {}", getCrawlsPath());
		}
	}

	@Override
	public String getCrawlDirPath(Long crawlId) {
		return this.crawlsDir.getAbsolutePath() + File.separator + crawlId;
	}
	
	@Override
	public void delete(Crawl c) throws IOException {
		File cDir = new File(this.getCrawlDirPath(c.getId()));
		if (cDir.exists()) {
			if (cDir.isDirectory()) {
				FileUtils.deleteDirectory(cDir);
			} else {
				Files.delete(Paths.get(cDir.getAbsolutePath()));
			}
		}
	}
	
	@Override
	public boolean existsAndHasData(Crawl c) {
		File cDir = new File(this.getCrawlDirPath(c.getId()));
		if (cDir.exists() && cDir.isDirectory() && cDir.listFiles().length>0) {
			return true;
		}		
		return false;
	}

	@Override
	public FileCountAndSize getFilesAndSize(Crawl c) {
		FileCountAndSize fileCountAndSize = new FileCountAndSize();
		File cDir = new File(this.getCrawlDirPath(c.getId()));
		int files = 0;
		long size = 0;
		if (cDir.exists() && cDir.isDirectory()) {
			if (cDir.listFiles().length>0) {
				for (File f : cDir.listFiles()) {
					if (f.isDirectory()) {
						continue;
					}
					files++;
					size += FileUtils.sizeOf(f);
				}
			}
		}
		fileCountAndSize.setSize(getHumanReadableFileSize(size, true));
		fileCountAndSize.setFiles(files);
		
		return fileCountAndSize;
	}
	
	public static String getHumanReadableFileSize(long sizeOfDirectory, boolean b) {
		int unit = b ? 1000 : 1024;
	    if (sizeOfDirectory < unit) return sizeOfDirectory + " B";
	    int exp = (int) (Math.log(sizeOfDirectory) / Math.log(unit));
	    String pre = (b ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (b ? "" : "i");
	    return String.format("%.1f %sB", sizeOfDirectory / Math.pow(unit, exp), pre);
	}
}
