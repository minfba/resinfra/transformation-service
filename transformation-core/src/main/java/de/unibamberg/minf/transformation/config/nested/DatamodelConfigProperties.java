package de.unibamberg.minf.transformation.config.nested;

import java.util.List;

import lombok.Data;

@Data
public class DatamodelConfigProperties {
	private String integration;
	private String presentation;
	private String indexing;
	private String metadata;
	private String crawling;
	private String oaidcModel;
	private List<String> modelsWithMessageCodes;
	private String modelsMessageCodePrefix;
}