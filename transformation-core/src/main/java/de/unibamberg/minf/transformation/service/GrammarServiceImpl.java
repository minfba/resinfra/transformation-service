package de.unibamberg.minf.transformation.service;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.dme.model.grammar.GrammarContainer;
import de.unibamberg.minf.dme.model.grammar.GrammarImpl;
import de.unibamberg.minf.gtf.MainEngine;
import de.unibamberg.minf.gtf.exceptions.GrammarProcessingException;
import de.unibamberg.minf.transformation.dao.fs.GrammarDao;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GrammarServiceImpl implements GrammarService {
	protected static final Logger logger = LoggerFactory.getLogger(GrammarServiceImpl.class);
	
	@Autowired private GrammarDao grammarDao;
	@Autowired private MainEngine engine;

	@Override
	public void importGrammarContainers(Map<String, GrammarContainer> grammars) {
		if (grammars==null) {
			return;
		}
		grammars.entrySet().stream()
			.filter(e -> e.getValue()!=null)
			.forEach(e -> this.importGrammarContainer(e.getKey(), e.getValue()));
	}
		
	private void importGrammarContainer(String grammarId, GrammarContainer gc) {
		try {
			this.clearGrammar(grammarId);			
			gc.setId(GrammarImpl.PERSISTENT_IDENTIFIER_PREFIX + grammarId);
			this.saveGrammar(gc);
		} catch (Exception e) {
			log.error("Failed to import grammar container", e);
		}
	}
		
	@Override
	public void clearGrammar(String grammarId) {
		engine.getDescriptionEngine().unloadGrammar(grammarId);
		try {
			grammarDao.deleteGrammar(grammarId);
		} catch (IOException e) {
			logger.error("Failed to delete grammar files from filesystem. Please attempt to remove manually.", e);
		}
	}
	
	@Override
	public void saveGrammar(GrammarContainer gc) {	
		try {
			if (gc==null) {
				return;
			}
			if (grammarDao.saveGrammar(gc)) {
				try {
					grammarDao.compileGrammar(gc);
				} catch (GrammarProcessingException e) {
					logger.error(String.format("Failed to compile grammar [%s]", gc.getId()), e);
				}
			}
		} catch (IOException e) {
			logger.error(String.format("Failed to save grammar [%s]", gc.getId()), e);
		}
	}


}
