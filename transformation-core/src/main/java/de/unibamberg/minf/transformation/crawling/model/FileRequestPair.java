package de.unibamberg.minf.transformation.crawling.model;

import lombok.Data;

@Data
public class FileRequestPair {
	final String param;
	final String value;
}