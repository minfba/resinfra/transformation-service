package de.unibamberg.minf.transformation.crawling.files;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;

import static java.net.http.HttpRequest.BodyPublishers.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import de.uniba.minf.core.rest.client.security.SecurityToken;
import de.uniba.minf.core.rest.client.security.SecurityTokenIssuer;
import de.unibamberg.minf.transformation.crawling.CrawlHelper;
import de.unibamberg.minf.transformation.crawling.crawler.Crawler;
import de.unibamberg.minf.transformation.crawling.model.FileRequestPair;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.base.AccessOperation;
import lombok.Getter;
import lombok.Setter;

public class FileDownloader extends BaseFileStreamCrawler implements Crawler {
	private static final int CHUNK_SIZE = 1048576;
	
	@Getter @Setter protected URI downloadURI;
	@Getter @Setter protected String method = "GET";
	@Getter @Setter protected String format = "XML";
	@Getter @Setter protected byte[] requestBody = null;
	@Getter @Setter protected List<FileRequestPair> requestHeaders = null;
	
	@Getter protected boolean initialized = false;

	@Autowired private SecurityTokenIssuer securityTokenIssuer;
	
	@Override public List<String> getInputFilenames() { return new ArrayList<>(); }


	@Override
	public String getUnitMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.file.downloader.unit";
	}

	@Override
	public String getTitleMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.file.downloader.title";
	}
	
	@Override
	public void init(Access access, AccessOperation op, ExtendedDatamodelContainer sc) {
		super.init(access, op, sc);
		try {
			this.setupPaths(op);
			this.downloadURI = new URL(CrawlHelper.renderAccessUrl(access)).toURI();
			this.initialized = true;
		} catch (IOException | URISyntaxException e) {
			logger.error("File downloader initialization failed: " + e.getMessage(), e);
		}
	}
	
	@Override
	public void run() {
		super.run();
		this.registerStart();
		this.download();
	}
	
	
	protected void download() {
		if (this.getDownloadURI() == null) {
			logger.error("Either download URL or fileName was not specified or both.");
			return;
		}
		if (this.setupOutputFile()) {
			this.downloadFile();
		}
	}
	
	protected void downloadFile() {
		String outputPath = this.getDirPath() + this.getUUIDForOutputFilename();
		
		try {
		    HttpRequest.Builder reqBlrd = HttpRequest.newBuilder();
			
		    URIBuilder uriBuilder = new URIBuilder(this.getDownloadURI());

		    if (securityTokenIssuer!=null) {
				Optional<SecurityToken> t = securityTokenIssuer.getSecurityToken(this.getDownloadURI().toString());
				if (t.isPresent()) {
					if (t.get().isHeaderToken()) {
						reqBlrd.header(t.get().getName(), t.get().getValue());
					} else {
						uriBuilder.addParameter(t.get().getName(), t.get().getValue());
					}
				}
			}
		    
		    reqBlrd.uri(uriBuilder.build());
			
			if (this.format.equals("JSON")) {
				reqBlrd.header("Content-Type", "application/json");
			} else if (this.format.equals("XML")) {
				reqBlrd.header("Content-Type", "application/xml");
			} else {
				reqBlrd.header("Content-Type", "text/plain");
			}
			
			if (this.requestHeaders != null && !this.requestHeaders.isEmpty()) {
				this.requestHeaders.stream().forEach(h -> reqBlrd.header("Content-Type", h.getParam()));
			}

			if (this.method.equals("POST")) {
				reqBlrd.POST(this.requestBody != null && this.requestBody.length > 0 ? ofByteArray(this.requestBody) : noBody());
			} else {
				reqBlrd.GET();
			}
			
			reqBlrd.setHeader(method, format);
			
			HttpRequest req = reqBlrd.build();
			
			if (logger.isDebugEnabled()) {
				logger.debug("Calling interface: {}", req.toString());
			}

			HttpClient.newBuilder()
				.followRedirects(HttpClient.Redirect.ALWAYS).build()
				.send(req, BodyHandlers.ofFile(Paths.get(outputPath)));

			this.addOutputFilename(outputPath);
			
			this.registerFileFinished();
		} catch (IOException | URISyntaxException e) {
			logger.error(String.format("Failed to download file from [%s] to [%s]", this.getDownloadURI().getPath(), outputPath, e));
			this.registerFileError();
		} catch (InterruptedException e) {
			logger.error("File download interrupted", e);
			this.registerFileError();
			Thread.currentThread().interrupt();
		}
	}

	protected void registerStart() {
		if (this.getListener() != null) {
			this.getListener().start(this.getUuid());
		}
	}
	
	protected void updateFileSize(long size) {
		if (this.getListener() != null) {
    		this.getListener().updateSize(this.getUuid(), size);
		}
	}
	
	protected void updateFileProcessed(long position) {
		if (this.getListener() != null) {
    		this.getListener().processed(this.getUuid(), position);
		}
	}
	
	protected void registerFileFinished() {
		if (this.getListener() != null) {
			this.getListener().finished(this.getUuid());
		}
	}
	
	protected void registerFileError() {
		if (this.getListener() != null) {
			this.getListener().error(this.getUuid());
		}
	}
	
	protected void detectAndUpdateFileSize() {
		try {
			HttpURLConnection httpConnection = (HttpURLConnection)(this.getDownloadURI().toURL().openConnection());
			long size = httpConnection.getContentLengthLong();
			if (size>0) {
				this.updateFileSize(size);
			}
		} catch (Exception e) { 
			logger.debug("Failed to determine file download size: {}", e.getMessage());
		}
	}
	
	private String getUUIDForOutputFilename() {
		return UUID.randomUUID().toString();
	}
	
	private boolean setupOutputFile() {
		Path outputDir = Paths.get(this.getDirPath());
		try {
			if (Files.exists(outputDir)) {
				Files.createDirectories(outputDir);
			}
			return true;
		} catch (IOException e) {
			logger.error("Failed to setup paths for FileDownloader", e);
			return false;
		}
	}
}