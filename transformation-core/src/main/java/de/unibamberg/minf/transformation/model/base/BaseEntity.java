package de.unibamberg.minf.transformation.model.base;

import java.time.OffsetDateTime;

import de.unibamberg.minf.dme.model.base.LongIdentifiable;

public interface BaseEntity extends LongIdentifiable {
	public OffsetDateTime getCreated();
	public void setCreated(OffsetDateTime created);
	
	public OffsetDateTime getModified();
	public void setModified(OffsetDateTime modified);
}
