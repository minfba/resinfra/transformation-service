package de.unibamberg.minf.transformation.model;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.unibamberg.minf.dme.model.base.Identifiable;
import de.unibamberg.minf.dme.model.serialization.MappingContainer;

public class ExtendedMappingContainer extends MappingContainer implements Identifiable {
	private static final long serialVersionUID = 4221400658668843428L;
	
	@Transient
	private boolean deleted;
	
	@Transient
	private boolean isNew;
	
	@Transient
	private boolean updated;
	
	@JsonIgnore @Override public String getId() { return this.getMapping().getId(); }
	@Override public void setId(String id) { }

	@JsonIgnore
	public String getSourceSchemaId() { return this.getMapping().getSourceId(); }
	
	@JsonIgnore
	public String getTargetSchemaId() { return this.getMapping().getTargetId(); }
		
	public boolean isDeleted() { return deleted; }
	public void setDeleted(boolean deleted) { this.deleted = deleted; }
	
	public boolean isNew() { return isNew; }
	public void setNew(boolean isNew) { this.isNew = isNew; }
	
	public boolean isUpdated() { return updated; }
	public void setUpdated(boolean updated) { this.updated = updated; }
}