package de.unibamberg.minf.transformation.config.nested;

import lombok.Data;

@Data
public class FileConfigProperties {
	private boolean downloadsDisabled = false;
}
