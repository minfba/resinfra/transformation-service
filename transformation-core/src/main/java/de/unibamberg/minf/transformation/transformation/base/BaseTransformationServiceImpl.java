package de.unibamberg.minf.transformation.transformation.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import de.unibamberg.minf.mapping.model.MappingExecGroup;
import de.unibamberg.minf.mapping.service.MappingExecutionService;
import de.unibamberg.minf.transformation.helper.MappingHelpers;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import de.unibamberg.minf.transformation.service.DatamodelService;

public class BaseTransformationServiceImpl implements TransformationService, ApplicationContextAware {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired private DatamodelService schemaService;
	private ApplicationContext appContext;
	
	
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;
	}
	
	protected MappingExecutionService getMappingExecutionService() {
		return appContext.getBean(MappingExecutionService.class);
	}
	
	@Override
	public MappingExecGroup buildMappingExecutionGroup(ExtendedMappingContainer m) {
		if (m==null) {
			return null;
		}
		ExtendedDatamodelContainer dcSource = schemaService.findById(m.getSourceSchemaId());		
		ExtendedDatamodelContainer dcTarget = schemaService.findById(m.getTargetSchemaId());
		return MappingHelpers.buildMappingExecutionGroup(m, dcSource, dcTarget);
	}
}
