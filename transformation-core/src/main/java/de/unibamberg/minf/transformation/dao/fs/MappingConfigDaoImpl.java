package de.unibamberg.minf.transformation.dao.fs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MappingConfigDaoImpl extends BaseFsDao implements MappingConfigDao, InitializingBean {
	@Autowired private ObjectMapper objectMapper;

	@Override
	public void afterPropertiesSet() throws Exception {
		File mappingDir = new File(getMappingPath());
		if (!mappingDir.exists()) {
			FileUtils.forceMkdir(mappingDir);
			log.info("Created mappings directory: " + getMappingPath());
		}
	}
	
	@Override
	public ExtendedMappingContainer findById(String schemaId) {
		File rootDir = new File(getMappingPath());
		if (!rootDir.isDirectory() || rootDir.listFiles()==null) {
			log.error(String.format("Failed to open mapping directory [%s]", getMappingPath()));
		} else {
			for (File mappingConfig : rootDir.listFiles()) {
				try {
					ExtendedMappingContainer s = objectMapper.readValue(mappingConfig, ExtendedMappingContainer.class);					
					this.renameIfIndicated(mappingConfig, s.getId());
					
					if (s.getId().equals(schemaId)) {
						return s;
					}
				} catch (Exception e) {
					log.error("Failed to parse mapping configuration from filesystem: " + mappingConfig.getAbsolutePath(), e);
				}
			}	
		}
		return null;
	}
	
	@Override
	public List<ExtendedMappingContainer> findAll() {
		List<ExtendedMappingContainer> mappings = new ArrayList<ExtendedMappingContainer>();
		
		File rootDir = new File(getMappingPath());
		if (!rootDir.isDirectory() || rootDir.listFiles()==null) {
			log.error(String.format("Failed to open mapping directory [%s]", getMappingPath()));
		} else {
			for (File mappingConfig : rootDir.listFiles()) {
				if (mappingConfig.isDirectory()) {
					continue;
				}
				try {
					ExtendedMappingContainer s = objectMapper.readValue(mappingConfig, ExtendedMappingContainer.class);					
					mappings.add(s);
					this.renameIfIndicated(mappingConfig, s.getId());
				} catch (Exception e) {
					log.error("Failed to parse mapping configuration from filesystem: " + mappingConfig.getAbsolutePath(), e);
				}
			}	
		}
		if (mappings.isEmpty()) {
			log.warn(String.format("No mapping was found at configured directory [%s]", getMappingPath()));
		}
		return mappings;
	}
	

	@Override
	public void saveOrUpdate(ExtendedMappingContainer mc) {
		this.saveMapping(mc);
	}
	
	@Override
	public void deleteMapping(String id) {
		File rootDir = new File(getMappingPath());
		if (!rootDir.isDirectory() || rootDir.listFiles()==null) {
			log.error(String.format("Failed to open mapping directory [%s]", getMappingPath()));
		} else {
			ExtendedMappingContainer m;
			try {
				for (File mappingConfig : rootDir.listFiles()) {
					if (mappingConfig.isDirectory()) {
						continue;
					}
					m = objectMapper.readValue(mappingConfig, ExtendedMappingContainer.class);	
					if (m.getId()!=null && m.getId().equals(id)) {
						FileUtils.forceDelete(mappingConfig);
						return;
					}
				}
			} catch (Exception e) {
				log.error("Failed to delete mapping configuration from filesystem", e);
			}
		}
	}
	
	private boolean saveMapping(ExtendedMappingContainer mc) {
		File rootDir = new File(getMappingPath());
		if (!rootDir.isDirectory() || rootDir.listFiles()==null) {
			log.error(String.format("Failed to open mapping directory [%s]", getMappingPath()));
		} else {
			ExtendedMappingContainer s;
			try {
				// Update?
				for (File mappingConfig : rootDir.listFiles()) {
					if (mappingConfig.isDirectory()) {
						continue;
					}					
					s = objectMapper.readValue(mappingConfig, ExtendedMappingContainer.class);					
					if (s.getId()!=null && s.getId().equals(mc.getId())) {
						objectMapper.writer().writeValue(mappingConfig, mc);
						log.debug("Updated mapping config at: " + mappingConfig.getAbsolutePath());
						return true;
					}					
				}
				// No match found -> new schema
				File newFilePath = new File(this.getAbsoluteFilePath(mc.getId()));
				
				objectMapper.writer().writeValue(newFilePath, mc);
				log.debug("Created mapping config at: " + newFilePath.getAbsolutePath());
				return true;
			} catch (Exception e) {
				log.error("Failed to write mapping configuration to filesystem", e);
				return false;
			}
		}
		return false;
	}

	private void renameIfIndicated(File mappingConfigFile, String mappingId) {
		try {
			if (!mappingConfigFile.getName().equals(mappingId +".json".toLowerCase())) {
				mappingConfigFile.renameTo(new File(this.getAbsoluteFilePath(mappingId)));
				log.info(String.format("Renamed out-of-sync mapping [%s]", mappingId));
			}
		} catch (Exception e) {
			log.warn("Failed to rename out-of-sync mapping", e);
		}
	}
	
	private String getAbsoluteFilePath(String mappingId) {
		return String.format("%s%s%s", getMappingPath(), File.separator, (mappingId + ".json").toLowerCase());
	}
}
