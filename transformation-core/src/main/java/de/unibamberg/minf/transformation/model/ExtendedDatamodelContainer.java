package de.unibamberg.minf.transformation.model;

import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.unibamberg.minf.dme.model.base.BaseElement;
import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.base.Identifiable;
import de.unibamberg.minf.dme.model.base.ModelElement;
import de.unibamberg.minf.dme.model.base.Nonterminal;
import de.unibamberg.minf.dme.model.reference.RootReference;
import de.unibamberg.minf.dme.model.serialization.DatamodelReferenceContainer;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ExtendedDatamodelContainer extends DatamodelReferenceContainer implements Identifiable {
	private static final long serialVersionUID = -2896815161379561045L;
	
	private boolean deleted;
	private boolean outdated;

	private String indexName;
	private Map<String, List<String>> analyzerFieldMap;
	
	
	private Element processingRootElement;
	private Element processingRootElementNoReuses;
	
	@Override public String getId() { return this.getModel().getId(); }
	@Override public void setId(String id) { /* Id should be explicity set in nested model */ }
	
	public boolean isDeleted() { return deleted; }
	public void setDeleted(boolean deleted) { this.deleted = deleted; }
	
	public boolean isOutdated() { return outdated; }
	public void setOutdated(boolean outdated) { this.outdated = outdated; }
	
	public String getIndexName() { return indexName; }
	public void setIndexName(String indexName) { this.indexName = indexName; }	
		
	public Map<String, List<String>> getAnalyzerFieldMap() { return analyzerFieldMap; }
	public void setAnalyzerFieldMap(Map<String, List<String>> analyzerFieldMap) { this.analyzerFieldMap = analyzerFieldMap; }
	
	
	@Override
	public void setElements(Map<String, ModelElement> elements) {
		super.setElements(elements);
		this.processingRootElement=null;
		this.processingRootElementNoReuses=null;
	}
	
	@Override
	public void setRoot(RootReference root) {
		super.setRoot(root);
		this.processingRootElement=null;
		this.processingRootElementNoReuses=null;
	}
	
	@Override
	public void clearRenderedHierarchies() {
		super.clearRenderedHierarchies();
		this.processingRootElement=null;
		this.processingRootElementNoReuses=null;	
	}
	
	@JsonIgnore
	@Transient
	public boolean isIncludeHeaders() {
		Element processingRoot = this.getOrRenderProcessingRoot(false);
		if (processingRoot!=null) {
			return processingRoot.isIncludeHeader();
		}
		return false;
	}
	
	@JsonIgnore
	@Transient
	public Element getOrRenderProcessingRoot() {
		return this.getOrRenderProcessingRoot(false);
	}
	
	@JsonIgnore
	@Transient
	public Element getOrRenderProcessingRoot(boolean skippedReuses) {
		if (this.getProcessingRootElement(skippedReuses)==null) {
			Element root = this.getOrRenderElementHierarchy(skippedReuses);
			if (root==null || !Nonterminal.class.isAssignableFrom(root.getClass())) {
				return null;
			} else {
				Element nRoot = root;
				if (nRoot.isProcessingRoot()) {
					this.setProcessingRootElement(nRoot, skippedReuses);
				} else {
					this.setProcessingRootElement(BaseElement.findProcessingRoot(nRoot), skippedReuses);
				}
				return this.getProcessingRootElement(skippedReuses);
			}
		} else {
			return this.getProcessingRootElement(skippedReuses);
		}
	}
	
	
	protected Element getProcessingRootElement(boolean skippedReuses) {
		return skippedReuses ? processingRootElementNoReuses : processingRootElement;
	}
	
	protected void setProcessingRootElement(Element processingRoot, boolean skippedReuses) {
		if (skippedReuses) {
			this.processingRootElementNoReuses = processingRoot;
		} else {
			this.processingRootElement=processingRoot;
		}
	}
	
	
}