package de.unibamberg.minf.transformation.user;

import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.transformation.model.PersistedUser;
import de.unibamberg.minf.transformation.service.UserService;
import eu.dariah.de.dariahsp.ProfileActionHandler;
import eu.dariah.de.dariahsp.model.ExtendedUserProfile;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserProfileActionHandler implements ProfileActionHandler {			
	
	@Autowired private UserService userService;
	
	
	@Override
	public void handleLogin(ExtendedUserProfile profile) {
		String queryId = profile.getUsername();
		if(queryId==null || queryId.isEmpty()) {
			queryId = profile.getId();
		}
		
		PersistedUser u = userService.loadUserByUsername(profile.getIssuerId(), queryId);
		if (u==null) {
			u = new PersistedUser();
		}
		profile.fillUser(u);		
		if (u.getUsername()==null) {
			u.setUsername(queryId);
		}
		u.setLastLogin(LocalDateTime.now());
		
		userService.saveUser(u);
		
		profile.setLocalId(u.getId());
		profile.setUniqueId(u.getUniqueId());
		log.debug("User has logged in: {}=>{}", queryId, u.getId());
	}

	@Override
	public void handleLogout(ExtendedUserProfile profile) {		
		log.debug("User has logged out: {}", profile.getId());
	}
}