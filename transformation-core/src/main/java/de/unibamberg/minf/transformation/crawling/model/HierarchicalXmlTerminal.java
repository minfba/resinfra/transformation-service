package de.unibamberg.minf.transformation.crawling.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.unibamberg.minf.dme.model.datamodel.natures.xml.XmlTerminal;

public class HierarchicalXmlTerminal extends XmlTerminal {
	private static final long serialVersionUID = -6931161006337266488L;

	private Map<String, String> namespacePrefixMap;
	private List<HierarchicalXmlTerminal> childTerminals;

	
	public Map<String, String> getNamespacePrefixMap() { return namespacePrefixMap; }
	public void setNamespacePrefixMap(Map<String, String> namespacePrefixMap) { this.namespacePrefixMap = namespacePrefixMap; }
	
	public List<HierarchicalXmlTerminal> getChildTerminals() { return childTerminals; }
	public void setChildTerminals(List<HierarchicalXmlTerminal> childTerminals) { this.childTerminals = childTerminals; }
	
	
	public void addChildTerminal(HierarchicalXmlTerminal child) {
		if (this.getChildTerminals()==null) {
			this.setChildTerminals(new ArrayList<HierarchicalXmlTerminal>());
		}
		this.getChildTerminals().add(child);
	}
	
	public void putNamespace(String namespace, String prefix) {
		if (this.getNamespacePrefixMap()==null) {
			this.setNamespacePrefixMap(new HashMap<String, String>());
		}
		this.getNamespacePrefixMap().put(namespace, prefix);
	}
}