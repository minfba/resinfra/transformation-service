package de.unibamberg.minf.transformation.api.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.unibamberg.minf.dme.model.serialization.MappingContainer;
import de.unibamberg.minf.transformation.api.client.base.BaseDmeClient;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MappingClient extends BaseDmeClient<MappingContainer, MappingContainer> {
	
	@Getter @Setter private List<ExtendedMappingContainer> currentMappings;

	@Override protected String getFetchAllUrl() { return dmeConfig.getMappingsUrl(); }
	@Override protected String getFetchDetailsUrl() { return dmeConfig.getMappingUrl(); }
	@Override protected String getPingUrl() { return dmeConfig.getBaseUrl(); }
	
	public MappingClient() {
		super(MappingContainer.class, (new MappingContainer[0]).getClass());
	}
	
	@Override
	protected void syncEntities(MappingContainer[] fetchedMappings) {
		// Nothing from DME -> mark everything deleted
		if (fetchedMappings==null || fetchedMappings.length==0) {
			this.setModelsDeleted(Optional.ofNullable(this.getCurrentMappings()).orElse(new ArrayList<>()).stream());
			return;
		}
		Map<String, ExtendedMappingContainer> outdatedMappingsMap = new HashMap<>(Optional.ofNullable(this.getCurrentMappings()).orElse(new ArrayList<>(0)).stream().collect(Collectors.toMap(ExtendedMappingContainer::getId, m -> m)));
		ExtendedMappingContainer existingMapping;
		for (MappingContainer fetchedMapping : fetchedMappings) {
			try {
				// New datamodel
				if (!outdatedMappingsMap.containsKey(fetchedMapping.getMapping().getId())) {
					this.importOrUpdateMapping(null, fetchedMapping.getMapping().getId());
				} else {
					existingMapping = outdatedMappingsMap.get(fetchedMapping.getMapping().getId());
									
					// TODO Test equals
					
					/*if (existingMapping.getMapping().getVersionId().equals(fetchedMapping.getMapping().getVersionId())) {
						log.trace("Skipping unmodified mapping [{}]", existingMapping.getMapping().getVersionId());
					} else {*/
						this.importOrUpdateMapping(existingMapping, fetchedMapping.getMapping().getId());
					//}
					outdatedMappingsMap.remove(fetchedMapping.getMapping().getId());
				}
			} catch (ApiConsumptionException e) {
				log.error("Failed to process fetched datamodel");
			}
		}
		// Flag deleted models
		this.setModelsDeleted(outdatedMappingsMap.values().stream());
	}
	
	private void setModelsDeleted(Stream<ExtendedMappingContainer> outdatedMappings) {
		outdatedMappings.forEach(m -> { 
			m.setDeleted(true);
			log.info("Setting mapping deleted [{}]", m.getId());
		});
	}	
	
	private void importOrUpdateMapping(ExtendedMappingContainer existingMapping, String mappingId) throws ApiConsumptionException {
		ExtendedMappingContainer importedMapping = this.fetchAndConvertMapping(mappingId);
		if (importedMapping.getMapping()==null || importedMapping.isDeleted()) {
			if (existingMapping!=null) {
				existingMapping.setDeleted(true);
			}
		} else if (existingMapping==null) {
			log.info("Importing new mapping [{}]", mappingId);
			importedMapping.setNew(true);
			this.getCurrentMappings().add(importedMapping);
			this.importGrammars(mappingId, null, importedMapping.getGrammars());
		} else {
			log.info("Updating mapping [{}]", mappingId);
			this.importGrammars(mappingId, existingMapping.getGrammars(), importedMapping.getGrammars());
			
			existingMapping.setElementPaths(importedMapping.getElementPaths());
			existingMapping.setFunctions(importedMapping.getFunctions());
			existingMapping.setGrammars(importedMapping.getGrammars());
			existingMapping.setMapping(importedMapping.getMapping());			
		}
	}
		
	private ExtendedMappingContainer fetchAndConvertMapping(String mappingId) throws ApiConsumptionException {
		MappingContainer mc = this.fetchDetails(mappingId);
		ExtendedMappingContainer result = new ExtendedMappingContainer();
		result.setFunctions(mc.getFunctions());
		result.setGrammars(mc.getGrammars());
		result.setMapping(mc.getMapping());

		return result;
	}
}
