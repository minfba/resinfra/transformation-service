package de.unibamberg.minf.transformation.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import de.unibamberg.minf.transformation.model.base.OnlineAccessMethod;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A specific implementation of a collection access, an endpoint represents a queryable interface 
 *  that does not provide harvesting or crawling capabilities, but answers queries.
 * 
 * An endpoint can provide access to data represented by multiple datamodels (as configured in the 
 *  Collection Registry) and can thus have multiple individual interfaces - one per datamodel. 
 * 
 * @author Tobias Gradl, University of Bamberg
 */
@Getter @Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"interfaces"})
@PrimaryKeyJoinColumn(name = "endpointId")
@NamedEntityGraph( attributeNodes = { @NamedAttributeNode("interfaces") } )
public class Endpoint extends Access {
	private static final long serialVersionUID = 2012138655635214422L;
	
	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy="endpoint", fetch = FetchType.LAZY)
	private Set<Interface> interfaces;
	
	@Override
	public boolean isEndpoint() { return true; }
	
	@Override
	public Set<OnlineAccessMethod> getAccessMethods() {
		if (this.getInterfaces()!=null) {
			return new HashSet<>(this.getInterfaces());
		} else {
			return new HashSet<>();
		}
	}
}
