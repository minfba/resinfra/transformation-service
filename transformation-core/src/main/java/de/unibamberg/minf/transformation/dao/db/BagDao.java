package de.unibamberg.minf.transformation.dao.db;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import de.unibamberg.minf.transformation.model.Bag;

@Repository
public interface BagDao extends JpaRepository<Bag, Long> {

	public Optional<Bag> findByUniqueId(String uniqueId);
	
}
