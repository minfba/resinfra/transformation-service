package de.unibamberg.minf.transformation;

public class TransformationConstants {
	public static final String ELEMENT_KEY_META = "_meta";
	
	public enum AccessMethods {
		OAI_PMH ("OAI-PMH"),
		FILE ("Online file"),
		GIT ("Git Repository"),
		OPAC ("OPAC"),
		REST ("REST API");
		
		private final String colregName;       
	    private AccessMethods(String m) {
	        colregName = m;
	    }
	    public boolean equalsName(String otherName) {
	        return otherName != null && colregName.equals(otherName);
	    }

	    @Override
	    public String toString() {
	       return this.colregName;
	    }
	}
	
	public enum FileTypes { XML, CSV, JSON, YAML, TEXT }
	
	public enum RootElementKeys {
		CONTENT ("_content"),
		INTEGRATIONS ("_integrations"),
		PRESENTATION ("_presentation"),
		META (ELEMENT_KEY_META);
		
		private final String key;       
	    private RootElementKeys(String k) {
	        key = k;
	    }
	    public boolean equalsName(String otherName) {
	        return otherName != null && key.equals(otherName);
	    }

	    public String toString() {
	       return this.key;
	    }
	}
	
	public enum ResourceEnrichmentStages { 
		RESOURCE_COLLECTION, 			// Collecting resources as provided
		RESOURCE_PRESENTATION,			// Extending presentation layer to resources as provided 
		INTEGRATIONS_TRANSFORMATION,	// Transforming collected resources to integrations model
		INTEGRATIONS_PRESENTATION		// Extending presentation layer to transformed resources
	}
	
	public enum ResourceConfigurationKeys {
		DATATYPE ("DataType"),
		ANALYZER ("Analyzer"),
		INDEXING ("Indexing");
		
		private final String elementName;       
	    private ResourceConfigurationKeys(String elementName) {
	        this.elementName = elementName;
	    }
	    public boolean equalsName(String otherName) {
	        return otherName != null && elementName.equals(otherName);
	    }
	    public String toString() {
	       return this.elementName;
	    }
	}
}
