package de.unibamberg.minf.transformation.dao.db;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.model.Dataset;

@Repository
public interface DatasetDao extends JpaRepository<Dataset, Long> {

	Optional<Dataset> findByUniqueId(String uniqueId); 
	

}
