package de.unibamberg.minf.transformation.transformation;

import java.util.List;

import de.unibamberg.minf.processing.consumption.ResourceConsumptionService;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import de.unibamberg.minf.transformation.transformation.base.TransformationService;

public interface ResourceTransformationService extends TransformationService {
	public void init(ExtendedMappingContainer emc, ResourceConsumptionService consumptionService);
	public void transformResources(List<? extends Resource> resources);
	public List<? extends Resource> syncTransformResources(List<? extends Resource> resources);
}
