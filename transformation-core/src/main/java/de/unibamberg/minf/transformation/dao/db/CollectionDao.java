package de.unibamberg.minf.transformation.dao.db;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.model.Collection;

@Repository
public interface CollectionDao extends JpaRepository<Collection, Long> {
		
	@Transactional
	@EntityGraph(attributePaths = {"names"}, type = EntityGraphType.LOAD)
	@Query("SELECT c FROM Collection c LEFT JOIN FETCH c.access a LEFT JOIN FETCH a.datasets ds LEFT JOIN FETCH a.interfaces i WHERE c.uniqueId = ?1")
	Optional<Collection> findByUniqueId(String uniqueId);

	@Transactional
	@Query("SELECT DISTINCT c FROM Collection c "
			+ "LEFT JOIN FETCH c.access a "
				+ "LEFT JOIN FETCH a.params p "
				+ "LEFT JOIN FETCH a.accessHeaders h "
				+ "LEFT JOIN FETCH a.datasets ds "
				+ "LEFT JOIN FETCH a.interfaces i")
	List<Collection> findAndLoadAll();
	
	@Transactional
	@EntityGraph(attributePaths = {"names", "access"}, type = EntityGraphType.LOAD)
	List<Collection> findAll();

	@Transactional
	@Query("SELECT DISTINCT c FROM Collection c "
			+ "LEFT JOIN FETCH c.access a "
				+ "LEFT JOIN FETCH a.params p "
				+ "LEFT JOIN FETCH a.accessHeaders h "
				+ "LEFT JOIN FETCH a.datasets ds "
				+ "WHERE ds.id = ?1")
	Collection findByDatasetId(Long datasetId);
}
