package de.unibamberg.minf.transformation.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import lombok.Data;

@Data
public abstract class PersistenceConfig {

    private String protocol = "jdbc:postgresql";
    private String host = "localhost";
    private int port = 5432;
    private String database = "transformation";
	private String username = "transformation";
	private String password = "transformation";
	private String driverClassName = "org.postgresql.Driver";
	private String databasePlatform = "postgresql";
	private boolean showSql = false;
	
	@Bean
	public PlatformTransactionManager transformationTransactionManager(DataSource transformationDataSource) {
		return new JpaTransactionManager(transformationEntityManagerFactory(transformationDataSource).getObject());
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean transformationEntityManagerFactory(DataSource transformationDataSource) {
		var jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setGenerateDdl(false);
		jpaVendorAdapter.setShowSql(showSql);
		jpaVendorAdapter.setDatabase(Database.valueOf(getDatabasePlatform().toUpperCase()));

		var factoryBean = new LocalContainerEntityManagerFactoryBean();

		factoryBean.setDataSource(transformationDataSource);
		factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
		factoryBean.setPackagesToScan(this.getPackagesToScan());
		factoryBean.setMappingResources(this.getMappingResources());
		
		return factoryBean;
	}
	
	@Bean
	public abstract DataSource transformationDataSource();
	
	
	protected String[] getPackagesToScan() {
		return new String[] {"de.unibamberg.minf.transformation.dao.db",
							 "de.unibamberg.minf.dme.model.base",
							 "de.unibamberg.minf.transformation.model"};
	}
	
	
	
	protected String[] getMappingResources() { return new String[0]; }
}
