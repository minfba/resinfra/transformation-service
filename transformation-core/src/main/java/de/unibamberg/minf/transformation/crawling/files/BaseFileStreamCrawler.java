package de.unibamberg.minf.transformation.crawling.files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.service.base.BaseProcessingService;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.TemporaryAccessOperation;
import de.unibamberg.minf.transformation.model.base.AccessOperation;
import de.unibamberg.minf.transformation.service.CrawlService;

public abstract class BaseFileStreamCrawler extends BaseProcessingService {
	@Autowired private CrawlService crawlService;
	
	private String dirPath;
	private String backupPath;
	
	private String accessOperationUid;
	
	protected String getDirPath() { return dirPath; }
	
	private List<String> inputFilenames;
	private List<String> outputFilenames = new ArrayList<>();
	
	@Override
	public void run() {
		MDC.put("uid", String.format("crawl_%s", accessOperationUid));
	}
	
	public void init(Access access, AccessOperation op, ExtendedDatamodelContainer sc) {
		try {
			super.init();
		} catch (ProcessingConfigException e) {}
		this.accessOperationUid = op.getUniqueId();
	}
	
	public List<String> getOutputFilenames() {
		return this.outputFilenames;
	}
	
	public List<String> getInputFilenames() {
		return this.inputFilenames;
	}
	
	public void setInputFilenames(List<String> inputFilenames) {
		this.inputFilenames = inputFilenames;
	}
	
	protected void setupPaths(AccessOperation op) throws IOException {

		if (TemporaryAccessOperation.class.isAssignableFrom(op.getClass())) {
			Path path = Paths.get(FileUtils.getTempDirectory().getAbsolutePath(), op.getUniqueId());
			dirPath = path.toString();
		} else {
			Crawl c = (Crawl)op;
			if (c.isOffline()) {
				dirPath = crawlService.getCrawlDirPath(c.getBaseCrawl().getId());
			} else {
				dirPath = crawlService.getCrawlDirPath(c.getId());
			}
		}
		
		if (!dirPath.endsWith(File.separator)) {
			dirPath = dirPath + File.separator;
		}
		
		backupPath = dirPath + ".backup";
		
		if (!Files.exists(Paths.get(new File(backupPath).toURI()))) {
			try {
				Files.createDirectories(Paths.get(new File(backupPath).toURI()));
			} catch (IOException e) {
				logger.error("Failed to create backup directory", e);
			}
		}
	}
	
	protected void moveInputToBackup(List<String> fileNames) throws IOException {
		for (String fileName : fileNames) {	
			this.moveInputToBackup(fileName);
		}
	}
	
	protected void moveInputToBackup(String fileName) throws IOException {
		Path sourcePath = Paths.get(new File(fileName).toURI());
		Path targetPath = Paths.get(new File(backupPath + File.separator + sourcePath.getFileName()).toURI());
		if (this.outputFilenames.contains(fileName)) {
			Files.copy(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
		} else {
			Files.move(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
		}
	}
	
	protected void moveInputToBackup() throws IOException {
		this.moveInputToBackup(this.getInputFilenames());
	}
	
	protected void addOutputFilename(String path) {
		if (!this.outputFilenames.contains(path)) {
			this.outputFilenames.add(path);
		}
	}
}
