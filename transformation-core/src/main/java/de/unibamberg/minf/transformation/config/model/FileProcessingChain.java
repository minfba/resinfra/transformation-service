package de.unibamberg.minf.transformation.config.model;

import lombok.Data;

@Data
public class FileProcessingChain {
	private final String key;
	private final String value;
}
