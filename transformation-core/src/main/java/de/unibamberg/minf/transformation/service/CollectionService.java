package de.unibamberg.minf.transformation.service;

import java.util.List;
import java.util.Optional;

import de.unibamberg.minf.transformation.model.Collection;

public interface CollectionService {
	public List<Collection> findAll();
	public void saveCollection(Collection c);
	public Collection findByColregEntityId(String colregEntityId);
	public Collection findById(Long id);
	public List<Collection> findByIds(List<Long> ids);
	//public List<Collection> findByDatamodelId(String datamodelId);	
	public List<Collection> findAndLoadAll();
	public long getCount();
	public boolean delete(Collection cDelete);
	public Optional<Collection> findByUniqueId(String datamodelUID);
	public Collection findByDatasetId(Long id);
}
