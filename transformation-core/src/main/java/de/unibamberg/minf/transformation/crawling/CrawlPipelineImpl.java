package de.unibamberg.minf.transformation.crawling;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import de.unibamberg.minf.gtf.context.ExecutionContext;
import de.unibamberg.minf.processing.exception.GenericProcessingException;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.listener.ProcessingListener;
import de.unibamberg.minf.transformation.crawling.crawler.Crawler;
import de.unibamberg.minf.transformation.crawling.files.BaseFileStreamCrawler;

public class CrawlPipelineImpl implements CrawlPipeline {
	protected static final Logger logger = LoggerFactory.getLogger(CrawlPipelineImpl.class);
			
	private LinkedHashMap<UUID, Crawler> runnablesMap;
	private UUID currentRunnableId = null;
	private String loggerUid;
	private UUID uuid = null;
	private ExecutorService pipelineExecutor = Executors.newSingleThreadExecutor();
	private ReentrantLock lock = new ReentrantLock();
	private ExecutionContext executionContext;
	
	private ProcessingServiceStates state = ProcessingServiceStates.WAITING;
	private long stageSize = 0;
	private long stageProgress = 0;
	
	private ProcessingListener listener;
	
	private boolean cancellationRequested;
	
	@Override public boolean isInitialized() { return true; }
	@Override public void init() throws ProcessingConfigException { }
	
	@Override public String getMdcUid() { return String.format("crawl_%s", loggerUid); }
	@Override public void setMdcUid(String mdcUid) { }
	
	@Override public UUID getUuid() { return uuid; }
		
	@Override public ProcessingListener getListener() { return this.listener; }
	@Override public void setListener(ProcessingListener listener) { this.listener = listener; }

	@Override public ExecutionContext getExecutionContext() { return executionContext; }
	@Override public void setExecutionContext(ExecutionContext executionContext) { this.executionContext = executionContext; }
	
	@Override public LinkedHashMap<UUID, Crawler> getRunnablesMap() { return runnablesMap; }
	
	
	@Override public boolean isCancellationRequested() { return cancellationRequested; }

	public CrawlPipelineImpl(String opId, List<Crawler> runnables) throws GenericProcessingException {		
		if (runnables==null || runnables.isEmpty()) {
			throw new GenericProcessingException("Non-empty array of processing services required to instantiate a processing pipeline");
		}
		
		this.uuid = UUID.randomUUID();
		this.runnablesMap = new LinkedHashMap<>();
		this.loggerUid = String.format("crawl_%s", opId);
		
		try {
			for (Crawler svc : runnables) {	
				if (svc.getUuid()==null) {
					throw new GenericProcessingException("Every processing service is required to present a UUID. was null");
				}
				runnablesMap.put(svc.getUuid(), svc);
				svc.setListener(this);
			}
		} catch (Exception e) {
			throw new GenericProcessingException("Failed to initialize processing pipeline", e);
		}
	}
	
	@Override
	public void requestCancellation() {
		try {
			lock.lock();
			cancellationRequested = true;
			Crawler c;
			for (UUID serviceUUID : this.runnablesMap.keySet()) {
				c = this.runnablesMap.get(serviceUUID);
				if (c!=null) {
					c.requestCancellation();
				}
			}
		} catch (Exception e) {
			logger.error("Failed to cancel", e);
		} finally {
			lock.unlock();
		}
	}
	
	@Override
	public ProcessingServiceStates getState() {
		try {
			lock.lock();
			return state;
		} finally {
			lock.unlock();
		}
	}
	
	private void setState(ProcessingServiceStates state) {
		this.state = state;

		if (state==ProcessingServiceStates.ACTIVE) {
			this.getListener().start(uuid);
		} else if (state==ProcessingServiceStates.COMPLETE) {
			this.getListener().finished(uuid);
		} else if (state==ProcessingServiceStates.ERROR) {
			this.getListener().error(uuid);
		}
	}
	
	@Override
	public int getSize() {
		try {
			lock.lock();
			return runnablesMap.keySet().size();
		} finally {
			lock.unlock();
		}
	}
	
	@Override
	public int getIndex() {
		try {
			lock.lock();
			if (currentRunnableId==null) {
				return -1;
			}	
			int i=0;
			for (UUID serviceId : runnablesMap.keySet()) {
				if (serviceId.equals(currentRunnableId)) {
					return i;
				}
				i++;
			}
			return -1;
		} finally {
			lock.unlock();
		}
	}
	
	@Override
	public long getStageSize() {
		try {
			lock.lock();
			if (currentRunnableId==null) {
				return -1;
			} else {
				return stageSize;
			}
		} finally {
			lock.unlock();
		}
	}
	
	@Override
	public long getStageProgress() {
		try {
			lock.lock();
			if (currentRunnableId==null) {
				return -1;
			} else {
				return stageProgress;
			}
		} finally {
			lock.unlock();
		}
	}
	
	
	@Override
	public void run() {		
		try {
			lock.lock();
			if (!cancellationRequested) {
				pipelineExecutor.execute(runnablesMap.get(runnablesMap.keySet().iterator().next()));
			} else {
				this.shutdownError();
			}
		} finally {
			lock.unlock();
		}
	}


	@Override
	public void start(UUID serviceId) {
		try {
			lock.lock();
			this.setState(ProcessingServiceStates.ACTIVE);
			
			this.currentRunnableId = serviceId;
			this.stageSize = 0;
			this.stageProgress = 0;
		} finally {
			lock.unlock();
		}
	}
	
	@Override
	public void updateSize(UUID serviceId, long size) {
		this.checkServiceConsistency(serviceId);
		try {
			lock.lock();
			this.stageSize = size;
		} finally {
			lock.unlock();
		}
	}


	@Override
	public void processed(UUID serviceId, long process) {
		this.checkServiceConsistency(serviceId);
		try {
			lock.lock();
			this.stageProgress = process;
		} finally {
			lock.unlock();
		}
	}


	@Override
	public void finished(UUID serviceId) {
		this.checkServiceConsistency(serviceId);
		try {
			lock.lock();
			if (this.runnablesMap==null || this.getIndex()==this.runnablesMap.keySet().size()-1) {
				this.runnablesMap = null;
				this.currentRunnableId = null;
				
				if (!this.isCancellationRequested()) {
					logger.info("Pipeline completed.");			
					this.setState(ProcessingServiceStates.COMPLETE);
				} else {
					logger.info("Pipeline cancelled.");
					this.setState(ProcessingServiceStates.ERROR);
				}
			} else {
				UUID[] serviceIds = runnablesMap.keySet().toArray(new UUID[0]);
				for (int i=0; i<serviceIds.length; i++) {
					if (serviceIds[i].equals(serviceId)) {
						this.currentRunnableId = serviceIds[i+1];
						
						Crawler last = runnablesMap.get(serviceId);
						Crawler next = runnablesMap.get(serviceIds[i+1]);
						
						if (BaseFileStreamCrawler.class.isAssignableFrom(last.getClass()) && BaseFileStreamCrawler.class.isAssignableFrom(next.getClass())) {
							BaseFileStreamCrawler.class.cast(next).setInputFilenames(BaseFileStreamCrawler.class.cast(last).getOutputFilenames());
						}
						
						pipelineExecutor.execute(next);
						break;
					}
				}
			}
		} finally {
			lock.unlock();
		}
	}


	@Override
	public void error(UUID serviceId) {
		this.shutdownError();
	}

	private void checkServiceConsistency(UUID notifyingServiceId) {
		if (this.currentRunnableId==null) {
			logger.error("Currently set runnable is null. Cancelling processing pipeline.");
			this.shutdownError();
		} else if (notifyingServiceId==null) {
			logger.error("Notifying service presented itself as null. Cancelling processing pipeline.");
			this.shutdownError();
		} else if (!notifyingServiceId.equals(this.currentRunnableId)) {
			logger.error("Registered state for an unstarted service. Cancelling processing pipeline");
			this.shutdownError();
		}
	}
	
	private void shutdownError() {
		try {
			lock.lock();
			this.runnablesMap = null;
			this.currentRunnableId = null;
			this.setState(ProcessingServiceStates.ERROR);
			pipelineExecutor.shutdownNow();
			logger.info("Pipeline instructed to shut down with errors.");
		} finally {
			lock.unlock();
		}
	}


	@Override
	public String[] getServiceTitleMessageCodes() {
		try {
			lock.lock();
			if (runnablesMap==null || runnablesMap.keySet().size()==0) {
				return null;
			}
			
			String[] serviceTitleMessageCodes = new String[runnablesMap.keySet().size()];
			int i=0;
			for (UUID serviceId : runnablesMap.keySet()) {
				serviceTitleMessageCodes[i++] = runnablesMap.get(serviceId).getTitleMessageCode();
			}
			return serviceTitleMessageCodes;
		} finally {
			lock.unlock();
		}
	}


	@Override
	public String[] getServiceUnitMessageCodes() {
		try {
			lock.lock();
			if (runnablesMap==null || runnablesMap.keySet().size()==0) {
				return null;
			}
			
			String[] serviceUnitMessageCodes = new String[runnablesMap.keySet().size()];
			int i=0;
			for (UUID serviceId : runnablesMap.keySet()) {
				serviceUnitMessageCodes[i++] = runnablesMap.get(serviceId).getUnitMessageCode();
			}
			return serviceUnitMessageCodes;
		} finally {
			lock.unlock();
		}
	}

}
