package de.unibamberg.minf.transformation.crawling;

import de.unibamberg.minf.processing.service.base.ProcessingService.ProcessingServiceStates;

public class CrawlState {
	private ProcessingServiceStates state;
	private int pipelineLength;
	private int pipelineIndex;
	private long currentSize;
	private long currentProgress;
	
	private String[] serviceUnitMessageCode;
	private String[] serviceTitleMessageCode;
	
	public CrawlState() {}
	
	public CrawlState(ProcessingServiceStates state) {
		this.state = state;
	}
	
	public ProcessingServiceStates getState() { return state; }
	public void setState(ProcessingServiceStates state) { this.state = state; }
	
	public int getPipelineLength() { return pipelineLength; }
	public void setPipelineLength(int pipelineLength) { this.pipelineLength = pipelineLength; }
	
	public int getPipelineIndex() { return pipelineIndex; }
	public void setPipelineIndex(int pipelineIndex) { this.pipelineIndex = pipelineIndex; }
	
	public long getCurrentSize() { return currentSize; }
	public void setCurrentSize(long currentSize) { this.currentSize = currentSize; }
	
	public long getCurrentProgress() { return currentProgress; }
	public void setCurrentProgress(long currentProgress) { this.currentProgress = currentProgress; }

	public String[] getServiceUnitMessageCode() { return serviceUnitMessageCode; }
	public void setServiceUnitMessageCode(String[] serviceUnitMessageCode) { this.serviceUnitMessageCode = serviceUnitMessageCode; }

	public String[] getServiceTitleMessageCode() { return serviceTitleMessageCode; }
	public void setServiceTitleMessageCode(String[] serviceTitleMessageCode) { this.serviceTitleMessageCode = serviceTitleMessageCode; }
}