package de.unibamberg.minf.transformation.config.nested;

import java.util.Locale;

import lombok.Data;

@Data
public class LocalizationConfig {
	private Locale defaultLocale = Locale.GERMAN; 
	private String[] baseNames;
	private boolean debug;
	private int cacheSeconds = 10;
	
	public void setBaseNames(String... basenames) {
		this.baseNames = basenames;
	}
}
