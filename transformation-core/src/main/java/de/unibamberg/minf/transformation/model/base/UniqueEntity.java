package de.unibamberg.minf.transformation.model.base;

import java.security.MessageDigest;
import java.util.SplittableRandom;
import java.util.regex.Pattern;

public interface UniqueEntity extends BaseEntity {

	static Pattern uniqueIdPattern = Pattern.compile("^[a-zA-Z0-9]{8,128}$");
	
	public String getUniqueId();
	public void setUniqueId(String uniqueId);
	
	public default boolean hasUniqueId() {
		return this.getUniqueId()!=null && !this.getUniqueId().isBlank();
	}
	
	public default void assignUniqueId() {
		this.setUniqueId(this.createUniqueId());
	}
	
	public static boolean isUniqueIdValidFormat(String uniqueId) {
		return uniqueId!=null && uniqueIdPattern.matcher(uniqueId).matches();
	}
	
	public default String createUniqueId() {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			String num = Long.toString(new SplittableRandom().nextLong());
			byte[] result = md.digest(num.getBytes());
			StringBuilder sb = new StringBuilder(2 * result.length);
			for (byte b : result) {
				sb.append("0123456789ABCDEF".charAt((b & 0xF0) >> 4));
				sb.append("0123456789ABCDEF".charAt((b & 0x0F)));
			}
			return sb.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
