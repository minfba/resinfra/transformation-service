package de.unibamberg.minf.transformation.crawling;

import java.util.Set;

import de.unibamberg.minf.processing.listener.ProcessingListener;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.OnlineDataset;

public interface CrawlManager extends ProcessingListener {
	public void performOfflineCrawl(OnlineDataset dataset, Long baseCrawlId);
	public void performOnlineCrawl(OnlineDataset dataset);
	
	public CrawlState getCrawlState(Long crawlId);
	
	public Set<String> getSupportedAccessTypes();
	public Set<String> getSupportedFileTypes();
	public void tryCancelCrawl(Long crawlId);
}
