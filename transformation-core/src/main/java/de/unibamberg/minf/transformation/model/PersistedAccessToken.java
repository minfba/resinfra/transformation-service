package de.unibamberg.minf.transformation.model;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.unibamberg.minf.transformation.model.base.UniqueEntity;
import eu.dariah.de.dariahsp.model.AccessToken;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"user"})
@Table(name="accesstoken", 
	   uniqueConstraints={@UniqueConstraint(name="uniqueId",columnNames={"uniqueId"}),
			              @UniqueConstraint(name="unique_name_per_user",columnNames={"name", "user_id"})
	                     })
@NamedEntityGraph( attributeNodes = { @NamedAttributeNode("allowedAdresses") } )
public class PersistedAccessToken extends AccessToken implements UniqueEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private PersistedUser user;
	
	@Column(name = "created", columnDefinition = "TIMESTAMP WITH TIME ZONE", nullable = false)
	private OffsetDateTime created;
	
	@Column(name = "modified", columnDefinition = "TIMESTAMP WITH TIME ZONE", nullable = false)
	private OffsetDateTime modified;
}
