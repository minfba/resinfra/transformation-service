package de.unibamberg.minf.transformation.service;

import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.dme.model.datamodel.DatamodelImpl;
import de.unibamberg.minf.transformation.dao.db.ApiDao;
import de.unibamberg.minf.transformation.model.Api;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;

@Component
public class ApiServiceImpl implements ApiService {

	@Autowired private DatamodelService datamodelService;
	@Autowired private ApiDao apiDao;
	
	@Override
	public Optional<Api> findByUniqueId(String uniqueId) {
		return this.setDatamodels(apiDao.findByUniqueId(uniqueId));
	}

	@Override
	public Collection<Api> findAll() {
		return apiDao.findAll();
	}
	
	private Optional<Api> setDatamodels(Optional<Api> api) {
		if (api.isPresent()) {
			if (api.get().getInputDatamodelId()!=null) {
				api.get().setInputDatamodel(this.getDatamodel(api.get().getInputDatamodelId()));
			}
			if (api.get().getOutputDatamodelId()!=null) {
				api.get().setOutputDatamodel(this.getDatamodel(api.get().getOutputDatamodelId()));
			}
		}
		return api;
	}
	
	private ExtendedDatamodelContainer getDatamodel(String datamodelId) {
		if (datamodelId!=null) {
			ExtendedDatamodelContainer dmc = datamodelService.findById(datamodelId);
			if (dmc==null) {
				dmc = new ExtendedDatamodelContainer();
				dmc.setId(datamodelId);
				dmc.setDeleted(true);
				dmc.setModel(new DatamodelImpl());
				dmc.getModel().setId(datamodelId);
			}
			return dmc;
		}
		return null;
	}

	@Override
	public void deleteById(Long id) {
		apiDao.deleteById(id);
	}

	@Override
	public void saveApi(Api api) {
		apiDao.save(api);
	}

	@Override
	public void removeInterfaceFromApis(Long interfaceId) {
		Collection<Api> apis = apiDao.findByInterfaceId(interfaceId);
		apis.stream().forEach(a -> { 
			a.getInterfaces().removeIf(i -> i.getId().equals(interfaceId));
		});
		apiDao.saveAll(apis);
	}

	@Override
	public Collection<Api> findByInterfaceIds(Collection<Long> interfaceIds) {
		return apiDao.findByInterfaceIds(interfaceIds);
	}
}
