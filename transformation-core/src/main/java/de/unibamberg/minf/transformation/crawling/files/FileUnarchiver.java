package de.unibamberg.minf.transformation.crawling.files;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.springframework.beans.factory.annotation.Value;

import de.unibamberg.minf.processing.exception.ResourceProcessingException;
import de.unibamberg.minf.transformation.crawling.crawler.Crawler;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.base.AccessOperation;

public class FileUnarchiver extends BaseFileStreamCrawler implements Crawler {
	
	@Value("${processing.unpack.filename:archive.tmp}")
	private String unpackFileName;
	
	@Value("${processing.unarchive.filename:/}")
	private String unarchiveFileName;
	
	private boolean initialized = false;

	@Override public boolean isInitialized() { return this.initialized; }
	
	@Override
	public String getUnitMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.file.unarchiver.unit";
	}

	@Override
	public String getTitleMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.file.unarchiver.title";
	}
	
	@Override
	public void init(Access access, AccessOperation op, ExtendedDatamodelContainer sc) {
		super.init(access, op, sc);
		try {
			this.setupPaths(op);
			this.initialized = true;
		} catch (IOException e) {
			logger.error("File downloader initialization failed: " + e.getMessage(), e);
		}
	}
	
	@Override
	public void run() {
		super.run();
		if (this.getListener()!=null) {
			this.getListener().start(this.getUuid());
		}
		this.unarchive();	
	}
	
	private void unarchive() {
		FileInputStream fin;
		BufferedInputStream in;
		ArchiveInputStream input = null;
		BufferedOutputStream out = null;
		
		for (String inputFile : this.getInputFilenames()) {

			try {
				if (this.isCancellationRequested()) {
					throw new ResourceProcessingException("Service cancellation has been requested");
				}
				if (!new File(inputFile).exists()) {
					logger.info("Input file for unarchiving does not exist. File might have been unarchived already");
					continue;
				}
				fin = new FileInputStream(new File(inputFile));
				in = new BufferedInputStream(fin);
				
				try {
					input = new ArchiveStreamFactory().createArchiveInputStream(in);
				} catch (ArchiveException e) {
					logger.info("Could not detect archive type based on stream signature. Assuming non-archived file.");
					
					this.addOutputFilename(inputFile);
					continue;
				}

				ArchiveEntry entry;
							
				while ((entry = input.getNextEntry()) != null) {
					if (this.isCancellationRequested()) {
						throw new ResourceProcessingException("Service cancellation has been requested");
					}
					byte [] btoRead = new byte[1024];
					
					String outPath = getDirPath() + File.separatorChar + entry.getName();
					
					out = new BufferedOutputStream(new FileOutputStream(outPath));
					int len = 0;

		            while((len = input.read(btoRead)) != -1) {
		                out.write(btoRead,0,len);
		            }

		            out.close();
		            btoRead = null;
		            this.addOutputFilename(outPath);
					
					/*byte[] content = new byte[(int) entry.getSize()];
					out = new FileOutputStream(outputDir.getAbsolutePath() + File.separatorChar + entry.getName());
					input.read(content);
					out.write(content);*/
					
					if (this.getListener()!=null) {
						this.getListener().processed(this.getUuid(), input.getBytesRead());
					}
				}
			} catch (Exception e) {
				logger.warn("An error occurred attepting to process file as tar. No unpacking might be required...");
				if (this.getListener()!=null) {
					this.getListener().error(this.getUuid());
				}
			} finally {
				if (input!=null) {
					try {
						input.close();
					} catch (IOException e) {}
				}
				if (out!=null) {
					try {
						out.close();
					} catch (IOException e) {}
				}
			}
			
			
		}
		
		try {
			this.moveInputToBackup();
		} catch (IOException e) {
			logger.error("Failed to move input to backup", e);
		}
		if (this.getListener()!=null) {
			this.getListener().finished(this.getUuid());
		}
		
	}
}
