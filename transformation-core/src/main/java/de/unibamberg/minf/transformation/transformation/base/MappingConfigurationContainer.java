package de.unibamberg.minf.transformation.transformation.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.transformation.TransformationConstants.ResourceConfigurationKeys;

public class MappingConfigurationContainer {	
	private Map<ResourceConfigurationKeys, Map<String, List<String>>> configElementIdKeysMap;
	private Map<ResourceConfigurationKeys, Element> referencedTargetElementMap;
	
	private Map<String, List<String>> analyzerFieldnameMap;
	
	
	public Map<String, List<String>> getElementIdKeyMap(ResourceConfigurationKeys type) { return configElementIdKeysMap.get(type); }
	public Map<ResourceConfigurationKeys, Element> getReferencedTargetElementMap() { return referencedTargetElementMap; }
	

	public Map<String, List<String>> getAnalyzerFieldnameMap() { return analyzerFieldnameMap; }


	public MappingConfigurationContainer() {
		this.analyzerFieldnameMap = new HashMap<>();
		this.referencedTargetElementMap = new HashMap<>();
		this.configElementIdKeysMap = new HashMap<>();
		for (ResourceConfigurationKeys c : ResourceConfigurationKeys.values()) {
			this.configElementIdKeysMap.put(c, new HashMap<String, List<String>>());
		}
	}
	
	
	public void addAnalyzerFieldnameMatch(String analyzer, String fieldName) {
		List<String> fieldNames;
		if (this.analyzerFieldnameMap.containsKey(analyzer)) {
			fieldNames = this.analyzerFieldnameMap.get(analyzer);
		} else {
			fieldNames = new ArrayList<>();
			this.analyzerFieldnameMap.put(analyzer, fieldNames);
		}
		
		if (!fieldNames.contains(fieldName)) {
			fieldNames.add(fieldName);
		}
	}
	
	public void addElementAssignment(ResourceConfigurationKeys configParent, String elementId, String key) {
		Map<String, List<String>> elementKeyMap = this.configElementIdKeysMap.get(configParent);
		List<String> keyList;
		if (!elementKeyMap.containsKey(elementId)) {
			keyList = new ArrayList<>(); 
			keyList.add(key);
			elementKeyMap.put(elementId, keyList);
		} else {
			keyList = elementKeyMap.get(elementId);
			if (!keyList.contains(key)) {
				keyList.add(key);
			}
		}
	}

	public void assignTargetElement(ResourceConfigurationKeys c, Element element) {
		this.referencedTargetElementMap.put(c, element);
	}
}
