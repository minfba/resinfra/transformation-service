package de.unibamberg.minf.transformation.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.Interface;
import de.unibamberg.minf.transformation.model.LocalDataset;
import de.unibamberg.minf.transformation.model.OnlineDataset;

public interface DatasetService {

	Optional<Dataset> findByUniqueId(String uniqueId);
	Optional<LocalDataset> findLocalByUniqueId(String uniqueId);
	Optional<OnlineDataset> findOnlineByUniqueId(String uniqueId);

	/*public Dataset findById(Long id);	
	
	public List<Dataset> findAll();
	public List<LocalDataset> findAllLocalDatasets();
	public List<OnlineDataset> findAllOnlineDatasets();
	
	public Dataset findDatasetByIds(Long datasourceId, String datamodelId);
	public void save(Long endpointId, Dataset dataset);
	public void saveDataset(Dataset ds);

	public List<OnlineDataset> findAndLoadAllOnlineDatasets();
	
	public List<OnlineDataset> findAndLoadOnlineDatasetsByDatamodelId(String datamodelId);

	public Optional<OnlineDataset> findAndLoadOnlineDatasetById(Long id);
*/
	public void delete(Dataset dataset);
	public void deleteAll(Iterable<? extends Dataset> datasets);
	
	
	List<Dataset> findAll();
	
	List<LocalDataset> findAllLocal();
	
	List<OnlineDataset> findAllOnline();
	List<OnlineDataset> findOnlineByDatamodelId(String datamodelId);
	
	
	void saveDataset(Dataset dataset);
	
	Dataset findById(Long id);
	
	

}
