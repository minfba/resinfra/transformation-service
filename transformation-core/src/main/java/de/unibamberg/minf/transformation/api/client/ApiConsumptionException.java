package de.unibamberg.minf.transformation.api.client;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ApiConsumptionException extends Throwable {
	private static final long serialVersionUID = 8660417182312827018L;

	private final String apiCall;
  
    public ApiConsumptionException(String apiCall) {
        super();
        this.apiCall = apiCall;
    }

    public ApiConsumptionException(String apiCall, String message) {
        super(message);
        this.apiCall = apiCall;
    }

    public ApiConsumptionException(String apiCall, String message, Throwable cause) {
        super(message, cause);
        this.apiCall = apiCall;
    }

    public ApiConsumptionException(String apiCall, Throwable cause) {
        super(cause);
        this.apiCall = apiCall;
    }
}
