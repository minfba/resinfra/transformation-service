@XmlSchema(
		namespace = "http://www.openarchives.org/OAI/2.0/", 
		elementFormDefault = XmlNsForm.QUALIFIED,
		xmlns={ @XmlNs(prefix="", namespaceURI="http://www.openarchives.org/OAI/2.0/"),
				@XmlNs(prefix="xsi", namespaceURI="http://www.w3.org/2001/XMLSchema-instance")}
)
package de.unibamberg.minf.transformation.crawling.oaipmh.model;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlNs;