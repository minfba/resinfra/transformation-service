package de.unibamberg.minf.transformation.service;

import java.util.List;

import de.unibamberg.minf.dme.model.mapping.base.Mapping;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;

public interface MappingService {
	public ExtendedMappingContainer findById(String id);
	
	public List<ExtendedMappingContainer> getMappingsBySource(String sourceId);
	public List<ExtendedMappingContainer> getMappingsByTarget(String targetId);
	public ExtendedMappingContainer getMappingBySourceAndTarget(String sourceId, String targetId);
	
	public List<ExtendedMappingContainer> findAll(boolean rebuildCache);
	public List<ExtendedMappingContainer> findAll();
	public void saveMapping(ExtendedMappingContainer mapping);
	public void saveMappings(List<ExtendedMappingContainer> mappings);
	public boolean deleteMapping(ExtendedMappingContainer removeMc);
}
