package de.unibamberg.minf.transformation.crawling.crawler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import de.unibamberg.minf.dme.model.datamodel.NonterminalImpl;
import de.unibamberg.minf.processing.consumption.ResourceConsumptionService;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.service.MatchingFileCollector;
import de.unibamberg.minf.processing.service.ParallelFileProcessingService;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.TemporaryAccessOperation;
import de.unibamberg.minf.transformation.model.base.AccessOperation;
import de.unibamberg.minf.transformation.service.CrawlService;

public class FileProcessor extends ParallelFileProcessingService implements Processor, ResourceConsumptionService {

	@Autowired private CrawlService crawlService; 
	@Autowired private List<String> fileProcessingAntiPatterns;
	
	private boolean initialized = false;
	
	private String accessOperationUid;
	
	@Override
	public String getUnitMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.xml_processing.unit";
	}

	@Override
	public String getTitleMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.xml_processing.title";
	}
	
	@Override
	public boolean isInitialized() {
		return super.isInitialized() && initialized;
	}
	
	@Override
	public void run() {
		this.setMdcUid(String.format("crawl_%s", accessOperationUid));
		MDC.put("uid", this.getMdcUid());
	
		super.run();
	}
	
	@Override
	public void init(Access access, AccessOperation op, ExtendedDatamodelContainer sc) throws IOException {
		this.setRoot((NonterminalImpl)sc.getOrRenderElementHierarchy());
		this.setSchema(sc.getModel());
		this.accessOperationUid = op.getUniqueId();
		
		if (TemporaryAccessOperation.class.isAssignableFrom(op.getClass())) {
			Path path = Paths.get(FileUtils.getTempDirectory().getAbsolutePath(), op.getUniqueId());
			this.setPath(path.toString());
		} else {
			Crawl crawl = (Crawl)op;
			// An original offline crawl
			if (crawl.getBaseCrawl()!=null) {
				this.setPath(crawlService.getCrawlDirPath(crawl.getBaseCrawl().getId()));
			} 
			// Pipeline had online component before accessing this processor
			else {
				this.setPath(crawlService.getCrawlDirPath(crawl.getId()));
			}
		}
		
		

		this.initialized = true;
		try {
			if (access.getFileAccessPatterns()!=null && !access.getFileAccessPatterns().isEmpty()) {
				this.setFileCollector(new MatchingFileCollector(Paths.get(this.getPath()), access.getFileAccessPatterns()==null?null:new ArrayList<>(access.getFileAccessPatterns())));
				this.getFileCollector().setAntiPatternStrings(fileProcessingAntiPatterns);		
			}
			super.init();
		} catch (ProcessingConfigException | IOException e) {
			logger.error("Failed to initialize file processing", e);
		}
	}
}
