package de.unibamberg.minf.transformation.crawling.oaipmh;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Locale;

import org.apache.commons.io.IOUtils;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import de.unibamberg.minf.transformation.crawling.crawler.OaiPmhCrawlerImpl;
import de.unibamberg.minf.transformation.crawling.oaipmh.model.OaiPmhResponseContainer;

public class OaiPmhClientImpl implements OaiPmhClient {
	protected static final Logger logger = LoggerFactory.getLogger(OaiPmhCrawlerImpl.class);
	
	private int maxRedirects = 5;
	
	public static DateTimeFormatter OAI_DATESTAMP_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd");
	public static DateTimeFormatter OAI_TIMESTAMP_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withLocale(Locale.ROOT).withChronology(ISOChronology.getInstanceUTC());

	@Autowired private RestTemplate restTemplate;
	
	public OaiPmhResponseContainer executeCommand(String baseUrl, String verb, String id, String metadataPrefix, String from, String until, String set, String resumptionToken) throws IOException {		
		try {
			if (verb==null || verb.trim().isEmpty()) {
				return this.processError("badVerb", "No verb provided");
			} else if (verb.trim().toLowerCase().equals("getrecord")) {
				return this.getRecord(baseUrl, id, metadataPrefix);
			} else if (verb.trim().toLowerCase().equals("identify")) {
				return this.identify(baseUrl);
			} else if (verb.trim().toLowerCase().equals("listidentifiers")) {
				return this.listIdentifiers(baseUrl, from, until, metadataPrefix, set, resumptionToken);
			} else if (verb.trim().toLowerCase().equals("listmetadataformats")) {
				return this.listMetadataFormats(baseUrl, id);
			} else if (verb.trim().toLowerCase().equals("listrecords")) {
				return this.listRecords(baseUrl, from, until, metadataPrefix, set, resumptionToken);
			} else if (verb.trim().toLowerCase().equals("listsets")) {
				return this.listSets(baseUrl, resumptionToken);
			} else {
				return this.processError("badVerb", String.format("Provided verb [%s] is not covered by OAI-PMH Standard", verb));
			}
		} catch (Exception e) {
			logger.error(String.format("Failed to execute OAI-PMH command. This typically means that the endpoint is not accessible or the specified set is unknown."));
			
			throw e;
		}
	}


	public OaiPmhResponseContainer listRecords(String baseUrl, String from, String until, String metadataPrefix, String set, String resumptionToken) {
		// TODO Auto-generated method stub
		return null;
	}


	public OaiPmhResponseContainer listIdentifiers(String baseUrl, String from, String until, String metadataPrefix, String set, String resumptionToken) {
		// TODO Auto-generated method stub
		return null;
	}


	public OaiPmhResponseContainer listSets(String baseUrl, String resumptionToken) {
		// TODO Auto-generated method stub
		return null;
	}


	public OaiPmhResponseContainer listMetadataFormats(String baseUrl, String identifier) {
		String requestUrl;
		if (identifier!=null && !identifier.trim().isEmpty()) {
			requestUrl = String.format("%s?verb=ListMetadataFormats&identifier=%s", baseUrl, identifier);
		} else {
			requestUrl = String.format("%s?verb=ListMetadataFormats", baseUrl);
		}
		
		OaiPmhResponseContainer response = getResponseContainer(requestUrl);
		return response;
	}

	
	private OaiPmhResponseContainer getResponseContainer(String requestURL) {

		URL url = null;
		URLConnection urlConnection = null;
		int httpStatusCode = 200;
		int redirectCounter = -1;
		
		try {
			while (url==null || httpStatusCode == HttpURLConnection.HTTP_SEE_OTHER || httpStatusCode == HttpURLConnection.HTTP_MOVED_PERM || httpStatusCode == HttpURLConnection.HTTP_MOVED_TEMP) {
				redirectCounter++;
				
				if (redirectCounter>maxRedirects) {
					throw new IOException("Too many redirects, configured maximum: " + this.maxRedirects);
				}
				
				if (url==null) {
					url = new URI(requestURL).toURL();
				}
				
				urlConnection = url.openConnection();
				
				// Handle redirects => update url and set httpStatusCode for loop
				HttpURLConnection httpConnection;
				String redirectLocation;
				if (HttpURLConnection.class.isAssignableFrom(urlConnection.getClass())) {
					httpConnection = (HttpURLConnection)urlConnection;
					httpConnection.setInstanceFollowRedirects(false);
					httpStatusCode = httpConnection.getResponseCode();
					if (httpStatusCode == HttpURLConnection.HTTP_SEE_OTHER || httpStatusCode == HttpURLConnection.HTTP_MOVED_PERM || httpStatusCode == HttpURLConnection.HTTP_MOVED_TEMP) {
						redirectLocation = httpConnection.getHeaderField("Location");
			            if (redirectLocation.startsWith("/")) {
			            	redirectLocation = url.getProtocol() + "://" + url.getHost() + redirectLocation;
			            }
						url = new URL(redirectLocation);
					}
				}
			}
			if (urlConnection==null) {
				return null;
			}

			return restTemplate.getForObject(url.toString(), OaiPmhResponseContainer.class);
		} catch (IOException | URISyntaxException e) {
			logger.error(String.format("Failed to get respose from [%s]", requestURL), e);
			return null;
		}
	}

	public OaiPmhResponseContainer identify(String baseUrl) {
		OaiPmhResponseContainer response = restTemplate.getForObject(String.format("%s?verb=Identify", baseUrl), OaiPmhResponseContainer.class);
		return response;
	}


	public OaiPmhResponseContainer getRecord(String baseUrl, String id, String metadataPrefix) {
		// TODO Auto-generated method stub
		return null;
	}


	private OaiPmhResponseContainer processError(String errorTitle, String errorMessage) {
		// TODO Auto-generated method stub
		return null;
	}
}
