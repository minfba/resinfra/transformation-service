package de.unibamberg.minf.transformation.dao.db;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.model.Endpoint;

@Repository
public interface EndpointDao extends JpaRepository<Endpoint, Long> {

	@Transactional
	@EntityGraph(attributePaths = {"interfaces", "params", "fileAccessPatterns", "collection"}, type = EntityGraphType.LOAD)
	public Optional<Endpoint> findByUniqueId(String uniqueId);

	@Transactional
	@Query("SELECT e FROM Endpoint e")
	@EntityGraph(attributePaths = {"interfaces", "params", "fileAccessPatterns", "collection", "collection.names"}, type = EntityGraphType.LOAD)
	public List<Endpoint> findAndLoadAll();
	
	@Override
	@Transactional
	@Query("SELECT e FROM Endpoint e")
	@EntityGraph(attributePaths = {"interfaces", "collection", "collection.names"}, type = EntityGraphType.LOAD)
	public List<Endpoint> findAll();
}
