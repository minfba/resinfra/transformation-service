package de.unibamberg.minf.transformation.dao.fs;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.config.LogConfigProperties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class LogFileDaoImpl extends BaseFsDao implements LogFileDao {	
	@Autowired private LogConfigProperties logConfig;
	
	@Override
	public String find(String id) {
		File searchDir = new File(logConfig.getIndexing().getBaseDir());
		if (!searchDir.exists()) {
			return null;
		}
		for (File f : searchDir.listFiles()) {
			if (f.getName().equals(id + ".html")) {
				return f.getName();
			}
		}
		return null;
	}
	
	@Override
	public boolean exists(String id) {
		return find(id)!=null;
	}
	
	@Override
	public void delete(String id) {
		File searchDir = new File(logConfig.getIndexing().getBaseDir());
		if (!searchDir.exists()) {
			return;
		}
		try {
			for (File f : searchDir.listFiles()) {
				if (f.getName().equals(id + ".html")) {
					Files.delete(Paths.get(f.toURI()));
					return;
				}
			}
		} catch (Exception e) {
			log.error("Failed to delete logfile", e);
		}
		
	}
}
