package de.unibamberg.minf.transformation.automation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.automation.base.BaseScheduledRunnable;
import de.unibamberg.minf.transformation.crawling.CrawlManager;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlCompleteFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlErrorFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlOnlineFlag;
import de.unibamberg.minf.transformation.data.service.DownloadDataService;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.OnlineDataset;
import de.unibamberg.minf.transformation.service.CrawlService;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.DatasetService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OfflineCrawlRunner extends BaseScheduledRunnable {
	
	@Autowired private DatamodelService datamodelService;
	
	@Autowired private CrawlService crawlService;
	@Autowired private CrawlManager crawlManager;	
	
	@Autowired private DatasetService datasetService;
	@Autowired private DownloadDataService downloadDataService;
	
	@Override
	public void init() {
		log.debug("Initializing automatic offline crawling capabilities");
	}
	
	@Override
	protected void executeAutomation() {
		log.debug("Checking status of available datamodels");
		if (this.isAutomationEnabled()) {
			this.checkAndRecreateIndices();
		}
	}
	
	@Transactional
	public void crawlOffline(String uniqueId, boolean clearAllData) {
		try {			
			Optional<OnlineDataset> ds = datasetService.findOnlineByUniqueId(uniqueId);
			if (ds.isPresent()) {
				if (clearAllData) {
					downloadDataService.clearDatasetData(ds.get().getUniqueId());
				}
				this.reindex(ds.get());
				log.info("Enqueued offline crawl for dataset: {}", uniqueId);
			} else {
				log.warn("Failed to enqueue offline crawl for unknown dataset: {}", uniqueId);
			}
		} catch (Exception e) {
			log.error(String.format("Failed to offline online crawl for dataset: %s", uniqueId), e);
			throw e;
		}		
	}
	
	public void checkAndRecreateIndices() {
		int count = 0;
		for (ExtendedDatamodelContainer dmc : datamodelService.findAll()) {
			if (dmc.isOutdated()) {
				this.reindexDatamodel(dmc);
				count++;
			}
		}
		log.debug("Executioned recreation task for outdated datamodels: processed {} outdated datamodels", count);
	}
	
	protected void reindexDatamodel(ExtendedDatamodelContainer dmc) {
		datasetService.findOnlineByDatamodelId(dmc.getModel().getId()).stream()
			.forEach(this::reindex);
	}
	
	private void reindex(OnlineDataset ds) {		
		List<Crawl> crawls = crawlService.findCrawls(ds.getId(), CrawlOnlineFlag.Online, CrawlCompleteFlag.Complete, CrawlErrorFlag.NoError, 1);
		if (!crawls.isEmpty()) {
			crawlManager.performOfflineCrawl(ds, crawls.get(0).getId());
		}
	}
}
