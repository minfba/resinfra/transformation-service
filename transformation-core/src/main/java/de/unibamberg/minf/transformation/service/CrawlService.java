package de.unibamberg.minf.transformation.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlCompleteFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlErrorFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlOnlineFlag;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.OnlineDataset;

public interface CrawlService {	
	public String getCrawlDirPath(Crawl c);
	public Crawl findById(Long crawlId);
	public void save(Crawl c);
	
	public void removeCrawlById(Long crawlId);
	public List<Crawl> findCurrentCrawls();
	public Crawl createEmptyCrawl(OnlineDataset dataset) throws IOException;
	public Crawl createOfflineCrawl(Long baseCrawlId);
	public Crawl createOnlineCrawl(OnlineDataset dataset);
	public String getCrawlDirPath(Long crawlId);
	public Crawl findCurrentCrawl(Long datasetId);
	
	public List<Crawl> findCrawls(Long datasetId, int limit);
	//public List<Crawl> findCrawls(Long endpointId, Long datasetId, CrawlOnlineFlag online, CrawlCompleteFlag complete, CrawlErrorFlag error, int limit);
	
	public List<Crawl> findCrawls(Long datasetId, CrawlOnlineFlag online, CrawlCompleteFlag complete, CrawlErrorFlag error, int limit);
	public Optional<Crawl> findByUniqueId(String uniqueId);
	
}
