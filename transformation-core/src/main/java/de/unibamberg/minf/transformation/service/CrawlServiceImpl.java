package de.unibamberg.minf.transformation.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;

import de.unibamberg.minf.transformation.dao.db.CollectionDao;
import de.unibamberg.minf.transformation.dao.db.CrawlDao;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlCompleteFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlErrorFlag;
import de.unibamberg.minf.transformation.dao.db.CrawlDao.CrawlOnlineFlag;
import de.unibamberg.minf.transformation.dao.db.DatasetDao;
import de.unibamberg.minf.transformation.dao.db.EndpointDao;
import de.unibamberg.minf.transformation.dao.fs.CrawlDataDao;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.OnlineDataset;

public class CrawlServiceImpl implements CrawlService, InitializingBean {
	protected static final Logger logger = LoggerFactory.getLogger(CrawlServiceImpl.class);
	
	@Autowired private CrawlDataDao crawlDataDao;
	@Autowired private CrawlDao crawlDao;
	
	@Autowired private CollectionDao datasourceDao;
	@Autowired private EndpointDao endpointDao;
	@Autowired private DatasetDao datasetDao;
	
	@Value("${crawling.autoremove_orphans:false}")
	private boolean autoremoveOrphans;
	
	public void afterPropertiesSet() throws Exception {
		// TODO Autoremove is good if crawl dir has no files (except empty .backup dir) 
		
		List<Crawl> crawls = this.findCurrentCrawls();
		if (crawls!=null) {
			for (Crawl c : crawls) {
				if (autoremoveOrphans) {
					this.removeCrawlById(c.getId());
					logger.info(String.format("Removed orphaned crawl [%s]", c.getId()));
				} else  {
					c.setError(true);
					c.setComplete(false);
					this.save(c);
				}
			}
		}
	
	}

	@Override
	public Crawl findById(Long crawlId) {
		return crawlDao.findById(crawlId).orElse(null);
	}

	@Override
	public void save(Crawl c) {
		crawlDao.save(c);
	}
	
	
	@Override
	public String getCrawlDirPath(Crawl c) {
		return crawlDataDao.getCrawlDirPath(c.getId());
	}
	
	@Override
	public String getCrawlDirPath(Long crawlId) {
		return crawlDataDao.getCrawlDirPath(crawlId);
	}
	
	@Override
	public void removeCrawlById(Long crawlId) {
		Optional<Crawl> c = crawlDao.findById(crawlId);
		if (c.isEmpty()) {
			return;
		}
		try {
			crawlDataDao.delete(c.get());
		} catch (Exception e) {
			logger.error("Failed to remove crawl data", e);
		} finally {
			crawlDao.deleteById(crawlId);
		}
	}

	@Override
	public List<Crawl> findCurrentCrawls() {
		return crawlDao.findCurrentCrawls();
	}
	
	@Override
	public Crawl findCurrentCrawl(Long datasetId) {
		List<Crawl> listResult = crawlDao.findCurrentCrawls(datasetId);
		if (listResult==null || listResult.isEmpty()) {
			return null;
		} 	
		if (listResult.size()>1) {
			logger.warn("Old active crawls detected. Setting failed.");
			for (int i=1; i<listResult.size(); i++) {
				listResult.get(i).setError(true);
				this.save(listResult.get(i));
			}			
		}
		return listResult.get(0);
	}
	
	@Override
	public List<Crawl> findCrawls(Long datasetId, CrawlOnlineFlag online, CrawlCompleteFlag complete, CrawlErrorFlag error, int limit) {
		return crawlDao.findCrawls(datasetId, online, complete, error, limit);
	}
	
	@Override
	public List<Crawl> findCrawls(Long datasetId, int limit) {
		if (limit > 0) {
			return crawlDao.findByEndpointAndDatamodel(datasetId, limit);
		}
		return crawlDao.findAllByEndpointAndDatamodel(datasetId);
	}
	
	@Override
	public Crawl createEmptyCrawl(OnlineDataset dataset) throws IOException {
		Crawl c = new Crawl();
		/*c.setCollection(datasource);
		c.setEndpoint(endpoint);
		c.setDataset(dataset);*/
		c.setComplete(true);
	
		this.save(c);
		
		FileUtils.forceMkdir(new File(this.getCrawlDirPath(c)));
		return c;
	}

	@Override
	public Crawl createOnlineCrawl(OnlineDataset dataset) {	
		Crawl c = new Crawl();
		/*c.setCollection(datasource);
		c.setEndpoint(endpoint);
		c.setDataset(dataset);
				
		Dataset ds = datasetDao.findById(dataset.getId()).orElse(null);
		if (ds!=null) {
			c.setPrefix(ds.getRemoteAlias());
		}*/
		
		c.setDataset(dataset);
		
		this.save(c);
		
		return c;
	}
	
	@Override
	public Crawl createOfflineCrawl(Long baseCrawlId) {
		Crawl cOnline = this.findById(baseCrawlId);
		if (cOnline==null || cOnline.isOffline()) {
			return null;
		}
		
		Crawl c = new Crawl();
		//c.setCollection(cOnline.getCollection());
		//c.setEndpoint(cOnline.getEndpoint());
		c.setDataset(cOnline.getDataset());
		c.setBaseCrawl(cOnline);
		
		this.save(c);
		
		return c;
	}

	@Override
	public Optional<Crawl> findByUniqueId(String uniqueId) {
		return crawlDao.findByUniqueId(uniqueId);
	}
}
