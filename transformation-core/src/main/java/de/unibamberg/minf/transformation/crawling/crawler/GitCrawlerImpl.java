package de.unibamberg.minf.transformation.crawling.crawler;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.slf4j.MDC;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import de.unibamberg.minf.processing.git.adapter.GitRepositoryAdapter;
import de.unibamberg.minf.processing.git.service.GitRepositoryProcessingService;
import de.unibamberg.minf.transformation.crawling.CrawlHelper;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.TemporaryAccessOperation;
import de.unibamberg.minf.transformation.model.base.AccessOperation;
import de.unibamberg.minf.transformation.service.CrawlService;

public class GitCrawlerImpl extends GitRepositoryProcessingService implements Crawler, ApplicationContextAware {
	@Autowired private CrawlService crawlService; 

	private boolean initialized = false;
	
	private String loggerUid;
	
	@Override
	public String getUnitMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.git_crawling.unit";
	}

	@Override
	public String getTitleMessageCode() {
		return "~eu.dariah.de.minfba.search.crawling.git_crawling.title";
	}
	
	@Override
	public boolean isInitialized() {
		return super.isInitialized() && initialized;
	}
	
	@Override
	public void run() {
		if (loggerUid!=null) {
			this.setMdcUid(loggerUid);
			MDC.put("uid", this.getMdcUid());
		}
		
		File cDir = new File(this.getCrawlDir()); 
		if (cDir.exists()) {
			FileUtils.deleteQuietly(cDir);
		}
		super.run();
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.setAdapter(applicationContext.getBean(GitRepositoryAdapter.class));
	}
	
	@Override
	public void init(Access access, AccessOperation op, ExtendedDatamodelContainer sc) throws IOException {
		this.setUrl(CrawlHelper.renderAccessUrl(access));
		this.setBranch(access.getSingleParamValue("branch"));
		
		if (TemporaryAccessOperation.class.isAssignableFrom(op.getClass())) {
			Path path = Paths.get(FileUtils.getTempDirectory().getAbsolutePath(), op.getUniqueId());
			this.setCrawlDir(path.toString());
		} else {
			Crawl crawl = (Crawl)op;
			if (crawl!=null) {
				this.loggerUid = String.format("crawl_%s", crawl.getId());
			}
			this.setCrawlDir(crawlService.getCrawlDirPath(crawl));
		}
		
		this.initialized = true;
	}	
}