package de.unibamberg.minf.transformation.data.service;

public interface DownloadDataService {
	public void clearDatasetData(String datasetUniqueId);
}
