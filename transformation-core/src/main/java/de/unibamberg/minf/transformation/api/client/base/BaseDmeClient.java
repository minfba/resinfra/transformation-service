package de.unibamberg.minf.transformation.api.client.base;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import de.unibamberg.minf.dme.model.base.Grammar;
import de.unibamberg.minf.dme.model.base.ModelElement;
import de.unibamberg.minf.dme.model.grammar.GrammarContainer;
import de.unibamberg.minf.transformation.config.nested.DmeConfigProperties;
import lombok.Getter;
import lombok.Setter;

public abstract class BaseDmeClient<T1, T2> extends BaseApiClientImpl<T1, T2> {
	
	@Getter @Setter protected DmeConfigProperties dmeConfig;
	@Getter @Setter protected Map<String, Map<String, GrammarContainer>> importGrammarsMap = new HashMap<>();
	
	public Map<String, GrammarContainer> getImportGrammars(String datamodelId) {
		return Optional.ofNullable(this.importGrammarsMap.get(datamodelId)).orElse(new HashMap<>(0));
	}
	
	protected BaseDmeClient(Class<T2> entityClass, Class<? extends T1[]> arrayClass) {
		super(entityClass, arrayClass);
	}
	
	protected void importGrammars(String entityId, Map<String, ? extends ModelElement> existingElements, Map<String, ? extends ModelElement> importedElements) {
		Map<String, GrammarContainer> importGrammars = new HashMap<>();
		Map<String, ModelElement> existing = new HashMap<>(Optional.ofNullable(existingElements).orElse(new HashMap<>(0)));
		
		// TODO: What about deleted grammars?
		
		if (importedElements==null) {
			return;
		}
		
		importedElements.entrySet().stream()
			.filter(e -> Grammar.class.isAssignableFrom(e.getValue().getClass()))
			.map(e -> Grammar.class.cast(e.getValue()))
			.filter(g -> !existing.containsKey(g.getId()) || 
					( existing.containsKey(g.getId()) && !this.isSameGrammar(Grammar.class.cast(existing.get(g.getId())), g) ))
			.forEach(g -> {
					if (g.getGrammarContainer()!=null) { 
						g.getGrammarContainer().setId(g.getIdentifier());
					}
					importGrammars.put(g.getId(), g.isPassthrough() ? null : g.getGrammarContainer());				
			});
		
		if (!importGrammars.isEmpty()) {
			this.importGrammarsMap.put(entityId, importGrammars);
		}
	}

	protected boolean isSameGrammar(GrammarContainer gcCurrent, GrammarContainer gcImported) {
		if (this.isSameGrammar(gcCurrent.getLexerGrammar(), gcImported.getLexerGrammar()) &&
				this.isSameGrammar(gcCurrent.getParserGrammar(), gcImported.getParserGrammar())) {
			return true;
		}
		
		String gcImportedLexer;
		String gcImportedParser;
		if (gcImported.getLexerGrammar()!=null && !gcImported.getLexerGrammar().trim().isEmpty()) {
			gcImportedLexer = "lexer grammar " + gcCurrent.getId() + "Lexer;\n\n" + gcImported.getLexerGrammar();
			gcImportedParser = "parser grammar " + gcCurrent.getId() + "Parser;\n\n" + 
							"options { tokenVocab= " + gcCurrent.getId() + "Lexer; }\n\n" + 
							gcImported.getParserGrammar();
		} else {
			gcImportedLexer = null;
			gcImportedParser = "grammar " + gcCurrent.getId() + ";\n\n" + gcImported.getParserGrammar();
		}
		
		return this.isSameGrammar(gcCurrent.getLexerGrammar(), gcImportedLexer) && this.isSameGrammar(gcCurrent.getParserGrammar(), gcImportedParser);
	}
	
	protected boolean isSameGrammar(Grammar gCurrent, Grammar gImported) {
		if (gCurrent.isPassthrough() && gImported.isPassthrough()) {
			return true;
		} else if (gCurrent.isPassthrough() || gImported.isPassthrough()) {
			return false;
		} else if (gCurrent.getGrammarContainer()==null && gImported.getGrammarContainer()==null) {
			return true;
		} else if (gCurrent.getGrammarContainer()==null || gImported.getGrammarContainer()==null) {
			return false;
		}
		return this.isSameGrammar(gCurrent.getGrammarContainer(), gImported.getGrammarContainer());
	}
	
	protected boolean isSameGrammar(String g1, String g2) {
		if (g1==null || g2==null) {
			return (g1==null||g1.isEmpty()) && (g2==null||g2.isEmpty());
		}
		return g1.equals(g2);
	}
}
