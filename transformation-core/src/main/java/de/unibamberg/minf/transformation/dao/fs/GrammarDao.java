package de.unibamberg.minf.transformation.dao.fs;

import java.io.IOException;

import de.unibamberg.minf.dme.model.base.Grammar;
import de.unibamberg.minf.dme.model.grammar.GrammarContainer;
import de.unibamberg.minf.gtf.exceptions.GrammarProcessingException;

public interface GrammarDao {
	public String getGrammarDirectory(String grammarId);
	public void deleteGrammar(String grammarId) throws IOException;
	public boolean saveGrammar(GrammarContainer gc) throws IOException;
	public void compileGrammar(GrammarContainer gc) throws GrammarProcessingException, IOException;
}
