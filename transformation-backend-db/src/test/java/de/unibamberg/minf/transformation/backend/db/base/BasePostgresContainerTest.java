package de.unibamberg.minf.transformation.backend.db.base;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import de.unibamberg.minf.transformation.backend.db.config.ContainerDbConfig;
import de.unibamberg.minf.transformation.backend.db.container.PostgresContainer;

@Testcontainers
@ContextConfiguration(classes = ContainerDbConfig.class)
@EnableConfigurationProperties(value = ContainerDbConfig.class)
@TestPropertySource(properties = {"spring.config.location=classpath:postgres-container-test.yml" })
public abstract class BasePostgresContainerTest extends BaseTest {
	@Container
    static final PostgresContainer postgresqlContainer = PostgresContainer.getInstance()
    		.withDatabaseName("transformation-tests-db")
			.withUsername("test")
			.withPassword("test");
}
