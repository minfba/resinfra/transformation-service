package de.unibamberg.minf.transformation.backend.db.base;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.backend.db.dao.ResourceDao;
import de.unibamberg.minf.transformation.backend.db.service.ResourceServiceImpl;

@Transactional(transactionManager = "transformationTransactionManager")
public abstract class BaseTest {
	@Autowired protected ResourceDao dao;
	@Autowired protected ResourceServiceImpl resourceService;
	@Autowired @Qualifier("transformationEntityManagerFactory") protected EntityManager em;
}
