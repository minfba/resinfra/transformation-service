package de.unibamberg.minf.transformation.backend.db.config;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import de.unibamberg.minf.transformation.backend.db.container.PostgresContainer;

@Configuration
@ConfigurationProperties(prefix="transformation.backend.db")
@ComponentScan("de.unibamberg.minf.transformation.backend.db.service")
@EnableJpaRepositories(
		basePackages = "de.unibamberg.minf.transformation.backend.db",
		entityManagerFactoryRef = "transformationEntityManagerFactory",
		transactionManagerRef = "transformationTransactionManager")
public class ContainerDbConfig extends BackendDbConfig {
	
	@Override
	public String getUrl() {
		return super.getUrl().replaceFirst("\\d+", PostgresContainer.getInstance().getMappedPort(5432).toString() );
	}
	
	@Bean
	public DataSource transformationDataSource() {
	    return DataSourceBuilder
	        .create()
		        .driverClassName(getDriverClassName())
		        .url(getUrl())
		        .username(getUsername())
		        .password(getPassword())
	        .build();
	}
}
