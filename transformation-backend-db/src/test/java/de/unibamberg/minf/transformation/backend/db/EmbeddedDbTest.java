package de.unibamberg.minf.transformation.backend.db;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import de.unibamberg.minf.transformation.backend.db.base.BaseEmbeddedTest;
import de.unibamberg.minf.transformation.backend.db.model.ResourceImpl;

@SpringBootTest
class EmbeddedDbTest extends BaseEmbeddedTest {
	
	@Test
	void findsAllResources() {
		
		ResourceImpl r = new ResourceImpl();
		r.setDsId(1L);
		r.setEpId(1L);
		r.setResource("{}");
		
		dao.save(r);
		
		var result = dao.findAll();
		assertThat(result).isNotEmpty();
	}
}
