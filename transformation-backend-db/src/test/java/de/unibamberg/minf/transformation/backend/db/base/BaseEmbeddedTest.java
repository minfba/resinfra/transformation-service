package de.unibamberg.minf.transformation.backend.db.base;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import de.unibamberg.minf.transformation.backend.db.config.EmbeddedDbConfig;

@ContextConfiguration(classes = EmbeddedDbConfig.class)
@EnableConfigurationProperties(value = EmbeddedDbConfig.class)
@TestPropertySource(properties = {"spring.config.location=classpath:embedded-test.yml" })
public abstract class BaseEmbeddedTest extends BaseTest {

}
