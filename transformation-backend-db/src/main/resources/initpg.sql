/*
 * Import in local installations via psql:
 *  psql -U root -d postgres -f initpg.sql
 * 
 * Import in docker container (pg_container)
 *  docker exec -i pg_container psql -U root -d postgres < ./initpg.sql
 */ 
CREATE DATABASE transformation;
CREATE USER transformation WITH ENCRYPTED PASSWORD 'transformation';
GRANT ALL PRIVILEGES ON DATABASE transformation TO transformation;