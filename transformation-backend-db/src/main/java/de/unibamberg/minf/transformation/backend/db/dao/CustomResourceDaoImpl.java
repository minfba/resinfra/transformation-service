package de.unibamberg.minf.transformation.backend.db.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.backend.db.model.ResourceImpl;

public class CustomResourceDaoImpl implements CustomResourceDao {
	@PersistenceContext private EntityManager entityManager;
	
	@Override
	@Transactional
	public List<String> findContainedByDatasetId(Long datasetId, int start, int limit) {
	    return this.findContainedByDatasetIdAndDatamodelId(datasetId, null, start, limit);
    }

    @Override
    public List<String> findContainedByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId, int start, int limit) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<String> query = cb.createQuery(String.class);
        Root<ResourceImpl> root = query.from(ResourceImpl.class);
  
        List<Predicate> predicates = new ArrayList<>();
        if (targetDatamodelId!=null) {
            predicates.add(cb.equal(root.get("dmId"), targetDatamodelId));
        } else {
          predicates.add(cb.isNull(root.get("dmId")));
        }
        predicates.add(cb.equal(root.get("dsId"), datasetId));
        
        
        query.select(root.get("resource"))
            .where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        
        return entityManager.createQuery(query)
                .setFirstResult(start)
                .setMaxResults(limit)
                .getResultList();
    }
}
