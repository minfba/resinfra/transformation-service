package de.unibamberg.minf.transformation.backend.db.dao;

import java.util.List;

public interface CustomResourceDao {
	List<String> findContainedByDatasetId(Long dsId, int start, int limit);
	List<String> findContainedByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId, int start, int limit);
}
