package de.unibamberg.minf.transformation.backend.db.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.unibamberg.minf.transformation.backend.db.dao.ResourceDao;
import de.unibamberg.minf.transformation.backend.db.model.ResourceImpl;

@Service
public class ResourceServiceImpl implements ResourceService {

	@Autowired private ResourceDao resourceDao;
	
	@Override
	public List<ResourceImpl> findAll() {
		return resourceDao.findAll();
	}

	@Override
	public void saveAll(Set<ResourceImpl> resourceBatch) {
		resourceDao.saveAll(resourceBatch);
	}

	@Override
	public void deleteByAccessId(Long endpointId) {
		resourceDao.deleteByEndpointId(endpointId);
	}

	@Override
	public void deleteByDatasetId(Long datasetId) {
		resourceDao.deleteByDatasetId(datasetId);
	}

	@Override
	public List<ResourceImpl> findByDatasetId(Long datasetId) {
		return resourceDao.findByDatasetId(datasetId);
	}
	
	@Override
	public long countByDatasetId(Long datasetId) {
		return resourceDao.countByDatasetId(datasetId);
	}
	
	@Override
	public List<String> findContainedByDatasetId(Long datasetId) {
		return resourceDao.findContainedByDatasetId(datasetId);
	}
	
	@Override
	public List<String> findContainedByDatasetId(Long datasetId, int start, int limit) {
		return resourceDao.findContainedByDatasetId(datasetId, start, limit);
	}
	
    @Override
    public List<ResourceImpl> findByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId) {
        return resourceDao.findByDatasetIdAndDatamodelId(datasetId, targetDatamodelId);
    }
    
    @Override
    public long countByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId) {
        return resourceDao.countByDatasetIdAndDatamodelId(datasetId, targetDatamodelId);
    }
    
    @Override
    public List<String> findContainedByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId) {
        return resourceDao.findContainedByDatasetIdAndDatamodelId(datasetId, targetDatamodelId);
    }
    
    @Override
    public List<String> findContainedByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId, int start, int limit) {
        return resourceDao.findContainedByDatasetIdAndDatamodelId(datasetId, targetDatamodelId, start, limit);
    }
}
