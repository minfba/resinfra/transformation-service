package de.unibamberg.minf.transformation.backend.db.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.unibamberg.minf.transformation.backend.db.model.ResourceImpl;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface ResourceDao extends JpaRepository<ResourceImpl, Long>, CustomResourceDao {
	
    @Transactional
    @Modifying
    @Query("DELETE FROM ResourceImpl WHERE epId = ?1")
    void deleteByEndpointId(Long epId);
    
    @Transactional
    @Modifying
    @Query("DELETE FROM ResourceImpl WHERE dsId = ?1")
    void deleteByDatasetId(Long dsId);

	@Transactional
	@Query("SELECT COUNT(r) FROM ResourceImpl r WHERE r.dsId=?1 AND r.dmId=null")
	long countByDatasetId(Long dsId);

	@Transactional
	@Query("SELECT r FROM ResourceImpl r WHERE dsId = ?1 AND r.dmId=null ORDER BY id DESC")
	List<ResourceImpl> findByDatasetId(Long dsId);
	
	@Transactional
	@Query("SELECT r.resource FROM ResourceImpl r WHERE dsId = ?1 AND r.dmId=null ORDER BY id DESC")
	List<String> findContainedByDatasetId(Long dsId);
	
	@Transactional
    @Query("SELECT r FROM ResourceImpl r WHERE dsId = ?1 AND r.dmId=?1 ORDER BY id DESC")
    List<ResourceImpl> findByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId);
	
	@Transactional
    @Query("SELECT COUNT(r) FROM ResourceImpl r WHERE r.dsId=?1 AND r.dmId=?2")
    long countByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId);

	@Transactional
    @Query("SELECT r.resource FROM ResourceImpl r WHERE dsId = ?1 AND r.dmId=?2 ORDER BY id DESC")
    List<String> findContainedByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId);
}
