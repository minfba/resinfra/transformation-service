package de.unibamberg.minf.transformation.backend.db.config;

import javax.sql.DataSource;

//import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import lombok.Data;

@Data
public abstract class BackendDbConfig {
	private String url = "jdbc:postgresql://localhost:5432/transformation";
	private String username = "transformation";
	private String password = "transformation";
	private String driverClassName = "org.postgresql.Driver";
	private String databasePlatform = "postgresql";
	private boolean showSql = false;
	
	@Bean
	public PlatformTransactionManager transformationTransactionManager(DataSource transformationDataSource) {
		return new JpaTransactionManager(transformationEntityManagerFactory(transformationDataSource).getObject());
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean transformationEntityManagerFactory(DataSource transformationDataSource) {
		var jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setGenerateDdl(true);
		jpaVendorAdapter.setShowSql(showSql);
		jpaVendorAdapter.setDatabase(Database.valueOf(getDatabasePlatform().toUpperCase()));
		
		var factoryBean = new LocalContainerEntityManagerFactoryBean();

		factoryBean.setDataSource(transformationDataSource);
		factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
		factoryBean.setPackagesToScan("de.unibamberg.minf.transformation.backend.db");

		return factoryBean;
	}
	
	@Bean
	public abstract DataSource transformationDataSource();
}
