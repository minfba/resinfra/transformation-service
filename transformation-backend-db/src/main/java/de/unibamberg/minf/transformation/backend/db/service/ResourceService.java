package de.unibamberg.minf.transformation.backend.db.service;

import java.util.List;
import java.util.Set;

import de.unibamberg.minf.transformation.backend.db.model.ResourceImpl;

public interface ResourceService {
	List<ResourceImpl> findAll();
	List<ResourceImpl> findByDatasetId(Long datasetId);
	List<String> findContainedByDatasetId(Long datasetId);
	void saveAll(Set<ResourceImpl> resourceBatch);
	void deleteByDatasetId(Long datasetId);
	void deleteByAccessId(Long accessId);
	long countByDatasetId(Long datasetId);
	List<String> findContainedByDatasetId(Long datasetId, int start, int limit);
    List<ResourceImpl> findByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId);
    long countByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId);
    List<String> findContainedByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId);
    List<String> findContainedByDatasetIdAndDatamodelId(Long datasetId, String targetDatamodelId, int start, int limit);
}
