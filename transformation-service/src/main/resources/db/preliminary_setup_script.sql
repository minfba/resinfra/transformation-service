
CREATE TABLE public.access (
    id bigint NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    deleted boolean NOT NULL,
    owner bytea,
    readprotected boolean NOT NULL,
    uniqueid character varying(255),
    writeprotected boolean NOT NULL,
    accessmodelid character varying(255),
    accesstype character varying(255),
    authrequired boolean NOT NULL,
    filetype character varying(255),
    httpmethod character varying(255),
    localname character varying(255),
    url character varying(255),
    collection_id bigint NOT NULL
);


ALTER TABLE public.access OWNER TO transformation;

--
-- TOC entry 216 (class 1259 OID 16398)
-- Name: accessheader; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.accessheader (
    id bigint NOT NULL,
    param character varying(255),
    value character varying(255),
    access_id bigint NOT NULL
);


ALTER TABLE public.accessheader OWNER TO transformation;

--
-- TOC entry 217 (class 1259 OID 16405)
-- Name: accessparam; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.accessparam (
    id bigint NOT NULL,
    param character varying(255),
    value character varying(255),
    access_id bigint NOT NULL
);


ALTER TABLE public.accessparam OWNER TO transformation;

--
-- TOC entry 218 (class 1259 OID 16412)
-- Name: accesstoken; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.accesstoken (
    id bigint NOT NULL,
    expires timestamp without time zone,
    name character varying(255),
    uniqueid character varying(255),
    writeaccess boolean NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    user_id bigint
);


ALTER TABLE public.accesstoken OWNER TO transformation;

--
-- TOC entry 219 (class 1259 OID 16419)
-- Name: accesstoken_allowedadresses; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.accesstoken_allowedadresses (
    accesstoken_id bigint NOT NULL,
    allowed_adresses character varying(255)
);


ALTER TABLE public.accesstoken_allowedadresses OWNER TO transformation;

--
-- TOC entry 220 (class 1259 OID 16422)
-- Name: api; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.api (
    id bigint NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    deleted boolean NOT NULL,
    owner bytea,
    readprotected boolean NOT NULL,
    uniqueid character varying(255),
    writeprotected boolean NOT NULL,
    inputdatamodelid character varying(255),
    label character varying(255),
    outputdatamodelid character varying(255)
);


ALTER TABLE public.api OWNER TO transformation;

--
-- TOC entry 221 (class 1259 OID 16429)
-- Name: api_interface; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.api_interface (
    api_id bigint NOT NULL,
    interface_id bigint NOT NULL
);


ALTER TABLE public.api_interface OWNER TO transformation;

--
-- TOC entry 222 (class 1259 OID 16434)
-- Name: bag; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.bag (
    id bigint NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    deleted boolean NOT NULL,
    owner bytea,
    readprotected boolean NOT NULL,
    uniqueid character varying(255),
    writeprotected boolean NOT NULL,
    dataset_id bigint NOT NULL
);


ALTER TABLE public.bag OWNER TO transformation;

--
-- TOC entry 223 (class 1259 OID 16441)
-- Name: collection; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.collection (
    id bigint NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    deleted boolean NOT NULL,
    owner bytea,
    readprotected boolean NOT NULL,
    uniqueid character varying(255),
    writeprotected boolean NOT NULL,
    collectionmetadata oid,
    colregentityid character varying(255),
    colregversionid character varying(255),
    imageurl character varying(255),
    updateperiod character varying(255)
);


ALTER TABLE public.collection OWNER TO transformation;

--
-- TOC entry 224 (class 1259 OID 16448)
-- Name: collection_names; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.collection_names (
    id bigint NOT NULL,
    name character varying(255),
    lang character varying(255) NOT NULL
);


ALTER TABLE public.collection_names OWNER TO transformation;

--
-- TOC entry 225 (class 1259 OID 16455)
-- Name: crawl; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.crawl (
    id bigint NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    deleted boolean NOT NULL,
    owner bytea,
    readprotected boolean NOT NULL,
    uniqueid character varying(255),
    writeprotected boolean NOT NULL,
    complete boolean NOT NULL,
    error boolean NOT NULL,
    prefix character varying(255),
    basecrawl_id bigint,
    dataset_id bigint NOT NULL
);


ALTER TABLE public.crawl OWNER TO transformation;

--
-- TOC entry 226 (class 1259 OID 16462)
-- Name: crawl_messages; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.crawl_messages (
    id bigint NOT NULL,
    messages character varying(255)
);


ALTER TABLE public.crawl_messages OWNER TO transformation;

--
-- TOC entry 227 (class 1259 OID 16465)
-- Name: dataset; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.dataset (
    id bigint NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    deleted boolean NOT NULL,
    owner bytea,
    readprotected boolean NOT NULL,
    uniqueid character varying(255),
    writeprotected boolean NOT NULL,
    datamodelid character varying(255)
);


ALTER TABLE public.dataset OWNER TO transformation;

--
-- TOC entry 228 (class 1259 OID 16472)
-- Name: datasource; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.datasource (
    datasourceid bigint NOT NULL
);


ALTER TABLE public.datasource OWNER TO transformation;

--
-- TOC entry 229 (class 1259 OID 16477)
-- Name: datasource_file_access_patterns; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.datasource_file_access_patterns (
    id bigint NOT NULL,
    file_access_patterns character varying(255)
);


ALTER TABLE public.datasource_file_access_patterns OWNER TO transformation;

--
-- TOC entry 230 (class 1259 OID 16480)
-- Name: endpoint; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.endpoint (
    endpointid bigint NOT NULL
);


ALTER TABLE public.endpoint OWNER TO transformation;

--
-- TOC entry 238 (class 1259 OID 16530)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: transformation
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.hibernate_sequence OWNER TO transformation;

--
-- TOC entry 231 (class 1259 OID 16485)
-- Name: interface; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.interface (
    id bigint NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    deleted boolean NOT NULL,
    owner bytea,
    readprotected boolean NOT NULL,
    uniqueid character varying(255),
    writeprotected boolean NOT NULL,
    datamodelid character varying(255),
    endpoint_id bigint NOT NULL
);


ALTER TABLE public.interface OWNER TO transformation;

--
-- TOC entry 232 (class 1259 OID 16492)
-- Name: localdataset; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.localdataset (
    expires timestamp with time zone,
    label character varying(255),
    local_dataset_id bigint NOT NULL
);


ALTER TABLE public.localdataset OWNER TO transformation;

--
-- TOC entry 233 (class 1259 OID 16497)
-- Name: onlinedataset; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.onlinedataset (
    nextexecution timestamp with time zone,
    remotealias character varying(255),
    online_dataset_id bigint NOT NULL,
    datasource_id bigint NOT NULL
);


ALTER TABLE public.onlinedataset OWNER TO transformation;

--
-- TOC entry 234 (class 1259 OID 16502)
-- Name: persisted_user; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.persisted_user (
    id bigint NOT NULL,
    expired boolean NOT NULL,
    issuer character varying(255),
    language character varying(255),
    lastlogin timestamp without time zone,
    username character varying(255),
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    uniqueid character varying(255)
);


ALTER TABLE public.persisted_user OWNER TO transformation;

--
-- TOC entry 235 (class 1259 OID 16509)
-- Name: persisted_user_authorities; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.persisted_user_authorities (
    persisted_user_id bigint NOT NULL,
    authorities character varying(255)
);


ALTER TABLE public.persisted_user_authorities OWNER TO transformation;

--
-- TOC entry 236 (class 1259 OID 16512)
-- Name: resources; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.resources (
    id bigint NOT NULL,
    dmid character varying(255),
    dsid bigint,
    epid bigint,
    resource oid
);


ALTER TABLE public.resources OWNER TO transformation;

--
-- TOC entry 237 (class 1259 OID 16517)
-- Name: versioninfoimpl; Type: TABLE; Schema: public; Owner: transformation
--

CREATE TABLE public.versioninfoimpl (
    id bigint NOT NULL,
    note character varying(255),
    updatewitherrors boolean NOT NULL,
    version character varying(255),
    versionhash character varying(255)
);


ALTER TABLE public.versioninfoimpl OWNER TO transformation;

--
-- TOC entry 3292 (class 2606 OID 16397)
-- Name: access access_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.access
    ADD CONSTRAINT access_pkey PRIMARY KEY (id);


--
-- TOC entry 3294 (class 2606 OID 16404)
-- Name: accessheader accessheader_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.accessheader
    ADD CONSTRAINT accessheader_pkey PRIMARY KEY (id);


--
-- TOC entry 3296 (class 2606 OID 16411)
-- Name: accessparam accessparam_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.accessparam
    ADD CONSTRAINT accessparam_pkey PRIMARY KEY (id);


--
-- TOC entry 3298 (class 2606 OID 16418)
-- Name: accesstoken accesstoken_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.accesstoken
    ADD CONSTRAINT accesstoken_pkey PRIMARY KEY (id);


--
-- TOC entry 3306 (class 2606 OID 16433)
-- Name: api_interface api_interface_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.api_interface
    ADD CONSTRAINT api_interface_pkey PRIMARY KEY (api_id, interface_id);


--
-- TOC entry 3304 (class 2606 OID 16428)
-- Name: api api_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.api
    ADD CONSTRAINT api_pkey PRIMARY KEY (id);


--
-- TOC entry 3308 (class 2606 OID 16440)
-- Name: bag bag_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.bag
    ADD CONSTRAINT bag_pkey PRIMARY KEY (id);


--
-- TOC entry 3312 (class 2606 OID 16454)
-- Name: collection_names collection_names_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.collection_names
    ADD CONSTRAINT collection_names_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 3310 (class 2606 OID 16447)
-- Name: collection collection_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.collection
    ADD CONSTRAINT collection_pkey PRIMARY KEY (id);


--
-- TOC entry 3314 (class 2606 OID 16461)
-- Name: crawl crawl_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.crawl
    ADD CONSTRAINT crawl_pkey PRIMARY KEY (id);


--
-- TOC entry 3316 (class 2606 OID 16471)
-- Name: dataset dataset_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.dataset
    ADD CONSTRAINT dataset_pkey PRIMARY KEY (id);


--
-- TOC entry 3318 (class 2606 OID 16476)
-- Name: datasource datasource_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.datasource
    ADD CONSTRAINT datasource_pkey PRIMARY KEY (datasourceid);


--
-- TOC entry 3320 (class 2606 OID 16484)
-- Name: endpoint endpoint_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.endpoint
    ADD CONSTRAINT endpoint_pkey PRIMARY KEY (endpointid);


--
-- TOC entry 3322 (class 2606 OID 16491)
-- Name: interface interface_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.interface
    ADD CONSTRAINT interface_pkey PRIMARY KEY (id);


--
-- TOC entry 3324 (class 2606 OID 16496)
-- Name: localdataset localdataset_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.localdataset
    ADD CONSTRAINT localdataset_pkey PRIMARY KEY (local_dataset_id);


--
-- TOC entry 3326 (class 2606 OID 16501)
-- Name: onlinedataset onlinedataset_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.onlinedataset
    ADD CONSTRAINT onlinedataset_pkey PRIMARY KEY (online_dataset_id);


--
-- TOC entry 3328 (class 2606 OID 16508)
-- Name: persisted_user persisted_user_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.persisted_user
    ADD CONSTRAINT persisted_user_pkey PRIMARY KEY (id);


--
-- TOC entry 3332 (class 2606 OID 16516)
-- Name: resources resources_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.resources
    ADD CONSTRAINT resources_pkey PRIMARY KEY (id);


--
-- TOC entry 3300 (class 2606 OID 16527)
-- Name: accesstoken unique_name_per_user; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.accesstoken
    ADD CONSTRAINT unique_name_per_user UNIQUE (name, user_id);


--
-- TOC entry 3330 (class 2606 OID 16529)
-- Name: persisted_user unique_username_per_issuer; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.persisted_user
    ADD CONSTRAINT unique_username_per_issuer UNIQUE (issuer, username);


--
-- TOC entry 3302 (class 2606 OID 16525)
-- Name: accesstoken uniqueid; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.accesstoken
    ADD CONSTRAINT uniqueid UNIQUE (uniqueid);


--
-- TOC entry 3334 (class 2606 OID 16523)
-- Name: versioninfoimpl versioninfoimpl_pkey; Type: CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.versioninfoimpl
    ADD CONSTRAINT versioninfoimpl_pkey PRIMARY KEY (id);


--
-- TOC entry 3339 (class 2606 OID 16551)
-- Name: accesstoken_allowedadresses fk2ndqo9fgx8i2egd2swige8aot; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.accesstoken_allowedadresses
    ADD CONSTRAINT fk2ndqo9fgx8i2egd2swige8aot FOREIGN KEY (accesstoken_id) REFERENCES public.accesstoken(id);


--
-- TOC entry 3335 (class 2606 OID 16531)
-- Name: access fk4p8pf7qxngl2aky52xwbk3gq9; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.access
    ADD CONSTRAINT fk4p8pf7qxngl2aky52xwbk3gq9 FOREIGN KEY (collection_id) REFERENCES public.collection(id);


--
-- TOC entry 3344 (class 2606 OID 16576)
-- Name: crawl fk5vpybwvl1cluydmefcdhrbau3; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.crawl
    ADD CONSTRAINT fk5vpybwvl1cluydmefcdhrbau3 FOREIGN KEY (basecrawl_id) REFERENCES public.crawl(id);


--
-- TOC entry 3352 (class 2606 OID 16616)
-- Name: onlinedataset fk77x7i4g3vbqghft16xnkctnoh; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.onlinedataset
    ADD CONSTRAINT fk77x7i4g3vbqghft16xnkctnoh FOREIGN KEY (datasource_id) REFERENCES public.datasource(datasourceid);


--
-- TOC entry 3340 (class 2606 OID 16556)
-- Name: api_interface fk89kbhs825exayt54gtrfoq19l; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.api_interface
    ADD CONSTRAINT fk89kbhs825exayt54gtrfoq19l FOREIGN KEY (interface_id) REFERENCES public.interface(id);


--
-- TOC entry 3342 (class 2606 OID 16566)
-- Name: bag fk8ene0fxtn1g2d6apa8fp9th5c; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.bag
    ADD CONSTRAINT fk8ene0fxtn1g2d6apa8fp9th5c FOREIGN KEY (dataset_id) REFERENCES public.dataset(id);


--
-- TOC entry 3338 (class 2606 OID 16546)
-- Name: accesstoken fkd662i761iniga1cnvf1avjsle; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.accesstoken
    ADD CONSTRAINT fkd662i761iniga1cnvf1avjsle FOREIGN KEY (user_id) REFERENCES public.persisted_user(id);


--
-- TOC entry 3345 (class 2606 OID 16581)
-- Name: crawl fkeeyjbhov12lnf9jb22jwbexi9; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.crawl
    ADD CONSTRAINT fkeeyjbhov12lnf9jb22jwbexi9 FOREIGN KEY (dataset_id) REFERENCES public.onlinedataset(online_dataset_id);


--
-- TOC entry 3351 (class 2606 OID 16611)
-- Name: localdataset fkfcxplw98gqinydg3cofa72s87; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.localdataset
    ADD CONSTRAINT fkfcxplw98gqinydg3cofa72s87 FOREIGN KEY (local_dataset_id) REFERENCES public.dataset(id);


--
-- TOC entry 3354 (class 2606 OID 16626)
-- Name: persisted_user_authorities fkg35na6pfnr2s32irmqgttppmj; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.persisted_user_authorities
    ADD CONSTRAINT fkg35na6pfnr2s32irmqgttppmj FOREIGN KEY (persisted_user_id) REFERENCES public.persisted_user(id);


--
-- TOC entry 3350 (class 2606 OID 16606)
-- Name: interface fkgn2lx91mg8li6b9fei9reapvj; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.interface
    ADD CONSTRAINT fkgn2lx91mg8li6b9fei9reapvj FOREIGN KEY (endpoint_id) REFERENCES public.endpoint(endpointid);


--
-- TOC entry 3349 (class 2606 OID 16601)
-- Name: endpoint fkjecvr59pn337oerk2bxl1g2r8; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.endpoint
    ADD CONSTRAINT fkjecvr59pn337oerk2bxl1g2r8 FOREIGN KEY (endpointid) REFERENCES public.access(id);


--
-- TOC entry 3343 (class 2606 OID 16571)
-- Name: collection_names fkjggaay5tw1kxj47hilw9786x7; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.collection_names
    ADD CONSTRAINT fkjggaay5tw1kxj47hilw9786x7 FOREIGN KEY (id) REFERENCES public.collection(id);


--
-- TOC entry 3346 (class 2606 OID 16586)
-- Name: crawl_messages fkla3puqvashcfd0ic6s9ag1yv4; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.crawl_messages
    ADD CONSTRAINT fkla3puqvashcfd0ic6s9ag1yv4 FOREIGN KEY (id) REFERENCES public.crawl(id);


--
-- TOC entry 3353 (class 2606 OID 16621)
-- Name: onlinedataset fknbnbejr83i3ye8055jypk8xj2; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.onlinedataset
    ADD CONSTRAINT fknbnbejr83i3ye8055jypk8xj2 FOREIGN KEY (online_dataset_id) REFERENCES public.dataset(id);


--
-- TOC entry 3341 (class 2606 OID 16561)
-- Name: api_interface fkolj7bsd8ptubicumha4hmbfat; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.api_interface
    ADD CONSTRAINT fkolj7bsd8ptubicumha4hmbfat FOREIGN KEY (api_id) REFERENCES public.api(id);


--
-- TOC entry 3348 (class 2606 OID 16596)
-- Name: datasource_file_access_patterns fkptjxukt47trh71vr8x3reuqfy; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.datasource_file_access_patterns
    ADD CONSTRAINT fkptjxukt47trh71vr8x3reuqfy FOREIGN KEY (id) REFERENCES public.access(id);


--
-- TOC entry 3336 (class 2606 OID 16536)
-- Name: accessheader fkqghpntuyg1i7kaovta6gvde7h; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.accessheader
    ADD CONSTRAINT fkqghpntuyg1i7kaovta6gvde7h FOREIGN KEY (access_id) REFERENCES public.access(id);


--
-- TOC entry 3337 (class 2606 OID 16541)
-- Name: accessparam fkqrkyjo88q88vdiutsyrjnkivy; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.accessparam
    ADD CONSTRAINT fkqrkyjo88q88vdiutsyrjnkivy FOREIGN KEY (access_id) REFERENCES public.access(id);


--
-- TOC entry 3347 (class 2606 OID 16591)
-- Name: datasource fkqw4xd0dktncmr4mtdd0cce2o1; Type: FK CONSTRAINT; Schema: public; Owner: transformation
--

ALTER TABLE ONLY public.datasource
    ADD CONSTRAINT fkqw4xd0dktncmr4mtdd0cce2o1 FOREIGN KEY (datasourceid) REFERENCES public.access(id);


-- Completed on 2023-11-13 15:48:51 UTC

--
-- PostgreSQL database dump complete
--
