ALTER TABLE access ADD COLUMN authrequired boolean;
UPDATE access SET authrequired = false WHERE true;
ALTER TABLE access ALTER COLUMN authrequired SET NOT NULL;