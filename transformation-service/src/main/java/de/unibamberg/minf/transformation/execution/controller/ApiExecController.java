package de.unibamberg.minf.transformation.execution.controller;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.minf.transformation.execution.api.ApiExecutionServiceImpl;
import de.unibamberg.minf.transformation.execution.exception.ApiExecutionException;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Api;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Locale;
import java.util.Optional;

import de.unibamberg.minf.transformation.rest.converter.ApiConverter;
import de.unibamberg.minf.transformation.rest.model.ApiRestResponse;
import de.unibamberg.minf.transformation.rest.model.RestResponse;
import de.unibamberg.minf.transformation.service.ApiService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/call/{uniqueId}")
public class ApiExecController implements ApplicationContextAware {
	
	@Autowired private ApiService apiService;
	@Autowired private ApiConverter apiConverter;
	@Autowired private ObjectMapper objectMapper;
	
	private ApplicationContext appContext;
	
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;
	}
	
	@GetMapping
	public RestResponse callApiGet(@PathVariable String uniqueId, @RequestParam(required=false) MultiValueMap<String, String> params, Locale locale) throws ApiItemNotFoundException, JsonProcessingException, ApiExecutionException {
		return this.innerCallApi(uniqueId, params!=null ? objectMapper.writeValueAsBytes(params) : null, false, locale);
	}
	
	@GetMapping(value="/debug")
	public RestResponse callApiGetDebug(@PathVariable String uniqueId, @RequestParam(required=false) MultiValueMap<String, String> params, Locale locale) throws ApiItemNotFoundException, JsonProcessingException, ApiExecutionException {
		return this.innerCallApi(uniqueId, params!=null ? objectMapper.writeValueAsBytes(params) : null, true, locale);
	}
	
	@PostMapping
	public RestResponse callApiPost(@PathVariable String uniqueId, HttpEntity<byte[]> requestEntity, Locale locale) throws ApiExecutionException, ApiItemNotFoundException {
		return this.innerCallApi(uniqueId, requestEntity.getBody(), false, locale);
	}
	
	@PostMapping(value="/debug")
	public RestResponse callApiPostDebug(@PathVariable String uniqueId, HttpEntity<byte[]> requestEntity, Locale locale) throws ApiExecutionException, ApiItemNotFoundException {
		return this.innerCallApi(uniqueId, requestEntity.getBody(), true, locale);
	}
	

	private RestResponse innerCallApi(@PathVariable String uniqueId, byte[] request, boolean debug, Locale locale) throws ApiExecutionException, ApiItemNotFoundException {
		ApiRestResponse response = new ApiRestResponse();
		Optional<Api> a = apiService.findByUniqueId(uniqueId);
		if (a.isEmpty()) {
			throw new ApiItemNotFoundException("api", uniqueId);
		}
		if (debug) {
			response.setApi(apiConverter.convert(a.get(), locale));
		}
		try (InputStream is = new ByteArrayInputStream(request)) {
			ApiExecutionServiceImpl execService = appContext.getBean(ApiExecutionServiceImpl.class);
			JsonNode res = execService.execute(a.get(), is, debug);
			if (res!=null && !res.isMissingNode()) {
				response.setResponse(res);
			}
		} catch (Exception e) {
			throw new ApiExecutionException(e.getMessage());
		}
		return response;
	}
}
