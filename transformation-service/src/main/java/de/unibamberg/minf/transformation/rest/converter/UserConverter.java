package de.unibamberg.minf.transformation.rest.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.PersistedAccessToken;
import de.unibamberg.minf.transformation.model.PersistedUser;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.UserPojo;

@Component
public class UserConverter extends BaseConverter<PersistedUser, UserPojo> {

	@Autowired private AccessTokenConverter tokenConverter;
	
	@Override
	public UserPojo convert(PersistedUser user, Locale locale, List<Class<?>> convertedTypes) {		
		if (!convertedTypes.contains(PersistedUser.class)) {
			convertedTypes.add(PersistedUser.class);
		}
		return this.fill(new UserPojo(), user, locale, convertedTypes);
	}
	
	@Override
	public UserPojo fill(UserPojo pojo, PersistedUser user, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setUniqueId(user.getUniqueId());
		pojo.setLanguage(user.getIssuer());
		pojo.setLanguage(user.getLanguage());
		pojo.setLastLogin(user.getLastLogin());
		pojo.setUsername(user.getUsername());
		pojo.setIssuer(user.getIssuer());
		
		if (this.isLoaded(user, "authorities") && !user.getAuthorities().isEmpty()) {
				pojo.setAuthorities(new ArrayList<>(user.getAuthorities().size()));
				pojo.getAuthorities().addAll(user.getAuthorities());
		}
		if (this.canProceedWithType(PersistedAccessToken.class, convertedTypes) && this.isLoaded(user, "accessTokens") && !user.getAccessTokens().isEmpty()) {
			pojo.setAccessTokens(new ArrayList<>(user.getAccessTokens().size()));
			for (PersistedAccessToken token : user.getAccessTokens()) {
				pojo.getAccessTokens().add(tokenConverter.convert(token, locale, convertedTypes));
			}
		
		}
		return pojo;
	}	
}
