package de.unibamberg.minf.transformation.execution.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ApiInsufficientPermissionsException extends ApiRuntimeException {
		
	public ApiInsufficientPermissionsException() {
		super("~de.unibamberg.minf.transformation.errors.insufficient_permissions", 403, null);
	}
}
