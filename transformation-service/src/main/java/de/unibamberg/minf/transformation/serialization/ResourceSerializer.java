package de.unibamberg.minf.transformation.serialization;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import de.unibamberg.minf.processing.model.base.Resource;

public class ResourceSerializer extends StdSerializer<Resource> {
	private static final long serialVersionUID = -2376703025566608715L;

	public ResourceSerializer() {
		super(Resource.class);
	}
	
	public ResourceSerializer(Class<Resource> t) {
		super(t);
	}

	@Override
	public void serialize(Resource resource, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeStartObject();
		this.serializeResource(resource, gen, false);
		gen.writeEndObject();
	}
	
	private void serializeResource(Resource resource, JsonGenerator gen, boolean inArray) throws IOException {
		if (resource.getChildResources()!=null && !resource.getChildResources().isEmpty()) {
			// Collect children with same name to produce JSON arrays
			Map<String, List<Resource>> childMap = this.collectChildren(resource);
			
			// If in array, the field name has been written before starting the array
			if (!inArray) {
				gen.writeFieldName(resource.getKey());
			}
			gen.writeStartObject();
			this.serializeChildren(childMap, gen);
			gen.writeEndObject();
		} else if (resource.getValue()!=null) {
			this.serializeValue(resource, gen, inArray);
		} 
	}

	private void serializeChildren(Map<String, List<Resource>> childMap, JsonGenerator gen) throws IOException {
		for (Entry<String, List<Resource>> childEntry : childMap.entrySet()) {
			if (childEntry.getValue().size()==1) {
				this.serializeResource(childEntry.getValue().get(0), gen, false);
			} else {
				this.serializeArray(childEntry.getKey(), childEntry.getValue(), gen);
			}
		}
	}
	
	private Map<String, List<Resource>> collectChildren(Resource resource) {
		Map<String, List<Resource>> childMap = new HashMap<>();
		for (Resource childR : resource.getChildResources()) {
			if (childMap.containsKey(childR.getKey())) {
				childMap.get(childR.getKey()).add(childR);
			} else {
				List<Resource> resources = new ArrayList<>(1);
				resources.add(childR);
				childMap.put(childR.getKey(), resources);
			}
		}
		return childMap;
	}
	
	
	private void serializeArray(String fieldName, List<Resource> resources, JsonGenerator gen) throws IOException {
		gen.writeFieldName(fieldName);
		gen.writeStartArray();
		for (Resource res : resources) {
			serializeResource(res, gen, true);
		}
		gen.writeEndArray();
	}
	
	private void serializeValue(Resource resource, JsonGenerator gen, boolean inArray) throws IOException {
		// in array only concatenate values, otherwise write key/falue field 
		if (inArray) {
			gen.writeString(resource.getValue().toString());
		} else {
			gen.writeStringField(resource.getKey(), resource.getValue().toString());
		}
	}
}
