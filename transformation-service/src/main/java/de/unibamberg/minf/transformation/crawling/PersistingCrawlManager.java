package de.unibamberg.minf.transformation.crawling;

import de.unibamberg.minf.processing.consumption.ResourceConsumptionService;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;

public class PersistingCrawlManager extends CrawlManagerImpl {
	@Override
	protected ResourceConsumptionService getConsumptionService(Crawl c, ExtendedDatamodelContainer dmc) {
		ResourcePersistenceServiceImpl indexer = appContext.getBean(ResourcePersistenceServiceImpl.class);
		indexer.setDatasetId(c.getDataset().getId());
		//indexer.setEndpointId(c.getEndpoint().getId());
		return indexer;
	}
}
