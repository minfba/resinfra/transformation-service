package de.unibamberg.minf.transformation.rest.pojo;

import java.time.LocalDateTime;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class UserPojo extends BasePojo {

	
	private Collection<String> authorities;
	
	private String issuer;
	private String username;
	private boolean expired;
	private String language;
	
	
	private LocalDateTime lastLogin;
	
	private Collection<AccessTokenPojo> accessTokens;
	
}
