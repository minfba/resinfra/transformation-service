package de.unibamberg.minf.transformation.crawling;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.processing.service.base.BaseProcessingService;
import de.unibamberg.minf.transformation.backend.db.service.ResourceService;
import de.unibamberg.minf.transformation.crawling.crawler.Crawler;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.base.AccessOperation;

public class DatabaseCleaner extends BaseProcessingService implements Crawler {

	@Autowired private ResourceService resourceService;
	
	private Long datasourceId;
	private Long datasetId;
	private Long crawlId;
	
	
	@Override public boolean isInitialized() { return this.datasourceId!=null || this.datasetId!=null; }
	@Override public String getUnitMessageCode() { return "~eu.dariah.de.minfba.search.crawling.cleaner.unit"; }
	@Override public String getTitleMessageCode() { return "~eu.dariah.de.minfba.search.crawling.cleaner.title"; }
	
	
	@Override
	public void init(Access access, AccessOperation op, ExtendedDatamodelContainer sc) {
		if (op!=null && Crawl.class.isAssignableFrom(op.getClass())) {
			Crawl crawl = (Crawl)op;
			crawlId = crawl.getId();
			datasetId = crawl.getDataset().getId();
			
		} 
		
		if (datasourceId==null && access!=null) {
			datasourceId = access.getId();
		}
		/*if (datasetId==null && sc!=null) {
			datasetId = sc.getId();
		}*/
	}

	@Override
	public void run() {
		if (this.crawlId!=null) {
			MDC.put("uid", String.format("crawl_%s", crawlId));
		}
		if (this.getListener()!=null) {
			this.getListener().start(this.getUuid());
		}
		try {
			if (datasetId!=null) {
				resourceService.deleteByDatasetId(datasetId);
			} else if (datasourceId!=null) {
				resourceService.deleteByAccessId(datasourceId);
			}  
			if (this.getListener()!=null) {
				this.getListener().finished(this.getUuid());
			}
		} catch (Exception e) {
			logger.error("Failed to process cleanup index request", e);
			if (this.getListener()!=null) {
				this.getListener().error(this.getUuid());
			}
		}
	}
}
	