package de.unibamberg.minf.transformation.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.Executor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import de.unibamberg.minf.transformation.automation.CollectionSyncService;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.CollectionConverter;
import de.unibamberg.minf.transformation.rest.model.RestActionResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemsResponse;
import de.unibamberg.minf.transformation.rest.model.RestActionResponse.ApiActionStatus;
import de.unibamberg.minf.transformation.rest.pojo.CollectionPojo;
import de.unibamberg.minf.transformation.service.CollectionService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Tag(name = "Collections", description = "API methods on collections")
@RestController
@RequestMapping("/api/collections")
public class CollectionRestController extends BaseRestController<CollectionPojo> {
	private static final String COLLECTIONS_SYNC_ACTION = "collections/sync";
	
	@Autowired private CollectionService collectionService;
	@Autowired private CollectionConverter collectionConverter;
	@Autowired private Executor syncAutomationTaskExecutor;
	@Autowired protected CollectionSyncService crSyncService;

	public CollectionRestController() {
		super("/api/collections");
	}

	@Hidden
	@GetMapping("/")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("/api/collections");
		return null;
	}
	
	@Operation(summary = "Get all collections")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Fetched collections",
					content = { @Content(mediaType = "application/json",
					schema = @Schema(implementation = RestItemsResponse.class)) 
					}) 
			})
	@GetMapping
    public RestItemsResponse getCollections(HttpServletRequest request, Locale locale) {
		RestItemsResponse response = new RestItemsResponse();
		List<CollectionPojo> collections = collectionConverter.convert(collectionService.findAll(), locale);
	
		response.setSize(collections.size());
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItems(this.getItems(collections, request.getRequestURL().toString()));
		
		return response;
	}
	
	@Operation(summary = "Get a collection by its uniqueId")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Found the collection",
					content = { @Content(mediaType = "application/json",
					schema = @Schema(implementation = RestItemResponse.class)) }),
			@ApiResponse(responseCode = "400", description = "Invalid uniqueId supplied", content = @Content),
			@ApiResponse(responseCode = "404", description = "Collection not found", content = @Content) })
	@GetMapping("/{uniqueId}")
    public RestItemResponse getDatamodel(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		
		Optional<Collection> c = collectionService.findByUniqueId(uniqueId);
		if (c.isEmpty()) {
			throw new ApiItemNotFoundException("collection", uniqueId);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(collectionConverter.convert(c.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}
	
	@Override
	protected ObjectNode getItem(CollectionPojo c, String requestUrl, boolean suffixUniqueId) {
		ObjectNode item = super.getItem(c, requestUrl, suffixUniqueId);
		if (item.has("endpoints")) {
			this.setLinksOnSubitems(item, "endpoints", requestUrl, false);
		}
		if (item.has("datasources")) {
			this.setLinksOnSubitems(item, "datasources", requestUrl, false);
		}
		return item;
	}
		
	@GetMapping("/sync")
	public RestActionResponse getSyncDatamodelsStatus(Model model, Locale locale) {
		if (crSyncService.isBusy()) {
			return new RestActionResponse(ApiActionStatus.BUSY, COLLECTIONS_SYNC_ACTION);
		}
		return new RestActionResponse(ApiActionStatus.NONE, COLLECTIONS_SYNC_ACTION);
	}
	
	@Operation(summary = "Trigger synchronization of collection data with associated registry", security = {
			@SecurityRequirement(name = "basicScheme") }, tags = { "Collections" })
	@PostMapping("/sync")
	public RestActionResponse triggerSyncDatamodels(Model model, Locale locale) {
		syncAutomationTaskExecutor.execute(() -> crSyncService.syncCollections());
		return new RestActionResponse(ApiActionStatus.STARTED, COLLECTIONS_SYNC_ACTION);
	}
}
