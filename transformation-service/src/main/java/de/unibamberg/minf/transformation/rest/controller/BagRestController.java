package de.unibamberg.minf.transformation.rest.controller;

import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Bag;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.BagConverter;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.pojo.BagPojo;
import de.unibamberg.minf.transformation.service.BagService;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Bags", description = "API methods on stored bags")
@RestController
@RequestMapping("/api/bags")
public class BagRestController extends BaseRestController<BagPojo> {
	
	@Autowired private BagService bagService;
	@Autowired private BagConverter bagConverter;
	
	public BagRestController() {
		super("/api/bags");
	}

	@GetMapping("/{uniqueId}")
    public RestItemResponse getBag(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		
		Optional<Bag> b = bagService.findByUniqueId(uniqueId);
		if (b.isEmpty()) {
			throw new ApiItemNotFoundException("bag", uniqueId);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(bagConverter.convert(b.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}	
}