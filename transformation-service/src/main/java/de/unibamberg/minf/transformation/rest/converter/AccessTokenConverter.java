package de.unibamberg.minf.transformation.rest.converter;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.PersistedAccessToken;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.AccessTokenPojo;

@Component
public class AccessTokenConverter extends BaseConverter<PersistedAccessToken, AccessTokenPojo> {

	@Override
	public AccessTokenPojo convert(PersistedAccessToken token, Locale locale, List<Class<?>> convertedTypes) {
		if (!convertedTypes.contains(PersistedAccessToken.class)) {
			convertedTypes.add(PersistedAccessToken.class);
		}
		return this.fill(new AccessTokenPojo(), token, locale, convertedTypes);
	}

	@Override
	public AccessTokenPojo fill(AccessTokenPojo pojo, PersistedAccessToken token, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setExpires(token.getExpires());
		pojo.setName(token.getName());
		pojo.setUniqueId(token.getUniqueId());
		pojo.setWriteAccess(token.isWriteAccess());
		
		if (this.isLoaded(token, "allowedAdresses")) {
			pojo.setAllowedAdresses(new HashSet<>());
			pojo.getAllowedAdresses().addAll(token.getAllowedAdresses());
		}
		return pojo;
	}
}
