package de.unibamberg.minf.transformation.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.html.HTMLLayout;
import ch.qos.logback.classic.sift.MDCBasedDiscriminator;
import ch.qos.logback.classic.sift.SiftingAppender;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.encoder.LayoutWrappingEncoder;
import ch.qos.logback.core.helpers.NOPAppender;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import ch.qos.logback.core.sift.AppenderFactory;
import ch.qos.logback.core.status.InfoStatus;
import ch.qos.logback.core.status.StatusManager;
import ch.qos.logback.core.util.FileSize;
import de.unibamberg.minf.core.web.init.logging.CustomHTMLLayout;
import de.unibamberg.minf.transformation.config.LogConfigProperties;
import de.unibamberg.minf.transformation.config.nested.IndexingLogConfigProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@EqualsAndHashCode(callSuper=true)
@Configuration
@ConfigurationProperties(prefix="log")
public class LogConfig extends LogConfigProperties {

	private static final String SIFT_DISCRIMINATOR_DEFAULT_VALUE = "uid_unset";
	
	@Autowired private MainConfig mainConfig;
	
	@PostConstruct
    public void reconfigureLogging() throws IOException {
		log.debug("Reconfiguring logging...");
		if (this.getIndexing()==null) {
			this.setIndexing(new IndexingLogConfigProperties());
		}
		/*if (this.getIndexing().getBaseDir()==null) {
			this.getIndexing().setBaseDir(mainConfig.setupPath(mainConfig.getPaths().getData(), "indexingLogs"));
		}*/
		
		LoggerContext lc = (LoggerContext) org.slf4j.LoggerFactory.getILoggerFactory();
		StatusManager statusManager = lc.getStatusManager();
        if (statusManager != null) {
            statusManager.add(new InfoStatus("Configuring logger", lc));
        }
     
        Logger rootLogger = lc.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        // RollingFileAppender
        if (this.getDir()!=null) {
        	rootLogger.addAppender(this.configureRollingFileAppender(lc));
        }
        //rootLogger.addAppender(this.configureSiftHtmlAppender(lc));
                
        this.logAppenderConfiguration(rootLogger);        
	}
	
	private void logAppenderConfiguration(Logger rootLogger) {
		log.info("Logging reconfigured");
		Iterator<Appender<ILoggingEvent>> appendersIterator = rootLogger.iteratorForAppenders();
        Appender<ILoggingEvent> configuredAppender;
        while(appendersIterator.hasNext()) {
        	configuredAppender = appendersIterator.next();
        	log.info("Configured appender: {}", configuredAppender.getClass().getName());
        }
	}
	
	private Appender<ILoggingEvent> configureSiftHtmlAppender(Context context) throws IOException {
		SiftingAppender sa = new SiftingAppender();
        sa.setName("SIFT-HTML");
        sa.setContext(context);

        MDCBasedDiscriminator discriminator = new MDCBasedDiscriminator();
        discriminator.setKey("uid");
        discriminator.setDefaultValue(SIFT_DISCRIMINATOR_DEFAULT_VALUE);
        discriminator.start();
        sa.setDiscriminator(discriminator);
        
        final String logPath = this.setupLogFile(this.getIndexing().getBaseDir(), "{}.html");
        final String pattern = this.getIndexing().getPattern();
        final String filesPrefix = this.getIndexing().getRemoteFilesPrefix();
        
        log.info("SiftingAppender (SIFT-HTML) logging to base directory: {}", logPath);
        
        sa.setAppenderFactory(new AppenderFactory<ILoggingEvent>() {
            @Override
            public Appender<ILoggingEvent> buildAppender(Context context, String discriminatingValue) throws JoranException {
            	// No appending for unset key, as we are not in an indexing process then            	
            	if (discriminatingValue.equals(SIFT_DISCRIMINATOR_DEFAULT_VALUE)) {
            		Appender<ILoggingEvent> nopAppender = new NOPAppender<>();
            		nopAppender.setContext(context);
            		return nopAppender;
				}
            	
            	FileAppender<ILoggingEvent> appender = new FileAppender<>();
            	appender.setFile(logPath.replace("{}", discriminatingValue));
            	appender.setContext(context);
            	
            	CustomHTMLLayout layout = new CustomHTMLLayout();
            	layout.setPattern(pattern);
            	layout.setContext(context);
            	layout.setRemoteFilesPrefix(filesPrefix);
            	
            	
            	LayoutWrappingEncoder<ILoggingEvent> pl = new LayoutWrappingEncoder<>();
				pl.setLayout(layout);
				pl.setContext(context);
				pl.start();
				
				layout.start();
				
				appender.setLayout(layout);
				appender.start();
				return appender;
            }
        });
        sa.start();
        return sa;
	}
	
	private Appender<ILoggingEvent> configureRollingFileAppender(Context context) throws IOException {
		RollingFileAppender<ILoggingEvent> appender = new RollingFileAppender<>();
		appender.setName("FILE");
		appender.setContext(context);
		appender.setFile(this.setupLogFile(this.getDir(), this.getLogFile()));

		TimeBasedRollingPolicy<ILoggingEvent> policy = new TimeBasedRollingPolicy<>();
		policy.setContext(context);
		policy.setMaxHistory(this.getMaxHistory());
		policy.setFileNamePattern(this.setupLogFile(this.getDir(), this.getLogFile() + this.getOldlogSuffix()));
		policy.setParent(appender);
		policy.start();

		SizeAndTimeBasedFNATP<ILoggingEvent> innerpolicy = new SizeAndTimeBasedFNATP<>();
		innerpolicy.setContext(context);
		innerpolicy.setMaxFileSize(FileSize.valueOf(this.getMaxFileSize()));
		innerpolicy.setTimeBasedRollingPolicy(policy);
		innerpolicy.start();

		policy.setTimeBasedFileNamingAndTriggeringPolicy(innerpolicy);
		policy.start();

		appender.setRollingPolicy(policy);

		PatternLayoutEncoder pl = new PatternLayoutEncoder();
		pl.setContext(context);
		pl.setPattern(this.getPattern());
		pl.start();
		appender.setEncoder(pl);

		log.info("RollingFileAppender logging to file: {}", appender.getFile());
		
		appender.start();
		
		return appender;
	}
	
	private String setupLogFile(String first, String... more) throws IOException {
		Path p = Paths.get(first, more);
		if (!Files.exists(p.getParent())) {
			Files.createDirectories(p.getParent());
		}
		return p.toString();
	}
}
