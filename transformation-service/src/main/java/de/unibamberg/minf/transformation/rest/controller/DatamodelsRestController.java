package de.unibamberg.minf.transformation.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import de.unibamberg.minf.transformation.automation.DmeSyncService;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.DatamodelConverter;
import de.unibamberg.minf.transformation.rest.model.RestActionResponse;
import de.unibamberg.minf.transformation.rest.model.RestActionResponse.ApiActionStatus;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemsResponse;
import de.unibamberg.minf.transformation.rest.pojo.DatamodelPojo;
import de.unibamberg.minf.transformation.service.DatamodelService;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Datamodels", description = "API methods on datamodels")
@RestController
@RequestMapping("/api/datamodels")
public class DatamodelsRestController extends BaseRestController<DatamodelPojo> {
	private static final String DATAMODELS_SYNC_ACTION = "datamodels/sync";
	
	@Autowired private DatamodelService datamodelService;
	@Autowired private DatamodelConverter datamodelConverter;
	@Autowired private Executor syncAutomationTaskExecutor;
	@Autowired protected DmeSyncService dmeSyncService;
	
	public DatamodelsRestController() {
		super("/api/datamodels");
	}
	
	@GetMapping("/")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("/api/datamodels");
		return null;
	}
	
	@GetMapping
    public RestItemsResponse getDatamodels(HttpServletRequest request, Locale locale) {
		RestItemsResponse response = new RestItemsResponse();
		List<DatamodelPojo> datamodels = datamodelConverter.convert(datamodelService.findAllModels(), locale);
	
		response.setSize(datamodels.size());
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItems(this.getItems(datamodels, request.getRequestURL().toString()));
		
		return response;
	}
	
	@GetMapping("/{datamodelUID}")
    public RestItemResponse getDatamodel(@PathVariable String datamodelUID, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		
		ExtendedDatamodelContainer edm = datamodelService.findById(datamodelUID);
		if (edm==null) {
			throw new ApiItemNotFoundException("datamodel", datamodelUID);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(datamodelConverter.convert(edm.getModel(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}
	
	@GetMapping("/sync")
	public RestActionResponse getSyncDatamodelsStatus(Model model, Locale locale) {
		if (dmeSyncService.isBusy()) {
			return new RestActionResponse(ApiActionStatus.BUSY, DATAMODELS_SYNC_ACTION);
		}
		return new RestActionResponse(ApiActionStatus.NONE, DATAMODELS_SYNC_ACTION);
	}
	
	@PostMapping("/sync")
	public RestActionResponse triggerSyncDatamodels(Model model, Locale locale) {
		syncAutomationTaskExecutor.execute(() -> dmeSyncService.syncModelsAndMappings());
		return new RestActionResponse(ApiActionStatus.STARTED, DATAMODELS_SYNC_ACTION);
	}
}