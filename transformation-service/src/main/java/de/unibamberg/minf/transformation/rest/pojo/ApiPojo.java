package de.unibamberg.minf.transformation.rest.pojo;

import java.util.List;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class ApiPojo extends BasePojo {
	
	@NotBlank
	private String label;
	
	private String metadata;
	
	@NotBlank
	private String inputDatamodelId;
	private DatamodelPojo inputDatamodel;
	
	@NotBlank
	private String outputDatamodelId;
	private DatamodelPojo outputDatamodel;
	
	private List<InterfacePojo> interfaces;
}
