package de.unibamberg.minf.transformation.execution.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ApiUnauthorizedException extends ApiRuntimeException {
		
	public ApiUnauthorizedException() {
		super("~de.unibamberg.minf.transformation.errors.unauthorized", 401, null);
	}
}
