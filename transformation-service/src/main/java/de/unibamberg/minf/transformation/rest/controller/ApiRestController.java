package de.unibamberg.minf.transformation.rest.controller;

import java.io.IOException;
import java.net.BindException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.unibamberg.minf.transformation.execution.exception.ApiInsufficientPermissionsException;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Api;
import de.unibamberg.minf.transformation.model.base.UniqueEntity;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.ApiConverter;
import de.unibamberg.minf.transformation.rest.model.ErrorRestResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemsResponse;
import de.unibamberg.minf.transformation.rest.model.RestResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse.ApiActions;
import de.unibamberg.minf.transformation.rest.pojo.ApiPojo;
import de.unibamberg.minf.transformation.rest.pojo.error.ValidationFieldError;
import de.unibamberg.minf.transformation.service.ApiService;
import de.unibamberg.minf.transformation.service.InterfaceService;
import eu.dariah.de.dariahsp.web.model.AuthPojo;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "APIs", description = "API methods on hosted APIs")
@RestController
@RequestMapping("/api/apis")
public class ApiRestController extends BaseRestController<ApiPojo> {
	@Autowired private ApiService apiService;
	@Autowired private InterfaceService interfaceService;
	
	@Autowired private ApiConverter apiConverter;

	public ApiRestController() {
		super("/api/apis");
	}

	@GetMapping("/")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("/api/apis");
		return null;
	}
	
	@GetMapping
    public RestItemsResponse getApis(HttpServletRequest request, Locale locale) {
		RestItemsResponse response = new RestItemsResponse();
		List<ApiPojo> apis = apiConverter.convert(apiService.findAll(), locale);
	
		response.setSize(apis.size());
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItems(this.getItems(apis, request.getRequestURL().toString()));
		
		return response;
	}
		
	@GetMapping("/{uniqueId}")
    public RestItemResponse getApi(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		Optional<Api> a = apiService.findByUniqueId(uniqueId);
		if (a.isEmpty()) {
			throw new ApiItemNotFoundException("api", uniqueId);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(apiConverter.convert(a.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}
	
	@DeleteMapping("/{uniqueId}")
	public RestItemResponse deleteApi(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		Api api = this.getApi(authInfoHelper.getAuth(), uniqueId);
		apiService.deleteById(api.getId());			
		return this.getItemResponse(apiConverter.convert(api, locale), request, ApiActions.DELETED);
	}
	
	@PostMapping(value = "/{uniqueId}", consumes = {"application/x-www-form-urlencoded"})
	public RestItemResponse updateApiForm(@PathVariable String uniqueId, @Valid ApiPojo updatedApi, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		return this.saveApi(uniqueId, updatedApi, request, locale);
	}
	
	@PostMapping(value = "/{uniqueId}", consumes = {"application/json"})
	public RestItemResponse updateApiJson(@PathVariable String uniqueId, @Valid @RequestBody ApiPojo updatedApi, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		return this.saveApi(uniqueId, updatedApi, request, locale);
	}
	
	@PostMapping(value = "/{uniqueId}/uid", consumes = {"application/json"})
	public RestResponse updateApiUid(@PathVariable String uniqueId, @RequestBody ApiPojo updatedApi, HttpServletResponse resp, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException, BindException {
		Api api = this.getApi(authInfoHelper.getAuth(), uniqueId);
		String updateUId = updatedApi.getUniqueId().trim();
		
		if (updateUId==null || updateUId.isEmpty()) {
			api.assignUniqueId();
		} else if (!UniqueEntity.isUniqueIdValidFormat(updateUId)) {
			List<ValidationFieldError> fieldErrors = new ArrayList<>();
			fieldErrors.add(new ValidationFieldError("uniqueId", messageSource.getMessage("~de.unibamberg.minf.transformation.validation.invalid_uniqueid", null, locale), updateUId));
			ObjectNode errors = objectMapper.createObjectNode();
			errors.set("fieldErrors", objectMapper.valueToTree(fieldErrors));
			ErrorRestResponse errResp = new ErrorRestResponse(HttpStatus.BAD_REQUEST.value(), messageSource.getMessage("~de.unibamberg.minf.transformation.errors.validation_failed", null, locale), request.getRequestURL().toString());
			errResp.setErrorDetails(errors);
			
			resp.setStatus(HttpStatus.BAD_REQUEST.value());
			return errResp;
		} else {
			api.setUniqueId(updateUId);
		}
		apiService.saveApi(api);
		return this.getItemResponse(apiConverter.convert(api, locale), request, ApiActions.UPDATED);
	}
	
	@PostMapping(consumes = {"application/x-www-form-urlencoded"})
	public RestItemResponse saveNewApiForm(@Valid ApiPojo newApi, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		return this.saveApi(null, newApi, request, locale);
	}
		
	@PostMapping(consumes = {"application/json"})
	public RestItemResponse saveNewApiJson(@Valid @RequestBody ApiPojo newApi, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		return this.saveApi(null, newApi, request, locale);
	}
	
	
	@Override
	protected ObjectNode getItem(ApiPojo c, String requestUrl, boolean suffixUniqueId) {
		ObjectNode item = super.getItem(c, requestUrl, suffixUniqueId);
		if (item.has("interfaces")) {
			for (JsonNode t : (ArrayNode)item.get("interfaces")) {
				this.setLinksOnSubitem((ObjectNode)t, "interfaces", requestUrl, false);
				if (t.has("datamodel")) {
					this.setLinksOnSubitem((ObjectNode)t.get("datamodel"), "datamodel", "datamodels", requestUrl, false);
				}
			}
		}
		return item;
	}
	
	
	private RestItemResponse saveApi(String uniqueId, ApiPojo setApi, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		AuthPojo authPojo = authInfoHelper.getAuth();
		Api api;
		
		if (uniqueId!=null) {
			api = this.getApi(authInfoHelper.getAuth(), uniqueId);
		} else {
			api = new Api();
			api.setInterfaces(new HashSet<>());
			api.setOwner(userService.findById(authPojo.getUserId()));
		}

		this.fillApi(api, setApi);
		apiService.saveApi(api);
		
		return this.getItemResponse(apiConverter.convert(api, locale), request, uniqueId!=null ? ApiActions.UPDATED : ApiActions.CREATED);
	}
			
	private void fillApi(Api saveApi, ApiPojo updateApi) {
		if (updateApi!=null) {
			saveApi.setInputDatamodelId(updateApi.getInputDatamodelId());
			saveApi.setOutputDatamodelId(updateApi.getOutputDatamodelId());
			saveApi.setLabel(updateApi.getLabel());
			saveApi.setMetadata(updateApi.getMetadata());
			
			if (updateApi.getInterfaces()!=null) {
				saveApi.setInterfaces( updateApi.getInterfaces().stream().map(i -> interfaceService.findByUniqueId(i.getUniqueId()).get()).collect(Collectors.toSet()) );
			} else {
				saveApi.getInterfaces().clear();
			}
			
		}
	}
	
	private Api getApi(AuthPojo authPojo, String reqUniqueId) throws ApiItemNotFoundException, ApiInsufficientPermissionsException {
		if (reqUniqueId==null) {
			throw new ApiItemNotFoundException(reqUniqueId, "api");
		}
		// TODO Check auth
		/*if (authPojo.getLevel()<100 && !authPojo.getUserUniqueId().equals(reqUniqueId)) {
			throw new ApiInsufficientPermissionsException();
		}*/
		Optional<Api> reqApi = apiService.findByUniqueId(reqUniqueId);
		if (!reqApi.isPresent()) {
			throw new ApiItemNotFoundException(reqUniqueId, "api");
		}
		return reqApi.get();
	}
	
}