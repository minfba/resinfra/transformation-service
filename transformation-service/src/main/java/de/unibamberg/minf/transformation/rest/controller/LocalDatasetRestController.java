package de.unibamberg.minf.transformation.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.LocalDataset;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.DatasetConverter;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemsResponse;
import de.unibamberg.minf.transformation.rest.pojo.DatasetPojo;
import de.unibamberg.minf.transformation.service.DatasetService;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Local Datasets", description = "API methods on local datasets")
@RestController
@RequestMapping("/api/localdataset")
public class LocalDatasetRestController extends BaseRestController<DatasetPojo> {
	
	@Autowired private DatasetService datasetService;
	@Autowired private DatasetConverter datasetConverter;
	
	public LocalDatasetRestController() {
		super("/api/localdataset");
	}

	@GetMapping("/")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("/api/localdataset");
		return null;
	}
	
	@GetMapping
    public RestItemsResponse getCollections(HttpServletRequest request, Locale locale) {
		RestItemsResponse response = new RestItemsResponse();
		List<DatasetPojo> datasets = datasetConverter.convert(datasetService.findAllLocal(), locale);
	
		response.setSize(datasets.size());
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItems(this.getItems(datasets, request.getRequestURL().toString()));
		
		return response;
	}
			
	@GetMapping("/{uniqueId}")
    public RestItemResponse getEndpoint(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		
		Optional<LocalDataset> i = datasetService.findLocalByUniqueId(uniqueId);
		if (i.isEmpty()) {
			throw new ApiItemNotFoundException("localDataset", uniqueId);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(datasetConverter.convert(i.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}	
}