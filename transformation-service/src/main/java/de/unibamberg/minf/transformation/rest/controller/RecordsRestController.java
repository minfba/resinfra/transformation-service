package de.unibamberg.minf.transformation.rest.controller;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import de.unibamberg.minf.processing.model.SerializableResource;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.transformation.backend.db.service.ResourceServiceImpl;
import de.unibamberg.minf.transformation.conversion.ResourceToJsonConverter;
import de.unibamberg.minf.transformation.execution.data.DatasetTransformationService;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.execution.exception.ApiRuntimeException;
import de.unibamberg.minf.transformation.model.Bag;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.DatamodelConverter;
import de.unibamberg.minf.transformation.rest.model.ErrorRestResponse;
import de.unibamberg.minf.transformation.rest.model.RestResponse;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.MappingService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;

@Tag(name = "Records", description = "API methods for accessing records")
@Slf4j
@RestController
@RequestMapping("/api/bags/{uniqueId}/rec")
public class RecordsRestController extends BaseRestController {	
	//@Autowired private BagService bagService;
	@Autowired private DatamodelService datamodelService;
	@Autowired private MappingService mappingService;
	//@Autowired private BagConverter bagConverter;
	@Autowired private DatamodelConverter datamodelConverter;
	@Autowired private ResourceServiceImpl resourceService;
	@Autowired private ObjectMapper objectMapper;
	@Autowired private DatasetTransformationService bagTransformationService;
		
	
	public RecordsRestController() {
		super("/api/bags");
	}
	
	/*
	 *	ToDo here?!
	 *		* Track transformation activity per dataset
	 *		* Trigger upon first use (automagically in the ResourceServiceImpl? bad idea?!) (or manually in UI) 
	 * 		* Just use here
	 */
	
	@GetMapping()
	public RestResponse identify(@PathVariable String uniqueId, Locale locale) {		
		RestResponse response = new RestResponse();
		//Bag b = bagService.findAndLoadByUniqueId(uniqueId);
		/*List<ExtendedMappingContainer> mappings = mappingService.getMappingsBySource(b.getDataset().getDatamodelId());
		if (mappings != null && !mappings.isEmpty()) {
			List<ExtendedDatamodelContainer> targetDatamodels = datamodelService.findByIds(mappings.stream().map(m -> m.getTargetSchemaId()).collect(Collectors.toList()));
			if (targetDatamodels!=null && !targetDatamodels.isEmpty()) {
				response.setTargetDatamodels(datamodelConverter.convert(targetDatamodels.stream().map(dm -> dm.getModel()).collect(Collectors.toList()), locale));
			}
			
		}
		response.setBag(bagConverter.convert(b, locale));*/
		return response;
	}

	@GetMapping({"/records", "/records/"})
    public RestResponse callApiGet(HttpServletRequest request, @PathVariable String uniqueId, @RequestParam(required=false) String format, @RequestParam(required=false) Integer limit, @RequestParam(defaultValue="0") int start, Locale locale) throws ApiRuntimeException {
		return this.getBagResponse(request, uniqueId, null, limit, start, locale);
	}
	
	@GetMapping("/records/{targetModelId}")
	public RestResponse callApiGetTransformed(HttpServletRequest request, @PathVariable String uniqueId, @PathVariable String targetModelId, @RequestParam(required=false) String format, @RequestParam(required=false) Integer limit, @RequestParam(defaultValue="0") int start, Locale locale) throws ApiRuntimeException  {
		return this.getBagResponse(request, uniqueId, null, limit, start, locale);
		// TODO: This still needs to happen
		//List<Resource> transformed = bagTransformationService.transformResources(b.getDataset().getDatamodelId(), targetModelId, resources);
	}
	
    public RestResponse getBagResponse(HttpServletRequest request, String uniqueId, String targetDatamodelId, Integer limit, int start, Locale locale) throws ApiRuntimeException {
		RestResponse response;
		try {
			response = this.setupRestResponse(uniqueId, this.getActualLimit(limit), start, locale);

			/*if (targetDatamodelId!=null && !this.getIsDatamodelAvailable(response.getBag().getLinkedDataset().getDatamodelId(), targetDatamodelId)) {
				throw new ApiRuntimeException("~de.unibamberg.minf.transformation.errors.bag_mapping_not_available");
			}
			
			//response.setSize(resourceService.countByDatasetIdAndDatamodelId(response.getBag().getLinkedDataset().geti(), targetDatamodelId));
			response.setLinks(this.getLinks(request.getRequestURL().toString(), response.getSize(), response.getLimit(), response.getStart()));
			
			if (response.getSize()>0) {*/
				/*List<String> resources = resourceService.findContainedByDatasetIdAndDatamodelId(response.getBag().getLinkedDataset().getId(), targetDatamodelId, response.getStart(), response.getLimit()); 
				if (resources!=null) {
					Resource res;
					// TODO: This requires parsing all JSON data, which is slow and needs to be changed
					ArrayNode array = objectMapper.createArrayNode();
					for (String resource : resources) {
						res = objectMapper.readValue(resource, SerializableResource.class);
						array.add(ResourceToJsonConverter.convertResource(res));
					}
					response.setResponse(array);
				}*/
			//}
			
			return response;
		} catch (ApiRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new ApiRuntimeException();
		}
	}
	
	private boolean getIsDatamodelAvailable(String sourceDatamodelId, String targetDatamodelId) {
		List<ExtendedMappingContainer> mappings = mappingService.getMappingsBySource(sourceDatamodelId);
		if (mappings!=null) {
			return mappings.stream().anyMatch(m -> m.getTargetSchemaId().equals(targetDatamodelId));
		}
		return false;
	}
	
	private RestResponse setupRestResponse(String uniqueId, int limit, int start, Locale locale) throws NoSuchMessageException, ApiRuntimeException {
  	  RestResponse response = new RestResponse();
        /*Bag b = bagService.findAndLoadByUniqueId(uniqueId);
        if (b==null) {
            throw new ApiItemNotFoundException("bag", uniqueId);
        }
        response.setBag(bagConverter.convert(b, locale));
        response.setStart(start);
        response.setLimit(limit);*/
        
        return response;
	}
}
