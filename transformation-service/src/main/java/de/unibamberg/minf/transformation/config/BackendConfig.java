package de.unibamberg.minf.transformation.config;

import javax.sql.DataSource;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ConfigurationProperties(prefix="transformation.db")
@ComponentScan("de.unibamberg.minf.transformation.backend.db.service")
@EnableJpaRepositories(
		basePackages = {"de.unibamberg.minf.transformation.backend.db", "de.unibamberg.minf.transformation.dao"},
		entityManagerFactoryRef = "transformationEntityManagerFactory",
		transactionManagerRef = "transformationTransactionManager")
public class BackendConfig extends PersistenceConfig {
	@Bean
	public DataSource transformationDataSource() {
	    return DataSourceBuilder
	        .create()
		        .driverClassName(getDriverClassName())
		        .url(String.format("%s://%s:%s/%s", this.getProtocol(), this.getHost(), this.getPort(), this.getDatabase()))
		        .username(getUsername())
		        .password(getPassword())
	        .build();
	}
	
	@Override
	protected String[] getPackagesToScan() {
		return ArrayUtils.addAll(super.getPackagesToScan(), 
			"de.unibamberg.minf.transformation.backend.db",
			"de.unibamberg.minf.dme.migration.model"
		);
	}
	
	@Override
	protected String[] getMappingResources() {
		return ArrayUtils.addAll(super.getMappingResources(), 
				"orm.xml"
			);
	}
}
