package de.unibamberg.minf.transformation.controller;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.rest.converter.DatasetConverter;
import de.unibamberg.minf.transformation.rest.pojo.ApiPojo;
import de.unibamberg.minf.transformation.rest.pojo.DatasetPojo;
import de.unibamberg.minf.transformation.service.AccessService;

@Controller
@RequestMapping("/access")
public class AccessController extends BaseController {	
	
	@Autowired private AccessService accessService;
	@Autowired private DatasetConverter datasetConverter;
	
	public AccessController() {
		super("access");
	}

	@GetMapping("")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("access/");
		return null;
	}
	
	@GetMapping("/")
	public String getHome(Model model, Locale locale) {
		return "access/list";
	}
	
	@GetMapping("/{uniqueId}/{datasetId}/edit")
	public String getEditForm(@PathVariable String uniqueId, @PathVariable String datasetId, Model model, Locale locale) throws ApiItemNotFoundException {
		Optional<Datasource> source = accessService.findDatasourceByUniqueId(uniqueId);
		Optional<DatasetPojo> dataset = Optional.empty();
		if (source.isPresent() && source.get().getDatasets()!=null) {
			for (Dataset ds : source.get().getDatasets()) {
				if (ds.getUniqueId().equals(datasetId)) {
					dataset = Optional.ofNullable(datasetConverter.convert(ds, locale));
					break;
				}
			}
		}
		if (dataset.isEmpty()) {
			throw new ApiItemNotFoundException("dataset", datasetId); 
		}
		model.addAttribute("dataset", dataset.get());
		model.addAttribute("actionPath", "/api/datasources/" + uniqueId + "/dataset/" + datasetId);
		return "access/edit_datasource";
	}
}
