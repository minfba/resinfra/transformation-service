package de.unibamberg.minf.transformation.execution.data;

import java.util.List;

import de.unibamberg.minf.processing.model.base.Resource;

public interface DatasetTransformationService {
	public List<Resource> transformResources(String sourceDatamodelId, String targetDatamodelId, List<Resource> resources);
}
