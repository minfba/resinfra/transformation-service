package de.unibamberg.minf.transformation.rest.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.unibamberg.minf.transformation.execution.exception.ApiInsufficientPermissionsException;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.PersistedAccessToken;
import de.unibamberg.minf.transformation.model.PersistedUser;
import de.unibamberg.minf.transformation.rest.controller.base.BaseUserRestController;
import de.unibamberg.minf.transformation.rest.converter.AccessTokenConverter;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemsResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse.ApiActions;
import de.unibamberg.minf.transformation.rest.pojo.AccessTokenPojo;
import de.unibamberg.minf.transformation.service.AccessTokenService;
import eu.dariah.de.dariahsp.web.model.AuthPojo;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Access Tokens", description = "API methods on user access tokens")
@RestController
@RequestMapping("/api/users/{userUID}/tokens")
public class AccessTokenRestController extends BaseUserRestController<AccessTokenPojo> {
	private static final String ITEM_TYPE = "accessToken";
	
	@Autowired private AccessTokenConverter tokenConverter;
	@Autowired private AccessTokenService tokenService;
	
	public AccessTokenRestController() {
		super("/api/users");
	}
	
	@GetMapping
	public RestItemsResponse getUserAccessTokens(@PathVariable String userUID, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		AuthPojo authPojo = authInfoHelper.getAuth();
		PersistedUser user = this.checkCanAccessUser(authPojo, userUID);
		
		List<AccessTokenPojo> pojos = tokenConverter.convert(tokenService.findByUserId(user.getId()), locale); 
		
		RestItemsResponse resp = new RestItemsResponse();
		resp.setSize(pojos.size());
		resp.setItems(this.getItems(pojos, request.getRequestURL().toString()));
		resp.setLinks(this.getLinks(request.getRequestURL().toString()));
		return resp;
	}
	
	@GetMapping("/{tokenUID}")
	public RestItemResponse getUserAccessToken(@PathVariable String userUID, @PathVariable String tokenUID, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		return this.getItemResponse(this.getToken(userUID, tokenUID), request, ApiActions.GET, locale);
	}
	
	@DeleteMapping("/{tokenUID}")
	public RestItemResponse deleteUserAccessToken(@PathVariable String userUID, @PathVariable String tokenUID, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		PersistedAccessToken token = this.getToken(userUID, tokenUID);
		tokenService.deleteById(token.getId());	
		
		
		return this.getItemResponse(token, request, ApiActions.DELETED, locale);
	}
	
	@PostMapping("/{tokenUID}")
	public RestItemResponse updateUserAccessToken(@PathVariable String userUID, @PathVariable String tokenUID, @Valid @RequestBody AccessTokenPojo updatedToken, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		PersistedAccessToken token = this.getToken(userUID, tokenUID);

		this.fillToken(token, updatedToken);
		tokenService.saveAccessToken(token);
		
		return this.getItemResponse(token, request, ApiActions.UPDATED, locale);
	}
		
	@PostMapping
	public RestItemResponse saveNewUserAccessToken(@PathVariable String userUID, @Valid @RequestBody AccessTokenPojo newToken, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		AuthPojo authPojo = authInfoHelper.getAuth();
		PersistedUser user = this.checkCanAccessUser(authPojo, userUID);
		
		PersistedAccessToken token = new PersistedAccessToken();
		this.fillToken(token, newToken);
		token.setUser(user);
		
		tokenService.saveAccessToken(token);
		
		return this.getItemResponse(token, request, ApiActions.CREATED, locale);
	}
			
	private RestItemResponse getItemResponse(PersistedAccessToken token, HttpServletRequest request, ApiActions created, Locale locale) {
		return super.getItemResponse(tokenConverter.convert(token, locale), request, created);
	}

	private PersistedAccessToken getToken(String userUID, String tokenUID) throws ApiItemNotFoundException, ApiInsufficientPermissionsException {
		AuthPojo authPojo = authInfoHelper.getAuth();
		PersistedUser user = this.checkCanAccessUser(authPojo, userUID);
		
		PersistedAccessToken token = tokenService.findByUniqueIdAndUserId(tokenUID, user.getId());
		if (token==null) {
			throw new ApiItemNotFoundException(ITEM_TYPE, tokenUID);			
		}
		return token;
	}
	
	private void fillToken(PersistedAccessToken saveToken, AccessTokenPojo updateToken) {
		if (updateToken!=null) {
			saveToken.setAllowedAdresses(updateToken.getAllowedAdresses());
			saveToken.setExpires(updateToken.getExpires());
			saveToken.setName(updateToken.getName());
			saveToken.setWriteAccess(updateToken.isWriteAccess());
		}
	}
}
