package de.unibamberg.minf.transformation.config;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.uniba.minf.core.rest.client.security.SecurityTokenIssuer;
import de.uniba.minf.core.rest.client.security.SecurityTokenIssuerImpl;
import de.uniba.minf.core.rest.client.security.memarc.MemarcSecurityTokenIssuer;
import de.unibamberg.minf.transformation.config.model.AccessChain;
import de.unibamberg.minf.transformation.config.model.AccessChain.AccessTypes;
import de.unibamberg.minf.transformation.config.model.FileProcessingChain;
import de.unibamberg.minf.transformation.config.nested.RemoteTokensConfigProperties;
import de.unibamberg.minf.transformation.crawling.DatabaseCleaner;
import de.unibamberg.minf.transformation.crawling.PersistingCrawlManager;
import de.unibamberg.minf.transformation.crawling.RepetitiveFileCrawlerImpl;
import de.unibamberg.minf.transformation.exceptions.ConfigurationException;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@EnableScheduling
@Configuration
@ConfigurationProperties(prefix = "crawling")
public class CrawlingConfig extends BaseCrawlingConfig  {
	
	@Autowired private SecurityConfig securityConfig;
	
	@Bean
	public SecurityTokenIssuer securityTokenIssuer(ObjectMapper objectMapper) throws ConfigurationException {
		SecurityTokenIssuerImpl issuer = new SecurityTokenIssuerImpl();
		if (securityConfig.getRemoteTokens()!=null) {
			for (RemoteTokensConfigProperties remoteTokensConfig : securityConfig.getRemoteTokens()) {
				if (remoteTokensConfig.getTokenRetrieval()!=null && remoteTokensConfig.getTokenRetrieval().equals("memarc")) {
					MemarcSecurityTokenIssuer memarcIssuer = new MemarcSecurityTokenIssuer();
					memarcIssuer.setObjectMapper(objectMapper);
					memarcIssuer.setApiServer(remoteTokensConfig.getTokenApiUrl());
					memarcIssuer.setUsername(remoteTokensConfig.getUsername());
					memarcIssuer.setPassword(remoteTokensConfig.getPassword());
					issuer.putTokenIssuer(remoteTokensConfig.getApiUrlRegex(), memarcIssuer);
				} else {
					throw new ConfigurationException("Unknown remoteTokens retrieval type: " + remoteTokensConfig.getTokenRetrieval()==null ? "NULL" : remoteTokensConfig.getTokenRetrieval());
				}
			}
		}
		return issuer;
	}
	
	
	@Bean
	@Override
	@Scope("prototype") // To prevent modifications of singleton list somewhere
	public List<AccessChain> accessChains() {
		List<AccessChain> accessChains = super.accessChains();
		accessChains.add(new AccessChain("REST API", "???", AccessTypes.ENDPOINT));
		return accessChains;
	}
    
	@Bean
	@Override
	@Scope("prototype")  // To prevent modifications of singleton list somewhere
	public List<FileProcessingChain> fileProcessingChains() {
		List<FileProcessingChain> fileProcessingChains = super.fileProcessingChains();
		fileProcessingChains.add(new FileProcessingChain("XML", "fileUnpacker,fileUnarchiver,databaseCleaner,xmlBatchFileProcessor"));
		fileProcessingChains.add(new FileProcessingChain("JSON", "fileUnpacker,fileUnarchiver,databaseCleaner,jsonBatchFileProcessor"));
		fileProcessingChains.add(new FileProcessingChain("YAML", "fileUnpacker,fileUnarchiver,databaseCleaner,yamlBatchFileProcessor"));
		fileProcessingChains.add(new FileProcessingChain("CSV", "fileUnpacker,fileUnarchiver,databaseCleaner,csvBatchFileProcessor"));
		fileProcessingChains.add(new FileProcessingChain("TSV", "fileUnpacker,fileUnarchiver,databaseCleaner,tsvBatchFileProcessor"));
		fileProcessingChains.add(new FileProcessingChain("TEXT", "fileUnpacker,fileUnarchiver,databaseCleaner,textBatchFileProcessor"));
		return fileProcessingChains;
	}
	
	@Bean
	@Scope("prototype")
	public DatabaseCleaner databaseCleaner() {
		return new DatabaseCleaner();
	}
	
	@Bean
	@Override
	@Primary
	public PersistingCrawlManager crawlManager(TransformationConfig mainConfig, List<AccessChain> accessChains, List<FileProcessingChain> fileProcessingChains) {
		PersistingCrawlManager crawlManager = new PersistingCrawlManager();
		crawlManager.setMaxPoolSize(this.getMaxThreads());
		crawlManager.setBaseDownloadPath(mainConfig.getPaths().getDownloads());
		crawlManager.setAccessChains(accessChains);
		crawlManager.setFileProcessingChains(fileProcessingChains);
		return crawlManager;
	}
	
	@Bean
	@Scope("prototype")
	public RepetitiveFileCrawlerImpl fileCrawler() {
		RepetitiveFileCrawlerImpl fileCrawler = new RepetitiveFileCrawlerImpl();
		Map<String, String> fileProcessingServiceMap = new HashMap<>();
		fileProcessingServiceMap.put("XML", "xmlStringProcessor");
		fileProcessingServiceMap.put("JSON", "jsonProcessingService");
		fileProcessingServiceMap.put("YAML", "yamlProcessingService");
		fileProcessingServiceMap.put("CSV", "csvStringProcessor");
		fileProcessingServiceMap.put("TSV", "tsvStringProcessor");
		fileProcessingServiceMap.put("TEXT", "textStringProcessor");
		fileCrawler.setFileProcessingServiceMap(fileProcessingServiceMap);
		
		return fileCrawler;
	}
}
