package de.unibamberg.minf.transformation.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.unibamberg.minf.transformation.automation.CollectionBagSyncService;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Configuration
@ConfigurationProperties(prefix = "api")
public class ApiConfig extends BaseApiConfig {
	
	/*@Bean
	@Override
	public CollectionBagSyncService collectionSyncService() {
		CollectionBagSyncService collectionSyncService = new CollectionBagSyncService();
		collectionSyncService.setAutomationEnabled(getColreg().isAutosync());
		collectionSyncService.setAutocrawlNewDatasets(getColreg().isAutocrawlNewDatasets());
		return collectionSyncService;
	}*/
}
