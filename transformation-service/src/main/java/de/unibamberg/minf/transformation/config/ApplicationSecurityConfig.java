package de.unibamberg.minf.transformation.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import de.unibamberg.minf.transformation.rest.auth.UserAccessTokenAuthenticator;
import eu.dariah.de.dariahsp.authentication.UserTokenAuthenticator;
import eu.dariah.de.dariahsp.config.web.AuthInfoConfigurer;

@Configuration
@ConfigurationProperties(prefix = "auth")
@Import({AuthInfoConfigurer.class})
public class ApplicationSecurityConfig extends eu.dariah.de.dariahsp.config.SecurityConfig {
		
	@Bean
	public UserTokenAuthenticator userTokenAuthenticator() {
		return new UserAccessTokenAuthenticator();
	}
}
