package de.unibamberg.minf.transformation.controller;

import java.io.IOException;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/datamodels")
public class DatamodelsController extends BaseController {
	public DatamodelsController() {
		super("datamodels");
	}

	@GetMapping("")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("datamodels/");
		return null;
	}
	
	@GetMapping("/")
	public String getHome(Model model, Locale locale) {
		return "datamodels/list";
	}
}
