package de.unibamberg.minf.transformation.execution.exception;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ApiRuntimeException extends Exception {
    private static final long serialVersionUID = 6785131852352003028L;

    private static final String DEFAULT_MESSAGE_CODE = "~de.unibamberg.minf.transformation.errors.internal_server_error";
    
    private final HttpStatus status;
    private final Object[] messageArgs;
    
    public ApiRuntimeException() {
    	super(DEFAULT_MESSAGE_CODE);
    	this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.messageArgs = new Object[0];
    }
    
    public ApiRuntimeException(String code) {
        super(code);
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.messageArgs = new Object[0];
    }
    
    public ApiRuntimeException(String code, Object[] messageArgs) {
        super(code);
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.messageArgs = messageArgs;
    }
    
    public ApiRuntimeException(String code, int status) {
    	this(code, HttpStatus.valueOf(status));
    }
    
    public ApiRuntimeException(String code, int status, Object[] messageArgs) {
        this(code, HttpStatus.valueOf(status), messageArgs);
    }
    
    public ApiRuntimeException(String code, HttpStatus status) {
        super(code);
        this.status = status;
        this.messageArgs = new Object[0];
    }
    
    public ApiRuntimeException(String code, HttpStatus status, Object[] messageArgs) {
        super(code);
        this.status = status;
        this.messageArgs = messageArgs;
    }
        
    public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
    	if (messageSource==null) {
    		return this.getMessage();
    	}
    	return messageSource.getMessage(this.getMessage(), this.getMessageArgs(), locale);
    }
}
