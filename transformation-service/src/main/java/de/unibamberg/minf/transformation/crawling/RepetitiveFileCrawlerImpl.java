package de.unibamberg.minf.transformation.crawling;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.unibamberg.minf.core.util.Stopwatch;
import de.unibamberg.minf.dme.model.base.Nonterminal;
import de.unibamberg.minf.mapping.model.MappingExecGroup;
import de.unibamberg.minf.mapping.service.MappingExecutionService;
import de.unibamberg.minf.processing.consumption.CollectingResourceConsumptionServiceImpl;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.processing.model.helper.ResourceHelper;
import de.unibamberg.minf.processing.service.base.BaseResourceProcessingServiceImpl;
import de.unibamberg.minf.transformation.config.TransformationConfig;
import de.unibamberg.minf.transformation.config.nested.DataProvisioningConfigProperties;
import de.unibamberg.minf.transformation.config.nested.PathsConfigProperties;
import de.unibamberg.minf.transformation.crawling.CrawlHelper;
import de.unibamberg.minf.transformation.crawling.files.FileDownloader;
import de.unibamberg.minf.transformation.crawling.model.FileRequest;
import de.unibamberg.minf.transformation.crawling.model.FileRequestPair;
import de.unibamberg.minf.transformation.dao.db.CollectionDao;
import de.unibamberg.minf.transformation.execution.data.DatasetExecutionContext;
import de.unibamberg.minf.transformation.helper.MappingHelpers;
import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import de.unibamberg.minf.transformation.model.base.AccessOperation;
import de.unibamberg.minf.transformation.service.CollectionService;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.MappingService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RepetitiveFileCrawlerImpl extends FileDownloader implements ApplicationContextAware {
	private ApplicationContext appContext;
		
	@Autowired private DataProvisioningConfigProperties provisioningConfig;
	@Autowired private PathsConfigProperties pathsConfig;
	@Autowired protected DatamodelService datamodelService;
	@Autowired private MappingService mappingService;
	@Autowired private MappingExecutionService mappingExecutionService;
	
	@Autowired private CollectionService collectionService;
	
	@Autowired private ObjectMapper objectMapper;
	
	@Getter @Setter private Map<String, String> fileProcessingServiceMap;
	@Getter @Setter private int politenessTimespan = 200; // 1 call per 200ms (to same endpoint)
	
	private Access access;
	private String collectionMetadata;
	private AccessOperation accessOperation;

	private ExtendedDatamodelContainer sourceDatamodel;
	private ExtendedDatamodelContainer targetDatamodel;
	private ExtendedMappingContainer mapping;
	
	private BaseResourceProcessingServiceImpl processingService;
	private MappingExecGroup mExecGroup;
	private CollectingResourceConsumptionServiceImpl sourceResCollector = null;
	private CollectingResourceConsumptionServiceImpl targetResCollector = null;
	
	private List<String> processedUrls;
	private Queue<FileRequest> downloadUris;
	
	Stopwatch swPoliteness = new Stopwatch();

	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}
		
	@Override
	public void init(Access access, AccessOperation op, ExtendedDatamodelContainer sc) {
		super.init(access, op, sc);
		
		this.access = access;
		this.collectionMetadata = collectionService.findById(access.getCollection().getId()).getCollectionMetadata();
		
		if (access.getAccessHeaders()!=null && !access.getAccessHeaders().isEmpty()) {
			this.requestHeaders = access.getAccessHeaders().stream().map(h -> new FileRequestPair(h.getParam(), h.getValue())).collect(Collectors.toList());
		}
	
		this.accessOperation = op;
		
		this.processedUrls = new ArrayList<>();
		this.downloadUris = new LinkedList<>();
		
		this.initCrawlModels(sc);
		if (mapping==null) {
			return;
		}
		this.initServices();
		if (processingService==null || mExecGroup==null) {
			return;
		}
	}
	
	@Override
	public void download() {
		swPoliteness.start();
		if (!downloadUris.isEmpty()) {
			this.downloadNextFile(false);
			return;
		}		
		
		super.download();
		if (this.mExecGroup==null) {
			logger.debug("Resumptive crawling not applicable -> crawl done");
			this.registerFinished();
		} else {
			this.reset();
			try {
				this.processDownloadedFile();
				logger.debug("Processing next downloadURI: {}, method: {}, requestBody: {}", this.downloadURI, this.method, this.requestBody==null||this.requestBody.length==0);
			} catch (IOException e) {
				logger.error("Failed to setup and download next file URI", e);
				this.registerError();
			}
			this.addDownloadUris(targetResCollector.getResources());
			this.setupAndDownloadNextFile(true);
		}
	}
	
	long currentSize = 0;
	long overallSize = 0;
	
	protected void registerFinished() {
		if (this.getListener() != null) {
			this.getListener().finished(this.getUuid());
		}
	}
		
	protected void registerError() {
		if (this.getListener() != null) {
			this.getListener().error(this.getUuid());
		}
	}
	
	@Override
	protected void updateFileSize(long size) {
		currentSize = size;
		overallSize += size;
		if (this.getListener() != null) {
    		this.getListener().updateSize(this.getUuid(), overallSize);
		}
	}
	
	@Override
	protected void updateFileProcessed(long position) {
		if (this.getListener() != null) {
    		this.getListener().processed(this.getUuid(), overallSize - currentSize + position);
		}
	}
	
	@Override
	protected void registerFileFinished() {
		currentSize = 0;
	}
	
	@Override
	protected void registerFileError() {}

	
	private void initCrawlModels(ExtendedDatamodelContainer sc) {
		mapping = null;
		
		// Crawl model available
		if (provisioningConfig.getProvisioningModelId()==null) {
			logger.warn("No GS: Repetitive Crawl Model configured; repetitive file crawling unavailable");
			return;
		}
		
		// Dedicated access model or datamodel?
		if (access.getAccessModelId()!=null) {
			logger.debug("Dedicated access model configured: {}", access.getAccessModelId());
			sourceDatamodel = datamodelService.findById(access.getAccessModelId());
			if (sourceDatamodel==null) {
				logger.warn("Dedicated access model configured but not available (Sync with DME required?)");
				return;
			}
		} else {
			logger.debug("No dedicated access model, using datamodel: {}", sc.getModel().getId());
			sourceDatamodel = sc;
		}
		
		// Crawl model
		targetDatamodel = datamodelService.findById(provisioningConfig.getProvisioningModelId());
		if (targetDatamodel==null) {
			logger.warn("Crawl model configured but not available (Sync with DME required?)");
			return;
		}
		
		// Mapping between source (access or data) model and target crawl model
		mapping = mappingService.getMappingBySourceAndTarget(sourceDatamodel.getModel().getId(), provisioningConfig.getProvisioningModelId());
		if (mapping==null) {
			logger.info("No mapping to GS: Repetitive Crawl Model modeled; repetitive file crawling not configured");
		}
	}
	
	private void initServices() {
		processingService = null;
		
		// Setup everything that is needed for file processing
		if (!fileProcessingServiceMap.containsKey(access.getFileType())) {
			logger.warn("Endpoint file type unsupported by repetitive crawling: {}", access.getFileType());
			return;
		}
		try {
			processingService = BaseResourceProcessingServiceImpl.class.cast(appContext.getBean(fileProcessingServiceMap.get(access.getFileType())));
			processingService.setSchema(sourceDatamodel.getModel());
			processingService.setRoot((Nonterminal)sourceDatamodel.getOrRenderElementHierarchy());
			
			sourceResCollector = new CollectingResourceConsumptionServiceImpl();
			processingService.addConsumptionService(sourceResCollector);
			
		} catch (Exception e) {
			logger.warn("No supporting processing service available for file type: {}", access.getFileType());
			return;
		}		
		
		// Mapping execution
		mExecGroup = this.buildMappingExecutionGroup(mapping, targetDatamodel);
		targetResCollector = new CollectingResourceConsumptionServiceImpl();
		mappingExecutionService.addConsumptionService(targetResCollector);
		if (mExecGroup!=null && (mExecGroup.getConcepts()==null || mExecGroup.getConcepts().isEmpty())) {
			mExecGroup = null;
		}
	}
		
	private MappingExecGroup buildMappingExecutionGroup(ExtendedMappingContainer m, ExtendedDatamodelContainer dcTarget) {
		if (m==null) {
			return null;
		}
		ExtendedDatamodelContainer dcSource = datamodelService.findById(m.getSourceSchemaId());
		return MappingHelpers.buildMappingExecutionGroup(m, dcSource, dcTarget);
	}
	
	private void reset() {
		if (sourceResCollector != null) {
			sourceResCollector.setResources(null);
		}
		if (targetResCollector != null) {
			targetResCollector.setResources(null);
		}
	}
		
	private void processDownloadedFile() throws IOException {
		String lastDownloadOutput = this.getOutputFilenames().get(this.getOutputFilenames().size()-1);
		
		logger.debug("Processing downloaded file: {}", lastDownloadOutput);
		try {
			processingService.setInputStream(new FileInputStream(lastDownloadOutput));
			processingService.setExecutionContext(new DatasetExecutionContext(this.pathsConfig.getData(), "" + access.getId(), this.getSessionData(collectionMetadata)));
			processingService.init();
			processingService.run();
		} catch (ProcessingConfigException | FileNotFoundException e) {
			logger.error("Exception while processing", e);
		}
		
		if (sourceResCollector.getResources()!=null && !sourceResCollector.getResources().isEmpty()) {
			try {
				mappingExecutionService.init(mExecGroup, sourceResCollector.getResources());
				mappingExecutionService.run();
			} catch (ProcessingConfigException e) {
				logger.error("Failed to initialize MappingExecutionService", e);
			}
		}
		logger.debug("Mapping execution transformed {} to {} resources", 
				sourceResCollector.getResources()==null? 0 : sourceResCollector.getResources().size(), 
				targetResCollector.getResources()==null? 0 : targetResCollector.getResources().size());
	}
	
	
	public void addDownloadUris(List<Resource> crawlDefResources) {
		List<Resource> resParam;
		List<Resource> resRemoveParam;
		List<Resource> resBodies;
		String newUrl;
		String postUrl;
		FileRequest fileReq;
		if (crawlDefResources!=null) {
			for (Resource r : crawlDefResources) {
				resParam = ResourceHelper.findRecursive(r, "GET.Param");
				resRemoveParam = ResourceHelper.findRecursive(r, "GET.RemoveParam");
				resBodies = ResourceHelper.findRecursive(r, "POST.RequestBody");
				newUrl = CrawlHelper.renderAccessUrl(this.access.getUrl(), this.access.getParams(), resParam, resRemoveParam);
				
				if (resBodies.isEmpty() && !processedUrls.contains(newUrl)) {
					processedUrls.add(newUrl);
					downloadUris.add(new FileRequest(newUrl, access.getHttpMethod()));
				} else {
					for (Resource resBody : resBodies) {
						postUrl = newUrl + "/" + resBody.getValue();
						if (!processedUrls.contains(postUrl)) {
							processedUrls.add(postUrl);
							fileReq = new FileRequest(newUrl, access.getHttpMethod(), resBody.getValue().toString(), this.requestHeaders);
							fileReq.setFormat(access.getFileType());
							downloadUris.add(fileReq);
						}
					}
				}
			}
		}
	}
	
	private void downloadNextFile(boolean sleep) {
		this.downloadURI = null;
		try {
			FileRequest req = downloadUris.remove();
			this.downloadURI = new URL(req.getUri()).toURI();
			this.method = req.getMethod();
			this.requestBody = req.getRequestBody();
			this.requestHeaders = req.getRequestHeaders();
			if (req.getFormat()!=null) {
				this.format = req.getFormat();
			}
			
			logger.debug("Processing next downloadURI: {}, method: {}, requestBody: {}", this.downloadURI, this.method, this.requestBody==null||this.requestBody.length==0);
		} catch (MalformedURLException | URISyntaxException e) {
			logger.error("Failed to setup and download next file URI", e);
			this.registerError();
		}
		
		// If next inputURI is setup -> download, else continue with next download URI
		if (this.downloadURI!=null) {
			this.continueDownload(sleep);
		} else {
			this.setupAndDownloadNextFile(sleep);
		}
	}
	
	private void setupAndDownloadNextFile(boolean sleep) {
		if (downloadUris.isEmpty()) {
			logger.debug("Download queue is empty -> file crawling is complete");
			this.registerFinished();
			return;
		}
		
		try {
			this.setupPaths(this.accessOperation);
		} catch (IOException e) {
			logger.error("Failed to setup paths for crawl", e);
			this.registerError();
			return;
		}
		
		this.downloadNextFile(sleep);
	}
	
	private void continueDownload(boolean sleep) {
		long sleepTs = this.politenessTimespan - swPoliteness.getElapsedTime();
		if (sleep && sleepTs > 0) {
			if (logger.isDebugEnabled()) {
				logger.debug(String.format("Crawl sleeping %sms due to politeness setting (%sms)", sleepTs, this.getPolitenessTimespan()));
			}
			try {
				Thread.sleep(sleepTs);
				this.download();
			} catch (InterruptedException e) {
				logger.error("Thead.sleep interrupted", e);
				this.registerError();
				Thread.currentThread().interrupt();
			}
		} else {
			this.download();
		}
	}

	// TODO This is redundant code to CrawlManagerImpl 
	private JsonNode getSessionData(String collectionMetadata) {
		if (collectionMetadata==null || collectionMetadata.isBlank()) {
			return MissingNode.getInstance();
		}
		try {
			ObjectNode cNode = objectMapper.createObjectNode();
			cNode.set("collection", objectMapper.readTree(collectionMetadata));
			return cNode;
		} catch (Exception e) {
			log.error("Failed to read collection metadata to session data", e);
			return MissingNode.getInstance();
		}
	}
	
}