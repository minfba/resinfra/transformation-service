package de.unibamberg.minf.transformation.execution;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.processing.consumption.ResourceConsumptionService;
import de.unibamberg.minf.processing.model.base.Resource;


@Component
@Scope("prototype")
public class CollectingResourceProcessingService implements ResourceConsumptionService {

	List<Resource> resources = null;
	
	@Override
	public void init(String datamodelId) {
		resources = new ArrayList<>();
	}
	
	@Override
	public boolean consume(Resource res) {
		return resources.add(res);
	}

	@Override
	public int commit() {
		return resources==null ? 0 : resources.size();
	}

}