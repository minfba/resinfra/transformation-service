package de.unibamberg.minf.transformation.execution.data;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.fasterxml.jackson.databind.JsonNode;

import de.unibamberg.minf.gtf.context.ExecutionContext;
import lombok.Getter;

@Getter
public class DatasetExecutionContext implements ExecutionContext {
	public static final String DATASET_EXECUTION_DATA_FOLDER = "dataset_execution_data";
	
	private final String pathPrefix;
	private final String datasetId;
	private final String workingDir;
	private final JsonNode sessionData;
	
	public DatasetExecutionContext(String pathPrefix, String datasetId, JsonNode sessionData) throws IOException {
		this.pathPrefix = pathPrefix;
		this.datasetId = datasetId;
		this.sessionData = sessionData;
		
		this.workingDir = pathPrefix + File.separator + this.datasetId + File.separator + DATASET_EXECUTION_DATA_FOLDER + File.separator;
		File workingDir = new File(this.workingDir); 
		if (!workingDir.exists()) {
			FileUtils.forceMkdir(workingDir);
		}
	}
}
