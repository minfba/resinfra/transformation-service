package de.unibamberg.minf.transformation.config;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.minf.gtf.DescriptionEngineImpl;
import de.unibamberg.minf.gtf.MainEngineImpl;
import de.unibamberg.minf.gtf.TransformationEngineImpl;
import de.unibamberg.minf.gtf.commands.CommandDispatcher;
import de.unibamberg.minf.gtf.extensions.nlp.commands.LanguageDispatcher;
import de.unibamberg.minf.gtf.extensions.nlp.commands.NlpMainDispatcher;
import de.unibamberg.minf.gtf.extensions.nlp.stanford.LexicalizedParserWrapper;
import de.unibamberg.minf.gtf.extensions.nlp.stanford.MaxentTaggerWrapper;
import de.unibamberg.minf.gtf.extensions.nlp.stanford.NerClassifierWrapper;
import de.unibamberg.minf.gtf.extensions.nlp.stanford.StanfordProcessor;
import de.unibamberg.minf.gtf.transformation.processing.GlobalCommandDispatcher;
import de.unibamberg.minf.transformation.config.nested.NlpConfigProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@Configuration
@ConfigurationProperties(prefix = "processing")
public class ProcessingConfig extends BaseProcessingConfig {
	
	private NlpConfigProperties[] nlp;
	
	@Bean
	@Scope("prototype")
	public MainEngineImpl mainEngine(ObjectMapper objectMapper) {
		MainEngineImpl mainEngine = new MainEngineImpl();
		mainEngine.setDescriptionEngine(descriptionEngine());
		mainEngine.setTransformationEngine(transformationEngine(objectMapper));
		return mainEngine;
	}
		
	@Bean
	@Scope("prototype")
	public DescriptionEngineImpl descriptionEngine() {
		DescriptionEngineImpl descriptionEngine = new DescriptionEngineImpl();
		descriptionEngine.setGrammarsRoot(new File(mainConfig.getPaths().getGrammars()));
		descriptionEngine.setParseErrorDumpPath(mainConfig.getPaths().getParseErrors());
		return descriptionEngine;
	}
	
	@Bean
	@Scope("prototype")
	public TransformationEngineImpl transformationEngine(ObjectMapper objectMapper) {
		TransformationEngineImpl transformationEngine = new TransformationEngineImpl();
		transformationEngine.setObjMapper(objectMapper);
		transformationEngine.setCommandDispatcher(commandDispatcher());
		return transformationEngine;
	}
	
	@Bean
	@Scope("prototype")
	public GlobalCommandDispatcher commandDispatcher() {
		GlobalCommandDispatcher commandDispatcher = new GlobalCommandDispatcher();
		Map<String, CommandDispatcher> commandDispatcherMap = new HashMap<>();
		commandDispatcherMap.put("CORE", coreCommandsDispatcher());
		commandDispatcherMap.put("FILE", fileCommandsDispatcher());
		commandDispatcherMap.put("WIKI", wikiCommandsDispatcher());
		commandDispatcherMap.put("PERSON", personCommandsDispatcher());
		commandDispatcherMap.put("VOCABULARY", vocabularyCommandsDispatcher());
		commandDispatcherMap.put("GEO", geoCommandsDispatcher());
		commandDispatcherMap.put("NLP", nlpDispatcher());
		
		commandDispatcher.setCommandDispatchers(commandDispatcherMap);
		return commandDispatcher;
	}
	
	@Bean
	public NlpMainDispatcher nlpDispatcher() {
		NlpMainDispatcher nlpDispatcher = new NlpMainDispatcher();
		nlpDispatcher.setLanguageDispatchers(new HashMap<>());
		
		LanguageDispatcher langDispatcher;
		if (nlp!=null) {
			for (NlpConfigProperties props : nlp) {
				langDispatcher = new LanguageDispatcher();
				langDispatcher.setProcessors(new HashMap<>());
				langDispatcher.getProcessors().put("Stanford", this.getStanfordProcessor(props));
				
				nlpDispatcher.getLanguageDispatchers().put(props.getLanguage().toUpperCase(), langDispatcher);
			}
		}	
		return nlpDispatcher;
	}
	
	
	private StanfordProcessor getStanfordProcessor(NlpConfigProperties props) {
		StanfordProcessor stanfordProcessor = new StanfordProcessor();
		MaxentTaggerWrapper tagger = getMaxentTaggerWrapper(props);
		NerClassifierWrapper classifier = getNerClassifierWrapper(props, tagger);
		stanfordProcessor.setTagger(tagger);
		stanfordProcessor.setNerClassifier(classifier);
		stanfordProcessor.setLexParser(getLexParserWrapper(props, tagger, classifier));
		return stanfordProcessor;
	}
	
	private MaxentTaggerWrapper getMaxentTaggerWrapper(NlpConfigProperties props) {
		MaxentTaggerWrapper taggerWrapper = new MaxentTaggerWrapper();
		taggerWrapper.setAutoInit(true);
		taggerWrapper.setLexParseModelPath(props.getLexParseModel());
		taggerWrapper.setTaggerModelPath(props.getTaggerModel());
		taggerWrapper.setClassifierModelPath(props.getClassifierModel());
		taggerWrapper.setModelsPath(props.getModels());
		return taggerWrapper;
	}
	
	private NerClassifierWrapper getNerClassifierWrapper(NlpConfigProperties props, MaxentTaggerWrapper tagger) {
		NerClassifierWrapper nerClassifierWrapper = new NerClassifierWrapper();
		nerClassifierWrapper.setAutoInit(true);
		nerClassifierWrapper.setLexParseModelPath(props.getLexParseModel());
		nerClassifierWrapper.setTaggerModelPath(props.getTaggerModel());
		nerClassifierWrapper.setClassifierModelPath(props.getClassifierModel());
		nerClassifierWrapper.setModelsPath(props.getModels());
		nerClassifierWrapper.setTagger(tagger);
		return nerClassifierWrapper;
	}
	
	private LexicalizedParserWrapper getLexParserWrapper(NlpConfigProperties props, MaxentTaggerWrapper tagger, NerClassifierWrapper classifier) {
		LexicalizedParserWrapper parserWrapper = new LexicalizedParserWrapper();
		parserWrapper.setAutoInit(true);
		parserWrapper.setLexParseModelPath(props.getLexParseModel());
		parserWrapper.setTaggerModelPath(props.getTaggerModel());
		parserWrapper.setClassifierModelPath(props.getClassifierModel());
		parserWrapper.setModelsPath(props.getModels());
		parserWrapper.setTagger(tagger);
		parserWrapper.setNerClassifier(classifier);
		return parserWrapper;
	}
}

