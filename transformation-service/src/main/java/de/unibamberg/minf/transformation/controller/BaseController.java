package de.unibamberg.minf.transformation.controller;

import org.pac4j.core.matching.matcher.csrf.DefaultCsrfTokenGenerator;
import org.pac4j.core.util.Pac4jConstants;
import org.pac4j.jee.context.JEEContext;
import org.pac4j.jee.context.session.JEESessionStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import de.unibamberg.minf.core.web.controller.BaseTranslationController;
import eu.dariah.de.dariahsp.web.AuthInfoHelper;
import eu.dariah.de.dariahsp.web.model.AuthPojo;

public abstract class BaseController extends BaseTranslationController {
	@Autowired private AuthInfoHelper authInfoHelper;
	@Autowired private JEEContext jeeContext;
	
	private final DefaultCsrfTokenGenerator csrfTokenGenerator;
	
	protected BaseController(String mainNavId) {
		super(mainNavId);
		csrfTokenGenerator = new DefaultCsrfTokenGenerator();
	}

	@ModelAttribute
	public void addAttributes(Model model) {
		model.addAttribute("__csrf_headerName", Pac4jConstants.CSRF_TOKEN);
		model.addAttribute("__csrf_token", csrfTokenGenerator.get(jeeContext, JEESessionStore.INSTANCE));
		
		model.addAttribute("__access_level", getCurrentAuthInfo().getLevel());
	}
	
	protected Long getCurrentUserId() {
		return authInfoHelper.getUserId();
	}
	
	protected AuthPojo getCurrentAuthInfo() {
		return authInfoHelper.getAuth();
	}
}
