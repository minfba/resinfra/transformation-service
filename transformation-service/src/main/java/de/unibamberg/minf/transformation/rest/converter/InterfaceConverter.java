package de.unibamberg.minf.transformation.rest.converter;

import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.Interface;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.DatamodelPojo;
import de.unibamberg.minf.transformation.rest.pojo.InterfacePojo;

@Component
public class InterfaceConverter extends BaseConverter<Interface, InterfacePojo> {

	@Autowired private DatamodelConverter datamodelConverter;
	
	@Override
	public InterfacePojo convert(Interface entity, Locale locale, List<Class<?>> convertedTypes) {
		if (!convertedTypes.contains(Interface.class)) {
			convertedTypes.add(Interface.class);
		}
		return this.fill(new InterfacePojo(), entity, locale, convertedTypes);
	}

	@Override
	public InterfacePojo fill(InterfacePojo pojo, Interface entity, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setUniqueId(entity.getUniqueId());
		if (entity.getDatamodel()!=null && entity.getDatamodel().getModel()!=null) {
			pojo.setDatamodel(datamodelConverter.convert(entity.getDatamodel().getModel()));
		} else {
			DatamodelPojo dmPojo = new DatamodelPojo();
			dmPojo.setUniqueId(entity.getDatamodelId());
			pojo.setDatamodel(dmPojo);
		}
		return pojo;
	}
}
