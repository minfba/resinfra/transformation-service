package de.unibamberg.minf.transformation.rest.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@JsonInclude(Include.NON_NULL)
public class EndpointPojo extends AccessPojo {
	private static final long serialVersionUID = 3780903004222179590L;
	
	private List<InterfacePojo> interfaces;
	
	public EndpointPojo () {
		this.setType(AccessTypes.Endpoint);
	}
}
