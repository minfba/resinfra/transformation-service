package de.unibamberg.minf.transformation.rest.controller;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import de.unibamberg.minf.transformation.execution.exception.ApiInsufficientPermissionsException;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.PersistedUser;
import de.unibamberg.minf.transformation.rest.controller.base.BaseUserRestController;
import de.unibamberg.minf.transformation.rest.converter.UserConverter;
import de.unibamberg.minf.transformation.rest.helper.RestLinksHelper;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemsResponse;
import de.unibamberg.minf.transformation.rest.model.RestResponse;
import de.unibamberg.minf.transformation.rest.pojo.UserPojo;
import eu.dariah.de.dariahsp.web.model.AuthPojo;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Users", description = "API methods on users")
@RestController
@RequestMapping("/api/users")
public class UserRestController extends BaseUserRestController<UserPojo> {
	@Autowired private UserConverter userConverter;

	public UserRestController() {
		super("/api/users");
	}
	
	@GetMapping
	public RestResponse getUsers(HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		AuthPojo authPojo = authInfoHelper.getAuth();
		if(authPojo.getLevel()<100) {
			PersistedUser authUser = userService.findById(authPojo.getUserId());
			return this.getUserByUniqueId(authUser.getUniqueId(), request, locale);
		}
		return this.getAllUsers(request, locale);
	}
	
	@GetMapping("/{userUniqueId}")
	public RestItemResponse getUserByUniqueId(@PathVariable String userUniqueId, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		RestItemResponse itemResponse = new RestItemResponse();
		AuthPojo authPojo = authInfoHelper.getAuth();
		itemResponse.setItem(this.getUserItem(userConverter.convert(this.checkCanAccessUser(authPojo, userUniqueId), locale), request.getRequestURL().toString(), false));
		itemResponse.setLinks(this.getLinks(request.getRequestURL().toString()));
		return itemResponse;
	}
	
	private RestItemsResponse getAllUsers(HttpServletRequest request, Locale locale) {		
		RestItemsResponse response = new RestItemsResponse();
		List<UserPojo> users = userConverter.convert(
				userService.findAll().stream().map(u -> { /*u.setAccessTokens(null); */return u;} ).collect(Collectors.toList()), 
				locale);
		
		response.setSize(users.size());
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItems(this.getUserItems(users, request.getRequestURL().toString()));
		return response;
	}
		
	private ArrayNode getUserItems(List<UserPojo> userPojos, String requestUrl) {
		ArrayNode array = objectMapper.createArrayNode();
		if (!userPojos.isEmpty()) {
			for (UserPojo pojo : userPojos) {				
				array.add(this.getUserItem(pojo, requestUrl, true));
			}
		}
		return array;
	}
	
	private JsonNode getUserItem(UserPojo userPojo, String requestUrl, boolean suffixUniqueId) {
		JsonNode u = this.getItem(userPojo, requestUrl, suffixUniqueId);
		
		if (u.has("accessTokens")) {
			ObjectNode objT;
			for (JsonNode t : (ArrayNode)u.path("accessTokens")) {
				objT = (ObjectNode)t;
				objT.set("_links", getTokenLink(requestUrl, userPojo.getUniqueId(), objT.get("uniqueId").textValue()));
			}

		}
		return u;
	}
	
	private TextNode getTokenLink(String requestUrl, String userUniqueId, String tokenUniqueId) {
		StringBuilder tokenLinkBuilder = new StringBuilder();
		tokenLinkBuilder
			.append(requestUrl)
			.append(RestLinksHelper.SLASH)
			.append("tokens");
		
		if (tokenUniqueId!=null) {
			tokenLinkBuilder
				.append(RestLinksHelper.SLASH)
				.append(tokenUniqueId); 
		}
		return new TextNode(tokenLinkBuilder.toString());
	}

}
