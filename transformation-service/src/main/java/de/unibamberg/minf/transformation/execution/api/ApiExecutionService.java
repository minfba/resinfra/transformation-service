package de.unibamberg.minf.transformation.execution.api;

import java.io.InputStream;

import org.springframework.context.ApplicationContextAware;

import com.fasterxml.jackson.databind.JsonNode;

import de.unibamberg.minf.processing.listener.ProcessingListener;
import de.unibamberg.minf.transformation.execution.exception.ApiExecutionException;
import de.unibamberg.minf.transformation.model.Api;

public interface ApiExecutionService extends ApplicationContextAware, ProcessingListener {
	public JsonNode execute(Api api, InputStream req, boolean includeDebugInAnswer) throws ApiExecutionException;
	
	public default JsonNode execute(Api api, InputStream req) throws ApiExecutionException {
		return this.execute(api, req, false);
	}
}
