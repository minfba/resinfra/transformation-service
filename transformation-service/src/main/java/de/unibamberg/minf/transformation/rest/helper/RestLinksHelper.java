package de.unibamberg.minf.transformation.rest.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RestLinksHelper {
	public static final String SLASH = "/";
	public static final String API = "api/";
	public static final String DATAMODELS = "datamodels/";
	public static final String BAGS = "bags/";
	
	@Value("${baseUrl}")
	protected String baseUrl;
	
	public String getDatamodelsLink() {
		return baseUrl + API + DATAMODELS;
	}
	
	public String getDatamodelLink(String datamodelId) {
		return baseUrl + API + DATAMODELS + datamodelId + SLASH;
	}
	
	public String getBagToDatamodelLink(String bagId, String datamodelId) {
		return baseUrl + API + BAGS + bagId + SLASH + DATAMODELS + datamodelId + SLASH;
	}
	
}
