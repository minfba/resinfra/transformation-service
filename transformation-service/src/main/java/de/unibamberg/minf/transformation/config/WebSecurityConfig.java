package de.unibamberg.minf.transformation.config;

import java.util.List;
import java.util.stream.Collectors;

import org.pac4j.springframework.security.web.Pac4jEntryPoint;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import eu.dariah.de.dariahsp.config.web.SecurityConfigurerAdapter;
import eu.dariah.de.dariahsp.SecurityFilter;
import eu.dariah.de.dariahsp.config.web.DefaultFiltersConfigurerAdapter;

/**
 * Web security configuration addressing protected areas and authorization patterns for this sample application
 * 
 * @author Tobias Gradl
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	private static final String API_URL_MATCHER = "/api/**";
	private static final String CALL_URL_MATCHER = "/call/**";
	private static final String DATA_URL_MATCHER = "/data/**";
	
	/**
	 * Adapt this as required in a target application
	 * 
	 * @author Tobias Gradl
	 */
	@Configuration
    @Order(1)
	public static class WebSecurityConfigAdapter extends SecurityConfigurerAdapter {
		
		public WebSecurityConfigAdapter() {
			super(true);
		}

		@Override
		protected List<String> getEnabledClientNames() {
			return securityConfig.getEnabledIndirectClientNames();
		}
		
		@Override
		protected void configure(final HttpSecurity http) throws Exception {
			http.csrf().disable();
			
			http
	        	.requestMatchers()
	        		.antMatchers(
	        				"/bags/**", 
	        				"/collections/**",
	        				"/datamodels/**", 
	        				"/apis/**", 
	        				"/protected/**", 
	        				"/blocked/**"
	        				)
	        	.and()
	        	.authorizeRequests()
	        		.expressionHandler(this.hierarchicalExpressionHandler())
	        			        		
	        		.antMatchers("/bags/**").authenticated()    
	        		.antMatchers("/collections/**").authenticated()
	        		.antMatchers("/apis/**").authenticated()
	        		.antMatchers("/datamodels/**").authenticated()
	        		
	        		.antMatchers("/protected/contributor").hasRole("CONTRIBUTOR")
	        		.antMatchers("/protected/admin").hasRole("ADMINISTRATOR")
	        		.antMatchers("/blocked/noaccess").denyAll();
			 
			 super.configure(http);
		}
	}
	
	@Configuration
    @Order(2)
	public static class RestSecurityConfigAdapter extends SecurityConfigurerAdapter {
		
		public RestSecurityConfigAdapter() {
			super(false);
		}
 
		@Override
		protected List<String> getEnabledClientNames() {
			return securityConfig.getEnabledDirectClientNames();
		}
		
		@Override
		public void configure(WebSecurity web) throws Exception {
			web.ignoring().antMatchers(DATA_URL_MATCHER, CALL_URL_MATCHER);
			super.configure(web);
		}
		
		@Override
		protected void configure(final HttpSecurity http) throws Exception {
			http
				.csrf().disable()
				.cors().disable();
			http
	        	.requestMatchers()
	        		.antMatchers(HttpMethod.PUT, API_URL_MATCHER)
	        		.antMatchers(HttpMethod.POST, API_URL_MATCHER)
	        		.antMatchers(HttpMethod.OPTIONS, API_URL_MATCHER)
	        		.antMatchers(HttpMethod.DELETE, API_URL_MATCHER)
	        	.and()
	        	.authorizeRequests()
	        		.expressionHandler(this.hierarchicalExpressionHandler())
	        		//.antMatchers("/api/datamodels/sync").hasRole("CONTRIBUTOR")
	        		/*.antMatchers("/protected/admin").hasRole("ADMINISTRATOR")
	        		.antMatchers("/blocked/noaccess").denyAll()*/
	        		//.anyRequest().hasRole("CONTRIBUTOR");
	        		.anyRequest().authenticated();
			 
			super.configure(http);	        
		}
	}
	
	
	
	/**
	 * Make sure to include this for logout, login and callback filters 
	 * 
	 * @author Tobias Gradl
	 */
	@Configuration
    @Order(3)
    public static class CallbackLoginLogoutConfigurationAdapter extends DefaultFiltersConfigurerAdapter {}
}
