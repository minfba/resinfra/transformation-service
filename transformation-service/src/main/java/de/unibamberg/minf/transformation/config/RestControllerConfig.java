package de.unibamberg.minf.transformation.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "rest")
public class RestControllerConfig {
	private int restControllerDefaultLimit = 100;
	private int restControllerMaxLimit = 1000;
}
