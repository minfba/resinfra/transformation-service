package de.unibamberg.minf.transformation.rest.converter;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.AccessParam;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.Interface;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.EndpointPojo;

@Component
public class EndpointConverter extends BaseConverter<Endpoint, EndpointPojo> {

	@Autowired private InterfaceConverter interfaceConverter;
	private CollectionConverter collectionConverter;
	
	
	public EndpointConverter(@Lazy CollectionConverter collectionConverter) {
		this.collectionConverter = collectionConverter;
	}
	
	@Override
	public EndpointPojo convert(Endpoint endpoint, Locale locale, List<Class<?>> convertedTypes) {
		if (!convertedTypes.contains(Endpoint.class)) {
			convertedTypes.add(Endpoint.class);
		}
		return this.fill(new EndpointPojo(), endpoint, locale, convertedTypes);
	}

	@Override
	public EndpointPojo fill(EndpointPojo pojo, Endpoint endpoint, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setUniqueId(endpoint.getUniqueId());
 
		if (this.canProceedWithType(Interface.class, convertedTypes) && this.isLoaded(endpoint, "interfaces")) {
			pojo.setInterfaces(interfaceConverter.convert(endpoint.getInterfaces()));
		}
		
		pojo.setAccessModelId(endpoint.getAccessModelId());
		pojo.setAccessType(endpoint.getAccessType());
		pojo.setUrl(endpoint.getUrl());
		pojo.setFileType(endpoint.getFileType());
		pojo.setAuthRequired(endpoint.isAuthRequired());
		
		if (this.isLoaded(endpoint, "params") && !endpoint.getParams().isEmpty()) {
			pojo.setParams(endpoint.getParams().stream().collect(Collectors.toMap(AccessParam::getParam, AccessParam::getValue)));
		}
		if (this.isLoaded(endpoint, "fileAccessPatterns") && !endpoint.getFileAccessPatterns().isEmpty()) {
			pojo.setFileAccessPatterns(endpoint.getFileAccessPatterns());
		}
		if (this.canProceedWithType(Collection.class, convertedTypes) && this.isLoaded(endpoint, "collection")) {
			pojo.setCollection(collectionConverter.convert(endpoint.getCollection(), locale, convertedTypes));
		}
				
		return pojo;
	}

}
