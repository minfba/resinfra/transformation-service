package de.unibamberg.minf.transformation.rest.controller;

import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.CrawlConverter;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.pojo.CrawlPojo;
import de.unibamberg.minf.transformation.service.CrawlService;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Crawls", description = "API methods on online dataset crawls")
@RestController
@RequestMapping("/api/crawls")
public class CrawlRestController extends BaseRestController<CrawlPojo> {
	
	@Autowired private CrawlService crawlService;
	@Autowired private CrawlConverter crawlConverter;
	
	public CrawlRestController() {
		super("/api/crawls");
	}

	@GetMapping("/{uniqueId}")
    public RestItemResponse getCrawl(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		
		Optional<Crawl> c = crawlService.findByUniqueId(uniqueId);
		if (c.isEmpty()) {
			throw new ApiItemNotFoundException("crawl", uniqueId);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(crawlConverter.convert(c.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}	
}