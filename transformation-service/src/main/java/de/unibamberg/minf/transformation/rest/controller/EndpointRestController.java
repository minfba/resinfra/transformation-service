package de.unibamberg.minf.transformation.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.EndpointConverter;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemsResponse;
import de.unibamberg.minf.transformation.rest.pojo.EndpointPojo;

import de.unibamberg.minf.transformation.service.AccessService;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Endpoints", description = "API methods on collection endpoints")
@RestController
@RequestMapping("/api/endpoints")
public class EndpointRestController extends BaseRestController<EndpointPojo> {
	
	@Autowired private AccessService accessService;
	@Autowired private EndpointConverter endpointConverter;
	
	public EndpointRestController() {
		super("/api/endpoints");
	}

	@GetMapping("/")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("/api/endpoints");
		return null;
	}
	
	@GetMapping
    public RestItemsResponse getEndpoints(HttpServletRequest request, Locale locale) {
		RestItemsResponse response = new RestItemsResponse();
		List<EndpointPojo> endpoints = endpointConverter.convert(accessService.findAllEndpoints(), locale);

		response.setSize(endpoints.size());
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItems(this.getItems(endpoints, request.getRequestURL().toString()));
		
		return response;
	}
		
	@GetMapping("/{uniqueId}")
    public RestItemResponse getEndpoint(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();

		Optional<Endpoint> e = accessService.findEndpointByUniqueId(uniqueId);
		if (e.isEmpty()) {
			throw new ApiItemNotFoundException("endpoint", uniqueId);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(endpointConverter.convert(e.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}
	
	@Override
	protected ObjectNode getItem(EndpointPojo c, String requestUrl, boolean suffixUniqueId) {
		ObjectNode item = super.getItem(c, requestUrl, suffixUniqueId);
		if (item.has("interfaces")) {
			for (JsonNode t : (ArrayNode)item.get("interfaces")) {
				this.setLinksOnSubitem((ObjectNode)t, "interfaces", requestUrl, false);
				if (t.has("datamodel")) {
					this.setLinksOnSubitem((ObjectNode)t.get("datamodel"), "datamodel", "datamodels", requestUrl, false);
				}
			}
		}
		if (item.has("collection")) {
			ObjectNode t = (ObjectNode)item.get("collection");
			this.setLinksOnSubitem(t, "collection", requestUrl, false);
		}
		return item;
	}
	
}