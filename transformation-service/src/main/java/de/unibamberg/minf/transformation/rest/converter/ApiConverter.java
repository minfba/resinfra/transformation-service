package de.unibamberg.minf.transformation.rest.converter;

import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.Api;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.ApiPojo;

@Component
public class ApiConverter extends BaseConverter<Api, ApiPojo> {

	@Autowired private InterfaceConverter interfaceConverter;
	@Autowired private DatamodelConverter datamodelConverter;
	
	@Override
	public ApiPojo convert(Api api, Locale locale, List<Class<?>> convertedTypes) {
		if (!convertedTypes.contains(Api.class)) {
			convertedTypes.add(Api.class);
		}
		return this.fill(new ApiPojo(), api, locale, convertedTypes);
	}

	@Override
	public ApiPojo fill(ApiPojo pojo, Api api, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setUniqueId(api.getUniqueId());
		pojo.setLabel(api.getLabel());
		pojo.setMetadata(api.getMetadata());
		
		if (this.isLoaded(api, "interfaces")) {
			pojo.setInterfaces(interfaceConverter.convert(api.getInterfaces()));
		}
		
		pojo.setInputDatamodelId(api.getInputDatamodelId());
		if (api.getInputDatamodel()!=null && api.getInputDatamodel().getModel()!=null) {
			pojo.setInputDatamodel(datamodelConverter.convert(api.getInputDatamodel().getModel(), locale, convertedTypes));
		}
		pojo.setOutputDatamodelId(api.getOutputDatamodelId());
		if (api.getOutputDatamodel()!=null && api.getOutputDatamodel().getModel()!=null) {
			pojo.setOutputDatamodel(datamodelConverter.convert(api.getOutputDatamodel().getModel(), locale, convertedTypes));
		}		
		return pojo;
	}

}
