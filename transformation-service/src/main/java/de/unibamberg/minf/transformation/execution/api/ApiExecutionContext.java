package de.unibamberg.minf.transformation.execution.api;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.fasterxml.jackson.databind.JsonNode;

import de.unibamberg.minf.gtf.context.ExecutionContext;
import lombok.Getter;

@Getter
public class ApiExecutionContext implements ExecutionContext {
	private final String pathPrefix;
	private final String apiId;
	private final String workingDir;
	private final JsonNode sessionData;
	
	public ApiExecutionContext(String pathPrefix, String apiId, JsonNode sessionData) throws IOException {
		this.pathPrefix = pathPrefix;
		this.apiId = apiId;
		this.sessionData = sessionData;
		
		this.workingDir = pathPrefix + File.separator + this.apiId + File.separator;
		File workingDir = new File(this.workingDir); 
		if (!workingDir.exists()) {
			FileUtils.forceMkdir(workingDir);
		}
	}
}
