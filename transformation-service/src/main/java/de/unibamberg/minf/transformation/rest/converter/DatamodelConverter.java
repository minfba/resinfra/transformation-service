package de.unibamberg.minf.transformation.rest.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Component;

import de.unibamberg.minf.dme.model.datamodel.base.Datamodel;
import de.unibamberg.minf.dme.model.datamodel.base.DatamodelNature;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.DatamodelPojo;

@Component
public class DatamodelConverter extends BaseConverter<Datamodel, DatamodelPojo> {

	@Override
	public DatamodelPojo convert(Datamodel datamodel, Locale locale, List<Class<?>> convertedTypes) {
		DatamodelPojo pojo = new DatamodelPojo();
		if (!convertedTypes.contains(Datamodel.class)) {
			convertedTypes.add(Datamodel.class);
		}
		return this.fill(pojo, datamodel, locale, convertedTypes);
	}

	@Override
	public DatamodelPojo fill(DatamodelPojo pojo, Datamodel datamodel, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setName(datamodel.getName());
		if (datamodel.getDescription()!=null && !datamodel.getDescription().isBlank()) {
			pojo.setDescription(datamodel.getDescription());
		}
		pojo.setUniqueId(datamodel.getId());
		
		if (datamodel.getNatures()!=null) {
			pojo.setNatures(new ArrayList<>());
			for (DatamodelNature nature : datamodel.getNatures()) {
				pojo.getNatures().add(messageSource.getMessage(nature.getClass().getName().toLowerCase(), null, locale));
			}
		}

		
		return pojo;
	}
}
