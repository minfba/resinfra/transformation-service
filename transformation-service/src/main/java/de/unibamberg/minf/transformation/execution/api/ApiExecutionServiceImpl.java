package de.unibamberg.minf.transformation.execution.api;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import de.unibamberg.minf.core.util.Stopwatch;
import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.base.Nonterminal;
import de.unibamberg.minf.dme.model.datamodel.natures.JsonDatamodelNature;
import de.unibamberg.minf.mapping.model.MappingExecGroup;
import de.unibamberg.minf.mapping.service.MappingExecutionService;
import de.unibamberg.minf.processing.consumption.CollectingResourceConsumptionServiceImpl;
import de.unibamberg.minf.processing.exception.GenericProcessingException;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.processing.model.helper.ResourceHelper;
import de.unibamberg.minf.processing.output.json.JsonStringOutputService;
import de.unibamberg.minf.processing.service.base.ResourceProcessingService;
import de.unibamberg.minf.processing.service.json.JsonProcessingService;
import de.unibamberg.minf.transformation.config.PathsConfig;
import de.unibamberg.minf.transformation.config.WebConfig;
import de.unibamberg.minf.transformation.config.nested.DataProvisioningConfigProperties;
import de.unibamberg.minf.transformation.crawling.CrawlHelper;
import de.unibamberg.minf.transformation.crawling.CrawlPipeline;
import de.unibamberg.minf.transformation.crawling.CrawlPipelineImpl;
import de.unibamberg.minf.transformation.crawling.RepetitiveFileCrawlerImpl;
import de.unibamberg.minf.transformation.crawling.crawler.Crawler;
import de.unibamberg.minf.transformation.crawling.crawler.Processor;
import de.unibamberg.minf.transformation.execution.BaseExecutionService;
import de.unibamberg.minf.transformation.execution.TransformingConsumptionServiceImpl;
import de.unibamberg.minf.transformation.execution.exception.ApiExecutionException;
import de.unibamberg.minf.transformation.helper.MappingHelpers;
import de.unibamberg.minf.transformation.model.Api;
import de.unibamberg.minf.transformation.model.Bag;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import de.unibamberg.minf.transformation.model.Interface;
import de.unibamberg.minf.transformation.model.TemporaryAccessOperation;
import de.unibamberg.minf.transformation.serialization.ResourceSerializer;
import de.unibamberg.minf.transformation.service.CollectionService;
import de.unibamberg.minf.transformation.service.CrawlService;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.InterfaceService;
import de.unibamberg.minf.transformation.service.MappingService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Scope("prototype")
public class ApiExecutionServiceImpl extends BaseExecutionService implements ApiExecutionService {
	
	@Autowired private PathsConfig pathsConfig;
	
	
	@Autowired private CollectionService collectionService;
	
	@Autowired private CrawlService crawlService;	
	
	@Autowired private InterfaceService interfaceService;
		
	@Autowired private DataProvisioningConfigProperties provisioningConfig;
	
	@Autowired private WebConfig webConfig;
	
	private CountDownLatch pipelineLatch;
	
	private List<TransformingConsumptionServiceImpl> targetResourceCollectors = new ArrayList<>();


	

	
	@Override public synchronized void start(UUID serviceId) { }
	@Override public synchronized void updateSize(UUID serviceId, long size) { }
	@Override public synchronized void processed(UUID serviceId, long process) { }

	@Override
	public synchronized void finished(UUID serviceId) {
		log.info("Pipeline [{}] successfully completed", serviceId);
		pipelineLatch.countDown();
	}

	@Override
	public synchronized void error(UUID serviceId) {
		log.error("Pipeline [{}] terminated with errors", serviceId);
		pipelineLatch.countDown();
	}
	
	@Override
	public JsonNode execute(Api api, InputStream req, boolean includeDebugInAnswer) throws ApiExecutionException {
		Stopwatch swOverall = new Stopwatch().start();
		Stopwatch swTask = new Stopwatch().start();
		
		ObjectNode result = exportObjectMapper.createObjectNode();

		List<Api> apis = new ArrayList<>(1);
		apis.add(api);
		
		try {
			// Parse provided request
			List<Resource> requestResources = processAgainstDatamodel(api.getInputDatamodelId(), api, req);		
			if (includeDebugInAnswer) {
				result.set("request", exportObjectMapper.valueToTree(requestResources));
			}
			log.debug("Parsed request resources in {}ms", swTask.getElapsedTime());		
			swTask.reset();		
			
			// Transform request into access model / crawl model representations and render requests
			Map<ApiDescriptionContainer, List<Resource>> crawlModelResMap = this.getCrawlModelResMap(result, requestResources, api, includeDebugInAnswer);
			log.debug("Rendered request URLs in {}ms", swTask.getElapsedTime());
			swTask.reset();
			
			
			List<CrawlPipeline> requestPipelines = this.createRequestPipelines(crawlModelResMap);
			log.debug("Prep request pipelines in {}ms", swTask.getElapsedTime());
			
			// Create latch and execute pipelines
			pipelineLatch = new CountDownLatch(requestPipelines.size());
			for (CrawlPipeline pipeline : requestPipelines) {
				pipeline.run();
			}
			
			// Wait for pipelines to conclude
			// TODO: Timeout has to be a configuration property of the API 
			pipelineLatch.await(40000, TimeUnit.MILLISECONDS);
			log.debug("Executed request pipelines in {}ms", swTask.getElapsedTime());
			swTask.reset();
			
			List<Resource> resultResources = new ArrayList<>();
			
			for (TransformingConsumptionServiceImpl targetResourceCollector : targetResourceCollectors) {
				if (targetResourceCollector.getTransformedResources()!=null) {
					resultResources.addAll(targetResourceCollector.getTransformedResources());
				}
			}

			if (api.getOutputDatamodel()!=null && api.getOutputDatamodel().getModel().getNature(JsonDatamodelNature.class)!=null) {
				JsonStringOutputService svc = appContext.getBean(JsonStringOutputService.class);
				svc.setUseTerminals(true);
				svc.setSchema(api.getOutputDatamodel().getModel());
				svc.setNature(api.getOutputDatamodel().getModel().getNature(JsonDatamodelNature.class));
				svc.setRoot(api.getOutputDatamodel().getOrRenderProcessingRoot());
				svc.init();
				
				String jsonString = svc.writeToString(resultResources.toArray(new Resource[0]));
				
				result.set("items", exportObjectMapper.readTree(jsonString));
			} else {
				result.set("items", exportObjectMapper.valueToTree(resultResources));
			}
			
			log.info("Answered call to API:{} in {}ms", api.getId(), swOverall.getElapsedTime());	
		} catch (Exception e) {
			log.error("Failed to execute API", e);
			throw new ApiExecutionException("Failed to execute API", e);
		}
		
		return result;
	}	
	
	private Map<ApiDescriptionContainer, List<Resource>> getCrawlModelResMap(ObjectNode result, List<Resource> requestResources, Api api, boolean includeDebugInAnswer) throws ProcessingConfigException {
		Map<ApiDescriptionContainer, List<Resource>> crawlModelResMap = new HashMap<>();
		
		ArrayNode bagRequests = exportObjectMapper.createArrayNode();
		ObjectNode bagRequest;
		ApiDescriptionContainer apiDesc;
		List<Resource> accessModelResources = null;
		List<Resource> crawlModelResources = null;

		for (Interface i : api.getInterfaces()) {
			apiDesc = this.getApiDescriptionContainer(i.getId());
			apiDesc.setApi(api);

			if (requestResources!=null) {
				// Transform request to access model of endpoint 
				accessModelResources = this.transformResourcesSync(api.getInputDatamodelId(), apiDesc.getEndpoint().getAccessModelId(), requestResources);
				
				// Transform from access model of endpoint to repetitive crawl model (produce GET params)
				// TODO: What else, POST request body, forms...?
				crawlModelResources = this.transformResourcesSync(apiDesc.getEndpoint().getAccessModelId(), provisioningConfig.getProvisioningModelId(), accessModelResources);
			}
			
			// Render request URLs for bag and
			// collect URLs with CrawlModel
			crawlModelResMap.put(apiDesc, crawlModelResources);
			
			if (includeDebugInAnswer) {
				bagRequest = exportObjectMapper.createObjectNode();
				bagRequest.set("interfaceId", new LongNode(i.getId()));
				bagRequest.set("inputModel", exportObjectMapper.valueToTree(accessModelResources));
				bagRequest.set("requestModel", exportObjectMapper.valueToTree(crawlModelResources));				
				bagRequests.add(bagRequest);
			}
		}
		
		if (includeDebugInAnswer) {
			result.set("remotes", bagRequests);
		}
		
		return crawlModelResMap;
	}
	
	private List<CrawlPipeline> createRequestPipelines(Map<ApiDescriptionContainer, List<Resource>> crawlModelResMap) {
		List<CrawlPipeline> pipelines = new ArrayList<>();
		for (Entry<ApiDescriptionContainer, List<Resource>> descEntity : crawlModelResMap.entrySet()) {
			try {
				pipelines.add(this.createRequestPipeline(descEntity.getKey(), descEntity.getValue()));
			} catch (Exception e) {
				log.error("Failed to create request pipeline", e);
			}
		}
		return pipelines;
	}
	
	private CrawlPipeline createRequestPipeline(ApiDescriptionContainer apiDesc, List<Resource> crawlModelResMap) throws URISyntaxException, ProcessingConfigException, GenericProcessingException, IOException {		
		ApiExecutionContext execContext = new ApiExecutionContext(this.pathsConfig.getData(), this.renderContextId(new Object[] { apiDesc.getApi().getUniqueId() }), getSessionData(apiDesc.getApi()));
		
		RepetitiveFileCrawlerImpl c = appContext.getBean(RepetitiveFileCrawlerImpl.class);
		c.init(apiDesc.getEndpoint(), apiDesc.getAccessOperation(), apiDesc.getDatamodelContainer());
		c.addDownloadUris(crawlModelResMap);
		c.setMethod(apiDesc.getEndpoint().getHttpMethod());
		c.setExecutionContext(execContext);
		
		MappingExecutionService mappingExecutionService = appContext.getBean(MappingExecutionService.class);
		ExtendedMappingContainer mc = mappingService.getMappingBySourceAndTarget(apiDesc.getDatamodelContainer().getId(), apiDesc.getApi().getOutputDatamodelId());
		ExtendedDatamodelContainer sourceDatamodel = datamodelService.findById(apiDesc.getDatamodelContainer().getId());
		ExtendedDatamodelContainer targetDatamodel = datamodelService.findById(apiDesc.getApi().getOutputDatamodelId()); 
		
		TransformingConsumptionServiceImpl targetResCollector = new TransformingConsumptionServiceImpl(mappingExecutionService, mc, sourceDatamodel, targetDatamodel);
		targetResourceCollectors.add(targetResCollector);
				
		Processor processore = Processor.class.cast(appContext.getBean("jsonBatchFileProcessor")); 
		processore.init(apiDesc.getEndpoint(), apiDesc.getAccessOperation(), apiDesc.getDatamodelContainer());
		processore.addConsumptionService(targetResCollector);
		processore.setExecutionContext(execContext);
		
		List<Crawler> crawlers = new ArrayList<>(2);
		crawlers.add(c);
		crawlers.add(processore);
		
		CrawlPipeline cp = new CrawlPipelineImpl(apiDesc.getAccessOperation().getUniqueId(), crawlers);
		cp.setExecutionContext(execContext);
		cp.setListener(this);
		
		return cp;		
	}	
	
	/*private List<String> collectDownloadUris(List<Resource> crawlDefResources, Endpoint endpoint) {
		List<Resource> resParam;
		List<Resource> resRemoveParam;
		List<String> newUrls = new ArrayList<>();
		if (crawlDefResources!=null) {
			for (Resource r : crawlDefResources) {
				resParam = ResourceHelper.findRecursive(r, "GET.Param");
				resRemoveParam = ResourceHelper.findRecursive(r, "GET.RemoveParam");
				newUrls.add(CrawlHelper.renderAccessUrl(endpoint.getUrl(), endpoint.getParams(), resParam, resRemoveParam));
			}
		}
		return newUrls;
	}*/
	
	private ApiDescriptionContainer getApiDescriptionContainer(Long interfaceId) {
		ApiDescriptionContainer cm = new ApiDescriptionContainer();
		
		Interface intf = interfaceService.findById(interfaceId).get();

		Endpoint ep = intf.getEndpoint();
		
		cm.setAccessOperation(new TemporaryAccessOperation());
		
		Collection c = collectionService.findById(ep.getCollection().getId());
		cm.setCollection(c);
		
		//for (Endpoint ep : c.getEndpoints()) {
		//	if (ep.getId().equals(linkedBag.getEndpointId())) {
		cm.setEndpoint(ep);
		
		/*Crawl crawl = new Crawl();
		//crawl.setCollection(c);
		crawl.setEndpoint(intf.getEndpoint());
		crawl.setDataset(linkedBag.getDataset());
		
		crawlService.save(crawl);
		
		cm.setCrawl(crawl);*/
		
		// TODO Fix
		cm.setDatamodelContainer(datamodelService.findById(intf.getDatamodelId()));
		
		return cm;

	}
	
	private List<Resource> processAgainstDatamodel(String datamodelId, Api api, InputStream inStream)  {
		Stopwatch sw = new Stopwatch().start();
		ExtendedDatamodelContainer datamodelContainer = datamodelService.findById(datamodelId);
		/*if (s==null) {
			Mapping m = mappingService.findMappingById(entityId);
			s = schemaService.findSchemaById(m.getSourceId());
		}		*/
		
		
		log.debug("Datamodel loaded [{}] took {}ms", datamodelId, sw.getElapsedTime());
		sw.reset();
		
		Element rootElement = datamodelContainer.getOrRenderElementHierarchy();
		log.debug("step (1) ... {}ms", sw.getElapsedTime());
		
				
		ResourceProcessingService processingSvc = (ResourceProcessingService)appContext.getBean("jsonProcessingService");
		log.debug("step (2) ... {}ms", sw.getElapsedTime());
	
		/*
		processingSvc = appContext.getBean(XmlProcessingService.class);	
		processingSvc = appContext.getBean(CsvProcessingService.class);
		((CsvProcessingService)processingSvc).setUseHeadings(true);
		processingSvc = appContext.getBean(TsvProcessingService.class);
		((TsvProcessingService)processingSvc).setUseHeadings(true);
		processingSvc = appContext.getBean(TextProcessingService.class);
		*/
		
		processingSvc.setInputStream(inStream);
		
		CollectingResourceConsumptionServiceImpl consumptionService = new CollectingResourceConsumptionServiceImpl();
		
		
		sw.reset();
		
		processingSvc.setSchema(datamodelContainer.getModel());
		processingSvc.addConsumptionService(consumptionService);
		try {
			processingSvc.setExecutionContext(new ApiExecutionContext(this.pathsConfig.getData(), this.renderContextId(new Object[] { api.getUniqueId() }), getSessionData(api)));
			processingSvc.setRoot(Nonterminal.class.cast(rootElement));
			processingSvc.init();
				
			
			log.debug("Preparation of sample for datamodel [{}] took {}ms", datamodelContainer.getId(), sw.getElapsedTime());
			
			sw.reset();
			processingSvc.run();
			
			
			log.debug("Parse of sample against datamodel [{}] took {}ms", datamodelContainer.getId(), sw.getElapsedTime());
			
			
			sw.stop();
			
			
			List<Resource> outputResources = consumptionService.getResources();
			
			return outputResources;
		} catch (Exception e) {
			log.error("Failed to convert API input", e.getMessage());
		}
		return null;
	}

	private String renderContextId(Object[] contextId) {
		if (contextId.length==1) {
			return contextId[0].toString();
		}
		StringBuilder idBuilder = new StringBuilder();
		for (int i=0; i<contextId.length-1; i++) {
			idBuilder.append(contextId[i]).append(File.separator);
		}
		idBuilder.append(contextId[contextId.length-1]);
		return idBuilder.toString();		
	}
	
	// TODO This is redundant code to CrawlManagerImpl 
	private JsonNode getSessionData(Collection collection) {
		if (collection.getCollectionMetadata()==null || collection.getCollectionMetadata().isBlank()) {
			return MissingNode.getInstance();
		}
		try {
			ObjectNode cNode = objectMapper.createObjectNode();
			cNode.set("collection", objectMapper.readTree(collection.getCollectionMetadata()));
			return cNode;
		} catch (Exception e) {
			log.error("Failed to read collection metadata to session data", e);
			return MissingNode.getInstance();
		}
	}
	
	private JsonNode getSessionData(Api api) {
		if (api.getMetadata()==null) {
			return MissingNode.getInstance();
		}
		try {
			ObjectNode aNode = objectMapper.createObjectNode();
			aNode.set("api", objectMapper.readTree(api.getMetadata()));
			
			ObjectNode apiEnv = objectMapper.createObjectNode();
			apiEnv.set("id", LongNode.valueOf(api.getId()));
			apiEnv.set("uniqueId", TextNode.valueOf(api.getUniqueId()));
			apiEnv.set("inputDatamodelId", TextNode.valueOf(api.getInputDatamodelId()));
			apiEnv.set("outputDatamodelId", TextNode.valueOf(api.getOutputDatamodelId()));
			apiEnv.set("label", TextNode.valueOf(api.getLabel()));
			apiEnv.set("assets", TextNode.valueOf(webConfig.getBaseUrlObj().getAbsoluteUrl("assets/" + api.getUniqueId() + "/")));
			
			aNode.set("_env", apiEnv);
						
			return aNode;
		} catch (Exception e) {
			log.error("Failed to read collection metadata to session data", e);
			return MissingNode.getInstance();
		}
	}
	
}