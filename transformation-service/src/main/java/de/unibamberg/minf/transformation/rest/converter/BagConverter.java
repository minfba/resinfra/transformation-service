package de.unibamberg.minf.transformation.rest.converter;

import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.Bag;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.BagPojo;

@Component
public class BagConverter extends BaseConverter<Bag, BagPojo> {

	@Override
	public BagPojo convert(Bag bag, Locale locale, List<Class<?>> convertedTypes) {
		if (!convertedTypes.contains(Bag.class)) {
			convertedTypes.add(Bag.class);
		}
		return this.fill(new BagPojo(), bag, locale, convertedTypes);
	}

	@Override
	public BagPojo fill(BagPojo pojo, Bag bag, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setUniqueId(bag.getUniqueId());
		return pojo;
	}
}
