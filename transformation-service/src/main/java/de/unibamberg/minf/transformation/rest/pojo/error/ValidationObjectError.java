package de.unibamberg.minf.transformation.rest.pojo.error;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.validation.ObjectError;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ValidationObjectError extends BaseValidationError {
	private String message;

	public static ValidationObjectError fromObjectError(ObjectError objError, MessageSource messageSource, Locale locale) {
		ValidationObjectError err = new ValidationObjectError();
		err.setMessage(getMessage(objError.getCodes(), objError.getDefaultMessage(), messageSource, locale));
		return err;
	}
	
	public static ValidationObjectError fromMessage(String[] codes, Object[] args, String defaultMessage, MessageSource messageSource, Locale locale) {
		ValidationObjectError err = new ValidationObjectError();
		err.setMessage(getMessage(codes, args, defaultMessage, messageSource, locale));
		return err;
	}
}
