package de.unibamberg.minf.transformation.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=true)
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
public class RestActionResponse extends RestResponse {	
	public enum ApiActionStatus { NONE, DONE, STARTED, BUSY, CANCELLED, WAITING }
	private ApiActionStatus status = ApiActionStatus.NONE;
	private String action;
	
	public RestActionResponse(ApiActionStatus status, String action) {
		this.action = action;
		this.status = status;
	}
}
