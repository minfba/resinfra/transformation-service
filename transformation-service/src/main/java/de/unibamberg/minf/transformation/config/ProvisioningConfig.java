package de.unibamberg.minf.transformation.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.unibamberg.minf.transformation.config.nested.DataProvisioningConfigProperties;
import de.unibamberg.minf.transformation.crawling.CrawlManager;
import de.unibamberg.minf.transformation.crawling.CrawlManagerImpl;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@Configuration
@ConfigurationProperties(prefix = "provisioning")
public class ProvisioningConfig extends DataProvisioningConfigProperties {

	private int maxThreads = 4;
	
	/*@Bean
	public CrawlManagerImpl crawlManager(TransformationConfig mainConfig, Map<String, String> accessChainMap, Map<String, String> fileProcessingChainMap) {
		CrawlManagerImpl crawlManager = new CrawlManagerImpl();
		crawlManager.setMaxPoolSize(this.getMaxThreads());
		crawlManager.setBaseDownloadPath(mainConfig.getPaths().getDownloads());
		crawlManager.setAccessChains(accessChainMap);
		crawlManager.setFileProcessingChains(fileProcessingChainMap);
		return crawlManager;
	}*/
}
