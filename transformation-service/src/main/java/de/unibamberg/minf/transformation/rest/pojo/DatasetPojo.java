package de.unibamberg.minf.transformation.rest.pojo;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class DatasetPojo extends BasePojo {	
	private static final long serialVersionUID = 4226900779983701035L;
	private DatamodelPojo datamodel;
	private boolean local;
	private boolean online;
	private boolean nocache;
	
	private OffsetDateTime expires;
	private String label;
	private String remoteAlias;
}
