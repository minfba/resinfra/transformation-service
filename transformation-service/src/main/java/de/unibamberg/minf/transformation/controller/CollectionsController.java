package de.unibamberg.minf.transformation.controller;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/collections")
public class CollectionsController extends BaseController {
	public CollectionsController() {
		super("collections");
	}

	@GetMapping("")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("collections/");
		return null;
	}
	
	@GetMapping("/")
	public String getHome(Model model, Locale locale) {
		return "collections/list";
	}
}
