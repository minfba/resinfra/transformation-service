package de.unibamberg.minf.transformation.rest.pojo.error;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.validation.FieldError;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class ValidationFieldError extends BaseValidationError {
	private String field;
	private String message;
	private Object rejectedValue;
	
	
	public static ValidationFieldError fromFieldError(FieldError fieldError, MessageSource messageSource, Locale locale) {
		ValidationFieldError err = new ValidationFieldError();
		err.setField(fieldError.getField());
		err.setRejectedValue(fieldError.getRejectedValue());
		err.setMessage(getMessage(fieldError.getCodes(), fieldError.getDefaultMessage(), messageSource, locale));
		return err;
	}
}
