package de.unibamberg.minf.transformation.rest.pojo.error;

import java.util.Locale;

import org.springframework.context.MessageSource;

public class BaseValidationError {
	
	protected static String getMessage(String[] codes, Object[] args, String defaultMessage, MessageSource messageSource, Locale locale) {
		String message = null;
		for (String code : codes) {
			message = messageSource.getMessage(code, args, null, locale);
			if (message!=null && !message.equals(code)) {
				return message;
			}
		}
		return defaultMessage;
	}
	
	protected static String getMessage(String[] codes, String defaultMessage, MessageSource messageSource, Locale locale) {
		return getMessage(codes, null, defaultMessage, messageSource, locale);
	}
}
