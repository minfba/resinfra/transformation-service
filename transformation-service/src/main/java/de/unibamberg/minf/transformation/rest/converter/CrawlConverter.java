package de.unibamberg.minf.transformation.rest.converter;

import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.CrawlPojo;

@Component
public class CrawlConverter extends BaseConverter<Crawl, CrawlPojo>{

	@Override
	public CrawlPojo convert(Crawl crawl, Locale locale, List<Class<?>> convertedTypes) {
		if (!convertedTypes.contains(Crawl.class)) {
			convertedTypes.add(Crawl.class);
		}
		return this.fill(new CrawlPojo(), crawl, locale, convertedTypes);
	}

	@Override
	public CrawlPojo fill(CrawlPojo pojo, Crawl crawl, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setUniqueId(crawl.getUniqueId());
		pojo.setError(crawl.isError());
		pojo.setComplete(crawl.isComplete());
		pojo.setOnline(crawl.isOffline());
		pojo.setOffline(crawl.isOffline());
		pojo.setPrefix(crawl.getPrefix());
		
		if (this.isLoaded(crawl, "messages")) {
			pojo.setMessages(crawl.getMessages());
		}
		if (crawl.getBaseCrawl()!=null && this.isLoaded(crawl, "baseCrawl")) {
			pojo.setBaseCrawl(this.convert(crawl.getBaseCrawl(), locale, convertedTypes));
		}		
		return pojo;
	}

}