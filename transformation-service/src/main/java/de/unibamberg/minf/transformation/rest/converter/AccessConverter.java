package de.unibamberg.minf.transformation.rest.converter;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.AccessParam;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.AccessPojo;
import de.unibamberg.minf.transformation.rest.pojo.AccessPojo.AccessTypes;
import de.unibamberg.minf.transformation.rest.pojo.DatasourcePojo;


@Component
public class AccessConverter extends BaseConverter<Access, AccessPojo> {

	private CollectionConverter collectionConverter;
	
	
	public AccessConverter(@Lazy CollectionConverter collectionConverter) {
		this.collectionConverter = collectionConverter;
	}
	
	@Override
	public AccessPojo convert(Access datasource, Locale locale, List<Class<?>> convertedTypes) {
		if (!convertedTypes.contains(Access.class)) {
			convertedTypes.add(Access.class);
		}
		return this.fill(new DatasourcePojo(), datasource, locale, convertedTypes);
	}

	@Override
	public AccessPojo fill(AccessPojo pojo, Access access, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setUniqueId(access.getUniqueId());
		pojo.setAuthRequired(access.isAuthRequired());
		
		pojo.setType(Datasource.class.isAssignableFrom(access.getClass()) ? AccessTypes.Datasource : AccessTypes.Endpoint);
		pojo.setAccessModelId(access.getAccessModelId());
		pojo.setAccessType(access.getAccessType());
		pojo.setUrl(access.getUrl());
		pojo.setFileType(access.getFileType());
		
		if (this.isLoaded(access, "params") && !access.getParams().isEmpty()) {
			pojo.setParams(access.getParams().stream().collect(Collectors.toMap(AccessParam::getParam, AccessParam::getValue)));
		}
		if (this.isLoaded(access, "fileAccessPatterns") && !access.getFileAccessPatterns().isEmpty()) {
			pojo.setFileAccessPatterns(access.getFileAccessPatterns());
		}
		if (this.canProceedWithType(Collection.class, convertedTypes) && this.isLoaded(access, "collection")) {
			pojo.setCollection(collectionConverter.convert(access.getCollection(), locale, convertedTypes));
		}
		
	
		return pojo;
	}

}
