package de.unibamberg.minf.transformation.execution;

import java.util.ArrayList;
import java.util.List;

import de.unibamberg.minf.mapping.model.MappingExecGroup;
import de.unibamberg.minf.mapping.service.MappingExecutionService;
import de.unibamberg.minf.processing.consumption.CollectingResourceConsumptionServiceImpl;
import de.unibamberg.minf.processing.consumption.ResourceConsumptionService;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.transformation.helper.MappingHelpers;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TransformingConsumptionServiceImpl implements ResourceConsumptionService {
	public enum STAGES { COLLECTION, TRANSFORMATION }
	
	@Getter private List<Resource> consumedResources;

	private final MappingExecutionService mappingExecutionService;
	private final ExtendedMappingContainer mc;
	private final ExtendedDatamodelContainer sourceDatamodel;
	private final ExtendedDatamodelContainer targetDatamodel;
	private final CollectingResourceConsumptionServiceImpl transformedCollector;
	
	private STAGES currentStage;
	@Getter @Setter private ResourceConsumptionService transformedConsumptionService;
	
	public TransformingConsumptionServiceImpl(MappingExecutionService mappingExecutionService, ExtendedMappingContainer mc, ExtendedDatamodelContainer sourceDatamodel, ExtendedDatamodelContainer targetDatamodel) {
		this.mappingExecutionService = mappingExecutionService;
		this.sourceDatamodel = sourceDatamodel;
		this.targetDatamodel = targetDatamodel;
		this.mc = mc;
		this.transformedCollector = new CollectingResourceConsumptionServiceImpl();
		
		this.consumedResources = new ArrayList<>();
		this.currentStage = STAGES.COLLECTION;
	}
	
	public List<Resource> getTransformedResources() {
		return transformedCollector.getResources();
	}
	
	
	@Override
	public boolean consume(Resource res) {
		if (currentStage==STAGES.COLLECTION) {
			return consumedResources.add(res);
		} else if (transformedConsumptionService!=null) {
			transformedConsumptionService.consume(res);
		}
		return true;
	}
	
	@Override
	public int commit() {
		if (currentStage==STAGES.COLLECTION) {
			currentStage=STAGES.TRANSFORMATION;
			log.debug("Commit on {} consumed resources", consumedResources==null ? 0 : consumedResources.size());
			if (consumedResources!=null && !consumedResources.isEmpty()) {
				this.transformResources();
				log.debug("Transformed to {} resources", this.getTransformedResources()==null ? 0 : this.getTransformedResources().size());
			}
			if (transformedConsumptionService!=null) {
				try {
					transformedConsumptionService.init(targetDatamodel.getId());	
				} catch (Exception ex) {
					log.error("Failed to init transformed consumption service");
				}
			}
			return consumedResources==null ? 0 : consumedResources.size();
		} else {
			if (transformedConsumptionService!=null) {
				transformedConsumptionService.commit();
			}
			return this.getTransformedResources()==null ? 0 : this.getTransformedResources().size();
		}
	}
	
	@Override
	public void init(String datamodelId) { }
	
	private void transformResources() {
		try {
			MappingExecGroup mExecGroup = MappingHelpers.buildMappingExecutionGroup(mc, sourceDatamodel, targetDatamodel);
			mappingExecutionService.addConsumptionService(transformedCollector);
			if (mExecGroup!=null && (mExecGroup.getConcepts()==null || mExecGroup.getConcepts().isEmpty())) {
				mExecGroup = null;
			}
			
			mappingExecutionService.addConsumptionService(this);
			
			mappingExecutionService.init(mExecGroup, consumedResources);
			mappingExecutionService.run();
		} catch (ProcessingConfigException e) {
			log.error("Failed to transform resources", e);
		}
	}
}