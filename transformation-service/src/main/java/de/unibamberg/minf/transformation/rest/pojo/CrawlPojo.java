package de.unibamberg.minf.transformation.rest.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class CrawlPojo extends BasePojo {
	private CrawlPojo baseCrawl;
	private String prefix;
	private List<String> messages;
	private boolean error;
	private boolean complete;
	private boolean online;
	private boolean offline;
}