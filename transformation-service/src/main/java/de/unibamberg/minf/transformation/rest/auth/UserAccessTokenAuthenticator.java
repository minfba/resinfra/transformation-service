package de.unibamberg.minf.transformation.rest.auth;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.pac4j.core.context.WebContext;
import org.pac4j.core.context.session.SessionStore;
import org.pac4j.core.credentials.Credentials;
import org.pac4j.core.credentials.UsernamePasswordCredentials;
import org.pac4j.core.exception.CredentialsException;
import org.pac4j.core.util.CommonHelper;
import org.pac4j.core.util.Pac4jConstants;
import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.transformation.model.PersistedUser;
import de.unibamberg.minf.transformation.service.UserService;
import eu.dariah.de.dariahsp.authentication.UserTokenAuthenticator;
import eu.dariah.de.dariahsp.config.PermissionDefinition;
import eu.dariah.de.dariahsp.model.ExtendedUserProfile;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserAccessTokenAuthenticator implements UserTokenAuthenticator {

	@Autowired protected UserService userService;
	
	@Setter List<PermissionDefinition> permissionDefinitions;
	
	@Override
	public void validate(Credentials credentials, WebContext context, SessionStore sessionStore) {
		try {
			if (credentials == null || !UsernamePasswordCredentials.class.isAssignableFrom(credentials.getClass()) ) {
	            throw new CredentialsException("No or invalid type of credential");
	        }
	        
	        String token = ((UsernamePasswordCredentials)credentials).getPassword();
	        if (CommonHelper.isBlank(token)) {
	        	throw new CredentialsException("Token cannot be blank");
	        }
	        
	        Optional<PersistedUser> optUser = userService.findByTokenUniqueId(token);
	        if (optUser.isEmpty()) {
	        	throw new CredentialsException("Invalid token");
	        }
	
	        PersistedUser user = optUser.get();
	
	        ExtendedUserProfile profile = new ExtendedUserProfile();
	        profile.setId(user.getUsername());
	        profile.setIssuerId(user.getIssuer());
	        profile.addAttribute(Pac4jConstants.USERNAME, user.getUsername());
	        
	        if (user.getAuthorities()!=null && !user.getAuthorities().isEmpty()) {
	        	profile.setRoles(new HashSet<>(user.getAuthorities()));
	        	profile.setLevel(this.getLevel(user.getAuthorities()));
	        }
	        
	        credentials.setUserProfile(profile);
		} catch (CredentialsException e) {
			log.info("User token authentication failed: {}", 
					e.getMessage());
			throw e;
		}
	}
	
	private int getLevel(Collection<String> roles) {
		return permissionDefinitions.stream()
			.filter(pd -> roles.contains(pd.getPermissionSet()))
			.mapToInt(PermissionDefinition::getLevel)
			.max().orElse(0);
	}
}
