package de.unibamberg.minf.transformation.rest.model;

import java.util.Calendar;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=true)
@RequiredArgsConstructor
public class ErrorRestResponse extends RestResponse {
	private final int status;
	private final long timestamp = Calendar.getInstance().getTimeInMillis();
	private final String message;
	private JsonNode errorDetails;
	private final String path;
		
	public String getError() {
		HttpStatus httpStatus = HttpStatus.resolve(status);
		if (httpStatus!=null) {
			return httpStatus.getReasonPhrase();
		}
		return "Unknown error";
	}
}
