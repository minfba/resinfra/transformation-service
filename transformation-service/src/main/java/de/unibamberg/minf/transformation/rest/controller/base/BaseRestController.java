package de.unibamberg.minf.transformation.rest.controller.base;

import org.springframework.validation.BindException;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import de.unibamberg.minf.transformation.config.RestControllerConfig;
import de.unibamberg.minf.transformation.execution.exception.ApiRuntimeException;
import de.unibamberg.minf.transformation.rest.helper.RestLinksHelper;
import de.unibamberg.minf.transformation.rest.model.ErrorRestResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse.ApiActions;
import de.unibamberg.minf.transformation.rest.pojo.BasePojo;
import de.unibamberg.minf.transformation.rest.pojo.error.ValidationFieldError;
import de.unibamberg.minf.transformation.rest.pojo.error.ValidationObjectError;
import de.unibamberg.minf.transformation.service.UserService;
import eu.dariah.de.dariahsp.web.AuthInfoHelper;

public class BaseRestController<T extends BasePojo> {
	private static final String REST_URL_STRING_FORMAT = "%s?start=%s&limit=%s";
	
	@Autowired private RestControllerConfig config;
	@Autowired protected ObjectMapper objectMapper;
	@Autowired protected RestLinksHelper linksHelper;
	@Autowired protected MessageSource messageSource;
	@Autowired protected AuthInfoHelper authInfoHelper;
	
	@Autowired protected UserService userService;
	
	
	@Value("${baseUrl}")
	private String baseUrl;
	
	private final String relativeBase;
	
	public BaseRestController(String relativeBase) {
		this.relativeBase = relativeBase;
	}
	
	public String getBaseUrl() {
		return (baseUrl.endsWith("/") ? baseUrl.substring(0, baseUrl.length()-1) : baseUrl);
	}
	
	public String getControllerBaseUrl() {
		return this.getBaseUrl() + relativeBase;
	}
	
	public String getControllerBaseUrlWithSuffix(String suffix) {
		return this.getControllerBaseUrl() + suffix;
	}
	
	@ExceptionHandler(ApiRuntimeException.class)
	public ErrorRestResponse handleRuntimeException(HttpServletResponse resp, HttpServletRequest req, ApiRuntimeException ex, Locale locale) {
		resp.setStatus(ex.getStatus().value());
		String message = ex.getLocalizedMessage(messageSource, locale);
		return new ErrorRestResponse(ex.getStatus().value(), message, req.getRequestURL().toString());
	}
	
	@ExceptionHandler(BindException.class)
	public ErrorRestResponse handleBindException(HttpServletResponse resp, HttpServletRequest req, BindException ex, Locale locale) {
		resp.setStatus(HttpStatus.BAD_REQUEST.value());

		ObjectNode errors = objectMapper.createObjectNode();
		if (ex.getBindingResult().hasFieldErrors()) {
			errors.set("fieldErrors", objectMapper.valueToTree(ex.getBindingResult().getFieldErrors().stream()
					.map(f -> ValidationFieldError.fromFieldError(f, messageSource, locale))
					.collect(Collectors.toList())));
		}
		if (ex.getBindingResult().hasGlobalErrors()) {
			errors.set("objectErrors", objectMapper.valueToTree(ex.getBindingResult().getGlobalErrors().stream()
					.map(f -> ValidationObjectError.fromObjectError(f, messageSource, locale))
					.collect(Collectors.toList())));
		}
		
		ErrorRestResponse errResp = new ErrorRestResponse(HttpStatus.BAD_REQUEST.value(), messageSource.getMessage("~de.unibamberg.minf.transformation.errors.validation_failed", null, locale), req.getRequestURL().toString());
		errResp.setErrorDetails(errors);
		return errResp;
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ErrorRestResponse handleException(HttpServletResponse resp, HttpServletRequest req, DataIntegrityViolationException ex, Locale locale) {
		resp.setStatus(HttpStatus.BAD_REQUEST.value());
		
		ObjectNode errors = objectMapper.createObjectNode();
		ArrayNode objErrors = objectMapper.createArrayNode();
		errors.set("objectErrors", objErrors); 
				
		if (ex.getCause()!=null && ex.getCause().getClass().isAssignableFrom(ConstraintViolationException.class)) {
			ValidationObjectError objError = new ValidationObjectError();
			objError.setMessage(((ConstraintViolationException)ex.getCause()).getConstraintName());			
			objErrors.add(objectMapper.valueToTree(objError));
		}
		
		ErrorRestResponse errResp = new ErrorRestResponse(HttpStatus.BAD_REQUEST.value(), messageSource.getMessage("~de.unibamberg.minf.transformation.errors.data_integrity_violation", null, locale), req.getRequestURL().toString());
		errResp.setErrorDetails(errors);
		return errResp;
	}
	
	protected RestItemResponse getItemResponse(T item, HttpServletRequest request, ApiActions action) {
		RestItemResponse itemResponse = new RestItemResponse();
		itemResponse.setItem(this.getItem(item, request.getRequestURL().toString()));
		itemResponse.setAction(action);
		itemResponse.setLinks(this.getLinks(request.getRequestURL().toString()));
		return itemResponse;
	}
		
	protected int getActualLimit(Integer limit) {
        if (limit!=null && limit.intValue()<=config.getRestControllerMaxLimit() && limit > 0) {
            return limit.intValue();
        } else {
            return config.getRestControllerDefaultLimit();
        }  
	}
	
	protected void setLinksOnSubitems(ObjectNode item, String fieldName, String requestUrl, boolean suffix) {
		this.setLinksOnSubitems(item, fieldName, fieldName, requestUrl, suffix);
	}
	
	protected void setLinksOnSubitems(ObjectNode item, String fieldName, String contextPath, String requestUrl, boolean suffix) {
		if (item.has(fieldName)) {
			JsonNode subItem = item.get(fieldName);
			if (subItem.isArray()) { 
				for (JsonNode t : (ArrayNode)subItem) {
					this.setLinksOnSubitem((ObjectNode)t, fieldName, contextPath, requestUrl, suffix);
				}
			} else {
				this.setLinksOnSubitem((ObjectNode)subItem, fieldName, contextPath, requestUrl, suffix);
			}
		}
	}
	
	protected void setLinksOnSubitem(ObjectNode subItem, String fieldName, String requestUrl, boolean suffix) {
		this.setLinksOnSubitem(subItem, fieldName, fieldName, requestUrl, suffix);
	}
	
	protected void setLinksOnSubitem(ObjectNode subItem, String fieldName, String contextPath, String requestUrl, boolean suffix) {
		StringBuilder tokenLinkBuilder = new StringBuilder();
		if (suffix) {
			tokenLinkBuilder.append(requestUrl);
		} else {
			tokenLinkBuilder
				.append(this.getBaseUrl())
				.append("/api");
		}
		tokenLinkBuilder
			.append(RestLinksHelper.SLASH)
			.append(contextPath);
		
		if (subItem.get("uniqueId")!=null) {
			tokenLinkBuilder
				.append(RestLinksHelper.SLASH)
				.append(subItem.get("uniqueId").textValue()); 
		}
		subItem.set("_links", new TextNode(tokenLinkBuilder.toString()));
	}
	
	protected ObjectNode getLinks(String requestUrl, long size, int limit, int start) {
		ObjectNode links = objectMapper.createObjectNode();
		links.put("self", this.getLink(requestUrl, start, limit));
			    
	    if (size>0) {
		    if (start+limit < size) {
		    	links.put("next", this.getLink(requestUrl, start+limit, limit));
	        }         
		    if (start>0) {
		    	links.put("prev", this.getLink(requestUrl, start<limit ? 0 : start-limit, limit));
	        }
	    }
  	    return links;
	}
	
	protected ObjectNode getLinks(String requestUrl) {
		ObjectNode links = objectMapper.createObjectNode();
		links.put("self", requestUrl);
		links.put("base", this.getControllerBaseUrl());
		return links;
	}
	
	protected ArrayNode getItems(List<T> items, String requestUrl) {
		ArrayNode array = objectMapper.createArrayNode();
		for (T rec : items) {
			array.add(this.getItem(rec, requestUrl, true));
		}
		return array;
	}
	
	protected ObjectNode getItem(T item, String requestUrl) {
		return this.getItem(item, requestUrl, true);
	}
	
	protected ObjectNode getItem(T item, String requestUrl, boolean suffixUniqueId) {
		ObjectNode recNode = objectMapper.valueToTree(item);
		recNode.set("_links", this.getItemLinks(item, requestUrl, suffixUniqueId));
		return recNode;
	}
	
	protected ObjectNode getItemLinks(T item, String requestUrl, boolean suffixUniqueId) {
		return this.getItemLinks(item.getUniqueId(), requestUrl, suffixUniqueId);
	}
	
	protected ObjectNode getItemLinks(String uniqueId, String requestUrl, boolean suffixUniqueId) {
		ObjectNode links = objectMapper.createObjectNode();
		links.set("self", new TextNode(requestUrl + (suffixUniqueId ? (RestLinksHelper.SLASH + uniqueId) : "")));
		return links;
	}
	
	private String getLink(String url, int start, int limit) {
		return String.format(REST_URL_STRING_FORMAT, url, start, limit);
	}
}
