package de.unibamberg.minf.transformation.rest.controller;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Interface;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.InterfaceConverter;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.pojo.InterfacePojo;
import de.unibamberg.minf.transformation.service.InterfaceService;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Interfaces", description = "API methods on endpoint interfaces")
@RestController
@RequestMapping("/api/interfaces")
public class InterfaceRestController extends BaseRestController<InterfacePojo> {
	
	@Autowired private InterfaceService interfaceService;
	@Autowired private InterfaceConverter interfaceConverter;
	
	public InterfaceRestController() {
		super("/api/interfaces");
	}

	@GetMapping("/{uniqueId}")
    public RestItemResponse getInterface(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		
		Optional<Interface> i = interfaceService.findByUniqueId(uniqueId);
		if (i.isEmpty()) {
			throw new ApiItemNotFoundException("interface", uniqueId);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(interfaceConverter.convert(i.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}	
}