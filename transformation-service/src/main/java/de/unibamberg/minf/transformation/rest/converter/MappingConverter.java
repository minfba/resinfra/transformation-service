package de.unibamberg.minf.transformation.rest.converter;

import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Component;

import de.unibamberg.minf.dme.model.mapping.base.Mapping;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.DatamodelPojo;
import de.unibamberg.minf.transformation.rest.pojo.MappingPojo;

@Component
public class MappingConverter extends BaseConverter<Mapping, MappingPojo> {

	@Override
	public MappingPojo convert(Mapping mapping, Locale locale, List<Class<?>> convertedTypes) {
		MappingPojo pojo = new MappingPojo();
		if (!convertedTypes.contains(Mapping.class)) {
			convertedTypes.add(Mapping.class);
		}
		return this.fill(pojo, mapping, locale, convertedTypes);
	}

	@Override
	public MappingPojo fill(MappingPojo pojo, Mapping mapping, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setUniqueId(mapping.getId());
		
		DatamodelPojo dmPojo = new DatamodelPojo();
		dmPojo.setUniqueId(mapping.getSourceId());
		pojo.setSource(dmPojo);
		
		dmPojo = new DatamodelPojo();
		dmPojo.setUniqueId(mapping.getTargetId());
		pojo.setTarget(dmPojo);
				
		return pojo;
	}
}
