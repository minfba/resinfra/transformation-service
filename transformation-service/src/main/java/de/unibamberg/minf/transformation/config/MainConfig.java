package de.unibamberg.minf.transformation.config;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import de.unibamberg.minf.core.util.json.JsonNodeHelper;
import de.unibamberg.minf.core.web.service.ImageService;
import de.unibamberg.minf.core.web.service.ImageServiceImpl;
import de.unibamberg.minf.core.web.service.ImageServiceImpl.SizeBoundsType;
import de.unibamberg.minf.transformation.config.nested.ImagesConfigProperties;
import de.unibamberg.minf.transformation.config.nested.ThumbnailsConfigProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@Configuration
@ConfigurationProperties
public class MainConfig extends TransformationConfig {
	
	@Autowired ResourceLoader resourceLoader;
		
	@Autowired PathsConfig paths; 
	private ImagesConfigProperties images;	
	
	@PostConstruct
    public void completeConfiguration() throws IOException {
		if (images==null) {
			images = new ImagesConfigProperties();
			images.setThumbnails(new ThumbnailsConfigProperties());
		}		
    }
	
	@Bean
	public ImageService imageService() {
		ImageServiceImpl imageService = new ImageServiceImpl();
		imageService.setImagePath(paths.getPictures());
		imageService.setImagesHeight(images.getHeight());
		imageService.setImagesWidth(images.getWidth());
		imageService.setImagesSizeType(SizeBoundsType.MAX_LONGEST_SIDE);
		imageService.setThumbnailsHeight(images.getThumbnails().getHeight());
		imageService.setThumbnailsWidth(images.getThumbnails().getWidth());
		imageService.setThumbnailsSizeType(SizeBoundsType.MAX_SHORTEST_SIDE);
		return imageService;
	}
	
	/*@Bean
	public CachedImageServiceImpl cachedImageService() {
		CachedImageServiceImpl cachedImageServiceImpl = new CachedImageServiceImpl();
		cachedImageServiceImpl.setImagePath(paths.getDownloads());
		cachedImageServiceImpl.setImagesSizeType(SizeBoundsType.MAX_SHORTEST_SIDE);
		cachedImageServiceImpl.setImagesWidth(images.getWidth());
		cachedImageServiceImpl.setImagesHeight(images.getHeight());
		cachedImageServiceImpl.setThumbnailsSizeType(SizeBoundsType.MAX_SHORTEST_SIDE);
		cachedImageServiceImpl.setThumbnailsWidth(images.getThumbnails().getWidth());
		cachedImageServiceImpl.setThumbnailsHeight(images.getThumbnails().getHeight());
		
		return cachedImageServiceImpl;
	}
	
	@Bean
	public Settings indexConfigSettings() throws ConfigurationException, IOException {
		File indexConfig = new File(this.getPaths().getConfig() + File.separator + this.getIndexing().getIndexConfigFile());
		if (indexConfig.exists()) {
			return Settings.builder().loadFromPath(Paths.get(indexConfig.getAbsolutePath())).build();
		}
		return Settings.builder().loadFromStream("index_config.json", resourceLoader.getResource("classpath:conf/" + this.getIndexing().getIndexConfigFile()).getInputStream(), true).build();
	}
	
	@Bean
	public UpdateServiceImpl updateService() throws NoSuchAlgorithmException {
		return new UpdateServiceImpl(paths.getBackups(), mongoConfig.getDatabase());
	}*/
	

	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper(); 
		mapper.findAndRegisterModules();
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		return mapper;
	}
	
	@Bean
	public JsonNodeHelper jsonNodeHelper(ObjectMapper objectMapper) {
		JsonNodeHelper jsonNodeHelper = new JsonNodeHelper();
		jsonNodeHelper.setObjMapper(objectMapper);
		return jsonNodeHelper;
	}	
}
