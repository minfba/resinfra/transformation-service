package de.unibamberg.minf.transformation.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.node.ArrayNode;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@JsonInclude(Include.NON_NULL)
public class RestItemsResponse extends RestResponse {
	private Long size;
	private Integer start;
	private Integer limit;
	private ArrayNode items;
	
	
	public void setSize(long size) {
		this.size = size;
	}
}
