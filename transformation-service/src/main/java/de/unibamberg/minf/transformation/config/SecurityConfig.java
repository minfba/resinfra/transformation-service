package de.unibamberg.minf.transformation.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import de.unibamberg.minf.transformation.config.nested.RemoteTokensConfigProperties;
import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix="security")
public class SecurityConfig {
	private RemoteTokensConfigProperties[] remoteTokens;
}
