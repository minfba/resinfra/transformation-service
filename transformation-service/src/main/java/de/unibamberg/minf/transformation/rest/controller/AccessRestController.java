package de.unibamberg.minf.transformation.rest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.DatasourceConverter;
import de.unibamberg.minf.transformation.rest.converter.EndpointConverter;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemsResponse;
import de.unibamberg.minf.transformation.rest.pojo.AccessPojo;
import de.unibamberg.minf.transformation.rest.pojo.DatasourcePojo;
import de.unibamberg.minf.transformation.rest.pojo.EndpointPojo;
import de.unibamberg.minf.transformation.service.AccessService;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Access Methods", description = "API methods on access methods (combines datasources and endpoints)")
@RestController
@RequestMapping("/api/access")
public class AccessRestController extends BaseRestController<AccessPojo> {
	@Autowired private AccessService accessService;
	@Autowired private DatasourceConverter datasourceConverter;
	@Autowired private EndpointConverter endpointConverter;
	
	public AccessRestController() {
		super("/api/access");
	}

	@GetMapping("/")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("/api/access");
		return null;
	}
	
	@GetMapping
    public RestItemsResponse getAccesses(HttpServletRequest request, Locale locale) {
		RestItemsResponse response = new RestItemsResponse();
		List<AccessPojo> access = new ArrayList<>(); 
		for (DatasourcePojo p : datasourceConverter.convert(accessService.findAllDatasources(), locale)) {
			access.add(p);
		}
		for (EndpointPojo p : endpointConverter.convert(accessService.findAllEndpoints(), locale)) {
			access.add(p);
		}				
		response.setSize(access.size());
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItems(this.getItems(access, request.getRequestURL().toString()));
		
		return response;
	}
		
	@GetMapping("/{uniqueId}")
    public RestItemResponse getAccess(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		Optional<Datasource> e = accessService.findDatasourceByUniqueId(uniqueId);
		if (e.isEmpty()) {
			throw new ApiItemNotFoundException("access", uniqueId);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(datasourceConverter.convert(e.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}
	
	@PostMapping("/{uniqueId}/lock")
    public RestItemResponse lockAccess(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		Optional<Datasource> e = accessService.findDatasourceByUniqueId(uniqueId);
		if (e.isEmpty()) {
			throw new ApiItemNotFoundException("access", uniqueId);
		}
		
		e.get().setAuthRequired(true);
		accessService.saveAccess(e.get());
		
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(datasourceConverter.convert(e.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}
	
	@PostMapping("/{uniqueId}/unlock")
    public RestItemResponse unlockAccess(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		Optional<Datasource> e = accessService.findDatasourceByUniqueId(uniqueId);
		if (e.isEmpty()) {
			throw new ApiItemNotFoundException("access", uniqueId);
		}
		
		e.get().setAuthRequired(false);
		accessService.saveAccess(e.get());
		
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(datasourceConverter.convert(e.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}
	
	@Override
	protected ObjectNode getItem(AccessPojo c, String requestUrl, boolean suffixUniqueId) {
		ObjectNode item = super.getItem(c, requestUrl, suffixUniqueId);
		if (item.has("datasets")) {
			for (JsonNode t : (ArrayNode)item.get("datasets")) {
				this.setLinksOnSubitem((ObjectNode)t, "datasets", requestUrl, false);
				if (t.has("datamodel")) {
					this.setLinksOnSubitem((ObjectNode)t.get("datamodel"), "datamodel", "datamodels", requestUrl, false);
				}
			}
		}
		if (item.has("interfaces")) {
			for (JsonNode t : (ArrayNode)item.get("interfaces")) {
				this.setLinksOnSubitem((ObjectNode)t, "interfaces", requestUrl, false);
				if (t.has("datamodel")) {
					this.setLinksOnSubitem((ObjectNode)t.get("datamodel"), "datamodel", "datamodels", requestUrl, false);
				}
			}
		}
		if (item.has("collection")) {
			ObjectNode t = (ObjectNode)item.get("collection");
			this.setLinksOnSubitem(t, "collection", requestUrl, false);
		}
		return item;
	}
	
}