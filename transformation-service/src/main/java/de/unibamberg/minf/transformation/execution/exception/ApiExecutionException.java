package de.unibamberg.minf.transformation.execution.exception;

public class ApiExecutionException extends Exception {
	private static final long serialVersionUID = 6785131852352003028L;

	public ApiExecutionException(String message) {
		super(message);
	}
	
	public ApiExecutionException(String message, Throwable source) {
		super(message, source);
	}
	
	public ApiExecutionException(Throwable source) {
		super(source);
	}
}
