package de.unibamberg.minf.transformation.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.JsonNode;

import de.unibamberg.minf.transformation.rest.pojo.ApiPojo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@JsonInclude(Include.NON_NULL)
public class ApiRestResponse extends RestResponse {
	private ApiPojo api;
	private JsonNode response;
}
