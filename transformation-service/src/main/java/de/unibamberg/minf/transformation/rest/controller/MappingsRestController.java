package de.unibamberg.minf.transformation.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import de.unibamberg.minf.transformation.automation.DmeSyncService;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.DatamodelConverter;
import de.unibamberg.minf.transformation.rest.converter.MappingConverter;
import de.unibamberg.minf.transformation.rest.model.RestActionResponse;
import de.unibamberg.minf.transformation.rest.model.RestActionResponse.ApiActionStatus;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemsResponse;
import de.unibamberg.minf.transformation.rest.pojo.DatamodelPojo;
import de.unibamberg.minf.transformation.rest.pojo.MappingPojo;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.MappingService;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Mappings", description = "API methods on mappings")
@RestController
@RequestMapping("/api/mappings")
public class MappingsRestController extends BaseRestController<MappingPojo> {

	
	@Autowired private MappingService mappingService;
	@Autowired private MappingConverter mappingConverter;

	
	public MappingsRestController() {
		super("/api/mappings");
	}
	
	@GetMapping("/")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("/api/mappings");
		return null;
	}
	
	@GetMapping
    public RestItemsResponse getMappings(HttpServletRequest request, Locale locale) {
		RestItemsResponse response = new RestItemsResponse();
		List<MappingPojo> mappings = mappingConverter.convert(mappingService.findAll().stream().map(t -> t.getMapping()).collect(Collectors.toList()), locale);
	
		response.setSize(mappings.size());
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItems(this.getItems(mappings, request.getRequestURL().toString()));
		
		return response;
	}
	
	@GetMapping("/{mappingUID}")
    public RestItemResponse getMapping(@PathVariable String mappingUID, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		
		ExtendedMappingContainer edm = mappingService.findById(mappingUID);
		if (edm==null) {
			throw new ApiItemNotFoundException("mapping", mappingUID);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(mappingConverter.convert(edm.getMapping(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}
}