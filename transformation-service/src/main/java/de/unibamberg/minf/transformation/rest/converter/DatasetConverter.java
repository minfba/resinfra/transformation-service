package de.unibamberg.minf.transformation.rest.converter;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.LocalDataset;
import de.unibamberg.minf.transformation.model.OnlineDataset;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.DatamodelPojo;
import de.unibamberg.minf.transformation.rest.pojo.DatasetPojo;

@Component
public class DatasetConverter extends BaseConverter<Dataset, DatasetPojo> {

	@Autowired private DatamodelConverter datamodelConverter;
	
	@Override
	public DatasetPojo convert(Dataset entity, Locale locale, List<Class<?>> convertedTypes) {
		if (!convertedTypes.contains(Dataset.class)) {
			convertedTypes.add(Dataset.class);
		}
		return this.fill(new DatasetPojo(), entity, locale, convertedTypes);
	}

	@Override
	public DatasetPojo fill(DatasetPojo pojo, Dataset entity, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setUniqueId(entity.getUniqueId());
		pojo.setOnline(entity.isOnline());
		pojo.setLocal(entity.isLocal());
		pojo.setNocache(entity.isNocache());
		
		if (OnlineDataset.class.isAssignableFrom(entity.getClass())) {
			OnlineDataset onlineDs = (OnlineDataset)entity;
			pojo.setExpires(onlineDs.getNextExecution());
			pojo.setRemoteAlias(onlineDs.getRemoteAlias());
		} else if (LocalDataset.class.isAssignableFrom(entity.getClass())) {
			LocalDataset localDs = (LocalDataset)entity;
			pojo.setExpires(localDs.getExpires());
			pojo.setLabel(localDs.getLabel());
		}
		if (entity.getDatamodel()!=null && entity.getDatamodel().getModel()!=null) {
			pojo.setDatamodel(datamodelConverter.convert(entity.getDatamodel().getModel()));
		} else {
			DatamodelPojo dmPojo = new DatamodelPojo();
			dmPojo.setUniqueId(entity.getDatamodelId());
			pojo.setDatamodel(dmPojo);
		}
		return pojo;
	}
}
