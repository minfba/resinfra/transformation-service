package de.unibamberg.minf.transformation.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import de.unibamberg.minf.transformation.config.nested.PathsConfigProperties;

@Configuration
@ConfigurationProperties(prefix="paths")
public class PathsConfig extends PathsConfigProperties {
	
	@PostConstruct
    public void completeConfiguration() throws IOException {
		// Config
		if (this.getConfig()==null) {
			this.setConfig(this.setupPath(this.getMain(), "config"));
		}
		
		// Backup
		if (this.getBackups()==null) {
			this.setBackups(this.setupPath(this.getMain(), "backups"));
		}
		
		// Data and subdirs
		if (this.getData()==null) {
			this.setData(this.setupPath(this.getMain(), "data"));
		}
		if (this.getDatamodels()==null) {
			this.setDatamodels(this.setupPath(this.getData(), "datamodels"));
		}
		if (this.getMappings()==null) {
			this.setMappings(this.setupPath(this.getData(), "mappings"));
		}
		if (this.getGrammars()==null) {
			this.setGrammars(this.setupPath(this.getData(), "grammars"));
		}
		if (this.getModels()==null) {
			this.setModels(this.setupPath(this.getData(), "models"));
		}
		if (this.getCrawls()==null) {
			this.setCrawls(this.setupPath(this.getData(), "crawls"));
		}
		if (this.getPictures()==null) {
			this.setPictures(this.setupPath(this.getData(), "pictures"));
		}
		if (this.getParseErrors()==null) {
			this.setParseErrors(this.setupPath(this.getData(), "parseErrors"));
		}
		if (this.getDownloads()==null) {
			this.setDownloads(this.setupPath(this.getData(), "downloads"));
		}
		if (this.getTemporary()==null) {
			this.setTemporary(this.setupPath(this.getData(), "temp"));
		}
    }
	
	protected String setupPath(String first, String... more) throws IOException {
		Path p = Paths.get(first, more);
		if (!Files.exists(p)) {
			Files.createDirectories(p);
		}
		return p.toString();
	}
}
