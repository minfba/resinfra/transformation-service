package de.unibamberg.minf.transformation.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@JsonInclude(Include.NON_NULL)
@EqualsAndHashCode(callSuper=false)
public class RestItemResponse extends RestResponse {
	public enum ApiActions { GET, DELETED, CREATED, UPDATED }
	private ApiActions action = ApiActions.GET;
	
	private JsonNode item;
}
