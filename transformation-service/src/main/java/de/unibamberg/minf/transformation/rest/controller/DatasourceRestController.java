package de.unibamberg.minf.transformation.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.unibamberg.minf.transformation.dao.db.DatasetDao;
import de.unibamberg.minf.transformation.execution.exception.ApiInsufficientPermissionsException;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.model.OnlineDataset;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.DatasetConverter;
import de.unibamberg.minf.transformation.rest.converter.DatasourceConverter;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemsResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse.ApiActions;
import de.unibamberg.minf.transformation.rest.pojo.ApiPojo;
import de.unibamberg.minf.transformation.rest.pojo.DatasetPojo;
import de.unibamberg.minf.transformation.rest.pojo.DatasourcePojo;

import de.unibamberg.minf.transformation.service.AccessService;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Datasources", description = "API methods on collection datasources")
@RestController
@RequestMapping("/api/datasources")
public class DatasourceRestController extends BaseRestController<DatasourcePojo> {
	@Autowired private AccessService accessService;
	@Autowired private DatasourceConverter datasourceConverter;
	
	@Autowired private DatasetDao datasetDao;
	
	public DatasourceRestController() {
		super("/api/datasources");
	}

	@GetMapping("/")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("/api/datasources");
		return null;
	}
	
	@GetMapping
    public RestItemsResponse getDatasources(HttpServletRequest request, Locale locale) {
		RestItemsResponse response = new RestItemsResponse();
		List<DatasourcePojo> datasources = datasourceConverter.convert(accessService.findAllDatasources(), locale);
	
		response.setSize(datasources.size());
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItems(this.getItems(datasources, request.getRequestURL().toString()));
		
		return response;
	}
		
	@GetMapping("/{uniqueId}")
    public RestItemResponse getDatasource(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		Optional<Datasource> e = accessService.findDatasourceByUniqueId(uniqueId);
		if (e.isEmpty()) {
			throw new ApiItemNotFoundException("datasource", uniqueId);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(datasourceConverter.convert(e.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}
	
	
	@PostMapping(value = "/{uniqueId}/dataset/{datasetId}", consumes = {"application/x-www-form-urlencoded"})
	public RestItemResponse updateApiJson(@PathVariable String uniqueId, @PathVariable String datasetId, @Valid DatasetPojo updatedDataset, HttpServletRequest request, Locale locale) throws ApiInsufficientPermissionsException, ApiItemNotFoundException {
		Optional<Datasource> source = accessService.findDatasourceByUniqueId(uniqueId);
		if (source.isPresent() && source.get().getDatasets()!=null) {
			for (Dataset ds : source.get().getDatasets()) {
				if (ds.getUniqueId().equals(datasetId)) {
					if (OnlineDataset.class.isAssignableFrom(ds.getClass())) {
						OnlineDataset.class.cast(ds).setNocache(updatedDataset.isNocache());
						datasetDao.save(ds);
					}
					return this.getItemResponse(datasourceConverter.convert(source.get(), locale), request, ApiActions.UPDATED);
				}
			}
		}
		throw new ApiItemNotFoundException("dataset", datasetId); 
	}
	
	@Override
	protected ObjectNode getItem(DatasourcePojo c, String requestUrl, boolean suffixUniqueId) {
		ObjectNode item = super.getItem(c, requestUrl, suffixUniqueId);
		if (item.has("datasets")) {
			for (JsonNode t : (ArrayNode)item.get("datasets")) {
				this.setLinksOnSubitem((ObjectNode)t, "datasets", requestUrl, false);
				if (t.has("datamodel")) {
					this.setLinksOnSubitem((ObjectNode)t.get("datamodel"), "datamodel", "datamodels", requestUrl, false);
				}
			}
		}
		if (item.has("collection")) {
			ObjectNode t = (ObjectNode)item.get("collection");
			this.setLinksOnSubitem(t, "collection", requestUrl, false);
		}
		return item;
	}
	
}