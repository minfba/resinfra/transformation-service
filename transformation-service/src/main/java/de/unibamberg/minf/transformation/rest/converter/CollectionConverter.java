package de.unibamberg.minf.transformation.rest.converter;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.Access;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.CollectionPojo;

@Component
public class CollectionConverter extends BaseConverter<Collection, CollectionPojo> {

	@Value("${api.colreg.base_url:null}")
	private String colregBaseUrl;
	
	private String colregCollectionUrl;
	
	@Autowired private EndpointConverter endpointConverter;
	@Autowired private DatasourceConverter datasourceConverter;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		super.afterPropertiesSet();
		if (colregBaseUrl!=null) {
			colregCollectionUrl = this.colregBaseUrl + "collections/%s";
		}
	}
	
	@Override
	public CollectionPojo convert(Collection collection, Locale locale, List<Class<?>> convertedTypes) {
		if (!convertedTypes.contains(Collection.class)) {
			convertedTypes.add(Collection.class);
		}
		return this.fill(new CollectionPojo(), collection, locale, convertedTypes);
	}

	@Override
	public CollectionPojo fill(CollectionPojo pojo, Collection collection, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setColregEntityId(collection.getColregEntityId());
		pojo.setColregVersionId(collection.getColregVersionId());
		pojo.setUniqueId(collection.getUniqueId());

		if (this.isLoaded(collection, "names")) {
			pojo.setName(collection.getName(locale.getLanguage()));
		}		
		if (this.canProceedWithType(Access.class, convertedTypes) && this.isLoaded(collection, "access")) {
			if (this.canProceedWithType(Endpoint.class, convertedTypes)) {
				pojo.setEndpoints(endpointConverter.convert(collection.getAccess().stream().filter(Access::isEndpoint).map(Access::asEndpoint).collect(Collectors.toList()), locale, convertedTypes));
			}
			if (this.canProceedWithType(Datasource.class, convertedTypes)) {
				pojo.setDatasources(datasourceConverter.convert(collection.getAccess().stream().filter(Access::isDatasource).map(Access::asDatasource).collect(Collectors.toList()), locale, convertedTypes));
			}
		}
		
		if (colregCollectionUrl!=null) {
			pojo.setColregUrl(String.format(colregCollectionUrl, collection.getColregEntityId()));
		}
		
		return pojo;
	}

}
