package de.unibamberg.minf.transformation.crawling;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.minf.processing.consumption.ResourceConsumptionService;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.transformation.backend.db.model.ResourceImpl;
import de.unibamberg.minf.transformation.backend.db.service.ResourceServiceImpl;
import de.unibamberg.minf.transformation.conversion.ResourceToJsonConverter;
import lombok.Data;

@Data
@Component
@Scope("prototype")
public class ResourcePersistenceServiceImpl implements ResourceConsumptionService {
	private Long datasetId;
	private Long endpointId;
	
	@Autowired private ResourceServiceImpl resourceService;
	@Autowired private ObjectMapper objectMapper;
	
	private Set<ResourceImpl> resourceBatch;
		
	@Override
	public void init(String schemaId) throws ProcessingConfigException {
		resourceBatch = new HashSet<>();
	}

	@Override
	public boolean consume(Resource obj) {
		try {
			resourceBatch.add(new ResourceImpl(null, endpointId, datasetId, null, objectMapper.writeValueAsString(obj)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public int commit() {
		resourceService.saveAll(resourceBatch);
		return resourceBatch.size();
	}

}
