package de.unibamberg.minf.transformation.execution.data;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.transformation.execution.BaseExecutionService;

@Component
@Scope("prototype")
public class DatasetTransformationServiceImpl extends BaseExecutionService implements DatasetTransformationService {
	@Override
	public List<Resource> transformResources(String sourceDatamodelId, String targetDatamodelId, List<Resource> resources) {
		return super.transformResourcesSync(sourceDatamodelId, targetDatamodelId, resources);
	}
}
