package de.unibamberg.minf.transformation.rest.pojo;

import java.util.Map;
import java.util.Set;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccessPojo extends BasePojo {
	public enum AccessTypes { Endpoint, Datasource }
	
	private AccessTypes type;
	private String url;
	private String accessType;
	private String accessModelId;
	private String fileType;
	private Map<String, String> params;
	private Set<String> fileAccessPatterns;
	private CollectionPojo collection;
	private boolean authRequired;
}
