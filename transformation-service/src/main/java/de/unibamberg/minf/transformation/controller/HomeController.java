package de.unibamberg.minf.transformation.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController extends BaseController {
	public HomeController() {
		super("home");
	}
	
	@GetMapping(value = "/")
	public String getHome(HttpServletRequest request, Model model, Locale locale) {		
		return "home";
	}
	
	@GetMapping(value = "/user")
	public String getUser(HttpServletRequest request, Model model, Locale locale) {
		return "common/user";
	}
}
