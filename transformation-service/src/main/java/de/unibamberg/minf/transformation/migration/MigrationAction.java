package de.unibamberg.minf.transformation.migration;

public interface MigrationAction {
	public boolean migrate();
}
