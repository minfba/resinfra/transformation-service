package de.unibamberg.minf.transformation.config;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

import de.unibamberg.minf.processing.ProcessingThreadFactory;
import de.unibamberg.minf.processing.service.ParallelFileProcessingService;

@Configuration
public class ThreadingConfig implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		ParallelFileProcessingService.setExecutorServiceInstance(10, new ProcessingThreadFactory("file-processing"));
	}

}
