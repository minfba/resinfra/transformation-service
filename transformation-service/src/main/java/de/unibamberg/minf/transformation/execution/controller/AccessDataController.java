package de.unibamberg.minf.transformation.execution.controller;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import de.unibamberg.minf.dme.model.datamodel.natures.CsvDatamodelNature;
import de.unibamberg.minf.dme.model.datamodel.natures.JsonDatamodelNature;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.model.SerializableResource;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.processing.output.csv.CsvStringOutputService;
import de.unibamberg.minf.processing.output.json.JsonStringOutputService;
import de.unibamberg.minf.transformation.automation.OnlineCrawlRunner;
import de.unibamberg.minf.transformation.backend.db.service.ResourceService;
import de.unibamberg.minf.transformation.execution.data.DatasetTransformationService;
import de.unibamberg.minf.transformation.execution.exception.ApiInsufficientPermissionsException;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.Datasource;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.AccessConverter;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemsResponse;
import de.unibamberg.minf.transformation.rest.model.RestResponse;
import de.unibamberg.minf.transformation.rest.pojo.AccessPojo;
import de.unibamberg.minf.transformation.service.AccessService;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.DatasetService;
import eu.dariah.de.dariahsp.web.model.AuthPojo;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Tag(name = "Data access", description = "Access data of available datasources in original or converted formats")
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/data")
public class AccessDataController extends BaseRestController<AccessPojo> implements ApplicationContextAware {
	@Autowired private AccessService accessService;
	@Autowired private DatasetService datasetService;
	@Autowired private AccessConverter accessConverter;
	@Autowired private ResourceService resourceService;
	@Autowired private DatasetTransformationService datasetTransformationService;
	@Autowired private DatamodelService datamodelService;
	@Autowired protected OnlineCrawlRunner onlineCrawlRunner;
	
	private ReentrantLock lock = new ReentrantLock();
	
	private Map<String, LocalDateTime> nocacheDatasetCallMap = new HashMap<>();	
	
	private ApplicationContext appContext;
	
	private static final int ONLINE_DATASET_MIN_CRAWL_AGE_MINUTES = 30;
	
	public AccessDataController() {
		super("/data");
	}
	
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;
	}

	
	@GetMapping("/{uniqueId}/identify")
	public RestResponse identify(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {		
		RestItemResponse response = new RestItemResponse();
		Optional<Datasource> e = accessService.findDatasourceByUniqueId(uniqueId);
		if (e.isEmpty()) {
			throw new ApiItemNotFoundException("access", uniqueId);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(accessConverter.convert(e.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}

	@GetMapping("/{accessUID}/{datasetUID}")
	public RestResponse callApiGet(@PathVariable String accessUID, @PathVariable String datasetUID, @RequestParam(required=false) String format, HttpServletRequest request, Locale locale) throws JsonMappingException, JsonProcessingException, ApiItemNotFoundException, ApiInsufficientPermissionsException, ProcessingConfigException {
		
		Optional<Datasource> e = accessService.findDatasourceByUniqueId(accessUID);
		if (e.isEmpty()) {
			throw new ApiItemNotFoundException("access", accessUID);
		}
		AuthPojo authPojo = authInfoHelper.getAuth();
		if (e.get().isAuthRequired() && !authPojo.isAuth()) {
			throw new ApiInsufficientPermissionsException();
		}
		
		Optional<Dataset> ds = datasetService.findByUniqueId(datasetUID);
		if (ds.isEmpty()) {
			throw new ApiItemNotFoundException("dataset", datasetUID);
		}
		
		this.loadDataIfApplicable(ds.get().getUniqueId());
		
		ExtendedDatamodelContainer dmc = datamodelService.findById(ds.get().getDatamodelId());

		RestResponse response = null;
		List<String> persistedResources = resourceService.findContainedByDatasetId(ds.get().getId()); 
		List<Resource> resources;
		if (persistedResources!=null) {
			resources = new ArrayList<>();
			for (String persistedResource : persistedResources) {
				resources.add(objectMapper.readValue(persistedResource, SerializableResource.class));
			}
						
			JsonStringOutputService svc = appContext.getBean(JsonStringOutputService.class);
			svc.setSchema(dmc.getModel());
			svc.setRoot(dmc.getOrRenderProcessingRoot());
			svc.setEscape(false);
			svc.setUseTerminals(dmc.getModel().getNatures()!=null && dmc.getModel().getNatures().stream().anyMatch(n -> JsonDatamodelNature.class.isAssignableFrom(n.getClass())));
			svc.init();
			
			JsonNode n = objectMapper.readTree(svc.writeToString(resources.toArray(new Resource[0])));
			if (n.isArray()) {
				response = new RestItemsResponse();
				// TODO: This requires parsing all JSON data, which is slow and needs to be changed
				((RestItemsResponse)response).setItems(ArrayNode.class.cast(n));
			} else {
				response = new RestItemResponse();
				((RestItemResponse)response).setItem(n);
			}
		}
		if (response==null) {
			response = new RestItemsResponse();
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		return response;
	}
	
	@GetMapping("/{accessUID}/{datasetUID}.csv")
	public void callApiGetCsv(@PathVariable String accessUID, @PathVariable String datasetUID, @RequestParam(defaultValue="false") boolean outputAllColumns, HttpServletRequest request, HttpServletResponse response, Locale locale) throws ApiItemNotFoundException, ProcessingConfigException, IOException, ApiInsufficientPermissionsException {
		Optional<Datasource> e = accessService.findDatasourceByUniqueId(accessUID);
		if (e.isEmpty()) {
			throw new ApiItemNotFoundException("access", accessUID);
		}
		AuthPojo authPojo = authInfoHelper.getAuth();
		if (e.get().isAuthRequired() && !authPojo.isAuth()) {
			throw new ApiInsufficientPermissionsException();
		}
		Optional<Dataset> ds = datasetService.findByUniqueId(datasetUID);
		if (ds.isEmpty()) {
			throw new ApiItemNotFoundException("dataset", datasetUID);
		}
		
		this.loadDataIfApplicable(ds.get().getUniqueId());
		
		ExtendedDatamodelContainer dmc = datamodelService.findById(ds.get().getDatamodelId());
		
		boolean includePaths = request.getParameter("paths")!=null && (request.getParameter("paths").equalsIgnoreCase("true") || request.getParameter("paths").equals("1"));
		
		List<String> persistedResources = resourceService.findContainedByDatasetId(ds.get().getId()); 
		List<Resource> resources;
		if (persistedResources!=null) {
			resources = new ArrayList<>();
			for (String persistedResource : persistedResources) {
				resources.add(objectMapper.readValue(persistedResource, SerializableResource.class));
			}
			CsvStringOutputService svc = appContext.getBean(CsvStringOutputService.class);
			svc.setSchema(dmc.getModel());
			svc.setRoot(dmc.getOrRenderProcessingRoot());
			svc.setEscape(false);
			svc.setPrefixPath(includePaths);
			svc.setUseTerminals(dmc.getModel().getNatures()!=null && dmc.getModel().getNatures().stream().anyMatch(n -> CsvDatamodelNature.class.isAssignableFrom(n.getClass())));
			svc.setOutputAllColumns(outputAllColumns);
			svc.init();
			
			response.setContentType("text/plain; charset=utf-8");
			response.getWriter().print(svc.writeToString(resources.toArray(new Resource[0])));
		}		
	}
	
	@GetMapping("/{accessUID}/{datasetUID}/{targetModelUID}.csv")
	public void callApiGetTransformCsv(@PathVariable String accessUID, @PathVariable String datasetUID, @PathVariable String targetModelUID, @RequestParam(defaultValue="false") boolean outputAllColumns, HttpServletRequest request, HttpServletResponse response, Locale locale) throws ApiItemNotFoundException, IOException, ProcessingConfigException, ApiInsufficientPermissionsException {
	
		Optional<Datasource> e = accessService.findDatasourceByUniqueId(accessUID);
		if (e.isEmpty()) {
			throw new ApiItemNotFoundException("access", accessUID);
		}
		AuthPojo authPojo = authInfoHelper.getAuth();
		if (e.get().isAuthRequired() && !authPojo.isAuth()) {
			throw new ApiInsufficientPermissionsException();
		}
		//response.setItem(this.getItem(accessConverter.convert(e.get(), locale), request.getRequestURL().toString(), false));
		
		Optional<Dataset> ds = datasetService.findByUniqueId(datasetUID);
		if (ds.isEmpty()) {
			throw new ApiItemNotFoundException("dataset", datasetUID);
		}
		
		this.loadDataIfApplicable(ds.get().getUniqueId());
		
		ExtendedDatamodelContainer dmc = datamodelService.findById(targetModelUID);
		
		boolean includePaths = request.getParameter("paths")!=null && (request.getParameter("paths").equalsIgnoreCase("true") || request.getParameter("paths").equals("1"));
		
		
		
		List<String> persistedResources = resourceService.findContainedByDatasetId(ds.get().getId()); 
		List<Resource> resources;
		if (persistedResources!=null) {
			resources = new ArrayList<>();
			for (String persistedResource : persistedResources) {
				resources.add(objectMapper.readValue(persistedResource, SerializableResource.class));
			}
			
			List<Resource> transformed = datasetTransformationService.transformResources(ds.get().getDatamodelId(), targetModelUID, resources);
			
			CsvStringOutputService svc = appContext.getBean(CsvStringOutputService.class);
			svc.setSchema(dmc.getModel());
			svc.setRoot(dmc.getOrRenderProcessingRoot());
			svc.setEscape(false);
			svc.setPrefixPath(includePaths);
			svc.setUseTerminals(dmc.getModel().getNatures()!=null && dmc.getModel().getNatures().stream().anyMatch(n -> CsvDatamodelNature.class.isAssignableFrom(n.getClass())));
			svc.setOutputAllColumns(outputAllColumns);
			svc.init();
			
			response.setContentType("text/plain; charset=utf-8");
			response.getWriter().print(svc.writeToString(transformed.toArray(new Resource[0])));
		}
	}
	
	@GetMapping("/{accessUID}/{datasetUID}/{targetModelUID}")
	public RestResponse callApiGetTransform(@PathVariable String accessUID, @PathVariable String datasetUID, @PathVariable String targetModelUID, @RequestParam(required=false) String format, HttpServletRequest request, Locale locale) throws JsonMappingException, JsonProcessingException, ApiItemNotFoundException, ApiInsufficientPermissionsException, ProcessingConfigException {
		
		Optional<Datasource> e = accessService.findDatasourceByUniqueId(accessUID);
		if (e.isEmpty()) {
			throw new ApiItemNotFoundException("access", accessUID);
		}
		AuthPojo authPojo = authInfoHelper.getAuth();
		if (e.get().isAuthRequired() && !authPojo.isAuth()) {
			throw new ApiInsufficientPermissionsException();
		}
		
		//response.setItem(this.getItem(accessConverter.convert(e.get(), locale), request.getRequestURL().toString(), false));
		
		Optional<Dataset> ds = datasetService.findByUniqueId(datasetUID);
		if (ds.isEmpty()) {
			throw new ApiItemNotFoundException("dataset", datasetUID);
		}
		
		this.loadDataIfApplicable(ds.get().getUniqueId());
		
		RestResponse response = null;
		ExtendedDatamodelContainer dmc = datamodelService.findById(targetModelUID);
		List<String> persistedResources = resourceService.findContainedByDatasetId(ds.get().getId()); 
		List<Resource> resources;
		if (persistedResources!=null) {
			resources = new ArrayList<>();
			for (String persistedResource : persistedResources) {
				resources.add(objectMapper.readValue(persistedResource, SerializableResource.class));
			}
			
			List<Resource> transformed = datasetTransformationService.transformResources(ds.get().getDatamodelId(), targetModelUID, resources);
			
			JsonStringOutputService svc = appContext.getBean(JsonStringOutputService.class);
			svc.setSchema(dmc.getModel());
			svc.setRoot(dmc.getOrRenderProcessingRoot());
			svc.setEscape(false);
			svc.setUseTerminals(dmc.getModel().getNatures()!=null && dmc.getModel().getNatures().stream().anyMatch(n -> JsonDatamodelNature.class.isAssignableFrom(n.getClass())));
			svc.init();
			
			JsonNode n = objectMapper.readTree(svc.writeToString(transformed.toArray(new Resource[0])));
			if (n.isArray()) {
				response = new RestItemsResponse();
				// TODO: This requires parsing all JSON data, which is slow and needs to be changed
				((RestItemsResponse)response).setItems(ArrayNode.class.cast(n));
			} else {
				response = new RestItemResponse();
				((RestItemResponse)response).setItem(n);
			}
		}
		if (response==null) {
			response = new RestItemsResponse();
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		return response;
	}

	private void loadDataIfApplicable(String datasetUniqueId) {
		if (!nocacheDatasetCallMap.containsKey(datasetUniqueId) || 
				Duration.between(nocacheDatasetCallMap.get(datasetUniqueId), LocalDateTime.now()).toMinutes()>ONLINE_DATASET_MIN_CRAWL_AGE_MINUTES) {
			log.info("Fetching nocache dataset");			
			try {
				lock.lock();
				nocacheDatasetCallMap.put(datasetUniqueId, LocalDateTime.now());
			} finally {
				try {
					lock.unlock();
				} catch (Exception e) {	}
			}
			onlineCrawlRunner.crawlOnline(datasetUniqueId, false);
		} else {
			log.info("Not fetching nocache dataset because not expired yet");
		}
	}
}
