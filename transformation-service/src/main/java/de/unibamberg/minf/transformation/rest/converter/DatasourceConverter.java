package de.unibamberg.minf.transformation.rest.converter;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.transformation.model.AccessParam;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Datasource;

import de.unibamberg.minf.transformation.pojo.conversion.BaseConverter;
import de.unibamberg.minf.transformation.rest.pojo.DatasourcePojo;

@Component
public class DatasourceConverter extends BaseConverter<Datasource, DatasourcePojo> {

	@Autowired private DatasetConverter datasetConverter;
	private CollectionConverter collectionConverter;
	
	
	public DatasourceConverter(@Lazy CollectionConverter collectionConverter) {
		this.collectionConverter = collectionConverter;
	}
	
	@Override
	public DatasourcePojo convert(Datasource datasource, Locale locale, List<Class<?>> convertedTypes) {
		if (!convertedTypes.contains(Datasource.class)) {
			convertedTypes.add(Datasource.class);
		}
		return this.fill(new DatasourcePojo(), datasource, locale, convertedTypes);
	}

	@Override
	public DatasourcePojo fill(DatasourcePojo pojo, Datasource endpoint, Locale locale, List<Class<?>> convertedTypes) {
		pojo.setUniqueId(endpoint.getUniqueId());

		if (this.isLoaded(endpoint, "datasets")) {
			pojo.setDatasets(datasetConverter.convert(endpoint.getDatasets()));
		}
		
		pojo.setAccessModelId(endpoint.getAccessModelId());
		pojo.setAccessType(endpoint.getAccessType());
		pojo.setUrl(endpoint.getUrl());
		pojo.setFileType(endpoint.getFileType());
		pojo.setAuthRequired(endpoint.isAuthRequired());
				
		if (this.isLoaded(endpoint, "params") && !endpoint.getParams().isEmpty()) {
			pojo.setParams(endpoint.getParams().stream().collect(Collectors.toMap(AccessParam::getParam, AccessParam::getValue)));
		}
		if (this.isLoaded(endpoint, "fileAccessPatterns") && !endpoint.getFileAccessPatterns().isEmpty()) {
			pojo.setFileAccessPatterns(endpoint.getFileAccessPatterns());
		}
		if (this.canProceedWithType(Collection.class, convertedTypes) && this.isLoaded(endpoint, "collection")) {
			pojo.setCollection(collectionConverter.convert(endpoint.getCollection(), locale));
		}
		
				
		return pojo;
	}

}
