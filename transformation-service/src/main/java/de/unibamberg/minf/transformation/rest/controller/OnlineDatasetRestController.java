package de.unibamberg.minf.transformation.rest.controller;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.Executor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.unibamberg.minf.transformation.automation.DmeSyncService;
import de.unibamberg.minf.transformation.automation.OfflineCrawlRunner;
import de.unibamberg.minf.transformation.automation.OnlineCrawlRunner;
import de.unibamberg.minf.transformation.backend.db.service.ResourceService;
import de.unibamberg.minf.transformation.data.service.DownloadDataService;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.OnlineDataset;
import de.unibamberg.minf.transformation.rest.controller.base.BaseRestController;
import de.unibamberg.minf.transformation.rest.converter.DatasetConverter;
import de.unibamberg.minf.transformation.rest.model.RestActionResponse;
import de.unibamberg.minf.transformation.rest.model.RestItemResponse;
import de.unibamberg.minf.transformation.rest.model.RestActionResponse.ApiActionStatus;
import de.unibamberg.minf.transformation.rest.pojo.DatasetPojo;
import de.unibamberg.minf.transformation.service.DatasetService;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Online Datasets", description = "API methods on datasources online datasets")
@RestController
@RequestMapping("/api/onlinedataset")
public class OnlineDatasetRestController extends BaseRestController<DatasetPojo> {
	@Autowired private DatasetService datasetService;
	@Autowired private DatasetConverter datasetConverter;
	@Autowired private Executor syncAutomationTaskExecutor;
	@Autowired protected OnlineCrawlRunner onlineCrawlRunner;
	@Autowired private OfflineCrawlRunner offlineCrawlRunner;
	@Autowired private ResourceService resourceService;
	
	public OnlineDatasetRestController() {
		super("/api/onlinedataset");
	}

	@GetMapping("/")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("/api/onlinedataset");
		return null;
	}
			
	@GetMapping("/{uniqueId}")
    public RestItemResponse getEndpoint(@PathVariable String uniqueId, HttpServletRequest request, Locale locale) throws ApiItemNotFoundException {
		RestItemResponse response = new RestItemResponse();
		
		Optional<OnlineDataset> i = datasetService.findOnlineByUniqueId(uniqueId);
		if (i.isEmpty()) {
			throw new ApiItemNotFoundException("onlineDataset", uniqueId);
		}
		response.setLinks(this.getLinks(request.getRequestURL().toString()));
		response.setItem(this.getItem(datasetConverter.convert(i.get(), locale), request.getRequestURL().toString(), false));
		
		return response;
	}	
	
	@GetMapping("/{uniqueId}/crawl")
	public RestActionResponse getSyncDatamodelsStatus(@PathVariable String uniqueId, Model model, Locale locale) {
		/*if (onlineCrawlRunner.isBusy()) {
			return new RestActionResponse(ApiActionStatus.BUSY, String.format("/api/onlinedataset/%s/crawl", uniqueId));
		}*/
		return new RestActionResponse(ApiActionStatus.NONE, String.format("/api/onlinedataset/%s/crawl", uniqueId));
	}
	
	@PostMapping("/{uniqueId}/crawl")
	public RestActionResponse triggerCrawl(@PathVariable String uniqueId, Model model, Locale locale) {
		// TODO: Implement clear option
		syncAutomationTaskExecutor.execute(() -> onlineCrawlRunner.crawlOnline(uniqueId, false));
		return new RestActionResponse(ApiActionStatus.STARTED, String.format("/api/onlinedataset/%s/crawl", uniqueId));
	}
	
	@PostMapping("/{uniqueId}/reprocess")
	public RestActionResponse triggerReprocess(@PathVariable String uniqueId, Model model, Locale locale) {
		// TODO: Implement clear option
		syncAutomationTaskExecutor.execute(() -> offlineCrawlRunner.crawlOffline(uniqueId, false));
		return new RestActionResponse(ApiActionStatus.STARTED, String.format("/api/onlinedataset/%s/reprocess", uniqueId));
	}
	
	@PostMapping("/{uniqueId}/clear")
	public RestActionResponse triggerClear(@PathVariable String uniqueId, Model model, Locale locale) {
		Optional<Dataset> ds = datasetService.findByUniqueId(uniqueId);
		if (ds.isPresent()) {
			resourceService.deleteByDatasetId(ds.get().getId());
			return new RestActionResponse(ApiActionStatus.DONE, String.format("/api/onlinedataset/%s/clear", uniqueId));
		}
		return new RestActionResponse(ApiActionStatus.CANCELLED, String.format("/api/onlinedataset/%s/clear", uniqueId));
	}
}