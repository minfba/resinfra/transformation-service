package de.unibamberg.minf.transformation.config;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.minf.core.web.init.LocaleAwareInitializationService;
import de.unibamberg.minf.core.web.init.LocaleAwareInitializationServiceImpl;
import de.unibamberg.minf.core.web.interceptor.UserLocaleChangeInterceptor;
import de.unibamberg.minf.core.web.localization.MessageSource;
import de.unibamberg.minf.core.web.navigation.Navigation;
import de.unibamberg.minf.core.web.theming.ThemeManagerImpl;
import de.unibamberg.minf.transformation.config.nested.LocalizationConfig;
import eu.dariah.de.dariahsp.config.BaseUrl;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter @Setter
@Configuration
@ConfigurationProperties
public class WebConfig implements WebMvcConfigurer {
	private String theme;
	private LocalizationConfig localization;
	
	@Autowired private MainConfig mainConfig;
	
	@Autowired ResourceLoader resourceLoader;
	
	@Setter private String baseUrl;
	
	@Getter private BaseUrl baseUrlObj;
	
	@PostConstruct
	public void completeConfiguration() {
		if (localization.getBaseNames()==null) {
			localization.setBaseNames("classpath:i18n/messages","/themes/" + theme + "/i18n/theme");
		}
		baseUrlObj = new BaseUrl(baseUrl);
	}
		
	@Bean
	public Navigation navigation(ObjectMapper objectMapper) throws IOException {	
		File navigation = new File(mainConfig.getPaths().getConfig() + File.separator + "navigation.json");
		if (navigation.exists()) {
			return objectMapper.readValue(navigation, Navigation.class);
		}		
		return objectMapper.readValue(resourceLoader.getResource("classpath:conf/navigation.json").getInputStream(), Navigation.class);
	}
	
	@Bean
	public ThemeManagerImpl themeManagerImpl() {
		ThemeManagerImpl themeManagerImpl = new ThemeManagerImpl();
		themeManagerImpl.setTheme("/themes/" + theme + "/");
		themeManagerImpl.setCheckExistsSubpaths(Arrays.asList("/i18n/theme_de.properties", "/css/bootstrap.css", "/img/theme-logo-de.svg", "/js", "/jsp/footer.jsp"));
		return themeManagerImpl;
	} 
	
	@Bean
    public TilesConfigurer tilesConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        tilesConfigurer.setDefinitions("/WEB-INF/view/templates.xml", "/WEB-INF/view/views.xml");
        tilesConfigurer.setCheckRefresh(true);
        
        return tilesConfigurer;
    }
    
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
    	TilesViewResolver viewResolver = new TilesViewResolver();
        viewResolver.setPrefix(theme + "/");
    	registry.viewResolver(viewResolver);
    }
    
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
        registry.addResourceHandler("/themes/**").addResourceLocations("/themes/");
        registry.addResourceHandler("/theme/**").addResourceLocations("/themes/" + theme + "/");
        
        registry.addResourceHandler("/assets/**").addResourceLocations("file://" + mainConfig.getPaths().getData() + "/");
        
        registry.addResourceHandler("/remoteFiles/**").addResourceLocations("file://" + mainConfig.getPaths().getParseErrors() + "/");
    }
        
	/*@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
	    configurer.
	    favorParameter(true).
	    parameterName("mediaType").
	    ignoreAcceptHeader(false).
	    //useJaf(false).
	    defaultContentType(MediaType.APPLICATION_JSON).
	    mediaType("xml", MediaType.APPLICATION_XML). 
	    mediaType("json", MediaType.APPLICATION_JSON); 
	}*/
    
    /*@Override
    public void configureMessageConverters(
      List<HttpMessageConverter<?>> converters) {
    
        //messageConverters.add(createXmlHttpMessageConverter());
    	converters.add(new MappingJackson2HttpMessageConverter());
    }*/
    
	@Bean
	public LocaleAwareInitializationService initService() {
		LocaleAwareInitializationServiceImpl initService = new LocaleAwareInitializationServiceImpl();
		initService.setBasename(localization.getBaseNames()[0]);
		
		log.debug("Initialized LocaleAwareInitializationServiceImpl");
		
	    return initService;
	}
	
	@Bean
	public LocaleResolver localeResolver() {
	    final SessionLocaleResolver localeResolver = new SessionLocaleResolver();
	    localeResolver.setDefaultLocale(new Locale("de", "DE"));
	    return localeResolver;
	}

	@Bean
	public UserLocaleChangeInterceptor localeChangeInterceptor() {
		UserLocaleChangeInterceptor lci = new UserLocaleChangeInterceptor();
		lci.setInitService(initService());
	    lci.setParamName("lang");
	    return lci;
	}
	
	@Bean
	public MessageSource messageSource() {
		MessageSource messageSource = new MessageSource();
		messageSource.setBasenames(localization.getBaseNames());
		messageSource.setLoggingMode(localization.isDebug());
		messageSource.setCacheSeconds(localization.getCacheSeconds());
		messageSource.setReturnMissingCodes(true);
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}
	
	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean validatorFactory = new LocalValidatorFactoryBean();
		validatorFactory.setValidationMessageSource(messageSource());
	    return validatorFactory;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
	}

}