package de.unibamberg.minf.transformation.execution;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import de.unibamberg.minf.mapping.model.MappingExecGroup;
import de.unibamberg.minf.mapping.service.MappingExecutionService;
import de.unibamberg.minf.processing.consumption.CollectingResourceConsumptionServiceImpl;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.transformation.helper.MappingHelpers;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.ExtendedMappingContainer;
import de.unibamberg.minf.transformation.serialization.ResourceSerializer;
import de.unibamberg.minf.transformation.service.DatamodelService;
import de.unibamberg.minf.transformation.service.MappingService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BaseExecutionService implements ApplicationContextAware  {
	
	@Autowired protected DatamodelService datamodelService;
	@Autowired protected MappingService mappingService;
	@Autowired protected ObjectMapper objectMapper;
	
	protected ObjectMapper exportObjectMapper;
	protected ApplicationContext appContext;
	
	
	
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;

		exportObjectMapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addSerializer(Resource.class, new ResourceSerializer());
		exportObjectMapper.registerModule(module);
	}
	
	protected List<Resource> transformResourcesSync(String sourceDatamodelId, String targetDatamodelId, List<Resource> resources) {
		
		CollectingResourceConsumptionServiceImpl targetResCollector = new CollectingResourceConsumptionServiceImpl();
		try {
			MappingExecutionService mappingExecutionService = appContext.getBean(MappingExecutionService.class);
			ExtendedMappingContainer mc = mappingService.getMappingBySourceAndTarget(sourceDatamodelId, targetDatamodelId);
			ExtendedDatamodelContainer sourceDatamodel = datamodelService.findById(sourceDatamodelId); 
			ExtendedDatamodelContainer targetDatamodel = datamodelService.findById(targetDatamodelId);
			MappingExecGroup mExecGroup = MappingHelpers.buildMappingExecutionGroup(mc, sourceDatamodel, targetDatamodel);
			
			mappingExecutionService.addConsumptionService(targetResCollector);
			if (mExecGroup!=null && (mExecGroup.getConcepts()==null || mExecGroup.getConcepts().isEmpty())) {
				mExecGroup = null;
			}
			mappingExecutionService.init(mExecGroup, resources);
			mappingExecutionService.run();			
		} catch (ProcessingConfigException e) {
			log.error("Failed to transform resources", e);
		}
		
		return targetResCollector.getResources();
	}


}
