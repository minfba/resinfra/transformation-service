package de.unibamberg.minf.transformation.execution.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ApiItemNotFoundException extends ApiRuntimeException {
		
	public ApiItemNotFoundException(String itemType, String identifier) {
		super("~de.unibamberg.minf.transformation.errors.item_not_found", 404, new Object[] {itemType, identifier});
	}
}
