package de.unibamberg.minf.transformation.rest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Resources", description = "API methods for accessing resources")
@RestController
@RequestMapping("/api/resources/{bagUuid}")
public class ResourcesRestController {

}
