package de.unibamberg.minf.transformation.rest.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public abstract class BasePojo implements Serializable{
	private String uniqueId;
	
	private Boolean deleted;
	private Boolean warning;
	private Boolean error;
	private Boolean unavailable;
	private Boolean outdated;
	private Boolean processing;
	private Boolean waiting;
	private Boolean busy;
}