package de.unibamberg.minf.transformation.rest.pojo;

import java.time.OffsetDateTime;
import java.util.Set;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class AccessTokenPojo extends BasePojo {
	private boolean writeAccess;
	
	@NotBlank(message = "Name must be provided")
	private String name;
	
	private String uniqueId;
	private OffsetDateTime expires;
	private Set<String> allowedAdresses;
}
