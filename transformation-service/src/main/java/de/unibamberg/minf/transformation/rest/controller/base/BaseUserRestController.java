package de.unibamberg.minf.transformation.rest.controller.base;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.transformation.execution.exception.ApiInsufficientPermissionsException;
import de.unibamberg.minf.transformation.execution.exception.ApiItemNotFoundException;
import de.unibamberg.minf.transformation.model.PersistedUser;
import de.unibamberg.minf.transformation.rest.pojo.BasePojo;
import de.unibamberg.minf.transformation.service.UserService;
import eu.dariah.de.dariahsp.web.model.AuthPojo;

public abstract class BaseUserRestController<T extends BasePojo> extends BaseRestController<T> {
	
	public BaseUserRestController(String relativeBase) {
		super(relativeBase);
	}
	
	protected PersistedUser checkCanAccessUser(AuthPojo authPojo, String reqUniqueId) throws ApiItemNotFoundException, ApiInsufficientPermissionsException {
		if (reqUniqueId==null) {
			throw new ApiItemNotFoundException(reqUniqueId, "user");
		}
		if (authPojo.getLevel()<100 && !authPojo.getUserUniqueId().equals(reqUniqueId)) {
			throw new ApiInsufficientPermissionsException();
		}
		Optional<PersistedUser> reqUser = userService.findByUniqueId(reqUniqueId);
		if (!reqUser.isPresent()) {
			throw new ApiItemNotFoundException(reqUniqueId, "user");
		}
		return reqUser.get();
	}
}
