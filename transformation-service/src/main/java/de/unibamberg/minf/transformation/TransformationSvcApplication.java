package de.unibamberg.minf.transformation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

@SpringBootApplication
@ConfigurationPropertiesScan
public class TransformationSvcApplication {	
	public static void main(String[] args) {
		SpringApplication.run(TransformationSvcApplication.class, args);
	}
}