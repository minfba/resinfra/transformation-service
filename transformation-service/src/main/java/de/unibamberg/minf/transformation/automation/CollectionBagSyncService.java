package de.unibamberg.minf.transformation.automation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.transformation.api.WrappedApiEntity;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Dataset;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.service.ApiService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CollectionBagSyncService extends CollectionSyncService {

	@Autowired private ApiService apiService;
	
	@Override
	protected void processSynchronizationResult(List<WrappedApiEntity<Collection>> collections) {
		super.processSynchronizationResult(collections);
		log.info("Synchronizing online bags");
		
		/*List<LinkedBag> deletableBags = bagService.findAllLinked();
		for (Collection c : collections) {
			if (c.getEndpoints()!=null) {
				for (Endpoint e : c.getEndpoints()) {
					if (e.getDatasets()!=null) {
						for (Dataset d : e.getDatasets()) {
							// Not handled -> save new
							if (!this.handleIfExists(deletableBags, c, e.getId(), d.getId())) {
								this.saveNewBag(e, d, c);
							}
						}
					}
				}
			}
		}*/
		
		// Actually delete if not used in any API...
		/*deletableBags.stream()
			.forEach(b -> {
				if (apiService.findByBagId(b.getId()).isEmpty()) {
					log.info("Deleting unused and deleted linked bag: {}", b.getId());
					bagService.delete(b);
				} else if (!b.isDeleted()) {
					log.info("Setting used but deleted linked bag to 'deleted': {}", b.getId());
					b.setDeleted(true);
					bagService.saveBag(b, null);
				}
			});		*/
	}
	
	private void saveNewBag(Endpoint endpoint, Dataset dataset, Collection c) {
		/*LinkedBag saveBag = new LinkedBag();
		saveBag.setEndpoint(endpoint);
		saveBag.setDeleted(c.isDeleted());
		saveBag.setName(c.getName(null));
		saveBag.setDataset(dataset);
		
		bagService.saveBag(saveBag, null);*/
	}
	
	//private boolean handleIfExists(List<LinkedBag> deletableBags, Collection c, Long endpointId, Long datasetId) {
		/*List<LinkedBag> matchingBags = deletableBags.stream()
			.filter(b -> idEquals(b.getEndpoint().getId(), endpointId) && idEquals(b.getDataset().getId(), datasetId))
			.map(b -> { 
				b.setDeleted(c.isDeleted());
				if (c.getNames()!=null) {
					b.setName(c.getNames().values().iterator().next());
				}
				bagService.saveBag(b, null);
				return b;
			})
			.collect(Collectors.toList());
	
		if (matchingBags.isEmpty()) {
			return false;
		} else { 
			deletableBags.removeAll(matchingBags);
			return true;
		}*/
		//return false;
	//}
	
	private boolean idEquals(Object id1, Object id2) {
		if (id1==null && id2==null) {
			return true;
		} else if (id1==null || id2==null) {
			return false;
		} else {
			return id1.equals(id2);
		}
		
	}
}
