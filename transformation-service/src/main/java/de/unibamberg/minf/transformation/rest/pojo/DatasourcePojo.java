package de.unibamberg.minf.transformation.rest.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@JsonInclude(Include.NON_NULL)
public class DatasourcePojo extends AccessPojo {
	private static final long serialVersionUID = -4476219312175453873L;
	
	private List<DatasetPojo> datasets;
	
	public DatasourcePojo () {
		this.setType(AccessTypes.Datasource);
	}
}
