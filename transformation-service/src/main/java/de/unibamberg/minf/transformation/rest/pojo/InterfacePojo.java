package de.unibamberg.minf.transformation.rest.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class InterfacePojo extends BasePojo {
	private static final long serialVersionUID = -1858004192273543086L;
	
	private DatamodelPojo datamodel;
}
