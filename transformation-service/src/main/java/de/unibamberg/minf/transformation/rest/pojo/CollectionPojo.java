package de.unibamberg.minf.transformation.rest.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class CollectionPojo extends BasePojo {
	private String colregEntityId;
	private String colregVersionId;
	private String updatePeriod;
	private String imageUrl;
	private String name;
	private String colregUrl;
	
	private List<DatasourcePojo> datasources;
	private List<EndpointPojo> endpoints;
}
