package de.unibamberg.minf.transformation.rest.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class MappingPojo extends BasePojo {
	private static final long serialVersionUID = -3260862849851629837L;
	
	private DatamodelPojo source;
	private DatamodelPojo target;
}