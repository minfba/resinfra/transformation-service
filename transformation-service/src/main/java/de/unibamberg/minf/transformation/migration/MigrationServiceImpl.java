package de.unibamberg.minf.transformation.migration;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.dme.migration.model.version.VersionInfo;
import de.unibamberg.minf.dme.migration.model.version.VersionInfoImpl;
import de.unibamberg.minf.transformation.config.BackendConfig;
import de.unibamberg.minf.transformation.config.PathsConfig;
import de.unibamberg.minf.transformation.dao.db.ApiDao;
import de.unibamberg.minf.transformation.dao.db.VersionDao;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MigrationServiceImpl implements MigrationService {
	private static final String VERSION_HASH_PREFIX = "DME";
	
	@Autowired private PathsConfig pathsConfig;
	@Autowired private BackendConfig backendConfig;
	@Autowired public DataSource transformationDataSource;
	
	
	@Autowired private VersionDao versionDao;
	
	private final MessageDigest md;
	
	public MigrationServiceImpl() throws NoSuchAlgorithmException {
		md = MessageDigest.getInstance("MD5");
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		if (!checkDatabaseInitialized()) {
			log.warn("Database seems to be not initialized, attempting backup and initialization now");
			this.backupDb();
			this.migrateWithScript("preliminary_setup_script");
		}
		
		List<String> versions = new ArrayList<>();
		List<VersionInfoImpl> versionInfos = versionDao.findAll();
		for (VersionInfo vi : versionInfos) {
			if (!vi.getVersionHash().equals(new String(md.digest(new String(VERSION_HASH_PREFIX + vi.getVersion()).getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8))) {
				log.error("Cancelling migration checks: failed to compare version hashes. Is the correct database configured?");
				return;
			}
			versions.add(vi.getVersion());
		}
		this.performMigrations(versions);
	}
	
	private boolean checkDatabaseInitialized() {
		try {
			versionDao.findAll();
			return true;
		} catch (Exception ex) {
			log.error("Failed to call findAll() on api model", ex);
			return false;
		}
	}
	
	private void performMigrations(List<String> existingVersions) throws Exception {
		boolean backedUp = false;
		
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "1.0", () -> true);
		try {
			backedUp = backedUp | this.migrate(existingVersions, !backedUp, "1.0.1", () -> this.migrateWithScript("1_0_1") );
		} catch (Exception e) {
			log.warn("Failed to migrate to v1.0.1, initialization might already have contained migration");
			this.saveVersionInfo("1.0.1", false);
		}
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "1.0.2", () -> this.migrateWithScript("1_0_2") );
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "1.0.3", () -> this.migrateWithScript("1_0_3") );
		
		log.info("Backup performed: {}", backedUp);
	}
		
	private boolean migrate(List<String> existingVersions, boolean backup, String version, MigrationAction migration) throws Exception {
		if ((version.equals("1.0")&&existingVersions.isEmpty()) || (!version.equals("1.0") && !existingVersions.contains(version))) {
			log.info("Migrating to version [{}]", version);	
			if (backup) {
				this.backupDb();
			}
			boolean success = migration.migrate();
			this.saveVersionInfo(version, success);
			log.info("Migration to version [{}] performed {}", version, success ? "sucessfully" : "WITH ERRORS");		
			return true;
		}
		return false;
	}
	
	
	private boolean migrateWithScript(String name) {
		ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
		populator.addScripts(new ClassPathResource(String.format("db/%s.sql", name)));
		populator.setSeparator(";");

		populator.execute(this.transformationDataSource);
		
		return true;
	}
	
	
	public void backupDb() throws IOException, InterruptedException {
		String backupPath = pathsConfig.getBackups() + File.separator + DateTime.now().toString(DateTimeFormat.forPattern("yyyyMMdd_HHmmss"));
		Files.createDirectories(Paths.get(new File(backupPath).toURI()), new FileAttribute<?>[0]);
		
	    Process p;
	    ProcessBuilder pb;
	    pb = new ProcessBuilder(
	            "pg_dump",
	            "--host", backendConfig.getHost(),
	            "--port", "" + backendConfig.getPort(),
	            "--username", backendConfig.getUsername(),
	            "--no-password", 
	            "--format", "custom",
	            "--blobs",
	            "--verbose", 
	            "--file", backupPath + File.separator + "pg.backup", 
	            backendConfig.getDatabase());
	   	
	    StringBuilder commandBuilder = new StringBuilder();
	    
	    for (String c : pb.command()) {
	    	commandBuilder.append(c).append(" ");
	    }
	    log.info(commandBuilder.toString());
	    
        final Map<String, String> env = pb.environment();
        env.put("PGPASSWORD", backendConfig.getPassword());
        try {
	        p = pb.start();
	        final BufferedReader r = new BufferedReader(new InputStreamReader(p.getErrorStream()));
	        String line = r.readLine();
	        while (line != null) {
	            log.info(line);
	            line = r.readLine();
	        }
	        r.close();
	        p.waitFor();
	        log.info("backup process exited with {}", p.exitValue());	 
        } catch (Exception e) {
			log.error("Failed to backup DB, is pg_dump missing?", e);
		}
	}
	
	private void saveVersionInfo(String version, boolean success) {
		VersionInfoImpl vi = new VersionInfoImpl();
		vi.setUpdateWithErrors(!success);
		vi.setVersion(version);		
		vi.setVersionHash(new String(md.digest(new String(VERSION_HASH_PREFIX + vi.getVersion()).getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8));
		
		versionDao.save(vi);
	}

}
