package de.unibamberg.minf.transformation.execution.api;

import de.unibamberg.minf.transformation.model.Api;
import de.unibamberg.minf.transformation.model.Collection;
import de.unibamberg.minf.transformation.model.Crawl;
import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.ExtendedDatamodelContainer;
import de.unibamberg.minf.transformation.model.TemporaryAccessOperation;
import lombok.Data;

@Data
public class ApiDescriptionContainer {
	private Endpoint endpoint;
	private TemporaryAccessOperation accessOperation;
	private ExtendedDatamodelContainer datamodelContainer;
	private Api api;
	private Collection collection;
}