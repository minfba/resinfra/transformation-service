package de.unibamberg.minf.transformation.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import de.unibamberg.minf.transformation.model.Endpoint;
import de.unibamberg.minf.transformation.model.Interface;
import de.unibamberg.minf.transformation.rest.converter.ApiConverter;
import de.unibamberg.minf.transformation.rest.pojo.ApiPojo;
import de.unibamberg.minf.transformation.service.AccessService;
import de.unibamberg.minf.transformation.service.ApiService;
import de.unibamberg.minf.transformation.service.DatamodelService;

@Controller
@RequestMapping("/apis")
public class ApisController extends BaseController {
	
	@Autowired private DatamodelService datamodelService;
	@Autowired private AccessService accessService;
	@Autowired private ApiService apiService;
	
	@Autowired private ApiConverter apiConverter;
	
	public ApisController() {
		super("apis");
	}

	@GetMapping("")
	public String redirectHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("apis/");
		return null;
	}
	
	@GetMapping("/")
	public String getHome(Model model, Locale locale) {
		return "apis/list";
	}
	
	@GetMapping(value={"/{uniqueId}/edit", "/new"})
	public String getEditForm(@PathVariable(required=false) String uniqueId, Model model, Locale locale) {
		model.addAttribute("datamodels", datamodelService.findAllModels());
		model.addAttribute("interfaces", this.getInterfaceIdNameMap(locale));
		if (uniqueId!=null) {
			model.addAttribute("api", apiConverter.convert(apiService.findByUniqueId(uniqueId).orElseThrow(), locale));
			model.addAttribute("actionPath", "/api/apis/" + uniqueId);
		} else {
			model.addAttribute("api", new ApiPojo());
			model.addAttribute("actionPath", "/api/apis");
		}		
		return "apis/edit";
	}
	
	private Map<String, String> getInterfaceIdNameMap(Locale locale) {
		StringBuilder nameBldr;
		Map<String, String> interfaceMap = new HashMap<>();
		for (Endpoint e : accessService.findAllEndpoints(true)) {
			for (Interface i : e.getInterfaces()) {
				nameBldr = new StringBuilder();
				if (e.getCollection()!=null) {
					nameBldr.append(e.getCollection().getName(locale.getDisplayLanguage()));
				} else {
					nameBldr.append(e.getLocalName());
				}
				nameBldr.append(" | ").append(e.getAccessType());
				
				interfaceMap.put(i.getUniqueId(), nameBldr.toString());
			}
		}
		
		return interfaceMap;
	}
	
}
