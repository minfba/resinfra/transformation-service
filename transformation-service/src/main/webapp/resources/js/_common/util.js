var _csrf = $('meta[name=_csrf]').attr("content");
var _csrfHeader = $('meta[name=_csrf_header]').attr("content");
var lang = $('html').attr("lang");

$.ajaxPrefilter(function(options, originalOptions, jqXHR){
    if (options['type'].toLowerCase() === "post") {
        jqXHR.setRequestHeader(_csrfHeader, _csrf);
    }
});

/* 
 * Mainly for Safari support
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
 */
if (!Function.prototype.bind) {
	  Function.prototype.bind = function (oThis) {
	    if (typeof this !== "function") {
	      // closest thing possible to the ECMAScript 5 internal IsCallable function
	      throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
	    }

	    var aArgs = Array.prototype.slice.call(arguments, 1),
	        fToBind = this,
	        fNOP = function () {},
	        fBound = function () {
	          return fToBind.apply(this instanceof fNOP && oThis
	                                 ? this
	                                 : oThis,
	                               aArgs.concat(Array.prototype.slice.call(arguments)));
	        };

	    fNOP.prototype = this.prototype;
	    fBound.prototype = new fNOP();

	    return fBound;
	  };
}


if (!String.format) {
        String.format = function(format) {
                var args = Array.prototype.slice.call(arguments, 1);
                if (format!==null && format!==undefined) {
	                return format.replace(/{(\d+)}/g, function(match, number) {
	                        return typeof args[number] != 'undefined' ? args[number] : match;
	                });
                } else {
                	return format;
                }
        };
}

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

String.prototype.startsWith = function(start) {
        return this.length >= start.length && this.substr(0,start.length)==start;
};

var Util = function() {
        __translator.addTranslations(["~eu.dariah.de.minfba.common.view.notifications.login_required.head",
                                      "~eu.dariah.de.minfba.common.view.notifications.login_required.body",
                                      "~eu.dariah.de.minfba.common.link.yes",
                                      "~eu.dariah.de.minfba.common.link.no",
                                      "~eu.dariah.de.minfba.common.error.servererror.head",
                                      "~eu.dariah.de.minfba.common.error.servererror.body",
                                      "~eu.dariah.de.minfba.common.error.insufficient_rights.head",
                                      "~eu.dariah.de.minfba.common.error.insufficient_rights.body",
                                      "~eu.dariah.de.minfba.common.error.page_reload_required.head",
                                      "~eu.dariah.de.minfba.common.error.page_reload_required.body"])
        // We depend on the view's main js for this call
    //__translator.getTranslations();
        this.entityMap = {
                "&" : "&amp;",
                "<" : "&lt;",
                ">" : "&gt;",
                '"' : '&quot;',
                "'" : '&#39;',
                "/" : '&#x2F;'
        };
        
        this.ulevel = $("#ulevel").val()=="" ? 0 : parseInt($("#ulevel").val());
        this.uauth = $("#uauth").val()==="true" ? true : false;
};
var __util = new Util();

Util.prototype.escapeHtml = function (unsafe) {
	 return unsafe
	     .replace(/&/g, "&amp;")
	     .replace(/</g, "&lt;")
	     .replace(/>/g, "&gt;")
	     .replace(/"/g, "&quot;")
	     .replace(/'/g, "&#039;");
};

Util.prototype.showLoginNote = function() {
	var _this = this;

    bootbox.dialog({
            message : __translator.translate("~eu.dariah.de.minfba.common.view.notifications.login_required.body"),
            title : __translator.translate("~eu.dariah.de.minfba.common.view.notifications.login_required.head"),
            buttons : {
                    no : {
                            label : __translator.translate("~eu.dariah.de.minfba.common.link.no"),
                            className : "btn-default"
                    },
                    yes : {
                            label : __translator.translate("~eu.dariah.de.minfba.common.link.yes"),
                            className : "btn-primary",
                            callback : function() {
                                    window.location = $("#login a").prop("href");
                            }
                    }
            }
    });
};

Util.prototype.processServerError = function(jqXHR) {
	var errorContainer = $("<div>");
	var _this = __util;
	
	if (jqXHR!=null && jqXHR.status!=null) {
		if (jqXHR.status===200) {
			// Just a unnecessary call to this function;
			return;
		}
		if (jqXHR.status===403) {
			if (_this.isLoggedIn()) {
				_this.showErrorAlert("~eu.dariah.de.minfba.common.error.insufficient_rights.head",
						"~eu.dariah.de.minfba.common.error.insufficient_rights.body", $(errorContainer).html());
			} else {
				_this.showLoginNote();
			}
			return;
		}
		if (jqXHR.status===205) {
			_this.showErrorAlert("~eu.dariah.de.minfba.common.error.page_reload_required.head",
					"~eu.dariah.de.minfba.common.error.page_reload_required.body", $(errorContainer).html(),
					function() { location.reload(true); });
			return;
		}
	}
	
	// Generic server error
	if (jqXHR.reponseText!==null && jqXHR.reponseText!==undefined) {
		var error = $('<div class="server-error-container">').append(jqXHR.responseText).get();
		$(errorContainer).append(error);
	}
	_this.showErrorAlert("~eu.dariah.de.minfba.common.error.servererror.head",
			"~eu.dariah.de.minfba.common.error.servererror.body", $(errorContainer).html());
};

Util.prototype.showErrorAlert = function(titleCode, messageCode, payload, callback) {	
	bootbox.alert(
			"<h3>" + __translator.translate(titleCode) + "</h3>" +
			"<p>" + __translator.translate(messageCode) + "</p>" + ( payload!==undefined ? payload : "")
		, callback);
};

Util.prototype.processClientError = function(jqXHR) {
	if (jqXHR!=null && jqXHR.status!=null) {
		if (jqXHR.status===200) {
			// Just a unnecessary call to this function;
			return;
		}
		let errorResponse = JSON.parse(jqXHR.responseText);

		if (jqXHR.status>=400 && jqXHR.status<500 && errorResponse!=undefined && 
			errorResponse.message!=undefined && errorResponse.message!=null) {
			__util.showErrorNotification(errorResponse);
			return;
		}
	}
	__util.processServerError(jqXHR);
};

Util.prototype.showErrorNotification = function(error) {	
	let errorDetails = "";
	if (error.errorDetails!=undefined && error.errorDetails!=null) {
		errorDetails = "<ul>";
		if (error.errorDetails.fieldErrors!=undefined && error.errorDetails.fieldErrors!=null) {
			for (const fieldError of error.errorDetails.fieldErrors) {
				errorDetails += "<li><strong>" + fieldError.field + "</strong>: " + fieldError.message + "</li>";
			}
		}
		if (error.errorDetails.objectErrors!=undefined && error.errorDetails.objectErrors!=null) {
			for (const element of error.errorDetails.objectErrors) {
				errorDetails += "<li>" + element + "</li>";
			}
		}
		errorDetails += "</ul>";
	}
	$("#notifications-area").html(
		'<div class="alert alert-warning alert-dismissible fade show float-right" role="alert">' +
		  '<strong>' + error.message + '</strong> ' + ( errorDetails.length>10 ? ": " + errorDetails : "" ) +
		    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
		   ' <span aria-hidden="true">&times;</span>' +
		  '</button>' +
		'</div>'
	);
};

Util.prototype.isLoggedIn = function() {
    var loggedIn = false;
    $.ajax({
    	url: __util.getBaseUrl() + "async/isAuthenticated",
    	type: "GET",
    	async: false,
    	encoding: "UTF-8",
    	dataType: "text",
    	success: function(data) {
    		loggedIn = (data=="true");
    	}
    });
   
    if (loggedIn) {
            $("#login").css("display", "none");
            $("#logout").css("display", "block");
    } else {
            $("#login").css("display", "block");
            $("#logout").css("display", "none");
    }
    return loggedIn;       
};

Util.prototype.getServerPath = function() {
	var path = "";
	var urlElements = window.location.href.split('/');
	for (var i=0; i<=2; i++) {
		if (i==1) {
			path += "//";
		}
		path += urlElements[i];
	}
	return path;
};

Util.prototype.getBaseUrl = function() {
	return __util.getServerPath() + $("#baseUrl").val();
};

Util.prototype.composeUrl = function(target) {
    return __util.getServerPath() + $("#baseUrl2").val().replace("{}", target);
};

Util.prototype.composeRelativeUrl = function(target) {
    return __util.getServerPath() + $("#currentUrl").val() + target;
};

Util.prototype.renderActivities = function(container, id, data) {
	$(container).html("");
	
	if (data!=null) {
		for (var i=0; i<data.length; i++) {
			$(container).append(
					"<div class=\"alert alert-sm alert-info activity-history-element\">" + 
						"<em>" + (data[i].timestamp==null ? "?" : data[i].timestampString) +"</em><br />" + 
						"<h4>" +
							" " + data[i].user + 
						"</h4>" +
						"<span class=\"glyphicon glyphicon-asterisk\" aria-hidden=\"true\"></span> " + 
							data[i].news + "&nbsp;&nbsp;" +
						"<span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> " + 
							data[i].edits + "&nbsp;&nbsp;" +
						"<span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span> " + 
							data[i].deletes +
					"</div>");
			
		}
	}
}