class Translator {
    constructor() {
        this.translations = [];
        this.translationsUrlSuffix = "async/getTranslations";
    }
    addTranslations(codes) {
        if (codes === null && codes === undefined) {
            return;
        }
        if ($.isArray(codes)) {
            for (const code of codes) {
                this.addTranslation(code);
            }
        } else {
            this.addTranslation(codes);
        }
    }
    addTranslation(code) {
        if (code === null && code === undefined) {
            return;
        }

        if ($.isArray(code)) {
            this.addTranslations(code);
        } else {
            let codeExists = false;
            for (const message of this.translations) {
                if (message.key === code) {
                    codeExists = true;
                    break;
                }
            }
            if (!codeExists) {
                this.translations.push({ key: code, translation: null });
            }
        }
    }
    getTranslations(callback) {
        const _this = this;
        $.ajax({
            url: window.location.pathname + _this.translationsUrlSuffix,
            type: "POST",
            dataType: "json",
            async: false,
            data: { keys: JSON.stringify(_this.getNonTranslated()) },
            success: function(data) {
                _this.updateTranslations(data);
                if (callback != undefined && typeof callback == 'function') {
                    callback();
                }
            }
        });
    }
    updateTranslations(data) {
        for (const newMessage of data) {
            for (const message of this.translations) {
                if (newMessage.key === message.key) {
                    message.translation = newMessage.translation;
                    break;
                }
            }
        }
    }
    getNonTranslated() {
        let result = [];
        for (const message of this.translations) {
            if (message.translation === null) {
                result.push(message);
            }
        }
        return result;
    }
    translate(code, args) {
        for (const message of this.translations) {
            if (message.key === code) {
                if (message.translation == undefined) {
                    return this.replaceArgs(message.defaultText, args);
                }
                return this.replaceArgs(message.translation, args);
            }
        }
        return code;
    }
    replaceArgs(message, args) {
        if (args == undefined || args == null) {
            return message;
        }
        if (Array.isArray(args)) {
            for (const arg of args) {
                message = message.replace("{}", arg);
            }
        } else {
            message = message.replace("{}", args);
        }
        return message;
    }
}








var __translator = new Translator();