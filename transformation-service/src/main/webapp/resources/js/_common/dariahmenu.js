var dariahMenu = null;
$(document).ready(function() {
	dariahMenu = new DariahMenu({});
});

var DariahMenu = function(options) {
	this.options = {
		fetchUrl : "https://res.de.dariah.eu/globalmenu/menu.json",
		menuSelector : "div#dariah-global-menu"
	};
	$.extend(true, this.options, options);
	this.fetchMenu();
};

DariahMenu.prototype.fetchMenu = function() {
	var _this = this;
	$.ajax({
		url: this.options.fetchUrl,
		dataType: 'json',
		error: function(jqXHR, textStatus, errorThrown) { console.log("Failed to load global menu (" + errorThrown +")"); },
		success: function(data, textStatus, jqXHR) { _this.setupMenu(data.menu, $(_this.options.menuSelector)); }
	});
};

DariahMenu.prototype.setupMenu = function(menu, container) {
	for (var i=0; i<menu.length; i++) {
		if (menu[i].divider!==undefined) {
			continue;
		}
		
		var menublock = $("<div>").addClass("dropdown-menu-block"); 
		var header;
		if (menu[i].link===undefined) {
			header = $("<span>");
		} else {
			header = $("<a>").prop("href", menu[i].link);
		}
		header.addClass("dropdown-header");
		header.text(menu[i].title);
		
		menublock.append(header);
		
		if (menu[i].submenu!==undefined) {
			this.setupSubmenu(menu[i].submenu, menublock);
		}
		container.append(menublock);
	}
};

DariahMenu.prototype.setupSubmenu = function(submenu, container) {
	for (var i=0; i<submenu.length; i++) {
		if (submenu[i].divider!==undefined) {
			container.append("<div class='dropdown-divider'></div>");
		} else {
			container.append($("<a>")
					.addClass("dropdown-item")
					.prop("href", submenu[i].link)
					.text(submenu[i].title));
		}
	}
};