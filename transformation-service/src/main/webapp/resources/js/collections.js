var editor;
$(document).ready(function() {
	editor = new CollectionsEditor(); 
});

var CollectionsEditor = function(options) {
	__translator.addTranslations(this.baseTranslations);
	__translator.addTranslations([
		"~eu.dariah.de.minfba.common.actions.cancel",
		"~eu.dariah.de.minfba.common.actions.confim",
		"~eu.dariah.de.minfba.common.actions.ok",
		"~de.unibamberg.minf.transformation.view.dialog.sync_with_colreg",
		"~de.unibamberg.minf.transformation.view.model.endpoint.collection",
		"~de.unibamberg.minf.transformation.view.model.interface",
		"~de.unibamberg.minf.transformation.view.model.online_dataset",
	]);
	__translator.getTranslations(function() {
		var locale = {
		    OK: __translator.translate("~eu.dariah.de.minfba.common.actions.ok"),
		    CONFIRM: __translator.translate("~eu.dariah.de.minfba.common.actions.confim"),
		    CANCEL: __translator.translate("~eu.dariah.de.minfba.common.actions.cancel")
		};
		bootbox.addLocale('current', locale);
	});
	
	this.createTable();
};

CollectionsEditor.prototype = new BaseTable(__util.getBaseUrl() + "api/collections", "#endpoints-table-container");

CollectionsEditor.prototype.createTable = function() {
	var _this = this;
	this._base.table = $('#model-table').DataTable($.extend(true, {
		"order": [[1, "asc"]],
		/*select: {
	        style: 'single',
	        info: false
	    },*/
		"columnDefs": [
	       {
	           "targets": [0], "class": "center", "cellType": "th",
	           "sortable" : false,
	           "data": function (row, type, val, meta) { return editor.renderStateColumn(row, type, meta); }
	       }, {
	    	   "targets": [1],
	    	    "data": "name"
	       }, {
	    	   "targets": [2],
	    	   "data": function (row, type, val, meta) { return editor.renderContentColumn(row); }
	       }, {
	    	   "targets": [3],
	    	   "data": function (row, type, val, meta) { return editor.renderRegistryColumn(row, type); }
	       }
	       
	   ],
	   "rowCallback": this.setRowState
	}, this.baseSettings));
};

CollectionsEditor.prototype.renderStateColumn = function(row, type, meta) {
	if (type==="display") {	
		var nodes = this._base.table.column(meta.column).nodes();
		$(nodes[meta.row]).addClass("state-" + this.getState(row));
		
		//return "<i class='" + this.getIcon(row) + " fa-lg' aria-hidden='true'></i>";
		var result = "<span class='badge badge-success'>Registry</span>";
		return result;
	} else {
		// TODO: render some status message instead
		return this.getIcon(row);
	}
};

CollectionsEditor.prototype.renderRegistryColumn = function(row, type) {
	if (type==="display" && row.colregUrl!==undefined && row.colregUrl!==null) {	
		return "<a href='" + row.colregUrl + "' target='_blank' class='btn btn-inline'>" + row.colregEntityId + " <i class='fas fa-external-link-alt'></i></a>";
	} else {
		return row.colregEntityId;
	}
};


CollectionsEditor.prototype.renderContentColumn = function(row) {
	let endpoints = Array.isArray(row.endpoints) ? row.endpoints.length : 0;
	let datasources = Array.isArray(row.datasources) ? row.datasources.length : 0;
	return  endpoints + " / " + datasources;
};

CollectionsEditor.prototype.handleSelection = function(id) {
	
	console.log(id);
	
};

CollectionsEditor.prototype.refresh = function() {

};

CollectionsEditor.prototype.triggerSync = function() {
	var _this = this;
	bootbox.confirm({
		message: __translator.translate("~de.unibamberg.minf.transformation.view.dialog.sync_with_colreg"),
		locale: "current",
		callback: function(result){
			if (result) { 
		    	$.ajax({
			        url: __util.composeUrl("api/collections/sync"),
			        //data: { name: result },
			        type: "POST",
			        //dataType: "json",
			        success: function(data) { 
			        	_this.refresh();
			        },
			        error: __util.processServerError
				});
			}
		}
	});
	
};