let editor;
$(document).ready(function() {
	editor = new ApisEditor(); 
});

let ApisEditor = function() {
	__translator.addTranslations(this.baseTranslations);
	__translator.addTranslations([
		"~eu.dariah.de.minfba.common.actions.cancel",
		"~eu.dariah.de.minfba.common.actions.confim",
		"~eu.dariah.de.minfba.common.actions.ok",
		"~de.unibamberg.minf.transformation.view.dialog.delete_api.head",
		"~de.unibamberg.minf.transformation.view.dialog.delete_api.body",
		"~de.unibamberg.minf.transformation.view.dialog.change_api_uid.head",
		"~de.unibamberg.minf.transformation.view.dialog.change_api_uid.body",
		"~de.unibamberg.minf.transformation.view.actions.api.delete",
		"~de.unibamberg.minf.transformation.view.actions.api.edit",
		"~de.unibamberg.minf.transformation.view.actions.api.change_uid"
	]);
	__translator.getTranslations(function() {
		const locale = {
		    OK: __translator.translate("~eu.dariah.de.minfba.common.actions.ok"),
		    CONFIRM: __translator.translate("~eu.dariah.de.minfba.common.actions.confim"),
		    CANCEL: __translator.translate("~eu.dariah.de.minfba.common.actions.cancel")
		};
		bootbox.addLocale('current', locale);
	});
	
	const _this = this;
	$.ajax({
        url: __util.composeUrl("api/datamodels"),
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function(data) {
			const datamodels = new Map();
			for (let model of data.items) {
				datamodels.set(model.uniqueId, model.name);
			}
			_this.createTable(datamodels); 
		},
        error: function() { _this.createTable(); },
	});
	
	
};

ApisEditor.prototype = new BaseTable(__util.getBaseUrl() + "api/apis", "#apis-table-container");

ApisEditor.prototype.createTable = function(datamodels) {
	const _this = this;
	this._base.table = $('#model-table').DataTable($.extend(true, {
		"order": [[1, "asc"]],
		/*select: {
	        style: 'single',
	        info: false
	    },*/
		"columnDefs": [
	       {
	           "targets": [0], "class": "center", "cellType": "th",
	           "sortable" : false,
	           "data": function (row, type, val, meta) { return editor.renderStateColumn(row, type, val, meta); }
	       }, {
	    	   "targets": [1],
	    	   "data" : "label",
	    	   "defaultContent": "-"
	       }, {
	    	   "targets": [2],
	    	   "data": function (row, type, val, meta) { return editor.renderLinkColumn(row, type, val, meta); }
	       }, {
	    	   "targets": [3],
	    	   "data": function (row, type, val, meta) { return editor.renderDatamodelColumn(datamodels, row.inputDatamodelId); }
	       }, {
	    	   "targets": [4],
	    	   "data": function (row, type, val, meta) { return editor.renderDatamodelColumn(datamodels, row.outputDatamodelId); }
	       }, {
	    	   "targets": [5],
	    	   "data": function (row, type, val, meta) { return editor.renderActionsColumn(row, type, val, meta); }
	       }	       
	   ],
	   "rowCallback": this.setRowState,
	}, this.baseSettings));
	
	$('#model-table tbody').on('click', 'button.btn-delete-row', function () {
        const data = _this._base.table.row($(this).parents('tr')).data();
        _this.deleteApi(data.uniqueId, data.label);
    });
    $('#model-table tbody').on('click', 'button.btn-edit-row', function () {
        const data = _this._base.table.row($(this).parents('tr')).data();
        _this.editApi(data.uniqueId);
    });
    $('#model-table tbody').on('click', 'button.btn-change-uid', function () {
        const data = _this._base.table.row($(this).parents('tr')).data();
        _this.changeApiUid(data.uniqueId, data.label);
    });
};


ApisEditor.prototype.renderDatamodelColumn = function(datamodels, modelId) {
	if (datamodels.has(modelId)) {
		return datamodels.get(modelId);	
	}
	return modelId;
};

ApisEditor.prototype.renderStateColumn = function(row, type, val, meta) {
	if (type==="display") {	
		var nodes = this._base.table.column(meta.column).nodes();
		$(nodes[meta.row]).addClass("state-" + this.getState(row));
		var result = "<span class='badge badge-success'>Live API</span>";
		//result += "<i class='" + this.getIcon(row.entity) + "' aria-hidden='true'></i>";
		return result;
	} else {
		// TODO: render some status message instead
		return this.getIcon(row);
	}
};

ApisEditor.prototype.renderLinkColumn = function(row, type, val, meta) {
	if (type==="display") {
		return '<a target="_black" href="' + __util.composeUrl("call/" + row.uniqueId) + '" class="btn btn-inline">' + row.uniqueId + ' <i class="fas fa-color-primary fa-external-link-alt"></i></a>';
	}
	return row.uniqueId;
}

ApisEditor.prototype.renderBagsColumn = function(row, type, val, meta) {
	if (row.bags==null || row.bags==undefined || row.bags.length==0) {
		return "N/A";
	}
	let bagsString = "";
	for(let i=0; i<row.bags.length; i++) {
		bagsString = bagsString + row.bags[i].name;
		if (i<row.bags.length-1) {
			bagsString = bagsString + "<br /> ";
		}
	}
	return bagsString;
}

ApisEditor.prototype.renderActionsColumn = function(row, type, val, meta) {
	if (type!=="display") {	
		return "";
	} else {		
		return '<div class="btn-group">' +
				  '<button class="btn btn-link btn-sm btn-edit-row"><i class="fas fa-color-primary fa-edit"></i> ' + __translator.translate("~de.unibamberg.minf.transformation.view.actions.api.edit") + '</button>' +
				  '<button type="button" class="btn btn-link btn-sm dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-expanded="false">' +
					'<span class="sr-only">Toggle Dropdown</span>' +
				  '</button>' +
				  '<div class="dropdown-menu">' +
				    '<button class="dropdown-item btn-delete-row"><i class="far fa-color-danger fa-trash-alt"></i> ' + __translator.translate("~de.unibamberg.minf.transformation.view.actions.api.delete") + '</button>' +
				    //'<div class="dropdown-divider"></div>' +
				    '<button class="dropdown-item btn-change-uid"><i class="fas fa-fingerprint"></i> ' + __translator.translate("~de.unibamberg.minf.transformation.view.actions.api.change_uid") + '</button>' +
				  '</div>' +
			   '</div>';
	}
}

ApisEditor.prototype.deleteApi = function(uid, name) {
	const _this = this;
	bootbox.confirm({
		title: __translator.translate("~de.unibamberg.minf.transformation.view.dialog.delete_api.head"),
		message: __translator.translate("~de.unibamberg.minf.transformation.view.dialog.delete_api.body", [name, uid]),
		locale: "current",
		callback: function(result){
			if (result) { 
		    	$.ajax({
			        url: __util.composeUrl("api/apis/" + uid),
			        type: "DELETE",
			        success: function(data) { _this.processSuccess(data); _this.refresh(); },
			        error: __util.processServerError
				});
			}
		}
	});
};

ApisEditor.prototype.changeApiUid = function(uid, name) {
	const _this = this;
	bootbox.prompt({
		title: __translator.translate("~de.unibamberg.minf.transformation.view.dialog.change_api_uid.head"),
		message: __translator.translate("~de.unibamberg.minf.transformation.view.dialog.change_api_uid.body", [name, uid]),
		locale: "current",
		callback: function(result){
			if (result!=null) { 
		    	$.ajax({
			        url: __util.composeUrl("api/apis/" + uid + "/uid"),
			        type: "POST",
			        data: JSON.stringify({ uniqueId : result}),
			        contentType: "application/json; charset=utf-8",
			        success: function(data) { _this.processSuccess(data); _this.refresh(); },
			        error: __util.processClientError
				});
			}
		}
	});
};





ApisEditor.prototype.editApi = function(id) {
	let form_identifier = "edit-api";
	let _this = this;
	let modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl(id==null ? "new" : (id + "/edit")),
		identifier: form_identifier,
		additionalModalClasses: "modal-lg",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],               
        completeCallback: function(data) {
			_this.processSuccess(data);
			_this.refresh();
		},
		beforeSubmitCallback: function(container, form) {
			let i=0;
			$(container).find("[data-indexable='interfaces']").each(function() { 
				$(this).prop("name", $(this).prop("name").replace("[]", "[" + i++ + "]") ) 
			});
		}
	});
		
	modalFormHandler.show(form_identifier);
}

ApisEditor.prototype.handleSelection = function(id) {
	
	//console.log(id);
	
};


ApisEditor.prototype.addInterfaceToApi = function(control) {
	let intf = $(control).find("option:selected");
	$("#intf-already-attached-warning").hide();
	
	// Placeholder option ('please select...')
	if (intf.val()=="") {
		return;
	}
		
	// IDs already in the ul -> skip
	let skip = false;
	$("#lst-interfaceIds input").each(function() {
		if ($(this).val()==intf.val()) {
			// Select placeholder option
			$(control).find("option:first").prop('selected', true);
			skip=true;
		}
	});
	if (skip) {
		$("#intf-already-attached-warning").show();
		return;
	}
		
	// Append new ID	
	$("#lst-interfaceIds").append(
		"<li>" + 
			"<input type='hidden' data-indexable='interfaces' name='interfaces[].uniqueId' value='" + intf.val() + "'>" + intf.text() +
			" <button class='btn btn-inline' onclick='$(this).closest(\"li\").remove(); return false;'><i class='fas fa-trash'></i></button>" + 
		"</li>");

	// Select placeholder option
	$(control).find("option:first").prop('selected', true);
};