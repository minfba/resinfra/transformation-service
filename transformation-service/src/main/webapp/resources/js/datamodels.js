var editor;
$(document).ready(function() {
	editor = new DatamodelsEditor(); 
});

var DatamodelsEditor = function(options) {
	__translator.addTranslations(this.baseTranslations);
	__translator.addTranslations([
		"~eu.dariah.de.minfba.common.actions.cancel",
		"~eu.dariah.de.minfba.common.actions.confim",
		"~eu.dariah.de.minfba.common.actions.ok",
		"~de.unibamberg.minf.transformation.view.dialog.sync_with_dme"
		
	]);
	__translator.getTranslations(function() {
		var locale = {
		    OK: __translator.translate("~eu.dariah.de.minfba.common.actions.ok"),
		    CONFIRM: __translator.translate("~eu.dariah.de.minfba.common.actions.confim"),
		    CANCEL: __translator.translate("~eu.dariah.de.minfba.common.actions.cancel")
		};
		bootbox.addLocale('current', locale);
	});
	
	this.createTable();
};


DatamodelsEditor.prototype = new BaseTable(__util.getBaseUrl() + "api/datamodels", "#datamodels-table-container");

DatamodelsEditor.prototype.createTable = function() {
	this._base.table = $('#model-table').DataTable($.extend(true, {
		"order": [[1, "asc"]],
		/*select: {
	        style: 'single',
	        info: false
	    },*/
		"columnDefs": [
	       {
	           "targets": [0], "class": "center", "cellType": "th",
	           "sortable" : false,
	           "data": function (row, type, val, meta) { return editor.renderStateColumn(row, type, val, meta); }
	       }, {
	    	   "targets": [1],
	    	   "data" : "name",
	    	   
	       }, {
	    	   "targets": [2],
	    	   "data" : "natures",
	    	   "defaultContent": ""
	       }
	   ],
	   "rowCallback": this.setRowState
	}, this.baseSettings));
};

DatamodelsEditor.prototype.renderStateColumn = function(row, type, val, meta) {
	if (type==="display") {	
		var nodes = this._base.table.column(meta.column).nodes();
		$(nodes[meta.row]).addClass("state-" + this.getState(row));
		
		return "<i class='" + this.getIcon(row) + " fa-lg' aria-hidden='true'></i>";
	} else {
		// TODO: render some status message instead
		return this.getIcon(row);
	}
};

DatamodelsEditor.prototype.handleSelection = function(id) {
	
	console.log(id);
	
};


DatamodelsEditor.prototype.refresh = function() {

};

DatamodelsEditor.prototype.triggerSync = function() {
	var _this = this;
	bootbox.confirm({
		message: __translator.translate("~de.unibamberg.minf.transformation.view.dialog.sync_with_dme"),
		locale: "current",
		callback: function(result){
			if (result) { 
		    	$.ajax({
			        url: __util.composeUrl("api/datamodels/sync"),
			        //data: { name: result },
			        type: "POST",
			        //dataType: "json",
			        success: function(data) { 
			        	_this.refresh();
			        },
			        error: __util.processServerError
				});
			}
		}
	});
	
};