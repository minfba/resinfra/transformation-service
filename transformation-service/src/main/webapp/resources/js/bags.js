var editor;
$(document).ready(function() {
	editor = new BagsEditor(); 
});

var BagsEditor = function(options) {
	__translator.addTranslations(this.baseTranslations);
	__translator.addTranslations([
		"~eu.dariah.de.minfba.common.actions.cancel",
		"~eu.dariah.de.minfba.common.actions.confim",
		"~eu.dariah.de.minfba.common.actions.ok",
		"~de.unibamberg.minf.transformation.view.actions.bag.cache",
		"~de.unibamberg.minf.transformation.view.actions.bag.fetch_data",
		"~de.unibamberg.minf.transformation.view.actions.bag.identify",
		"~de.unibamberg.minf.transformation.view.messages.bag.trigger_cache"
		
	]);
	__translator.getTranslations(function() {
		var locale = {
		    OK: __translator.translate("~eu.dariah.de.minfba.common.actions.ok"),
		    CONFIRM: __translator.translate("~eu.dariah.de.minfba.common.actions.confim"),
		    CANCEL: __translator.translate("~eu.dariah.de.minfba.common.actions.cancel")
		};
		bootbox.addLocale('current', locale);
	});
	
	this.createTable();
};

BagsEditor.prototype = new BaseTable(__util.getBaseUrl() + "api/bags", "#bags-table-container");

BagsEditor.prototype.createTable = function() {
	var _this = this;
	this._base.table = $('#model-table').DataTable($.extend(true, {
		"order": [[1, "asc"]],
		/*select: {
	        style: 'single',
	        info: false
	    },*/
		"columnDefs": [
	       {
	           "targets": [0], "class": "center", "cellType": "th",
	           "sortable" : false,
	           "data": function (row, type, val, meta) { return editor.renderStateColumn(row, type, val, meta); }
	       }, {
	    	   "targets": [1],
	    	   "data" : ".name",
	       }, {
	    	   "targets": [2],
	    	   "data" : ".records",
	       }, {
	    	   "targets": [3],
	    	   "data" : ".files",
	       }, {
	    	   "targets": [4],
	    	   "data": function (row, type, val, meta) { return editor.renderActionsColumn(row, type, val, meta); }
	       }
	   ],
	   "rowCallback": this.setRowState,
	   /*"rowGroup": {
	        "dataSrc": ".collectionId",
            "startRender": _this.setGroupRow
	    }*/
	}, this.baseSettings));
};

BagsEditor.prototype.renderStateColumn = function(row, type, val, meta) {
	if (type==="display") {	
		var nodes = this._base.table.column(meta.column).nodes();
		$(nodes[meta.row]).addClass("state-" + this.getState(row));
		var result = "<span class='badge badge-success'>" + row.localizedType + "</span>";
		//result += "<i class='" + this.getIcon(row.) + "' aria-hidden='true'></i>";
		return result;
	} else {
		return row.localizedType;
	}
};

BagsEditor.prototype.renderActionsColumn = function(row, type, val, meta) {
	if (type!=="display") {	
		return "";
	} else {		
		var actions = '<div class="btn-group">' +
				  '<a class="btn btn-link btn-sm" target="_black" href="' + __util.composeUrl("bag/" + row.uniqueId + "/identify") + '"><i class="fas fa-color-primary fa-external-link-alt"></i> ' + __translator.translate("~de.unibamberg.minf.transformation.view.actions.bag.identify") + '</a>' +
				  '<button type="button" class="btn btn-link btn-sm dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-expanded="false">' +
					'<span class="sr-only">Toggle Dropdown</span>' +
				  '</button>' +
				  '<div class="dropdown-menu">' +
				    '<a class="dropdown-item" target="_blank" href="' + __util.composeUrl("bag/" + row.uniqueId + "/") + '"><i class="fas fa-color-primary fa-external-link-alt"></i> ' + __translator.translate("~de.unibamberg.minf.transformation.view.actions.bag.fetch_data") + '</a>';

		if (row.targetDatamodels!==undefined && row.targetDatamodels!==null) {
			for (var i=0; i<row.targetDatamodels.length; i++) {
				actions +='<a class="dropdown-item" target="_blank" href="' + __util.composeUrl("bag/" + row.uniqueId + "/" + row.targetDatamodels[i].id + "/" ) + '"><i class="fas fa-color-primary fa-external-link-alt"></i> ' + __translator.translate("~de.unibamberg.minf.transformation.view.actions.bag.fetch_data") + ' (' + row.targetDatamodels[i].name + ')</a>';
			}	
		}

		actions +=	'<div class="dropdown-divider"></div>' +
				    '<a class="dropdown-item" onclick="editor.triggerCrawl(\'' + row.id + '\');" href="#">' + __translator.translate("~de.unibamberg.minf.transformation.view.actions.bag.cache") + '</a>' +
				  '</div>' +
			   '</div>';
			
			
		return actions;
	}
}

BagsEditor.prototype.handleSelection = function(id) {

};




BagsEditor.prototype.refresh = function() {

};
