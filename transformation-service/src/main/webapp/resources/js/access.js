var editor;
$(document).ready(function() {
	editor = new AccessEditor(); 
});

var AccessEditor = function(options) {
	__translator.addTranslations(this.baseTranslations);
	__translator.addTranslations([
		"~eu.dariah.de.minfba.common.actions.cancel",
		"~eu.dariah.de.minfba.common.actions.confim",
		"~eu.dariah.de.minfba.common.actions.ok",
		"~de.unibamberg.minf.transformation.view.dialog.sync_with_colreg",
		"~de.unibamberg.minf.transformation.view.model.endpoint.collection",
		
	]);
	__translator.getTranslations(function() {
		var locale = {
		    OK: __translator.translate("~eu.dariah.de.minfba.common.actions.ok"),
		    CONFIRM: __translator.translate("~eu.dariah.de.minfba.common.actions.confim"),
		    CANCEL: __translator.translate("~eu.dariah.de.minfba.common.actions.cancel")
		};
		bootbox.addLocale('current', locale);
	});
	
	this.datamodels = null;
	this.mappings = null;
	
	this.getDatamodels();
	this.getMappings();
};

AccessEditor.prototype = new BaseTable(__util.getBaseUrl() + "api/access", "#datasources-table-container");

AccessEditor.prototype.getDatamodels = function() {
	const _this = this;
	$.ajax({
        url: __util.composeUrl("api/datamodels"),
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function(data) {
			_this.datamodels = new Map();
			for (let model of data.items) {
				_this.datamodels.set(model.uniqueId, model.name);
			}
			if (_this.datamodels!=null && _this.mappings!=null) {
				_this.createTable(_this.datamodels, _this.mappings);
			}
		},
        error: function() { _this.createTable(); },
	});
};

AccessEditor.prototype.getMappings = function() {
	const _this = this;
	$.ajax({
        url: __util.composeUrl("api/mappings"),
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function(data) {
			_this.mappings = [];
			for (let mapping of data.items) {
				_this.mappings.push({
					source: mapping.source.uniqueId, 
					target: mapping.target.uniqueId
				});
			}
			if (_this.datamodels!=null && _this.mappings!=null) {
				_this.createTable(_this.datamodels, _this.mappings);
			}
		},
        error: function() { _this.createTable(); },
	});
};


AccessEditor.prototype.createTable = function(datamodels) {
	var _this = this;
	this._base.table = $('#model-table').DataTable($.extend(true, {
		"order": [[1, "asc"]],
		/*select: {
	        style: 'single',
	        info: false
	    },*/
		"columnDefs": [
	       {
	           "targets": [0], "class": "center", "cellType": "th",
	           "sortable" : false,
	           "data": function (row, type, val, meta) { return editor.renderStateColumn(row, type, val, meta); }
	       }, {
	    	   "targets": [1],
	    	   "data": function (row, type, val, meta) { return row?.collection?.name || "N/A"; }
	       }, {
   	    	   "targets": [2],
   	    	   "data": function (row, type, val, meta) { return editor.renderUrlColumn(row, type, val, meta); }
   	       }, {
   	    	   "targets": [3],
   	    	   "data" : "accessType",
   	       }, {
	    	   "targets": [4],
	    	   "data" : "fileType",
	       }, {
	    	   "targets": [5],
	    	   "data": function (row, type, val, meta) { return editor.renderActionColumn(datamodels, row, type, val, meta); }
	       }
	       
	   ],
	   "rowCallback": this.setRowState
	}, this.baseSettings));
};

AccessEditor.prototype.renderStateColumn = function(row, type, val, meta) {
	if (type==="display") {	
		var nodes = this._base.table.column(meta.column).nodes();
		$(nodes[meta.row]).addClass("state-" + this.getState(row));

		let state = "";
		if (row.type=="Endpoint") {
			state += "<a class='badge badge-info' href='" + __util.composeUrl("data/" + row.uniqueId + "/identify") + "'>" + row.type + "</a>";
		} else {
			state += "<a class='badge badge-primary' href='" + __util.composeUrl("data/" + row.uniqueId + "/identify") + "'>" + row.type + "</a>";
		}
		
		if (row.authRequired) {
			state += " <span style='cursor: pointer;' onclick='editor.unlockAccess(\"" + row.uniqueId + "\"); return false;' class='badge badge-warning'><i class='fas fa-lock'></i></span>";
		} else {
			state += " <span style='cursor: pointer;' onclick='editor.lockAccess(\"" + row.uniqueId + "\"); return false;' class='badge badge-success'><i class='fas fa-unlock'></i></span>";
		}

		return state;
	} else {
		return row.type;
	}
};

AccessEditor.prototype.renderUrlColumn = function(row, type, val, meta) {
	if (type==="display") {	
		return "<a href='" + row.url + "' target='_blank'>" + row.url + " <i class='fas fa-external-link-alt'></i></a>";
	} else {
		return row.url;
	}
};

AccessEditor.prototype.renderActionColumn = function(datamodels, row, type, val, meta) {
	if (type!=="display") {	
		return "";
	}
	let result = '';
	if (row.datasets!=undefined) {
		for (let dm of row.datasets) {
			result += this.showDataset(datamodels, row.uniqueId, dm.uniqueId, dm.datamodel.uniqueId, row, dm);
		}
	}
	return result;
};

AccessEditor.prototype.showDataset = function(datamodels, accessId, datasetId, modelId, row, dataset) {
	let model = modelId;
	if (datamodels!=undefined && datamodels.has(modelId)) {
		model = datamodels.get(modelId);	
	}
	let icon;
	if (dataset?.nocache==true) {
		icon = "<i class='fas fa-spinner'></i>";
	} else {
		icon = "<i class='fas fa-circle-notch'></i>";
	}
	
	let result = '<div class="btn-group">' +
			  '<button type="button" class="btn btn-sm btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
			    icon + ' Dataset' +
			  '</button>' +
			  '<div class="dropdown-menu">';
	
	result +=     '<h6 class="dropdown-header">Data</h6>' + 
	  				"<a class='dropdown-item' href='" + __util.composeUrl("data/" + accessId + "/" + datasetId) + "' target='_blank'>" + model + " <i class='fas fa-external-link-alt'></i></a> ";
	  				
	let targets = "";
	if (this.mappings != undefined) {
		for (const mapping of this.mappings) {
			if (mapping.source == modelId) {
				let model = modelId;
				let targetId = mapping.target;
				if (datamodels != undefined && datamodels.has(targetId)) {
					model = datamodels.get(targetId);
				}
				targets += "<a class='dropdown-item' href='" + __util.composeUrl("data/" + accessId + "/" + datasetId + "/" + targetId) + "' target='_blank'>" + model + " <i class='fas fa-external-link-alt'></i></a> ";
			}
		}
	}

	if (targets.length > 0) {
		result += '<div class="dropdown-divider"></div><h6 class="dropdown-header">Mappings</h6>' + targets
	}

	result += '<div class="dropdown-divider"></div><h6 class="dropdown-header">Actions</h6>';

	if (!dataset.local) {
		result += "<a href='#' class='dropdown-item' onclick='editor.triggerEditDataset(\"" + row.uniqueId + "\", \"" + dataset.uniqueId + "\"); return false;'>Edit</a> ";
	}

	if (!dataset.local && !dataset.nocache) {
		result += "<a href='#' class='dropdown-item' onclick='editor.triggerClear(\"" + dataset.uniqueId + "\"); return false;'>Clear</a> " +
			"<a href='#' class='dropdown-item' onclick='editor.triggerCrawl(\"" + dataset.uniqueId + "\"); return false;'>Load</a> " +
			"<a href='#' class='dropdown-item' onclick='editor.triggerReprocess(\"" + dataset.uniqueId + "\"); return false;'>Reprocess</a> ";
	}
	

		  	
	result +=  '</div>' +
			'</div>';
			
	return result;
}

AccessEditor.prototype.triggerCrawl = function(id) {
	var _this = this;
	bootbox.confirm({
		message: __translator.translate("~de.unibamberg.minf.transformation.view.messages.bag.trigger_cache"),
		locale: "current",
		callback: function(result){
			if (result) { 
		    	$.ajax({
			        url: __util.composeUrl("api/onlinedataset/" + id + "/crawl"),
			        type: "POST",
			        success: function(data) { 
			        	//_this.reload();
			        },
			        error: __util.processServerError
				});
			}
		}
	});
};

AccessEditor.prototype.triggerReprocess = function(id) {
	var _this = this;
	bootbox.confirm({
		message: __translator.translate("~de.unibamberg.minf.transformation.view.messages.bag.trigger_reprocess"),
		locale: "current",
		callback: function(result){
			if (result) { 
		    	$.ajax({
			        url: __util.composeUrl("api/onlinedataset/" + id + "/reprocess"),
			        type: "POST",
			        success: function(data) { 
			        	//_this.reload();
			        },
			        error: __util.processServerError
				});
			}
		}
	});
};

AccessEditor.prototype.triggerEditDataset = function(accessId, datasetId) {
	let form_identifier = "edit-dataset";
	let _this = this;
	let modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl(accessId + "/" + datasetId + "/edit"),
		identifier: form_identifier,
		additionalModalClasses: "modal-lg",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],               
        completeCallback: function(data) {
			setTimeout(function() { _this.refresh(); }, 500);
		}
	});
		
	modalFormHandler.show(form_identifier);
}

AccessEditor.prototype.triggerClear = function(id) {
	var _this = this;
	bootbox.confirm({
		message: __translator.translate("~de.unibamberg.minf.transformation.view.messages.bag.trigger_clear"),
		locale: "current",
		callback: function(result){
			if (result) { 
		    	$.ajax({
			        url: __util.composeUrl("api/onlinedataset/" + id + "/clear"),
			        type: "POST",
			        success: function(data) { 
			        	//_this.reload();
			        },
			        error: __util.processServerError
				});
			}
		}
	});
};

AccessEditor.prototype.lockAccess = function(id) {
	var _this = this;
	bootbox.confirm({
		message: "Lock access?",
		locale: "current",
		callback: function(result){
			if (result) { 
		    	$.ajax({
			        url: __util.composeUrl("api/access/" + id + "/lock"),
			        type: "POST",
			        success: function(data) { 
			        	//_this.reload();
			        },
			        error: __util.processServerError
				});
			}
		}
	});
};

AccessEditor.prototype.unlockAccess = function(id) {
	var _this = this;
	bootbox.confirm({
		message: "Unlock access?",
		locale: "current",
		callback: function(result){
			if (result) { 
		    	$.ajax({
			        url: __util.composeUrl("api/access/" + id + "/unlock"),
			        type: "POST",
			        success: function(data) { 
			        	//_this.reload();
			        },
			        error: __util.processServerError
				});
			}
		}
	});
};

AccessEditor.prototype.handleSelection = function(id) {
	
	console.log(id);
	
};

AccessEditor.prototype.refresh = function() {

};