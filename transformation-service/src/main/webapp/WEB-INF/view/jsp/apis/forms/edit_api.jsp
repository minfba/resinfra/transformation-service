<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<s:url value="${actionPath}" var="saveUrl" />
<sf:form method="POST" action="${saveUrl}" modelAttribute="api" >
	<div class="form-header">
		<c:choose>
			<c:when test="${api.uniqueId!=null && api.uniqueId!=''}">
				<h2 id="form-header-title"><s:message code="~de.unibamberg.minf.transformation.view.model.api.forms.edit_api" /></h2>
			</c:when>
			<c:otherwise>
				<h2 id="form-header-title"><s:message code="~de.unibamberg.minf.transformation.view.model.api.forms.new_api" /></h2>
			</c:otherwise>
		</c:choose>
		<sf:hidden path="uniqueId"/>
	</div>
	<div class="form-content">
		<div class="form-group row">
			<label class="col-form-label col-3" for="label"><s:message code="~de.unibamberg.minf.transformation.view.model.api.name" /></label>
			<div class="col-9">
				<sf:input path="label" class="form-control"/>
				<sf:errors path="label" cssClass="error" />
			</div>
		</div>
		<h3><s:message code="~de.unibamberg.minf.transformation.view.model.api.io_definition" /></h3>
		<div class="form-group row">
			<label class="col-form-label col-3" for="inputDatamodelId"><s:message code="~de.unibamberg.minf.transformation.view.model.api.inputDatamodelId" /></label>
			<div class="col-9">
				<sf:select path="inputDatamodelId" items="${datamodels}" itemValue="id" itemLabel="name" class="form-control"/>
				<sf:errors path="inputDatamodelId" cssClass="error" />
			</div>
		</div>
		<div class="form-group row">
			<label class="col-form-label col-3" for="outputDatamodelId"><s:message code="~de.unibamberg.minf.transformation.view.model.api.outputDatamodelId" /></label>
			<div class="col-9">
				<sf:select path="outputDatamodelId" items="${datamodels}" itemValue="id" itemLabel="name" class="form-control"/>
				<sf:errors path="outputDatamodelId" cssClass="error" />
			</div>
		</div>
		
		<div class="form-group row">
			<label class="col-form-label col-3" for="metadata"><s:message code="~de.unibamberg.minf.transformation.view.model.api.metadata" /></label>
			<div class="col-9">
				<sf:textarea path="metadata" class="form-control"/>
				<sf:errors path="metadata" cssClass="error" />
			</div>
		</div>
		
		<h3><s:message code="~de.unibamberg.minf.transformation.view.model.api.bags" /></h3>
		<div class="form-group row">
			<label class="col-form-label col-3" for="lst-bagIds"><s:message code="~de.unibamberg.minf.transformation.view.model.api.bagIds" /></label>
			<div class="col-8">
				<select onchange="editor.addInterfaceToApi(this); return false;" class="form-control" style="margin-bottom: 0.5em;">
					<option selected="selected" value=""><s:message code="~de.unibamberg.minf.transformation.view.labels.select_to_add" /></option>
					<option disabled="disabled">________</option>
					
					<c:forEach items="${interfaces}" var="intf">
						<option value="${intf.key}">${intf.value}</option>
					</c:forEach>
				
					
				</select>		
				<div id="intf-already-attached-warning" class="alert alert-warning" style="display: none;"><s:message code="~de.unibamberg.minf.transformation.view.errors.bag_already_attached" /></div>
			</div>
			<div class="col-8 offset-3">
				<ul id="lst-interfaceIds">
					<c:forEach items="${api.interfaces}" var="intf">
						<li><input type="hidden" data-indexable="interfaces" name="interfaces[].uniqueId" value="${intf.uniqueId}">
							<c:forEach items="${interfaces}" var="intf2">
								<c:if test="${intf.uniqueId == intf2.key}">
									${intf2.value}
								</c:if>
							</c:forEach>
							<button class="btn btn-inline" onclick="$(this).closest('li').remove(); return false;"><i class="fas fa-trash"></i></button>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
		
		
		
	</div>
	<div class="form-footer control-group">
		<div class="controls">
			<button class="btn btn-default cancel form-btn-cancel" type="reset"><s:message code="~eu.dariah.de.minfba.common.actions.cancel" /></button>
			<button class="btn btn-primary start form-btn-submit" type="submit"><s:message code="~eu.dariah.de.minfba.common.actions.save" /></button>
		</div>
	</div>
</sf:form>
