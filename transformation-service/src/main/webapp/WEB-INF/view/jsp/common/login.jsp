<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<tiles:importAttribute name="fluidLayout" />

<div class="container<c:if test="${fluidLayout==true}">-fluid</c:if>">
	<div class="row">
	    <div id="notifications-area" class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4"></div>		
	    <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
	    	<h1><s:message code="~eu.dariah.de.minfba.common.local_login.heading" /></h1>
	    	<c:if test="${not empty error}">
				<div class="alert alert-warning" role="alert"><s:message code="~eu.dariah.de.minfba.common.local_login.invalid_credentials" /></div>
			</c:if>
			<form name='loginForm' action="<s:url value='${callbackUrl}?client_name=local&url=${requestUrl}' />" method='POST'>
				<input type="hidden" name="redirectUrl" id="redirectUrl" value="${redirectUrl}" />
				<div class="form-group">
					<label for="username"><s:message code="~eu.dariah.de.minfba.common.local_login.username" /></label>
				    <div>
				     	<input type="text" class="form-control" id="username" name="username" autofocus>
				    </div>
				</div>
				<div class="form-group">
					<label for="password"><s:message code="~eu.dariah.de.minfba.common.local_login.password" /></label>
				    <div>
				     	<input type="password" class="form-control" id="password" name="password">
				    </div>
				</div>
				<div class="form-group">
				    <div>
				      <button name="submit" type="submit" value="submit" class="btn btn-primary"><s:message code="~eu.dariah.de.minfba.common.local_login.signin" /></button>
				    </div>
				</div>
			</form>
	    </div>
	</div>
</div>