<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<tiles:importAttribute name="fluidLayout" />

<div class="container<c:if test="${fluidLayout==true}">-fluid</c:if>">
	<div class="row">
		<div class="col-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
		    <div id="endpoints-table-container" class="col-12">
			    <div class="row">
			    	<div class="col-sm-12 col-md-8">
			    		<div>
			    			<h1><s:message code="~de.unibamberg.minf.transformation.view.titles.collections" /></h1>
			    		</div>
			    		<div class="data-table-count float-left" style="margin-right: 12px;">
							<label><s:message code="~de.unibamberg.minf.common.labels.show" />:
								<select class="custom-select custom-select-sm form-control form-control-sm" aria-controls="model-table">
								  <option>10</option>
								  <option>25</option>
								  <option>50</option>
								  <option>100</option>
								  <option><s:message code="~de.unibamberg.minf.common.link.all" /></option>
								</select>
							</label>
						</div>	
			    		<div class="data-table-filter float-left">
			    			<label><s:message code="~de.unibamberg.minf.common.link.filter" />: 
								<input type="text" class="form-control form-control-sm" aria-controls="model-table">
							</label>
						</div>
						<div class="float-left" style="padding-top: 0.8em; padding-left: 1em;">
							<button class="btn btn-link" onclick="editor.triggerSync();"><i class="fas fa-sync"></i> <s:message code="~de.unibamberg.minf.transformation.actions.sync_with_colreg" /></button>
						</div>
	   				</div>
	   				<!-- Notifications -->
	   				<div id="notifications-area" class="col-sm-8 col-md-4"></div>
	 			</div>
	 			<div class="row">
	 				<div class="col-sm-12">
					    <table id="model-table" class="table table-striped table-bordered table-hover table-compact" style="width:100%" role="grid">
					    	<thead>
								<tr>
									<th></th> <!-- Status -->
									<th>~Name</th>
									<th>~E / ~D</th>
									<th>~Registry entry</th>
								</tr>
							</thead>
							<tbody>
							<tr>
								<td colspan="6" align="center"><s:message code="~de.unibamberg.minf.common.view.no_data_fetched_yet" /></td>
							</tr>
							</tbody>
					    </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
