<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tpl" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:importAttribute name="collapsePanel" ignore="true" />

<header>
	<nav class="navbar navbar-expand-xl bg-primary navbar-dark">
	
		<div class="container-fluid" style="max-width: 1200px">
	
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
	
				<span class="ti-menu"></span>
				<s:message code="~de.unibamberg.minf.common.labels.menu" />
			</button>
		
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<c:forEach items="${_nav.rootItems}" var="_navItem">
	    				<c:set var="hide" value="false" />
						<c:forEach items="${_navItem.attributes}" var="attr">
					        <c:if test="${customSearch!=null && customSearch.staticCustomSearch==false && attr.key=='hideInCustomSearch' && attr.value==true}">
					        	<c:set var="hide" value="true" />
					        </c:if>
					    </c:forEach>
	    				<c:if test="${hide==false && (_navItem.authRequired==false ||  ( (_navItem.authRequired==true && _auth!=null && _auth.auth==true) &&
							( _navItem.authMinLevel==0 || (_navItem.authMinLevel<=_auth.level)) ))}">
		    				<c:choose>
				    			<c:when test="${_navItem.subItems!=null && fn:length(_navItem.subItems)>0}">					
									<li class="nav-item dropdown<c:if test="${_navItem.active || _navItem.childActive}"> active</c:if>">
										<a class="nav-link" href="#" id="${_navItem.id}Dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
											<c:if test="${_navItem.iconClass!=null && fn:length(_navItem.iconClass)>0}">
												<i class="${_navItem.iconClass}"></i>&nbsp;
											</c:if>
											<c:if test="${_navItem.displayCode!=null && fn:length(_navItem.displayCode)>0}">
												<s:message code="${_navItem.displayCode}" />
											</c:if>&nbsp;
											<span class="ti-angle-down"></span>
										</a>
										<div class="dropdown-menu" aria-labelledby="${_navItem.id}Dropdown">
											<tpl:topNav navItem="${_navItem}" />
										</div>
									</li>
				    			</c:when>
				    			<c:otherwise>
				    				<li class="nav-item<c:if test="${_navItem.active || _navItem.childActive}"> active</c:if>">
										<a class="nav-link" href="<s:url value='${_navItem.linkUrl}'/>">
											<c:if test="${_navItem.iconClass!=null && fn:length(_navItem.iconClass)>0}">
												<i class="${_navItem.iconClass}"></i>&nbsp;
											</c:if>
											<c:if test="${_navItem.displayCode!=null && fn:length(_navItem.displayCode)>0}">
												<s:message code="${_navItem.displayCode}" />
											</c:if>
										</a>
									</li>
				    			</c:otherwise>
			    			</c:choose>
		    			</c:if>
					</c:forEach>
				</ul>
			</div>
			
			<div class="navbar-expand" id="navbarSupportedContent2" style="margin-right: 40px;">
			
				<!-- Language, login, search -->
				<ul class="navbar-nav">
					<li class="nav-item dropdown navbar-separator">
						<a class="nav-link" href="#" id="languageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="ti-world"> </span>
							<c:forEach items="${_LANGUAGES}" var="_LANGUAGE">
								<c:if test="${fn:startsWith(pageContext.response.locale, _LANGUAGE.key)}">
									${_LANGUAGE.value}
								</c:if>	
							</c:forEach>
						</a>
						<div class="dropdown-menu" aria-labelledby="languageDropdown">
							<c:forEach items="${_LANGUAGES}" var="_LANGUAGE">
								<c:if test="${fn:startsWith(pageContext.response.locale, _LANGUAGE.key)==false}">
									<a class="dropdown-item" href="?lang=${_LANGUAGE.key}">${_LANGUAGE.value}</a>
								</c:if>
							</c:forEach>
						</div>
					</li>
					<li class="nav-item">
						<c:set var="currentUrl" value="${requestScope['javax.servlet.forward.request_uri']}" />
						<c:choose>
							<c:when test="${_auth==null || _auth.auth!=true}">							
								<a class="nav-link account_toggle" href="<s:url value='/startLogin?url=${currentUrl}' />"><span class="ti-login"></span></a>
							</c:when>
							<c:otherwise>
								<a class="nav-link account_toggle" href="<s:url value='/centralLogout' />"><span class="ti-logout"></span></a>
							</c:otherwise>
						</c:choose>
					</li>
					<c:if test="${_auth!=null && _auth.auth==true}">
						<li class="nav-item">
							<a class="nav-link" href="<s:url value='/user' />" title="${_auth.displayName}"><span class="ti-user"></span></a>
						</li>
					</c:if>
					
					
					<c:catch>
						<tiles:importAttribute name="theme" />
						<jsp:include page="../../../../themes/${theme}/jsp/add_navigation.jsp" />
					</c:catch>

					<c:if test="${collapsePanel!=null}">
			        	<li class="nav-item navbar-separator grid-xl-hidden">
							<a class="nav-link version-panel-toggle collapse-panel-trigger" href="javascript:void"><i class="fas fa-archive fa-lg"></i></a>
						</li>
			        </c:if>
				</ul>
			</div>
			
		</div>
		<div class="container-fluid "style="position: absolute; top: 0; right: 0; width: 55px; padding: 0;">
			<div class="grid-hidden grid-xl-visible">
				<ul class="navbar-nav float-right">
					<c:if test="${collapsePanel!=null}">
			        	<li class="nav-item navbar-separator" style="width: 55px;">
							<a class="nav-link version-panel-toggle collapse-panel-trigger" href="javascript:void"><i class="fas fa-archive fa-lg"></i></a>
						</li>
			        </c:if>
			    </ul>
			</div>
		</div>
	</nav>
	<h1 class="logobar">
		<a class="logobar-link<c:if test="${smallLogo==true}"> logobar-link-sm</c:if>" href="" title="Startseite">
			<img class="logobar-logo" src='<s:url value="/theme/img/${smallLogo==true ? 'theme-logo-small.svg' : 'theme-logo-de.svg'}" />' alt='<s:message code="~eu.dariah.de.minfba.theme.name" />'>
			<span class="logobar-title">
				<s:message code="${smallLogo==true ? '~eu.dariah.de.minfba.theme.application_titles.transformation_svc' : '~eu.dariah.de.minfba.theme.application_titles.br.transformation_svc'}" />
			</span>
		</a>
	</h1>
</header>
<input id="currentUrl" type="hidden" value="${requestScope['javax.servlet.forward.request_uri']}" />
<input id="baseUrl" type="hidden" value="<s:url value="/" />" />
<input id="baseUrl2" type="hidden" value="<s:url value="/{}" />" />