<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<head>
	<meta charset="utf-8">
    <title><s:message code="~eu.dariah.de.minfba.theme.application_titles.long.transformation_svc" /></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="Tobias Gradl, University of Bamberg">
    <meta name="description" content="<s:message code="~eu.dariah.de.minfba.theme.application_titles.long.transformation_svc" />">
    <meta name="_csrf" content="${__csrf_token}"/>
	<meta name="_csrf_header" content="${__csrf_headerName}"/>
    <tiles:importAttribute name="genericStyles" />  	
  	<c:forEach items="${genericStyles}" var="css">
  		<link rel="stylesheet" href="<s:url value="/resources/css/${css}" />" type="text/css" media="screen, projection" />
  	</c:forEach>
  	<tiles:importAttribute name="templateStyles" />
  	<c:forEach items="${templateStyles}" var="css">
  		<link rel="stylesheet" href="<s:url value="/themes/${css}" />" type="text/css" media="screen, projection" />
  	</c:forEach>	
  	
  	<script type="text/javascript">
	  	var AppProperties = function() {
	  		this.refreshViews = <c:out value="${_refreshViews==null ? true : _refreshViews}"/>;
	  		this.refreshIntervalMs = 5000;
	  		this.notificationsArea = "#notifications-area";
	  		this.notificationsTimeoutMs = 5000;
	  		this.notificationsFadeSpeed = 400;
	  	};
	  	var __properties = new AppProperties();
  	</script>
  	
 		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
 	<link rel="shortcut icon" type="image/x-icon" href="<s:url value="/theme/img/favicon.ico" />">
	<link rel="icon" type="image/png" href="<s:url value="/theme/img/favicon.png" />">
</head>