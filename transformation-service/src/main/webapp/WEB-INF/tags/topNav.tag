<%@ tag description="Recursively display top navigation items" pageEncoding="UTF-8"%>
<%@ attribute name="navItem" type="de.unibamberg.minf.core.web.navigation.NavigationItem" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:forEach items="${navItem.subItems}" var="navSubItem">
	<c:if test="${navSubItem.authRequired==false || 
					( (navSubItem.authRequired==true && _auth!=null && _auth.auth==true) &&
					( navSubItem.authMinLevel==0 || (navSubItem.authMinLevel<=_auth.level)) )}">
		
		<c:set var="hide" value="false" />
		<c:forEach items="${navSubItem.attributes}" var="attr">
	        <c:if test="${customSearch!=null && customSearch.staticCustomSearch==false && attr.key=='hideInCustomSearch' && attr.value==true}">
	        	<c:set var="hide" value="true" />
	        </c:if>
	    </c:forEach>		
				
		<c:if test="${hide==false}">			
			<div class="<c:if test="${navSubItem.groupHeader==true}">dropdown-menu-block<c:if test="${navSubItem.iconClass!=null && fn:length(navSubItem.iconClass)>0}"> block-with-icon</c:if></c:if>">
				<c:set var="itemHref" value="${navSubItem.linkUrl!=null && fn:length(navSubItem.linkUrl)>0 ? navSubItem.linkUrl : '#'}" />
				<a class="${navSubItem.groupHeader==true ? 'dropdown-header' : 'dropdown-item'}" href="<s:url value='${itemHref}'/>">
					<c:if test="${navSubItem.iconClass!=null && fn:length(navSubItem.iconClass)>0}">
						<i class="${navSubItem.iconClass}"></i>&nbsp;
					</c:if>
					<c:if test="${navSubItem.displayCode!=null && fn:length(navSubItem.displayCode)>0}">
						<s:message code="${navSubItem.displayCode}" />
					</c:if>
				</a>
				<c:if test="${navSubItem.subItems!=null && fn:length(navSubItem.subItems)>0}">
					<c:forEach items="${navSubItem.subItems}" var="navSubSubItem">
						<c:set var="hideSub" value="false" />
						<c:forEach items="${navSubSubItem.attributes}" var="attr">
					        <c:if test="${customSearch!=null && customSearch.staticCustomSearch==false && attr.key=='hideInCustomSearch' && attr.value==true}">
					        	<c:set var="hideSub" value="true" />
					        </c:if>
					    </c:forEach>
					    <c:if test="${hideSub==false}">
							<a class="dropdown-item" href="<s:url value='${navSubSubItem.linkUrl}'/>">
								<c:if test="${navSubSubItem.iconClass!=null && fn:length(navSubSubItem.iconClass)>0}">
									<i class="${navSubSubItem.iconClass}"></i>&nbsp;
								</c:if>
								<c:if test="${navSubItem.displayCode!=null && fn:length(navSubItem.displayCode)>0}">
									<s:message code="${navSubItem.displayCode}" />
								</c:if>
							</a>
						</c:if>
					</c:forEach>
				</c:if>
			</div>
		</c:if>
	</c:if>
</c:forEach>