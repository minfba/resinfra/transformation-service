FROM eclipse-temurin:17-jdk
RUN apt update && apt install -y postgresql-client
ARG VERSION
ARG WAR_FILE=transformation-service/build/libs/transformation-service-${VERSION}.war
COPY ${WAR_FILE} app.war
EXPOSE 8080
ENTRYPOINT ["/app.war","-Djava.security.egd=file:/dev/./urandom","-Dspring.config.additional-location=optional:file:/config/application.yml","-XX:+HeapDumpOnOutOfMemoryError"]
